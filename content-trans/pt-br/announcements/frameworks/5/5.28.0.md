---
aliases:
- ../../kde-frameworks-5.28.0
date: 2016-11-15
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nova plataforma: syntax-highlighting (realce de sintaxe)

Motor de realce da sintaxe para as definições de sintaxe do Kate

Esta é uma implementação autónoma do motor de realce de sintaxe do Kate. Pretende ser um bloco de construção para os editores de texto, assim como para motores de visualização de texto realçado simples (p.ex., como HTML), suportando tanto a integração com um editor personalizado, assim como uma sub-classe do QSyntaxHighlighter pronta a usar.

### Ícones Breeze

- Actualização dos ícones de acção do KStars (erro 364981)
- O Brisa Escuro aparece como Brisa no ficheiro .themes da Configuração do Sistema errado (erro 370213)

### Módulos extra do CMake

- Fazer com que o KDECMakeSettings funcione com o KDE_INSTALL_DIRS_NO_DEPRECATED
- Não obrigar às dependências da interface de Python do ECM
- Adição do módulo PythonModuleGeneration

### KActivitiesStats

- Ignorar o estado da ligação ao ordenar o modelo UsedResources e LinkedResources

### Ferramentas Doxygen do KDE

- [CSS] reversão das alterações feitas pelo doxygen 1.8.12
- Adição do ficheiro 'doxygenlayout'
- Actualização da forma de definição dos nmes dos grupos

### KAuth

- Garantia de que é possível fazer mais que um pedido
- Garantia de que é possível saber o progresso ao analisar o resultado do programa

### KConfig

- Garantia de que não se quebra a compilação com unidades antigas com problemas
- Não dar um erro fatal se o campo File não for processado correctamente

### KCoreAddons

- Mostrar o URL inválido
- Carregamento dos 'avatars' do utilizador do AccountsServicePath se existirem (erro 370362)

### KDeclarative

- [QtQuickRendererSettings] Correcção do valor predefinido como vazio em vez de "false"

### KDELibs 4 Support

- Mudança da bandeira Francesa para usar de facto toda a imagem

### KDocTools

- Correção do bug #371987 'checkXML5 generates html files in workdir for valid docbooks'

### KIconThemes

- Suporte de factores de escala não-inteiros no 'kiconengine' (erro 366451)

### KIdleTime

- Desactivação do abuso de mensagens na consola do tipo 'waiting for' (à espera de)

### KImageFormats

- imageformats/kra.h - substituições do capabilities() e do create() do KraPlugin

### KIO

- Correcção do formato de datas em HTTP enviados pelo 'kio_http' para usar sempre o formato regional C (erro 372005)
- KACL: correcção de fugas de memória detectadas pelo ASAN
- Correcção de fugas de memória no KIO::Scheduler, detectadas pelo ASAN
- Remoção de botão de limepza duplicado (erro 369377)
- Correcção da edição de itens do arranque quando o /usr/local/share/applications não existe (erro 371194)
- [KOpenWithDialog] Ocultação do cabeçalho do TreeView
- Mudança para um formato são do tamanho do tampão do nome da ligação simbólica (erro 369275)
- Finalização correcta dos DropJobs quando o 'triggered' não é emitido (erro 363936)
- ClipboardUpdater: correcção de outro estoiro no Wayland (erro 359883)
- ClipboardUpdater: correcção de estoiro no Wayland (erro 370520)
- Suporte para factores de escala não-inteiros no KFileDelegate (erro 366451)
- kntlm: Distinção entre um domínio NULL e vazio
- Não mostrar a janela de sobreposição se o nome do ficheiro estiver vazio
- kioexec: uso de nomes de ficheiros amigáveis
- Correcção do dono do foco, caso o URL seja alterado antes de mostrar o elemento gráfico
- Grande melhoria de performance ao desactivar as antevisões na janela de ficheiros (erro 346403)

### KItemModels

- Adição de interfaces em Python

### KJS

- Exportação do FunctionObjectImp, usado pelo depurador do KHTML

### KNewStuff

- Separação dos papéis de ordenação e dos filtros
- Possibilidade de consulta dos elementos instalados

### KNotification

- Não remover todas as referências de um objecto que não tenha sido referenciado quando a notificação não tem nenhuma acção
- O KNotification não vai estoirar mais quando o usar numa QGuiApplication e quando nenhum serviço de notificações estiver em execução (erro 370667)
- Correcção dos estoiros no NotifyByAudio

### Plataforma KPackage

- Garantia de que são pesquisados tanto os meta-dados em JSON como dos 'desktop'
- Guarda contra a destruição do Q_GLOBAL_STATIC no encerramento da aplicação
- Correcção de ponteiro 'pendurado' no KPackageJob (erro 369935)
- Remoção da descoberta associada a uma tecla ao remover uma definição
- Geração do ícone no ficheiro 'appstream'

### KPty

- Uso do 'ulog-helper' no FreeBSD em vez do 'utempter'
- pesquisa mais detalhada pelo 'utempter', usando também o prefixo básico 'cmake'
- solução alternativa para os erros do 'find_program ( utempter ...)'
- Uso da localização do ECM para descobrir o binário 'utempter', o que é mais fiável que um prefixo simples do 'cmake'

### KRunner

- i18n: tratamento dos textos nos ficheiros 'kdevtemplate'

### KTextEditor

- Brisa Escuro: Escurecimento da cor de fundo do 'current-line' para uma melhor legibilidade (erro 371042)
- Ordenação das instruções do Dockerfile
- Brisa (Escuro): Tornar os comentários um pouco mais claros para melhor legibilidade (erro 371042)
- Correção do bug #370715 CStyle and C++/boost indenters when automatic brackets enabled
- Adição da linha de modo 'auto-brackets'
- Correcção da inserção do texto após o fim-do-ficheiro (caso raro)
- Correcção de ficheiros de realce em XML inválidos
- Maxima: Remoção de cores fixas, correcção da Legenda do 'itemData'
- Adição das definições de sintaxe do OBJ, PLY e STL
- Adição do realce de sintaxe para o Praat

### KUnitConversion

- Novas Unidades Térmicas e Eléctricas e Função de Conveniência de Unidades

### KWallet Framework

- Se não for encontrado o 'Gpgmepp', tentar usar o KF5Gpgmepp
- Uso do Gpgmepp do GpgME-1.7.0

### KWayland

- Melhorias na mudança de local das exportações do CMake
- [ferramentas] Correcção da geração do wayland_pointer_p.h
- [ferramentas] Geração dos métodos 'eventQueue' apenas para as classes globais
- [servidor] Correcção de estoiro ao actualizar a superfície do teclado em primeiro plano
- [servidor] Correcção de possível estoiro na criação do DataDevice
- [servidor] Garantia de que temos um DataSource no DataDevice no 'setSelection'
- [ferramentas/gerador] Melhoria na destruição de recursos do lado do servidor
- Adição de pedido de foco numa PlasmaShellSurface do tipo Painel
- Adição do suporte para ocultação automática do painel na interface PlasmaShellSurface
- Suporte à passagem de um QIcon genérico à interface PlasmaWindow
- [servidor] Implementação da propriedade genérica 'window' no QtSurfaceExtension
- [cliente] Adição de métodos para obter uma ShellSurface de uma QWindow
- [servidor] Envio de eventos de ponteiro para todos os recursos 'wl_pointer' de um cliente
- [servidor] Não invocar o wl_data_source_send_send se o DataSource não estiver associado
- [servidor] Uso do 'deleteLater' quando um ClientConnection é destruído (erro 370232)
- Implementação do suporte para o protocolo de ponteiro relativo
- [servidor] Cancelamento da selecção anterior do SeatInterface::setSelection
- [servidor] Enviar eventos de teclas a todos os recursos 'wl_keyboard' de um cliente

### KWidgetsAddons

- Passagem do 'kcharselect-generate-datafile.py' para a sub-pasta 'src'
- Importação do 'kcharselect-generate-datafile.py' com o histórico
- Remoção de secção antiga
- Adição das notas de direitos de cópia e permissões do Unicode
- Correcção de aviso: 'Missing override' (Substituto em falta)
- Adição dos blocos SMP do símbolo
- Correção das referências em "Ver também"
- Adição de blocos Unicode em falta; melhoria na ordenação (erro 298010)
- adição das categorias de caracteres ao ficheiro de dados
- actualização das categorias Unicode no programa de geração dos ficheiros de dados
- ajuste do ficheiro de geração do ficheiros de dados para conseguir processar os ficheiros de dados do Unicode 5.2.0
- correcção posterior de migração para gerar traduções
- possibilidade de o programa gerar o ficheiro de dados para o 'kcharselect' e também fazer um teste de tradução
- Adição do programa para gerar o ficheiro de dados do KCharSelect
- nova aplicação do KCharSelect (usando agora o elemento gráfico 'kcharselect' do 'kdelibs')

### KWindowSystem

- Melhorias na mudança de local das exportações do CMake
- Adição do suporte para o 'desktopFileName' no NETWinInfo

### KXMLGUI

- Permitir o uso do novo estilo de sintaxe do 'connect' com o KActionCollection::add<a href="">Action</a>

### ModemManagerQt

- Correcção da pasta de inclusão no ficheiro 'pri'

### NetworkManagerQt

- Correcção da pasta de inclusão no ficheiro 'pri'
- Correcção de erro do 'moc' devido ao uso do Q_ENUMS num espaço de nomes com o Qt na versão 5.8

### Plasma Framework

- validação de que o OSD não usa a opção Dialog (erro 370433)
- definição das propriedades do contexto antes de recarregar o QML (erro 371763)
- Não voltar a processar o ficheiro de meta-dados se já estiver carregado
- Correcção do estoiro no 'qmlplugindump' quando não está disponível nenhuma QApplication
- Não mostrar o menu "Alternativas" por omissão
- Novo booleano para usar o sinal 'activated' como uma comutação do 'expanded' (erro 367685)
- Correcção da compilação do 'plasma-framework' no Qt 5.5
- [PluginLoader] Uso do operator&lt;&lt; para o 'finalArgs' em vez da lista de inicialização
- uso do 'kwayland' para sombras e posicionamento das janelas
- Aplicação dos ícones restantes em falta e melhorias na rede
- Passagem do 'availableScreenRect/Region' para o AppletInterface
- Não carregar as acções dos contentores incorporados (bandejas do sistema)
- Actualização da visibilidade a pedido do item de menu das alternativas da 'applet'

### Solid

- Correcção de ordenação instável dos resultados da pesquisa mais uma vez
- Adição de uma opção do CMake para alternar entre os gestores HAL e UDisks no FreeBSD
- Modificações para a infra-estrutura do UDisks2 compilar no FreeBSD (e provavelmente noutros UNIX'es)
- Windows: Não mostrar as mensagens de erro (erro 371012)

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
