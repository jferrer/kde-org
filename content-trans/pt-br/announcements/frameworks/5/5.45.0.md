---
aliases:
- ../../kde-frameworks-5.45.0
date: 2018-04-14
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Definir explicitamente o tipo de conteúdo nos dados do formulário

### Baloo

- Simplificação dos operadores de termos &amp;&amp; e ||
- Não obter o ID do documento para os itens de resultados ignorados
- Não obter o 'mtime' (hora de modificação) da base de dados de forma repetida ao ordenar
- Não exportar o 'databasesanitizer' por omissão
- baloodb: Adição de mensagem experimental
- Introdução de ferramenta CLI do 'baloodb'
- Introdução de uma classe de sanitização
- [FileIndexerConfig] Atraso no preenchimento das pastas até serem usadas de facto
- src/kioslaves/search/CMakeLists.txt - ligação às seguintes alterações do Qt5Network no 'kio'
- balooctl: o 'checkDb' deverá também verificar o último URL conhecido para o 'documentId'
- Monitor do Balooctl: Retomar para esperar pelo serviço

### Ícones Breeze

- adição do ícone 'window-pin' (adição do ícone 'window-pin' do erro 385170)
- mudança de nome dos ícones de 64 px adicionados para o Elisa
- mudança dos ícones a 32px para a reprodução aleatória e repetida da lista de reprodução
- Ícones em falta para as Mensagens incorporadas (erro 392391)
- Novo ícone para o leitor de música Elisa
- Adição de ícones do estado multimédia
- Remoção do contorno à volta dos ícones de acção multimédia
- adição dos ícones 'media-playlist-append' e 'play'
- adição do ícone 'view-media-album-cover' para o Babe

### Módulos extra do CMake

- Tirar partido da infra-estrutura do CMake posterior para detectar o conjunto de compiladores
- Documentação da API: correcção de algumas linhas de "bloco-de-código" que têm linhas vazias antes/depois
- Adição do ECMSetupQtPluginMacroNames
- Oferta do 'androiddeployqt' com todos os locais de prefixos
- Inclusão do "stdcpp-path" no ficheiro JSON
- Resolução de ligações simbólicas nos locais de importação em QML
- Adição dos locais de importação de QML do 'androiddeployqt'

### Integração do Framework

- kpackage-install-handlers/kns/CMakeLists.txt - ligação às seguintes alterações do Qt::Xml no 'knewstuff'

### KActivitiesStats

- Não assumir que o SQLite funciona e não terminar em caso de erro

### Ferramentas Doxygen do KDE

- Procurar primeiro pelo 'qhelpgenerator-qt5' na geração da ajuda

### KArchive

- karchive, kzip: tentar lidar com os ficheiros duplicados de uma forma mais correcta
- Uso do 'nullptr' para assar um ponteiro nulo ao 'crc32'

### KCMUtils

- Possibilidade de pedir um módulo de configuração de 'plugin' de forma programática
- Uso consistente do X-KDE-ServiceTypes em vez do ServiceTypes
- Adição do X-KDE-OnlyShowOnQtPlatforms à definição do tipo de serviço do KCModule

### KCoreAddons

- KTextToHTML: retornar quando o URL estiver vazio
- Limpeza do 'm_inotify_wd_to_entry' antes de invalidar os ponteiros Entry (erro 390214)

### KDeclarative

- Configuração do QQmlEngine apenas uma vez no QmlObject

### KDED

- Adição do X-KDE-OnlyShowOnQtPlatforms à definição do tipo de serviço do KDEDModule

### KDocTools

- adição de entidades para o Elisa, Markdown, KParts, DOT, SVG ao 'general.entities'
- customization/ru: Correcção da tradução do 'underCCBYSA4.docbook' e 'underFDL.docbook'
- Correcção do 'lgpl-notice'/'gpl-notice'/'fdl-notice' duplicado
- customization/ru: Tradução do 'fdl-notice.docbook'
- mudança ortográfica do kwave a pedido do responsável de manutenção

### KFileMetaData

- taglibextractor: Reorganização para melhor legibilidade

### KGlobalAccel

- Não estoirar por erro se for usado incorrectamente a partir do 'dbus' (erro 389375)

### KHolidays

- holidays/plan2/holiday_in_en-gb - actualização do ficheiro de feriados para a Índia (erro 392503)
- Este pacote não foi actualizado. Talvez tenha ocorrido um problema com o programa
- Remodelação dos ficheiros de feriados para a Alemanha (erro 373686)
- Formatação do README.md como as ferramentas estão à espera (com uma secção de Introdução)

### KHTML

- evitar pedir por um protocolo vazio

### KI18n

- Certificação de que o 'ki18n' consegue compilar as suas próprias traduções
- Não invocar o 'PythonInterp.cmake' no KF5I18NMacros
- Possibilidade de geração de ficheiros PO em paralelo
- Criação de um construtor para o KLocalizedStringPrivate

### KIconThemes

- Tornar o comentário de exportação do KIconEngine mais adequado
- Evitar um erro em execução do 'asan'

### KInit

- Apagar IdleSlave's que tenham uma autorização temporária

### KIO

- Garantia de que o modelo está definido quando é invocado o 'resetResizing'
- O 'pwd.h' não está presente no Windows
- Remoção dos elementos 'Gravados Recentemente Este Mês' e 'Gravados Recentemente no Mês Passado' por omissão
- Capacidade de ter o KIO a compilar no Android
- Desactivação temporária da instalação do utilitário do 'kauth' do IO-Slave 'file' e do seu ficheiro de política
- Tratamento das mensagens de confirmação das operações privilegiadas no SlaveBase em vez do KIO::Job
- Melhoria da consistência da janela "Abrir Com", mostrando sempre a aplicação de topo incorporada
- Correcção de estoiro quando o dispositivo emite uma leitura do 'ready' após a tarefa ter terminado
- Realce dos itens seleccionados quando mostrar a pasta-mãe a partir da janela de abertura/gravação (erro 392330)
- Suporte para os ficheiros escondidos do NTFS
- Uso consistente do X-KDE-ServiceTypes em vez do ServiceTypes
- Correcção de validação no 'concatPaths' quando é colada uma localização completa para o campo de texto do KFileWidget
- [KPropertiesDialog] Suporte da página de Códigos de Validação para qualquer localização local (erro 392100)
- [KFilePlacesView] Invocação do KDiskFreeSpaceInfo apenas se necessário
- FileUndoManager: não apagar ficheiros locais inexistentes
- [KProtocolInfoFactory] Não limpar a 'cache' se tiver acabado de ser criada
- Não tentar procurar um ícone com um URL relativo (p.ex. '~')
- Uso do URL correcto do item para o menu de contexto 'Criar um Novo' (erro 387387)
- Correcção de mais casos de parâmetros incorrectos no 'findProtocol'
- KUrlCompletion: retorno prévio se o URL for inválido, como p.ex. ":/"
- Não tentar procurar um ícone para um URL vazio

### Kirigami

- Ícones maiores no modo para dispositivos móveis
- Forçar um tamanho de conteúdo no item do estilo de fundo
- Adição do tipo InlineMessage e página de exemplo com aplicação de Galeria
- coloração selectiva com melhores heurísticas
- tornar o carregamento dos SVG's locais realmente funcional
- suporte também para o método de carregamento de ícones do Android
- uso de uma estratégia de coloração semelhante para os diferentes estilos anteriores
- [Card] Uso de uma implementação própria do "findIndex"
- eliminação das transferências de rede se mudar o ícone durante a execução
- primeiro protótipo para um módulo de reciclagem delegada
- Possibilidade de os clientes do OverlaySheet omitirem o botão de fecho incorporado
- Componentes dos Cards
- Correção de tamanho do ActionButton
- Fazer as notificações passivadas demorarem mais, para que os utilizadores as consigam realmente ler
- Remoção da dependência do QQC1 não usada
- Formato do ToolbarApplicationHeader
- Possibilidade de mostrar o título, apesar de ter acções de contexto

### KNewStuff

- Votação de facto quando se carregam nas estrelas da lista (erro 391112)

### Plataforma KPackage

- Tentativa de correcção da compilação para FreeBSD
- Uso do Qt5::rcc em ve de procurar pelo executável
- Uso do NO_DEFAULT_PATH para garantir que é escolhido o comando correcto
- Pesquisa também pelos executáveis do 'rcc' com prefixos
- definição do componente para a geração correcta do 'qrc'
- Correcção da geração do pacote binário do 'rcc'
- Geração do ficheiro 'rcc' de cada vez, na altura da instalação
- Possibilidade de os componentes 'org.kde.' incluírem um URL de doação
- Marcação do 'kpackage_install_package' como válida para o 'plasma_install_package'

### KPeople

- Exposição do PersonData::phoneNumber no QML

### Kross

- Sem necessidade de ter o 'kdoctools' como obrigatório

### KService

- Documentação da API: uso consistente do X-KDE-ServiceTypes em vez do ServiceTypes

### KTextEditor

- Possibilidade de o KTextEditor compilar no gcc 4.9 do Android NDK
- eliminação do erro de execução do Asan: 'shift exponent -1 is negative' (expoente negativo no desvio)
- optimização do 'TextLineData::attribute'
- Não calcular o attribute() duas vezes
- Reversão de Correcção: A janela salta quando está activada a opção para 'Deslocar para além do fim do documento' (erro 391838)
- não poluir o histórico da área de transferência com duplicados

### KWayland

- Adição da interface de Acesso Remoto ao KWayland
- [servidor] Adição do suporte para a semântica da envolvência do Pointer na versão 5 (erro 389189)

### KWidgetsAddons

- KColorButtonTest: remoção de código 'por-fazer'
- ktooltipwidget: Subtracção das margens nos tamanhos disponíveis
- [KAcceleratorManager]: só definir o iconText() se tiver alterado de facto (erro 391002)
- ktooltipwidget: Impedimento de desenho fora do ecrã
- KCapacityBar: definição do estado QStyle::State_Horizontal
- Sincronização com as mudanças do KColorScheme
- ktooltipwidget: Correcção do posicionamento das dicas (erro 388583)

### KWindowSystem

- Adição do "SkipSwitcher" à API
- [xcb] Correcção da implementação do _NET_WM_FULLSCREEN_MONITORS (erro 391960)
- Redução do tempo de bloqueio do 'plasmashell'

### ModemManagerQt

- cmake: não assinalar a 'libnm-util' como encontrada quando o ModemManager é encontrado

### NetworkManagerQt

- Exportação das pastas de inclusão do NetworkManager
- Início da obrigatoriedade do NM 1.0.0
- dispositivo: definição do StateChangeReason e do MeteredStatus como Q_ENUMs
- Correcção da conversão das opções do AccessPoint para capacidades

### Plasma Framework

- Modelos de papéis de parede: definição da cor de fundo para garantir o contraste com o conteúdo do texto de exemplo
- Adição de modelo para o papel de parede do Plasma com uma extensão QML
- [ToolTipArea] Adição do sinal "aboutToShow"
- windowthumbnail: Uso da escala de correcção do 'gama'
- WindowThumbnail: Uso da filtragem de texturas por 'mipmaps' (erro 390457)
- Remoção de elementos 'X-Plasma-RemoteLocation' não usados
- Modelos: eliminação do X-Plasma-DefaultSize não usado dos meta-dados dos elementos
- Uso consistente do X-KDE-ServiceTypes em vez do ServiceTypes
- Modelos: eliminação dos itens X-Plasma-Requires-* não usados dos meta-dados dos elementos
- remoção das âncoras do item numa disposição
- Redução do tempo de bloqueio do 'plasmashell'
- pré-carregar somente após a emissão pelo contentor do 'uiReadyChanged'
- Correcção de erros nas listas 'combobox' (erro 392026)
- Correcção da escala do texto com factores de escala não-inteiros quando a opção PLASMA_USE_QT_SCALING=1 estiver definida (erro 356446)
- novos ícones para os dispositivos desligados/desactivados
- [Dialog] Possibilidade de definir o 'outputOnly' para uma janela NoBackground
- [ToolTip] Verificação do nome do ficheiro na rotina do KDirWatch
- Desactivação do aviso de descontinuação do 'kpackage_install_package' por agora
- [Tema Brisa do Plasma] Aplicação do 'currentColorFix.sh' aos ícones multimédia alterados
- [Tema Brisa do Plasma] Adição de ícones de estado multimédia com círculos
- Remoção dos contornos à volta dos botões multimédia
- [Miniatura de Janela] Permitir o uso da textura Atlas
- [Dialog] Remoção das chamadas KWindowSystem::setState, agora obsoletas
- Suporte das texturas Atlas no FadingNode
- Correcção do fragmento FadingMaterial com o perfil básico

### QQC2StyleBridge

- correcção do desenho quando estiver desactivado
- melhor disposição
- suporte experimental para mnemónicas automáticas
- Validação de que é tido em conta o tamanho do elemento na aplicação do estilo
- Correcção do desenho dos tipos de letra nos ecrãs não-HiDPI e com factores de escala inteiros (erro 391780)
- correcção das cores dos ícones com os conjuntos de cores
- correcção das cores dos ícones para os botões de ferramentas

### Solid

- O Solid agora consegue consultar as baterias p.ex. nos dispositivos de jogos e 'joysticks'
- Uso dos enumerados do UP recentemente introduzidos
- adição dos dispositivos 'gaming_input' e outros à Bateria
- Adição ao Enumerado de Dispositivos com Bateria
- [UDevManager] Pesquisar também explicitamente por câmaras
- [UDevManager] Filtrar já pelo sub-sistema antes da pesquisa (erro 391738)

### Sonnet

- Não impor a utilização do cliente predefinido, escolhendo um que suporte a língua pedida.
- Inclusão de textos de substituição na lista de sugestões
- implementação do NSSpellCheckerDict::addPersonal()
- O NSSpellCheckerDict::suggest() devolve uma lista de sugestões
- inicialização da língua do NSSpellChecker no construtor do NSSpellCheckerDict
- implementação da categoria de registo do NSSpellChecker
- O NSSpellChecker necessita do AppKit
- Passagem do NSSpellCheckerClient::reliability() para fora da linha
- uso do código de plataforma Mac preferido
- Uso da pasta correcta para pesquisa de trigramas na pasta de compilação do Windows

### Realce de sintaxe

- Possibilidade de compilar por completo o projecto nas compilações multi-plataforma
- Remodelação do gerador de sintaxe do CMake
- Optimização do realce para Bash, Cisco, Clipper, Coffee, Gap, Haml, Haskell
- Adição do realce de sintaxe para os ficheiros MIB

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
