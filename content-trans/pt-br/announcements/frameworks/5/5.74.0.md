---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---
### Attica

+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções

### Baloo

+ Mudança da licença de muitos ficheiros para LGPL-2.0-ou-posterior
+ Uso do código de criação comum do UDS para as marcas (erro 419429)
+ Retirada de algum código comum de criação do UDS dos módulos do KIO
+ [balooctl] Mostrar os formatos permitidos no texto de ajuda
+ [balooctl] Mostrar o ficheiro actual no resultado do estado de indexação
+ [balooctl] Definição do modo do QDBusServiceWatcher a partir do construtor
+ [balooctl] Limpeza da formatação
+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções
+ [OrPostingIterator] Não avançar quando o ID desejado é inferior ao actual
+ [Extracção] Remoção da dependência do QWidgets no utilitário de extracção
+ [Extractor] Remoção do KAboutData do executável auxiliar de extracção
+ Actualização de várias referências no README
+ [FileContentIndexer] Remoção do argumento e membro do construtor de configuração não-usado
+ [balooctl] Correcção do aviso de descontinuação do QProcess::start, passagem de uma lista vazia de argumentos
+ [Motor] Propagação dos erros de transacções (erro 425017)
+ [Motor] Remoção do método não usado 'hasChanges' do {Write}Transaction
+ Não indexar os ficheiros .ytdl (erro 424925)

### Ícones Breeze

+ Tornar o ícone do 'keepassxc' mais fiel ao oficial
+ Adição de mais ligações simbólicas para os novos ícones da bandeja do 'keepassxc' (erro 425928)
+ Adição de outro nome alternativo para o ícone do keepass (erro 425928)
+ Adição de ícone para o tipo MIME dos projectos de Godot
+ Adição de ícone para o instalador Anaconda
+ Adição de ícones de locais a 96px
+ Remoção de locais não usados para comunicar
+ Execução do application-x-bzip-compressed-tar através do <code>scour</code> (erro 425089)
+ Tornar o 'application-gzip' uma ligação simbólica para 'application-x-gzip' (erro 425059)
+ Adição de nomes alternativos para os ícones de MP3 (erro 425059)

### Módulos extra do CMake

+ Eliminação dos zeros iniciais dos números de versão numéricos no código em C++
+ Adição de tempo-limite nas chamadas do 'qmlplugindump'
+ Adição do módulo de pesquisa WaylandProtocols
+ Invocação do 'update-mime-database' com '-n'

### Integração do Framework

+ Clarificação da declaração da licença de acordo com o histórico do KDElibs

### KActivitiesStats

+ Uso do Boost::boost para as versões mais antigas do CMake

### KActivities

+ Eliminação do X-KDE-PluginInfo-Depends vazio

### Ferramentas Doxygen do KDE

+ Documentação das dependências no 'requirements.txt' e instalá-las no 'setup.py'
+ Apresentação da Aceitação da Licença da Biblioteca

### KAuth

+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções

### KBookmarks

+ KBookmarkManager: limpeza da árvore da memória quando o ficheiro é removido

### KCalendarCore

+ Guardar sempre as propriedades X-KDE-VOLATILE-XXX como voláteis
+ Documentação das transições de fuso-horário esperadas em Praga

### KCMUtils

+ KCModuleData: Correcção dos ficheiros de inclusão, melhoria na documentação da API e mudança de nome do método
+ Extensão do KCModuleData com a funcionalidade 'revertToDefaults' e 'matchQuery'
+ Tentar evitar uma barra de deslocamento horizontal no KCMultiDialog
+ Adição do KCModuleData como classe de base para o 'plugin'
+ Possibilidade de adição de um botão extra no KPluginSelector (erro 315829)

### KConfig

+ Mudança do método KWindowConfig::allConnectedScreens() para estático e interno (erro 425953)
+ Adição de um atalho-padrão para "Criar uma Pasta"
+ Introdução de método para consultar o valor predefinido do KConfigSkeletonItem
+ Recordar os tamanhos das janelas com base na organização por ecrã
+ Extracção do código para obter a lista de ecrãs ligados para uma função reutilizável
+ Adição de funções para gravar e repor as posições das janelas nas plataformas não-Wayland (erro 415150)

### KConfigWidgets

+ Evitar a troca de valores predefinidos para ler o valor predefinido do KConfigSkeletonItem
+ Correcção do KLanguageName::nameForCodeInLocale para os códigos que o QLocale não conhece
+ KLanguageName::allLanguageCodes: Ter em conta que poderá existir mais que uma pasta da região
+ KConfigDialog: Tentar evitar barras de deslocamento horizontais
+ Função que devolve a Lista de Códigos de Línguas

### KContacts

+ Addressee::parseEmailAddress(): Verificação do tamanho do nome completo antes do corte

### KCoreAddons

+ Adição do padrão global *.kcrash para o tipo MIME do Relatório do KCrash
+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções
+ Não esperar indefinidamente pelos eventos do 'fam' (erro 423818)
+ [KFormat] Permitir a formatação de valores com unidades binárias arbitrárias
+ Possibilidade de uso do KPluginMetadata a partir do QML
+ [KFormat] Correcção do exemplo em binário

### KDAV

+ metainfo.yaml: Adição da chave do nível e ajustes na descrição

### KDeclarative

+ [KeySequenceItem] Fazer com que os atalhos Meta+Shift+número funcionem
+ Exposição da propriedade 'checkForConflictsAgainst'
+ Adição de uma nova classe AbstractKCM
+ Migração do KRunProxy para fora do KRun

### KDED

+ org.kde.kded5.desktop: Adição da chave obrigatória "Name" em falta (erro 408802)

### KDocTools

+ Actualização do contributor.entities
+ Uso de marcações de substituição

### KEmoticons

+ Recuperação da informação da licença do EmojiOne

### KFileMetaData

+ Conversão dos fins de linha antigos do Mac nas marcas de letras musicais (erro 425563)
+ Migração para o novo módulo FindTaglib do ECM

### KGlobalAccel

+ Carregamento dos ficheiros de serviços de atalhos da pasta de dados das aplicações em alternativa (erro 421329)

### KI18n

+ Correcção de possível condição de acesso concorrente do pré-processador com as definições do 'i18n'

### KIO

+ StatJob: fazer com que o 'mostLocalUrl' funcione apenas com o protoclClass == :local
+ KPropertiesDialog: carregar também os 'plugins' com meta-dados em JSON
+ Reversão do "[KUrlCompletion] Não adicionar o '/' às pastas completas" (erro 425387)
+ Simplificação do construtor do KProcessRunner
+ Permitir as palavras-chave CCBUG e FEATURE no bugs.kde.org (erro )
+ Actualização do texto de ajuda para editar o comando da aplicação num item .desktop para ficar em conformidade com a especificação actual (erro 425145)
+ KFileFilterCombo: Não adicionar a opção 'allTypes' se só tiver 1 item
+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções
+ Correcção de potencial estoiro na remoção de um menu no tratamento de eventos (erro 402793)
+ [filewidgets] Correcção do preenchimento do KUrlNavigatorButton na área de navegação (erro 425570)
+ ApplicationLauncherJob: ajustes da documentação
+ Correcção de itens não traduzidos kfileplacesmodel
+ ApplicationLauncherJob: correcção de estoiro caso não exista uma aplicação para 'abrir-com'
+ Mudança de nome do KCM de "Atalhos da Web" para "Palavras-Chave de Pesquisa na Web"
+ KFileWidget: processamento da configuração de novo para capturar as pastas adicionadas por outras instâncias da aplicação (erro 403524)
+ Passagem do KCM de atalhos da Web para a categoria de pesquisa
+ Remoção automática dos espaços finais em vez de mostrar um aviso
+ Impedir que as aplicações lançadas pelo 'systemd' fechem os processos-filhos replicados (erro 425201)
+ smb: correcção da verificação de disponibilidade do nome da partilha
+ smb: manter o 'stderr' dos comandos 'net' ao adicionar/remover (erro 334618)
+ smb: correcção da opção 'permitido' e publicação como 'areGuestsAllowed'
+ KFileWidget: Limpeza dos URL's antes de reconstruir a lista
+ KFileWidget: remoção da lista de localizações de topo dos URL's predefinidos
+ KFilePlacesModel: adição dos locais predefinidos ao actualizar de uma versão mais antiga
+ Correcção de uma regressão de 2 anos no KUrlComboBox, onde o setUrl() não adicionava mais

### Kirigami

+ kirigami.pri: Adição do managedtexturenode.cpp
+ fazer com que o item-companheiro de um posicionamento personalizado funcione de novo
+ Ter em conta o tamanho do botão Mais se já soubermos que vai estar visível
+ Adição de uma propriedade ao ToolBarLayout para controlar como lidar com a altura do item (erro 425675)
+ tornar as legendas das opções de marcação legíveis de novo
+ suporte do 'displayComponent' nas acções
+ tornar os submenus visíveis
+ Correcção da declaração posterior QSGMaterialType
+ Fazer com que o cabeçalho e o rodapé do OverlaySheet usem as cores de fundo apropriadas
+ Verificação do valor da variável de ambiente para energia baixa e não só se está definida
+ Adição da função 'setShader' para o ShadowedRectangleShader para simplificar a definição de 'shaders'
+ Adiçãod de uma versão para energia baixa do ficheiro de funções do SDF
+ Criação de substitutos do sdf_render que usam a função 'sdf_render' com todos os argumentos
+ Remoção da necessidade das definições de perfil de base no código principal do 'shader' do 'shadowedrect'
+ Basear o rectângulo interno no rectângulo externo para o 'shadowedrectangle' com contorno
+ Uso dos 'shaders' correctos no ShadowedTexture ao usar o perfil de base
+ Adição das versões "lowpower" dos 'shaders' do 'shadowedrectangle'
+ Inicialização do membro m_component no PageRoute
+ Correcção do SwipeListItem Material
+ nova lógica para remover as marcas de aceleração (erro 420409)
+ remoção do 'forceSoftwarerendering' por agora
+ apenas mostrar o item de origem no desenho por 'software'
+ uma alternativa por 'software' para uma textura sombreada
+ Uso da nova propriedade 'showMenuArrow' no fundo para a seta do menu
+ não esconder o cabeçalho ao recolher para cima
+ passagem do ManagedTextureNode para um ficheiro próprio
+ ToolBarLayout: Adição de espaço ao 'visibleWidth' se estivermos a mostrar o botão Mais
+ Não substituir o tamanho em pixels do Cabeçalho no BreadCrumbControl (erro 404396)
+ Actualização do modelo de aplicação
+ [passivenotification] Definição de um intervalo de preenchimento específico (erro 419391)
+ Activação do recorte no StackView do GlobalDrawer
+ Remoção d aopacidae do PrivateActionToolButton desactivado
+ Tornar o ActionToolBar um controlo
+ swipenavigator: adição de controlo sobre quais as páginas apresentadas
+ Declaração do tipo subjacente do enumerado DisplayHint como um 'uint'
+ Adição do ToolBarLayout/ToolBarLayoutDelegate ao ficheiro .pri
+ kirigami.pro: Uso da lista de ficheiros de origem do kirigami.pri
+ Não apagar os incubadores na rotina de retorno da finalização
+ Garantia de que o 'menuActions' continua um vector em vez de uma propriedade de lista
+ Adição do tipo de retorno do 'lambda' do 'singleton' DisplayHint
+ Ter em conta a altura do item ao centrar os itens delegados na vertical
+ Colocar em espera uma nova disposição, mesmo que já esteja a executar uma disposição
+ Remodelação da disposição da InlineMessage com âncoras
+ Uso da largura total para verificar se cabem todas as acções
+ Adição de um comentário sobre o espaço extra no alinhamento ao centro
+ Correcção da verificação do texto da dica no PrivateActionToolButton
+ Truque no ToolButton do 'qqc2-desktop-style' que não respeita o IconOnly não-plano
+ Preferir recolher as acções KeepVisible em vez de esconder as acções KeepVisible posteriores
+ Esconder todas as acções seguintes quando a primeira acção fica escondida
+ Adição de um sinal 'notify' para o ToolBarLayout::actions e emiti-lo na altura correcta
+ Exposição de acção para o separador do ActionsMenu
+ Mudança de nome da propriedade 'kirigamiAction' do 'loaderDelegate' do ActionsMenu para 'action'
+ Adição do 'loaderDelegate' em falta que verifica a visibilidade
+ Esconder os delegados das acções até que sejam posicionados
+ ToolBarLayout: Substituição do carregamento posterior personalizado dos delegados pelo QQmlIncubator
+ Passagem do 'displayHintSet' para C++ para que possa ser invocado a partir daí
+ Suporte para o modo da direita-para-esquerda no ToolBarLayout
+ Adição da propriedade 'minimumWidth' no ToolBarLayout
+ Remodelação do PrivateActionToolButton para uma performance melhorada
+ Descontinuação do ActionToolBar::hiddenActions
+ Definição das sugestões de tamanho da ActionToolBar
+ Uso da DisplayHint não-obsoleta no ToolBarPageHeader
+ Mostrar os erros do componente se os itens delegados falham a sua inicialização
+ Esconder os delegados das acções que foram removidas
+ Limpeza os itens delegados completos/dos ícones na destruição do delegado
+ Forçar a visibilidade do item delegado completo/do ícone
+ Uso do ToolBarLayout na disposição do ActionToolBar
+ Adição de alguns comentários ao ToolBarLayout::maybeHideDelegate
+ Adição da propriedade 'visibleWidth' ao ToolBarLayout
+ Introdução do objecto nativo do ToolBarLayout
+ Migração do DisplayHint de uma Acção para um ficheiro de enumerados em C++
+ Adição dos ícones usados na página 'Acerca' para a macro de pacotes de ícones do CMake

### KNewStuff

+ Correcção dos problemas de sincronização da 'cache' com a janela do QtQuick (erro 417985)
+ Eliminação do X-KDE-PluginInfo-Depends vazio
+ Remoção dos itens se não corresponderem mais ao filtro (erro 425135)
+ Correcção de um caso-limite onde o KNS fica bloqueado (erro 423055)
+ Tornar a tarefa interna do 'kpackage' menos frágil (erro 425811)
+ Remoção do ficheiro transferido ao usar a instalação do 'kpackage'
+ Suporte de um estilo diferente do 'knsrc' do 'kpackage' como salvaguarda
+ Tratamento da notação /* no RemoveDeadEntries (erro 425704)
+ Facilitar a obtenção da 'cache' para um motor já inicializado
+ Adição de uma pesquisa inversa para os itens com base nos seus ficheiros instalados
+ Uso da notação /* para a descompressão de sub-pastas e permitir a descompressão de sub-pastas se o ficheiro for um pacote
+ Correcção de uma inundação de avisos "Pixmap is a null pixmap" (A imagem é uma instância nula)
+ Remoção das barras do nome do item ao interpretá-la como uma localização (erro 417216)
+ Correcção de um estoiro ocasional com a tarefa do KPackageJob (erro 425245)
+ Uso do mesmo elemento no carregamento e inicialização (erro 418031)
+ Remoção de botão de detalhes (erro 424895)
+ Execução assíncrona do programa de desinstalação (erro 418042)
+ Desactivar a pesquisa quando não estiver disponível
+ Esconder a roda de progresso do carregamento ao actualizar/instalar (erro 422047)
+ Não adicionar a pasta de destino da transferência aos ficheiros do item
+ Remoção do carácter '*' ao passar uma pasta ao programa
+ Não colocar em primeiro plano o primeiro elemento no modo de ícones (erro 424894)
+ Evitar o início desnecessário da tarefa de desinstalação
+ Adição de uma opção RemoveDeadEntries para os ficheiros 'knsrc' (erro 417985)
+ [Janela do QtQuick] Correcção da última instância do ícone de actualização incorrecto
+ [Janela do QtQuick] Uso de ícones de desinstalação mais apropriados

### Plataforma KPackage

+ Não apagar a base do pacote se o pacote tiver sido removido (erro 410682)

### KQuickCharts

+ Adição de uma propriedade 'fillColorSource' nos gráficos de Linhas
+ Cálculo da suavização com base no tamanho do item e na proporção de pixels do dispositivo
+ Uso da média dos tamanhos em vez do valor máximo para determinar a suavização da linha
+ Basear a quantidade de suavização nos gráficos de linhas com base no tamanho do gráfico

### KRunner

+ Adição de um modelo para um módulo de execução de Python
+ Actualização do modelo para compatibilidade com a Loja do KDE e melhorias no README
+ Adição do suporte para sintaxes de módulos de execução 'dbus'
+ Gravação do RunnerContext depois de cada sessão correspondente (erro 424505)

### KService

+ Implementação do invokeTerminal no Windows com a pasta de trabalho, comando e variáveis de ambiente
+ Correcção da ordenação de preferência de aplicações para os tipos MIME com herança múltipla (erro 425154)
+ Criação de pacote do tipo de serviço Application num ficheiro .qrc
+ Expansão do carácter til (~) ao ler a pasta de trabalho (erro 424974)

### KTextEditor

+ Modo VI: Tornar o pequeno registo de remoção (-) directamente acessível
+ Modo VI: Cópia do comportamento dos registos numerados do 'vim'
+ Modo VI: Simplificação da implementação da adição-cópia
+ Fazer com que o "procurar pela selecção" pesquise caso não haja nenhuma selecção
+ O modo multi-linhas só faz sentido para expressões regulares multi-linhas
+ Adição de comentário sobre a verificação do padrao.isEmpty()
+ Migração da interface de pesquisa do QRegExp para o QRegularExpression
+ Modo VI: Implementação de adição-cópia
+ Aceleração *grande* do carregamento de ficheiros grandes
+ Só mostrar o nível de ampliação quando não for 100%
+ Adição de um indicador de ampliação à barra de estado
+ Adição de uma opção de configuração separada para a antevisão da correspondência de parêntesis
+ Mostrar uma antevisão da linha do parêntesis aberto correspondente
+ Permitir mais controlo sobre a invocação dos modelos de completação quando a invocação automática não for usada

### KWallet Framework

+ Evitar o conflito com uma macro no ctype.h do OpenBSD

### KWidgetsAddons

+ Adição do KRecentFilesMenu para substituir o KRecentFileAction

### KWindowSystem

+ Instalação de 'plugins' da plataforma numa pasta sem pontos no nome do ficheiro (erro 425652)
+ [xcb] Ajuste da escala de geometria dos ícones correctamente em todo o lado

### KXMLGUI

+ Permitir a desactivação da recordação das posições das janelas no X11 (erro 415150)
+ Gravar e repor a posição da janela principal (erro 415150)

### Plasma Framework

+ [PC3/BusyIndicator] Evitar a execução de uma animação invisível
+ Não usar o HighlightedTextColor nos TabButton's
+ Remoção do Layout.minimumWidth no Button e no ToolButton
+ Uso da propriedade 'spacing' para o intervalo entre os ícones e as legendas do Button/ToolButton
+ Adição do 'private/ButtonContent.qml' para os Button's e ToolButton's do PC3
+ Modificação do 'implicitWidth' e 'implicitHeight' do Button e do ToolButton do PC3 para ter em conta os valores internos
+ Adição do 'implicitWidth' e do 'implicitHeight' ao ButtonBackground
+ Correcção do valor predefinido incorrecto para o PlasmaExtras.ListItem (erro 425769)
+ Não permitir que o fundo fique mais pequeno que o SVG (erro 424448)
+ Uso do Q_DECLARE_OPERATORS_FOR_FLAGS no mesmo espaço de nomes que a definição das opções
+ Fazer com que os visuais do BusyIndicator do PC3 tenham as proporções de 1:1 (erro 425504)
+ Uso do ButtonFocus e do ButtonHover na ComboBox do PC3
+ Uso do ButtonFocus e do ButtonHover no RoundButton do PC3
+ Uso do ButtonFocus e do ButtonHover no CheckIndicator do PC3
+ Unificação do comportamento plano/normal dos Button's/ToolButton's do PC3 (erro 425174)
+ Fazer com que a classe Heading use o Label do PC3
+ [Componentes do Plasma 3] Fazer com que o texto da opção de marcação preencha a sua disposição
+ Atribuição do 'implicitWidth' e 'implicitHeight' à barra deslizante do PC2
+ Copiar os ficheiros em vez de quebrar ligações simbólicas
+ Correcção das margens à passagem do cursor sobre ToolButton's no button.svg (erro #425255)
+ [Componentes do Plasma 3] pequena remodelação do código de sombras do ToolButton
+ [Componentes do Plasma 3] Correcção da condição inversa da sombra plana do ToolButton
+ [Componentes do Plasma 3] Eliminação de 'E-comerciais' mnemónicos do texto da dica
+ Só desenhar o indicador de foco quando tiver obtido o foco através do teclado (erro 424446)
+ Eliminação das dimensões mínimas implícitas dos Button's do PC2 e do PC3
+ Adição do equivalente em PC3 ao ListItem do PC2
+ Correcção do SVG da barra de ferramentas
+ [pc3] Tornar o ToolBar mais alinhado com o o do 'qqc2-desktop-style'
+ Exposição dos meta-dados do elemento gráfico na AppletInterface
+ Não truncar o DPR a um inteiro no ID da 'cache'
+ Adição da propriedade 'timeout' no ToolTipArea
+ Definição do tipo como Dialog nas opções se este for Dialog::Normal

### Purpose

+ Aplicar os dados de configuração iniciais ao carregar a interface de configuração
+ Reposição do comportamento do AlternativesView
+ [jobcontroller] Desactivação do processo separado
+ Remodelação do tratamento de vistas de tarefas (erro 419170)

### QQC2StyleBridge

+ Correcção das sequências de atalho StandardKey no MenuItem a aparecerem como números
+ Não usar a largura/altura do item-pai no dimensionamento implícito do ToolSeparator (erro 425949)
+ Só usar o estilo "foco" para os botões de ferramentas não-planos ao carregar
+ Adição de preenchimento no topo e no fundo do ToolSeparator
+ Fazer com que o ToolSeparator respeite os valores 'topPadding' e 'bottomPadding'
+ Fazer com que o MenuSeparator use a altura calculada do fundo, não o 'implicitHeight'
+ Correcção dos botões de ferramentas com menus que usam o Brisa mais recente
+ Desenhar o controlo inteiro do CheckBox através do QStyle

### Solid

+ Adição do método estático 'storageAccessFromPath', necessário para o https://phabricator.kde.org/D28745

### Sindicância

+ Correcção da excepção da licença

### Realce de sintaxe

+ conversão de todos os temas para chaves novas nas cores do editor
+ mudança do formato JSON do tema, usando os nomes dos enumerados do meta-objecto das cores do editor
+ validação do kateversion &gt;= 5.62 para o 'fallthroughContext' sem fallthrough="true" e usar o 'attrToBool' para os atributos booleanos
+ Adição da definição de sintaxe para o todo.txt
+ correcção do matchEscapedChar(): o último carácter de uma linha é ignorado
+ Correcção do isDigit(), isOctalChar() e isHexChar(): só devem corresponder a caracteres ASCII
+ geração da visão geral dos temas
+ adição dos meta-dados do tema ao cabeçalho das páginas HTML que são geradas
+ iniciar a geração da página da colecção de temas
+ mudança de nome de 'Predefinição' para 'Brisa Claro'
+ Varnish, Vala &amp; TADS3: usar o estilo de cores predefinido
+ Melhoria no tema de cores do Vim Escuro
+ Adição do tema de cores Vim Escuro
+ Adição de ficheiros de temas de realce de sintaxe para o JSON
+ Ruby/Rails/RHTML: adição de 'spellChecking' no 'itemDatas'
+ Ruby/Rails/RHTML: usar o estilo de cores predefinido e outras melhorias
+ LDIF, VHDL, D, Clojure &amp; ANS-Forth94: usar o estilo de cores predefinido
+ ASP: uso do estilo de cores predefinido e outras melhorias
+ permitir que o Objective-C seja o preferido para os ficheiros .m
+ O .mm é mais provavelmente Objective-C++ que 'meta math'
+ usar o 'notAsciiDelimiters' apenas quando não é um carácter ASCII
+ optimização do isWordDelimiter(c) com caracteres ASCII
+ optimização do Context::load
+ uso do std::make_shared que remove a alocação no bloco de controlo
+ adição de licença adequada para actualizar o 'scripty'
+ passagem do programa 'update' para o kate-editor.org/syntax para o repositório de sintaxe
+ SELinux CIL &amp; Scheme: actualização das cores dos parêntesis para os temas escuros
+ POV-Ray: usar o estilo de cores predefinido
+ usar o programa NSIS de instalação do Krita como entrada de exemplo, retirado do krita.git
+ adição de sugestão para o programa de actualização da página Web
+ Actualização de um exemplo de Template de Django mínimo
+ actualização das referências após as últimas mudanças de 'hl'
+ CMake: correcção de cores ilegíveis em temas escuros e outras melhorias
+ Adição do exemplo 'pipe' e 'ggplot2'
+ correcção de erro no nome do realce de Varnish
+ uso do realce correcto para o ASM 68k: Motorola 68k (VASM/Devpac)
+ correcção do XML para ser válido face ao XSD
+ BrightScript: Adição da sintaxe de excepções
+ Melhoria dos comentários em algumas definições de sintaxe (Parte 3)
+ R Script: uso de estilo de cores predefinido e outras melhorias
+ PicAsm: correcção do realce de palavras-chave do pré-processador desconhecidas
+ Melhoria dos comentários em algumas definições de sintaxe (Parte 2)
+ Linhas de Modo: remoção das regras LineContinue
+ correcção rápida de ficheiro de realce danificado
+ Optimização: verificar se estamos num comentário antes de usar o ##Doxygen que contém diversas RegExpr
+ Linguagens Assembly: diversas correcções e mais realce de sintaxe
+ ColdFusion: usar o estilo de cores predefinido e substituição de algumas regras  RegExpr
+ Melhoria dos comentários em algumas definições de sintaxe, parte 1
+ Uso de parêntesis em ângulo para a informação de contexto
+ Reversão da remoção do 'byte-order-mark'
+ xslt: mudança da cor das marcas XSLT
+ Ruby, Perl, QML, VRML &amp; xslt: usar o estilo de cores predefinido e melhoria nos comentários
+ txt2tags: melhorias e correcções, uso do estilo de cores predefinido
+ Correcção de erros e adição de 'fallthroughContext=AttrNormal' para cada contexto do diff.xml, dado que todas as regras contêm column=0
+ correcção dos problemas encontrados pela verificação estática
+ importação do realce de Pure de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ correcção do atributo 'kateversion'
+ tornar a ordem da pesquisa de realces independente das traduções
+ correcção de problemas no formato da versão + espaços
+ importação do realce de Modula-3 de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ correcção do formato da versão
+ correcção de questões de espaços detectadas pela verificação estática
+ importação do realce de LLVM de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ correcção de questões de espaços detectadas pela verificação estática
+ importação do Idris de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ correcção de problemas detectados pela verificação estática
+ importação do ATS de https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ destacar apenas para os dados não criados recentemente
+ criação do StateData a pedido

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
