---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Módulos extra do CMake

- Novos argumentos para o ecm_add_tests(). (erro <a href='https://bugs.kde.org/show_bug.cgi?id=345797'>345797</a>)

### Integração do Framework

- Usar a <i>initialDirectory</i> correta para o KDirSelectDialog
- Confirmação de que o esquema é indicado ao substituir o valor da URL inicial
- Aceitar apenas pastas existentes no modo FileMode::Directory

### KActivities

(nenhum registro de alterações indicado)

### KAuth

- Tornar o KAUTH_HELPER_INSTALL_ABSOLUTE_DIR disponível para todos os usuários do KAuth

### KCodecs

- KEmailAddress: Adição de sobrecarga para o <i>extractEmailAddress</i> e o <i>firstEmailAddress</i> que devolve uma mensagem de erro.

### KCompletion

- Correção de uma seleção indesejada ao editar o nome do arquivo na janela de arquivos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=344525'>344525</a>)

### KConfig

- Correção da falha quando o QWindow::screen() for nulo
- Adição do KConfigGui::setSessionConfig() (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346768'>346768</a>)

### KCoreAddons

- Nova API de conveniência KPluginLoader::findPluginById()

### KDeclarative

- Suporte para criação de um ConfigModule a partir do KPluginMetdata
- Correção dos eventos <i>pressAndhold</i>

### KDELibs 4 Support

- Usar o <i>QTemporaryFile</i> em vez de criar um arquivo temporário com nome fixo.

### KDocTools

- Atualização das traduções
- Atualização de 'customization/ru'
- Correção de entidades com links inválidos

### KEmoticons

- Cache do tema no plugin de integração

### KGlobalAccel

- [execução] O código específico da plataforma foi movido para plugins

### KIconThemes

- Otimização do KIconEngine::availableSizes()

### KIO

- Não tentar completar os usuários e declarar quando o <i>prepend</i> não é vazio. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346920'>346920</a>)
- Usar o KPluginLoader::factory() ao carregar o KIO::DndPopupMenuPlugin
- Correção de um bloqueio ao usar proxies de rede (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346214'>346214</a>)
- Correção do KIO::suggestName para preservar as extensões dos arquivos
- Retirar o <i>kbuildsycoca4</i> ao atualizar o <i>sycoca5</i>.
- KFileWidget: Não aceitar arquivos no modo apenas para pastas
- KIO::AccessManager: Permitir o tratamento de QIODevice sequenciais de forma assíncrona

### KNewStuff

- Adição de um novo método <i>fillMenuFromGroupingNames</i>
- KMoreTools: Adição de diversos agrupamentos novos
- KMoreToolsMenuFactory: Tratamento do "git-clients-and-actions"
- createMenuFromGroupingNames: Tornar o parâmetro <i>url</i> opcional

### KNotification

- Correção de uma falha no NotifyByExecute quando nenhum widget for definido (erro <a href='https://bugs.kde.org/show_bug.cgi?id=348510'>348510</a>)
- Melhoria do tratamento de notificações ao fechá-las (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342752'>342752</a>)
- Substituição do uso do QDesktopWidget pelo QScreen
- Garantir que o KNotification pode ser usado a partir de uma tarefa não-gráfica

### Package Framework

- Guardar o acesso à estrutura <i>qpointer</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347231'>347231</a>)

### KPeople

- Usar o QTemporaryFile em vez de definir como /tmp.

### KPty

- Usar o <i>tcgetattr</i> &amp; <i>tcsetattr</i> se estiverem disponíveis

### Kross

- Correção do carregamento dos módulos "forms" e "kdetranslation" do Kross

### KService

- Ao executar como <i>root</i> preservar o proprietário dos arquivos nos arquivos de <i>cache</i> existentes (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342438'>342438</a>)
- Proteção contra a impossibilidade de abrir <i>streams</i> (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342438'>342438</a>)
- Correção da verificação de permissões inválidas para gravação de arquivos (erro <a href='https://bugs.kde.org/show_bug.cgi?id=342438'>342438</a>)
- Correção da consulta do <i>ksycoca</i> com pseudo-tipos MIME x-scheme-handler/*. (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347353'>347353</a>)

### KTextEditor

- Permissão para que os plugins/aplicativos de terceiros instalem seus próprios arquivos XML de realce de sintaxe na pasta 'katepart5/syntax', como ocorre no KDE 4.x
- Adição do KTextEditor::Document::searchText()
- Reativação do uso do KEncodingFileDialog (erro <a href='https://bugs.kde.org/show_bug.cgi?id=343255'>343255</a>)

### KTextWidgets

- Adição de um método para limpar um decorador
- Permissão para uso de um decorador personalizado do Sonnet
- Implementação da ação "procurar anterior" no KTextEdit.
- Retorno do suporte de texto-para-fala

### KWidgetsAddons

- KAssistantDialog: Retorno do botão de Ajuda que existia na versão para o KDELibs4

### KXMLGUI

- Adição do gerenciamento de sessões para o KMainWindow (erro <a href='https://bugs.kde.org/show_bug.cgi?id=346768'>346768</a>)

### NetworkManagerQt

- Eliminação do suporte para WiMAX no NM 1.2.0+

### Plasma Framework

- Os componentes de calendário conseguem agora mostrar os números das semanas (erro <a href='https://bugs.kde.org/show_bug.cgi?id=338195'>338195</a>)
- Usar o QtRendering nas fontes dos campos de senha
- Correção da pesquisa do AssociatedApplicationManager quando um determinado tipo MIME tem (erro <a href='https://bugs.kde.org/show_bug.cgi?id=340326'>340326</a>)
- Correção das cores do plano de fundo do painel (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347143'>347143</a>)
- Eliminação da mensagem "Não foi possível carregar o miniaplicativo"
- Capacidade de carregamento de KCMs em QML nas janelas de configuração dos plasmoides
- Não usar o DataEngineStructure nos miniaplicativos
- Remoção do máximo de dependências do <i>sycoca</i> na <i>libplasma</i>
- [plasmacomponents] Fazer o SectionScroller seguir o ListView.section.criteria
- As barras de rolagem não ficam mais ocultas automaticamente quando estiver usando uma tela sensível ao toque (erro <a href='https://bugs.kde.org/show_bug.cgi?id=347254'>347254</a>)

### Sonnet

- Uso de um <i>cache</i> central para o SpellerPlugins.
- Redução de alocações temporárias.
- Optimização: Não remover o <i>cache</i> de dicionários ao copiar os objetos do verificador ortográfico.
- Otimização de diversas chamadas save(), fazendo apenas uma única no fim, caso haja necessidade.

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
