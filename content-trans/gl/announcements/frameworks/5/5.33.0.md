---
aliases:
- ../../kde-frameworks-5.33.0
date: 2017-04-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Engadíronse descricións ás ordes (balooctl)s
- Buscar tamén en directorios que sexan ligazóns simbólicas (fallo 333678)

### BluezQt

- Fornecer un tipo de dispositivo para dispositivos de baixa enerxía

### Módulos adicionais de CMake

- Indicar qml-root-path como o directorio de compartición no prefixo
- Corrixir a compatibilidade de ecm_generate_pkgconfig_file compatibility co novo CMake
- Só rexistrar as opcións APPLE_* cando APPLE é verdadeiro

### KActivitiesStats

- Engadíronse predefinicións á aplicación de proba
- Mover elementos de maneira axeitada á posición desexada
- Sincronizando o cambio de orde con outras instancias do modelo
- Se a orde non está definida, ordenar as entradas por identificador

### Ferramentas de Doxygen de KDE

- [Meta] Cambiar o mantedor en setup.py

### KAuth

- Infraestrutura para Mac
- Engadir a posibilidade de matar un KAuth::ExecuteJob

### KConfig

- Desinfectar a lista de atallos ao ler ou escribir de kdeglobals
- Evitar cambios de asignación innecesarios retirando a chamada a squeeze no búfer temporal

### KDBusAddons

- KDBusService: engadir un accesor para o nome de servizo de D-Bus que rexistramos

### KDeclarative

- Con Qt ≥ 5.8 usar a nova API para definir a infraestrutura de grafo de escena
- Non definir acceptHoverEvents en DragArea xa que non os usaremos

### KDocTools

- meinproc5: ligar aos ficheiros, non á biblioteca (fallo 377406)

### KFileMetaData

- Facer que PlainTextExtractor volva a coincidir con «text/plain»

### KHTML

- Páxina de erro, cargar correctamente a imaxe (cun URL real)

### KIO

- Facer que a redirección de URL file:// remotos a smb:// volva a funcionar
- Manter a codificación da consulta ao usar un proxy de HTTP
- Actualizáronse os axentes de usuarios (Firefox 52 ESR, Chromium 57)
- Xestionar e truncar a cadea de visualización de URL asignada á descrición do traballo. Evita que os URL de data: longos se inclúan nas notificacións da interface gráfica
- Engadir KFileWidget::setSelectedUrl() (fallo 376365)
- Corrixir o modo de garda de KUrlRequester engadindo setAcceptMode

### KItemModels

- Mencionar o novo QSFPM::setRecursiveFiltering(true), que fai KRecursiveFilterProxyModel obsoleto

### KNotification

- Non retirar as notificacións encoladas cando se inicia o servizo fd.o
- Adaptacións para a plataforma Mac

### KParts

- Documentación da API: corrixir a nota que falta para chamar a setXMLFile con KParts::MainWindow

### KService

- Corrixir as mensaxes de terminal “Non se atopou: «»”

### KTextEditor

- Expoñer funcionalidade interna adicional de View na API pública
- Aforrar moitas asignacións en setPen
- Corrixir ConfigInterface de KTextEditor::Document
- Engadíronse opcións de fonte e de corrección ortográfica sobre a marcha a ConfigInterface

### KWayland

- Engadir compatibilidade con wl_shell_surface::set_popup e popup_done

### KWidgetsAddons

- Permitir construír con Qt sen a11y activada
- Corrixir un consello de tamaño incorrecto cando se chama a animatedShow cun pai agochado (fallo 377676)
- Corrixir que os caracteres de KCharSelectTable se elidan
- Activar todos os planos no diálogo de probas de kcharselect

### NetworkManagerQt

- WiredSetting: devolver autonegotiate incluso cando estea desactivado
- Impedir que Qt defina os sinais de glib2
- WiredSetting: A velocidade e o dúplex poden definirse unicamente cando a negociación automática está desactivada (fallo 376018)
- O valor de negociación automática da opción con fíos debería ser falso

### Infraestrutura de Plasma

- [ModelContextMenu] Usar Instantiator en vez do apaño «Repeater e cambiar de pai»
- [Calendario] Encoller e elidir os nomes das semanas como se fai no delegado dos días (fallo 378020)
- [Elemento de icona] Facer que a propiedade «smooth» funcione de verdade
- Definir o tamaño implícito a partir do tamaño de orixe para fontes de URL de imaxe ou SVG
- engadir unha nova propiedade no contedor para un modo de edición
- corrixir maskRequestedPrefix cando non se usa prefixo (fallo 377893)
- [Menu] Harmonizar a colocación de openRelative
- A maioría dos menús (contextuais) agora teñen aceleradores (atallos Alt+letra) (fallo 361915)
- Controis de Plasma baseados en QtQuickControls2
- Xestionar applyPrefixes cunha cadea baleira (fallo 377441)
- eliminar realmente as cachés vellas de temas
- [Interface de contedor] Abrir os menús contextuais ao premer a tecla de menú
- [Tema Breeze de Plasma] Mellorar as iconas action-overlay (fallo 376321)

### Realce da sintaxe

- TOML: Corrixir o salientado de secuencias de escape de cadeas
- Actualizar o realce de sintaxe de Clojure
- Algunhas actualizacións na sintaxe de OCaml
- Salientar os ficheiros *.sbt como código Scala
- Usar o salientador de QML tamén para ficheiros .qmltypes

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
