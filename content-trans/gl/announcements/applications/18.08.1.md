---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE publica a versión 18.08.1 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.08.1 das aplicacións de KDE
version: 18.08.1
---
September 6, 2018. Today KDE released the first stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

As máis dunha ducia de correccións de erros inclúen melloras en, entre outros, Kontact, Cantor, Gwenview, Okular e Umbrello.

Entre as melloras están:

- O compoñente KIO-MTP xa non quebra cando ao dispositivo xa accedeu outra aplicación
- Agora o envío de mensaxes desde KMail usa o contrasinal cando se indica nunha consulta de contrasinal
- Agora Okular lembra o modo de barra lateral tras gardar documentos PDF
