---
aliases:
- ../announce-applications-18.04.0
changelog: true
date: 2018-04-19
description: KDE publica a versión 18.04.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.04.0 das aplicacións de KDE
version: 18.04.0
---
19 de abril de 2018. Publicouse a versión 18.04.0 das aplicacións de KDE.

Traballamos continuamente en mellorar o software que se inclúe nas aplicacións de KDE, e esperamos que as novas melloras e correccións de fallos lle resulten de utilidade!

## What's new in KDE Applications 18.04

### Sistema

{{<figure src="/announcements/applications/18.04.0/dolphin1804.png" width="600px" >}}

The first major release in 2018 of <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, features many improvements to its panels:

- Agora poden agocharse as seccións do panel de lugares se prefire non mostralas, e agora hai unha nova sección «Rede» dispoñíbel para as entradas de lugares remotos.
- O panel do terminal pode ancorarse en calquera parte da xanela, e se intenta abrilo sen Konsole instalado, Dolphin mostrará un aviso e axudaralle a instalalo.
- Mellorouse a compatibilidade do panel de «Información» con HiDPI.

Tamén se actualizaron a vista de cartafol e os menús:

- Agora o cartafol do lixo mostra un botón de «Baleirar o lixo».
- Engadiuse un elemento de menú de «Mostrar o destino» para axudar a atopar os destinos de ligazóns simbólicas.
- Mellorouse a integración con Git, xa que agora o menú contextual para cartafoles de Git mostra dúas novas accións para «git log» e «git merge».

As melloras adicionais inclúen:

- Introduciuse un novo atallo que permite abrir a barra de filtro premendo simplemente a tecla de barra inclinada (/).
- Agora pode ordenar e organizar fotos polo día que se sacaron.
- Acelerouse arrastrar e soltar para moitos ficheiros pequenos con Dolphin e agora os usuarios poden desfacer traballos de cambio de nome por lotes.

{{<figure src="/announcements/applications/18.04.0/konsole1804.png" width="600px" >}}

To make working on the command line even more enjoyable, <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, can now look prettier:

- Pode descargar esquemas de cores mediante KNewStuff.
- A barra de desprazamento intégrase mellor co esquema de cores activo.
- De maneira predeterminada, a barra de separadores móstranse só cando se necesita.

Os colaboradores de Konsole non pararon aquí, e introduciron moitas novas funcionalidades:

- Engadiuse un novo modo de só lectura e unha propiedade de perfil para conmutar a copia de texto como HTML.
- En Wayland, Konsole agora goza do menú de arrastrar e soltar.
- Aplicáronse varias melloras relacionadas co protocolo ZMODEM: agora Konsole pode xestionar o indicador B01 de envío de zmodem, mostrará o progreso mentres se transfiren datos, agora o botón de «Cancelar» do diálogo funciona como debería, e transferir os ficheiros máis grandes funciona mellor léndoos en memoria en anacos de 1 MB.

As melloras adicionais inclúen:

- Corrixiuse o desprazamento coa roda do rato con libinput, e agora evítase rotar polo historial do intérprete de ordes ao desprazar coa roda do rato.
- As buscas actualízanse tras cambiar a opción de expresión regular da coincidencia de busca e ao premer Ctrl + Retroceso Konsole comportarase como xterm.
- Corrixiuse o atallo «--background-mode».

### Son e vídeo

<a href='https://juk.kde.org/'>JuK</a>, KDE's music player, now has Wayland support. New UI features include the ability to hide the menu bar and having a visual indication of the currently playing track. While docking to the system tray is disabled, JuK will no longer crash when attempting to quit via the window 'close' icon and the user interface will remain visible. Bugs regarding JuK autoplaying unexpectedly when resuming from sleep in Plasma 5 and handling the playlist column have also been fixed.

For the 18.04 release, contributors of <a href='https://kdenlive.org/'>Kdenlive</a>, KDE's non-linear video editor, focused on maintenance:

- O cambio de tamaño de fragmentos xa non corrompe as transicións e os fotogramas clave.
- As iconas terán mellor resolución para usuarios de cambio de escala de pantalla en monitores HiDPI.
- Corrixiuse unha quebra que podía afectar a algunhas configuracións no inicio.
- Agora requírese a versión 6.6.0 ou unha posterior de MLT, e mellorouse a compatibilidade.

#### Gráficos

{{<figure src="/announcements/applications/18.04.0/gwenview1804.png" width="600px" >}}

Over the last months, contributors of <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer, worked on a plethora of improvements. Highlights include:

- Engadiuse a compatibilidade con controladores de MPRIS para que agora poida controlar as presentacións a pantalla completa mediante KDE Connect, as teclas de reprodución do teclado e o plasmoide de reprodutor.
- Agora poden desactivarse os botóns que aparecen ao cubrir miniaturas.
- A ferramenta de recorte recibiu varias melloras, por exemplo agora a configuracións lémbrase tras cambiar de imaxe, a forma da caixa de selección pode bloquearse mantendo premidas as teclas Maiús ou Ctrl e tamén pode bloquearse para usar as proporcións da imaxe mostrada actualmente.
- No modo a pantalla completa, agora pode saír usando a tecla Esc e a paleta de cores reflectirá o tema de cores activo. Se sae de Gwenview desde este modo, esta opción lembrarase e a nova sesión tamén comezará a pantalla completa.

A atención aos detalles é importante, polo que se realizaron melloras nas seguintes áreas de Gwenview:

- Gwenview mostrará máis rutas de ficheiro lexíbeis por humanos na lista de «Cartafoles recentes», mostrará o menú contextual axeitado para os elementos da lista de «Ficheiros recentes», e esquecerá todo correctamente ao usar a funcionalidade de «Desactivar o historial».
- Premer un cartafol na barra lateral permite conmutar entre os modos de navegación e de vista e lembra o último modo usado ao cambiar entre cartafoles, permitindo así navegar máis rapidamente por coleccións enormes de imaxes.
- Simplificouse a navegación con teclado indicando o foco do teclado no modo de exploración.
- A funcionalidade de «Axustar a anchura» substituíuse por unha función «Encher» máis xeneralizada.

{{<figure src="/announcements/applications/18.04.0/gwenviewsettings1804.png" width="600px" >}}

Incluso as melloras máis pequenas poden facer os fluxos de traballo dos usuarios máis satisfactorios:

- Para mellorar a consistencia, agora as imaxes SVG agrándanse como o resto de imaxes ao activarse «Vista de imaxe → Agrandar as imaxes máis pequenas».
- Tras editar unha imaxe ou desfacer cambios non se volverá perder a sincronización entre a vista de imaxe e a miniatura.
- Ao cambiar imaxes de nome, a extensión de nome de ficheiro retirarase da selección de maneira predeterminada e agora o diálogo de mover, copiar e ligar mostra o cartafol actual de maneira predeterminada.
- Corrixíronse unha morea de cortes de papel visuais, p. ex. na barra de URL, para a barra de ferramentas a pantalla completa, e para as animacións do consello da miniatura. Tamén se engadiron iconas que faltaban.
- Por último, pero non por iso menos importantes, o botón de cubrir a pantalla completa verá directamente a imaxe en vez de mostrar o contido do cartafol, e agora a configuración avanzada permite máis control pola intención de renderizado de cor de ICC.

#### Oficina

In <a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's universal document viewer, PDF rendering and text extraction can now be cancelled if you have poppler version 0.63 or higher, which means that if you have a complex PDF file and you change the zoom while it's rendering it will cancel immediately instead of waiting for the render to finish.

Atopará unha mellora da compatibilidade con JavaScript de PDF para AFSimple_Calculate, e se ten unha versión de poppler ≥ 0.64 Okular permitirá cambios de JavaScript de PDF nos formularios en estado de só lectura.

Management of booking confirmation emails in <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, has been significantly enhanced to support train bookings and uses a Wikidata-based airport database for showing flights with correct timezone information. To make things easier for you, a new extractor has been implemented for emails not containing structured booking data.

As melloras adicionais inclúen:

- Co novo complemento «Experto» se pode mostrar de novo a estrutura da mensaxe.
- Engadiuse un complemento no editor de sieve para seleccionar correos electrónicos da base de datos de Akonadi.
- A busca de texto do editor mellorouse e agora permite expresións regulares.

#### Utilidades

{{<figure src="/announcements/applications/18.04.0/spectacle1804.png" width="600px" >}}

Improving the user interface of <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, was a major focus area:

- Cambiouse completamente o deseño da fila inferior de botóns e agora mostra un botón para abrir a xanela de configuración e o novo botón de «Ferramentas» que revela formas de abrir o cartafol de capturas de pantalla usadas por última vez e iniciará un programa de gravación de vídeo.
- Agora, de maneira predeterminada, lémbrase o último modo de garda usado.
- Agora o tamaño de xanela adáptase ás proporcións da captura de pantalla, o que da lugar a unha miniatura de captura de pantalla máis agradábel e eficiente a nivel de espazo.
- Simplificouse de maneira significativa a xanela de configuración.

Ademais, os usuarios poderán simplificar os seus fluxos de traballo con estas novas funcionalidades:

- Ao capturar unha xanela concreta o seu título pode engadirse automaticamente ao nome do ficheiro da captura de pantalla.
- Agora o usuario pode escoller se Spectacle sae automaticamente tras calquera operación de garda ou copia.

Entre as correccións de fallo importantes están:

- Arrastras e soltar sobre xanelas de Chromium xa funciona como corresponde.
- Usando de maneira repetitiva os atallos de teclado para gardar unha captura de pantalla xa non provoca un diálogo de aviso de atallo ambiguo.
- O bordo inferior da selección de rexión rectangular de capturas de pantalla pode axustarse con maior precisión.
- Mellorouse a fiabilidade da captura de xanelas que tocan bordos de pantalla cando os efectos de escritorio están activados.

{{<figure src="/announcements/applications/18.04.0/kleopatra1804.gif" width="600px" >}}

With <a href='https://www.kde.org/applications/utilities/kleopatra/'>Kleopatra</a>, KDE's certificate manager and universal crypto GUI, Curve 25519 EdDSA keys can be generated when used with a recent version of GnuPG. A 'Notepad' view has been added for text based crypto actions and you can now sign/encrypt and decrypt/verify directly in the application. Under the 'Certificate details' view you will now find an export action, which you can use to export as text to copy and paste. What's more, you can import the result via the new 'Notepad' view.

In <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's graphical file compression/decompression tool with support for multiple formats, it is now possible to stop compressions or extractions while using the libzip backend for ZIP archives.

### Aplicacións que se unen ao calendario de publicación das aplicacións de KDE

KDE's webcam recorder <a href='https://userbase.kde.org/Kamoso'>Kamoso</a> and backup program <a href='https://www.kde.org/applications/utilities/kbackup/'>KBackup</a> will now follow the Applications releases. The instant messenger <a href='https://www.kde.org/applications/internet/kopete/'>Kopete</a> is also being reintroduced after being ported to KDE Frameworks 5.

### As aplicacións adquiren un calendario de publicación de seu

The hex editor <a href='https://community.kde.org/Applications/18.04_Release_Notes#Tarballs_that_we_do_not_ship_anymore'>Okteta</a> will have its own release schedule after a request from its maintainer.

### Destrución de fallos

Solucionáronse máis de 170 fallos en aplicacións como, entre outros, a colección de Kontact, Ark, Dolphin, Gwenview, K3b, Kate, Kdenlive, Konsole, Okular e Umbrello!

### Historial completo de cambios
