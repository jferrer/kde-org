---
aliases:
- ../announce-applications-16.08.0
changelog: true
date: 2016-08-18
description: KDE에서 KDE 프로그램 16.08.0 출시
layout: application
title: KDE에서 KDE 프로그램 16.08.0 출시
version: 16.08.0
---
2016년 8월 18일. KDE에서는 KDE 프로그램 16.08을 출시했습니다. 이 릴리스에는 사용 편의성 개선, 새로운 기능 추가, 사소한 버그 수정 등이 있었으며 KDE 프로그램을 통해서 장치를 더 완벽하게 사용하는 것에 한 걸음 더 다가갔습니다.

<a href='https://www.kde.org/applications/graphics/kolourpaint/'>Kolourpaint</a>, <a href='https://www.kde.org/applications/development/cervisia/'>Cervisia</a> and KDiskFree have now been ported to KDE Frameworks 5 and we look forward to your feedback and insight into the newest features introduced with this release.

In the continued effort to split Kontact Suite libraries to make them easier to use for third parties, the kdepimlibs tarball has been split into akonadi-contacts, akonadi-mime and akonadi-notes.

We have discontinued the following packages: kdegraphics-strigi-analyzer, kdenetwork-strigi-analyzers, kdesdk-strigi-analyzers, libkdeedu and mplayerthumbs. This will help us focus in the rest of the code.

### Kontact로 사람들과 대화하기

<a href='https://userbase.kde.org/Kontact'>The Kontact Suite</a> has got the usual round of cleanups, bug fixes and optimizations in this release. Notable is the use of QtWebEngine in various compontents, which allows for a more modern HTML rendering engine to be used. We have also improved VCard4 support as well as added new plugins that can warn if some conditions are met when sending an email, e.g. verifying that you want to allow sending emails with a given identity, or checking if you are sending email as plaintext, etc.

### 새로운 Marble 버전

<a href='https://marble.kde.org/'>Marble</a> 2.0 is part of KDE Applications 16.08 and includes more than 450 code changes including improvements in navigation, rendering and an experimental vector rendering of OpenStreetMap data.

### 다양한 압축 파일

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a> can now extract AppImage and .xar files as well as testing the integrity of zip, 7z and rar archives. It can also add/edit comments in rar archives

### 터미널 개선

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a> has had improvements regarding font rendering options and accessibility support.

### 그리고 더 있습니다!

<a href='https://kate-editor.org'>Kate</a> got movable tabs. <a href='https://kate-editor.org/2016/06/15/kates-tabbar-gets-movable-tabs/'>More information...</a>

<a href='https://www.kde.org/applications/education/kgeography/'>KGeography</a>, has added provinces and regions maps of Burkina Faso.

### 공격적인 버그 잡기

Kontact 제품군, Ark, Cantor, Dolphin, KCalc, Kdenlive 등 프로그램에서 120개 이상의 버그가 수정되었습니다!

### 전체 변경 기록
