---
aliases:
- ../../kde-frameworks-5.12.0
date: 2015-07-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### 추가 CMake 모듈

- query_qmake 매크로의 오류 보고 개선

### BluezQt

- 어댑터를 삭제하기 전에 모든 장치 삭제(버그 349363)
- README.md의 링크 업데이트

### KActivities

- 특정한 활동에 있을 때 사용자를 추적하지 않는 옵션 추가(웹 브라우저의 '사생활 보호 모드'와 유사함)

### KArchive

- copyTo()에 지정한 파일의 실행 권한 유지
- 호출되지 않는 코드를 삭제해서 ~KArchive를 명확하게 합니다.

### KAuth

- 다른 원본에서 kauth-policy-gen을 사용할 수 있도록 허용

### KBookmarks

- url과 텍스트가 비어 있는 책갈피를 추가하지 않음
- KBookmark URL을 인코딩하여 KDE4 프로그램과의 호환성 수정

### KCodecs

- x-euc-tw 탐지 도구 제거

### KConfig

- kconfig_compiler를 libexec에 설치
- New code generation option TranslationDomain=, for use with TranslationSystem=kde; normally needed in libraries.
- 여러 원본에서 kconfig_compiler를 사용할 수 있도록 수정

### KCoreAddons

- KDirWatch: 필요한 경우에만 FAM과 연결
- Allow filtering plugins and applications by formfactor
- Make it possible to use desktoptojson from different sources

### KDBusAddons

- Clarify exit value for Unique instances

### KDeclarative

- KColorButton의 QQC 클론 추가
- 가능한 경우 각 kdeclarative 인스턴스에 대해 QmlObject 할당
- QML 코드에서 Qt.quit() 호출이 작동하도록 변경
- 'mart/singleQmlEngineExperiment' 브랜치 합침
- implicitWidth/height를 기반으로 sizeHint 구현
- 정적 엔진을 사용하는 QmlObject의 하위 클래스

### KDELibs 4 지원

- KMimeType::Ptr::isNull 구현 수정
- Reenable support for KDateTime streaming to kDebug/qDebug, for more SC
- kdebugdialog의 올바른 번역 카탈로그 불러오기
- Don't skip documenting deprecated methods, so that people can read the porting hints

### KDESU

- Fix CMakeLists.txt to pass KDESU_USE_SUDO_DEFAULT to the compilation so it is used by suprocess.cpp

### KDocTools

- K5 Docbook 템플릿 업데이트

### KGlobalAccel

- private runtime API gets installed to allow KWin to provide plugin for Wayland.
- componentFriendlyForAction 이름 해석 폴백

### KIconThemes

- 아이콘 크기가 올바르지 않을 때 표시하지 않음

### KItemModels

- New proxy model: KRearrangeColumnsProxyModel. It supports reordering and hiding columns from the source model.

### KNotification

- org.kde.StatusNotifierItem.xml의 픽스맵 종류 수정
- [ksni] 이름으로 동작을 가져오는 메서드 추가(버그 349513)

### KPeople

- PersonsModel 필터링 기능 구현

### KPlotting

- KPlotWidget: add setAutoDeletePlotObjects, fix memory leak in replacePlotObject
- x0&gt; 0일 때 눈금 누락 수정
- KPlotWidget: SetMinimumSize 또는 resize를 호출할 필요가 없음.

### KTextEditor

- debianchangelog.xml: Debian/Stretch, Debian/Buster, Ubuntu-Wily 추가
- UTF-16 서로게이트 쌍 백스페이스/삭제 동작 수정.
- QScrollBar가 WheelEvents를 처리하도록 함(버그 340936)
- Apply patch from KWrite devel top update pure basic HL, "Alexander Clay" &lt;tuireann@EpicBasic.org&gt;

### KTextWidgets

- 확인 단추 활성화/비활성화 수정

### KWallet 프레임워크

- kwallet-query 명령행 도구 재도입 및 개선.
- Support to overwrite maps entries.

### KXMLGUI

- KDE 정보 대화 상자에 "KDE 프레임워크 버전"을 표시하지 않음

### Plasma 프레임워크

- Make the dark theme completely dark, also the complementary group
- Cache naturalsize separately by scalefactor
- ContainmentView: Do not crash on an invalid corona metadata
- AppletQuickItem: Do not access KPluginInfo if not valid
- Fix occasional empty applet config pages (bug 349250)
- Improve hidpi support in the Calendar grid component
- Verify KService has valid plugin info before using it
- [calendar] Ensure the grid is repainted on theme changes
- [calendar] Always start counting weeks from Monday (bug 349044)
- [calendar] Repaint the grid when show week numbers setting changes
- An opaque theme is now used when only the blur effect is available (bug 348154)
- Whitelist applets/versions for separate engine
- Introduce a new class ContainmentView

### Sonnet

- Allow to use highlight spellchecking in a QPainTextEdit

<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>Dot 기사</a>에서 릴리스에 대해 토론할 수 있습니다.
