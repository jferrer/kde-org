---
aliases:
- ../announce-4.11.2
date: 2013-10-01
description: KDE Ships Plasma Workspaces, Applications and Platform 4.11.2.
title: KDE에서 Plasma 작업 공간, 프로그램, 플랫폼 10월 업데이트 출시
---
2013년 10월 1일. 오늘 KDE에서는 작업 공간, 프로그램, 개발 플랫폼 업데이트를 출시했습니다. 이 업데이트는 4.11 시리즈의 두 번째 월간 업데이트입니다. 릴리스에서 밝힌 대로 이 작업 공간 릴리스는 향후 2년간 계속 업데이트됩니다. 이 릴리스는 버그 수정 및 번역 업데이트만 포함하기 때문에 업데이트 시 큰 문제가 발생하지 않을 것입니다.

창 관리자 KWin, 파일 관리자 Dolphin, 개인 정보 관리자 Kontact 등에 70개 이상의 버그 수정 및 기능 개선이 있었습니다. 많은 안정성 수정과 번역 개선이 있었습니다.

A more complete <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.2&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>list</a> of changes can be found in KDE's issue tracker. For a detailed list of changes that went into 4.11.2, you can also browse the Git logs.

소스 코드를 다운로드하거나 패키지를 설치하려면 <a href='/info/4/4.11.2'>4.11.2 정보 페이지</a>를 참조하십시오. KDE 작업 공간, 프로그램, 개발 플랫폼의 4.11 버전에 대해서 더 알아보려면 <a href='/announcements/4.11/'>4.11 릴리스 노트</a>를 참조하십시오.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Kontact의 새로운 나중에 보내기 작업 방식` width="600px">}}

KDE software, including all libraries and applications, is available for free under Open Source licenses. KDE's software can be obtained as source code and various binary formats from <a href='http://download.kde.org/stable/4.11.2/'>download.kde.org</a> or from any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.
