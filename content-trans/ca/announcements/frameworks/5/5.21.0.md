---
aliases:
- ../../kde-frameworks-5.21.0
date: 2016-04-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Framework nou: «KActivitiesStats», una biblioteca per a accedir a l'ús de dades estadístiques recollides pel gestor d'activitats del KDE.

### Tots els frameworks

Ara es requereixen les Qt &gt;= 5.4, és a dir, ja no s'accepten més les Qt 5.3.

### Attica

- Afegir una variant «const» al mètode «getter»

### Baloo

- Centralitzar la mida del lot en el «config»
- Eliminar la indexació de codi de bloqueig de fitxers «text/plain» sense extensió .txt (error 358098)
- Verificar tant el nom de fitxer com el contingut del fitxer per determinar el tipus MIME (error 353512)

### BluezQt

- ObexManager: Dividir els missatges d'error per als objectes que manquen

### Icones Brisa

- Afegir icones Brisa del Lokalize
- Sincronitzar les icones d'aplicació entre Brisa i Brisa fosca
- Actualitzar les icones de tema i eliminar els grups del «kicker» per solucionar les icones d'aplicació del sistema
- Afegir la implementació «xpi» per als complements del Firefox (error 359913)
- Actualitzar la icona de l'Okular per la correcta
- Afegir la implementació per les icones de l'aplicació «ktnef»
- Afegir icones per al Kmenueditor, Kmouse i Knotes
- Canviar la icona de volum d'àudio silenciat a usar «-» per silenci en lloc de només color vermell (error 360953)
- Afegir la implementació del tipus MIME «djvu» (error 360136)
- Afegir un enllaç en lloc d'una entrada doble
- Afegir una icona de «ms-shortcut» per al GnuCash (error 360776)
- Canviar el fons del fons de pantalla al genèric
- Actualitzar les icones a usar en un fons de pantalla genèric
- Afegir la icona per al Konqueror (error 360304)
- Afegir la icona de feina en curs per l'animació de progrés en el KDE (error 360304)
- Afegir una icona d'instal·lació de programari i actualitzar la icona d'actualització amb el color correcte
- Afegir icones d'emblema d'afegir i eliminar per a la selecció del Dolphin, afegir icona de muntatge
- Eliminar el full d'estil de les icones de les miniaplicacions Analogclock i Kickerdash
- Sincronitzar Brisa i Brisa fosca (error 360294)

### Mòduls extres del CMake

- Esmenar «_ecm_update_iconcache» per actualitzar només la ubicació d'instal·lació
- Revertir «ECMQtDeclareLoggingCategory: Incloure &lt;QDebug&gt; amb el fitxer generat»

### Integració del marc de treball

- Contingència a la implementació de QCommonStyle de «standardIcon»
- Definir un temps d'espera predeterminat de tancament del menú

### KActivities

- S'han eliminat les verificacions del compilador ara que tots els Frameworks requereixen el C++11
- S'ha eliminat el ResourceModel del QML, ja que s'ha substituït per KAStats::ResultModel
- La inserció en un QFlatSet buit retorna un iterador no vàlid

### KCodecs

- Simplificar el codi (qCount -&gt; std::count, millora amb isprint -&gt; QChar::isPrint)
- Detecció de la codificació: Esmenar una fallada en un ús incorrecte de «isprint» (error 357341)
- Esmenar una fallada per una variable sense inicialitzar (error 357341)

### KCompletion

- KCompletionBox: Forçar una finestra sense marc i no establir el focus
- KCompletionBox *no* hauria de ser un consell d'eina

### KConfig

- Afegir la implementació per obtenir les ubicacions QStandardPaths dins de fitxers «desktop»

### KCoreAddons

- Esmenar «kcoreaddons_desktop_to_json()» al Windows
- src/lib/CMakeLists.txt - Esmenar l'enllaçat a una biblioteca Threads
- Afegir adaptadors per permetre la compilació a l'Android

### KDBusAddons

- Evitar la introspecció d'una interfície de D-Bus quan no s'usa

### KDeclarative

- Ús uniforme de std::numeric_limits
- [DeclarativeDragArea] No sobreescriure el «text» de les dades MIME

### Compatibilitat amb les KDELibs 4

- Esmenar un enllaç obsolet al «docbook» del «kdebugdialog5»
- No deixar Qt5::Network com a biblioteca requerida per la resta dels «ConfigureChecks»

### KDESU

- Definició de macros de funcionalitats per permetre la construcció en la «libc» de «musl»

### KEmoticons

- KEmoticons: Esmenar una fallada quan «loadProvider» dona error per qualsevol motiu

### KGlobalAccel

- Fer que «kglobalaccel5» es pugui matar adequadament, esmenant una sortida molt lenta

### KI18n

- Usar el sistema Qt de configuració regional d'idiomes com a contingència en sistemes no UNIX

### KInit

- Netejar i refactoritzar l'adaptació al «xcb» del «klauncher»

### KIO

- FavIconsCache: Sincronitzar després d'escriure, per tal que les altres aplicacions puguin veure'l, i per evitar una fallada en la destrucció
- Esmenar diversos problemes de fils a KUrlCompletion
- Esmenar una fallada en el diàleg per reanomenar (error 360488)
- KOpenWithDialog: Millorar el títol de la finestra i el text descriptiu (error 359233)
- Permetre un desplegament multiplataforma millor dels esclaus IO empaquetant la informació del protocol en les metadades del connector

### KItemModels

- KSelectionProxyModel: Simplificar la gestió d'eliminació de files, simplificar la lògica per desseleccionar
- KSelectionProxyModel: Tornar a crear el mapatge en eliminar només si cal (error 352369)
- KSelectionProxyModel: Netejar només els mapatges de «firstChild» per al nivell superior
- KSelectionProxyModel: Assegurar la senyalització adequada en eliminar el darrer seleccionat
- Fer que es pugui cercar en el DynamicTreeModel per rol de visualització

### KNewStuff

- No fallar si els fitxers «.desktop» manquen o estan trencats

### KNotification

- Gestionar el botó del clic esquerre a les icones antigues de la safata del sistema (error 358589)
- Usar l'indicador X11BypassWindowManagerHin només en la plataforma X11

### Paquets dels Frameworks

- Després d'instal·lar un paquet, carregar-lo
- No fallar si el paquet existeix i està actualitzat
- Afegir Package::cryptographicHash(QCryptographicHash::Algorithm)

### KPeople

- Definir l'URI del contacte com a URI de la persona a PersonData quan no existeix cap persona
- Definir un nom per a la connexió de la base de dades

### KRunner

- Importar la plantilla de l'executor des de KAppTemplate

### KService

- Esmenar un avís nou del «kbuildsycoca», quan un tipus MIME hereta d'un àlies
- Esmenar la gestió de «x-scheme-handler/*» a «mimeapps.list»
- Esmenar la gestió de «x-scheme-handler/*» a l'anàlisi del «mimeapps.list» (error 358159)

### KTextEditor

- Revertir «Obrir/Desar la pàgina de configuració: Usar el terme «Carpeta» en lloc de «Directori»»
- Fer complir l'UTF-8
- Obrir/Desar la pàgina de configuració: Usar el terme «Carpeta» en lloc de «Directori»
- kateschemaconfig.cpp: Usar els filtres correctes en els diàlegs obrir/desar (error 343327)
- c.xml: Usar l'estil predeterminat per les paraules clau de flux de control
- isocpp.xml: Usa l'estil «dsControlFlow» predeterminat per les paraules clau de flux de control
- c/isocpp: Afegir més tipus estàndards del C
- KateRenderer::lineHeight() retorna un enter
- Impressió: Usa la mida del tipus de lletra de l'esquema d'impressió seleccionat (error 356110)
- Acceleració del cmake.xml: Usar «WordDetect» en lloc de «RegExpr»
- Canvia l'amplada dels tabuladors a 4 en lloc de 8
- Esmenar el canvi del color del número de línia actual
- Esmenar la selecció de l'element a completar amb el ratolí (error 307052)
- Afegir un fitxer de ressaltat de sintaxi per al «gcode»
- Esmenar el pintat del fons de la selecció del MiniMap
- Esmenar la codificació del gap.xml (usa UTF-8)
- Esmenar els blocs de comentaris imbricats (error 358692)

### KWidgetsAddons

- Tenir en compte els marges del contingut en calcular la mida dels consells

### KXMLGUI

- Esmenar que l'edició de les barres d'eines perd les accions connectades

### NetworkManagerQt

- ConnectionSettings: Inicialitzar el temps d'espera del «ping» de la passarel·la
- Tipus de connexions noves «TunSetting» i «Tun»
- Crear dispositius per a tots els tipus coneguts

### Icones de l'Oxygen

- Instal·lar «index.theme» en el mateix directori que sempre ha estat
- Instal·lar a oxygen/base/ de manera que si les icones es mouen des de les aplicacions, no entrin en conflicte amb la versió instal·lada per aquestes aplicacions
- Replicar els enllaços simbòlics des de les icones Brisa
- Afegir les icones noves «emblema afegit» i «emblema eliminat» per sincronisme amb el Brisa

### Frameworks del Plasma

- [Calendari] Esmenar la miniaplicació de calendari que no netejava la selecció en ocultar-se (error 360683)
- Actualitzar la icona d'àudio per usar el full d'estil
- Actualitzar la icona de silenci d'àudio (error 360953)
- Esmenar la creació forçada de miniaplicacions quan el Plasma és immutable
- [Node a esvair] No barrejar separadament l'opacitat (error 355894)
- [SVG] No tornar a analitzar la configuració en resposta a Theme::applicationPaletteChanged
- Diàleg: Definir els estats de «SkipTaskbar/Pager» abans de mostrar la finestra (error 332024)
- Tornar a introduir la propietat «busy» a l'«Applet»
- Assegura que el fitxer d'exportació PlasmaQuick s'ha trobat adequadament
- No importar una disposició no existent
- Fer possible que una miniaplicació ofereixi un objecte de prova
- Substituir QMenu::exec per QMenu::popup
- FrameSvg: Esmenar alguns apuntadors penjants («dangling») a «sharedFrames» en canviar el tema
- IconItem: Planificar l'actualització del mapa de píxels quan la finestra canvia
- IconItem: Animar el canvi actiu i habilitat encara que l'animació estigui desactivada
- DaysModel: Fer que s'actualitzi un sòcol
- [Icon Item] No animar des del mapa de píxels previ quan era invisible
- [Icon Item] No invocar «loadPixmap» a «setColorGroup»
- [Applet] No sobreescriure l'indicador «Persistent» de la notificació «Undo»
- Permetre substituir la definició de mutabilitat en la creació de contenidors
- Afegir «icon/titleChanged»
- Eliminar la dependència de QtScript
- La capçalera de «plasmaquick_export.h» és en una carpeta del «plasmaquick»
- Instal·lar diverses capçaleres «plasmaquick»

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
