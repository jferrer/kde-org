---
aliases:
- ../../kde-frameworks-5.44.0
date: 2018-03-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl: optie checkDb verwijderen (bug 380465)
- indexerconfig: enige functies beschrijven
- indexerconfig: functie canBeSearched openbaren (bug 388656)
- balooctl monitor: wacht op dbus-interface
- fileindexerconfig: canBeSearched() introduceren (bug 388656)

### Breeze pictogrammen

- view-media-playlist verwijderen uit pictogrammen met voorkeur
- 24px pictogram voor media-album-cover toevoegen
- Babe QML ondersteuning toevoegen (22px)
- bijwerk-handvat - pictogram voor kirigami
- 64px mediapictogram voor elisa toevoegen

### Extra CMake-modules

- **ANDROID_API** definiëren
- commandonaam op x86 readelf repareren
- Android toolchain: ANDROID_COMPILER_PREFIX variabele toevoegen, include-pad voor x86 targets repareren, zoekpad voor NDK afhankelijkheden uitbreiden

### KDE Doxygen hulpmiddelen

- Met fout eindigen als de uitvoermap niet leeg is (bug 390904)

### KConfig

- Enige geheugenallocaties besparen door de juiste API te gebruiken
- kconf_update exporteren met hulpmiddelen

### KConfigWidgets

- KLanguageButton::insertLanguage verbeteren wanneer geen naam wordt doorgegeven
- Pictogrammen toevoegen voor KStandardActions Deselect and Replace

### KCoreAddons

- m_inotify_wd_to_entry opschonen alvorens Entry pointers ongeldig te maken (bug 390214)
- kcoreaddons_add_plugin: zonder effect zijnd OBJECT_DEPENDS op json-bestand verwijderen
- Automoc helpen om JSON-bestanden met metagegevens gerefereerd in de code te vinden
- kcoreaddons_desktop_to_json: let op het gegenereerde bestand in de bouwlog
- shared-mime-info ophogen naar 1.3
- K_PLUGIN_CLASS_WITH_JSON introduceren

### KDeclarative

- Het mislukken van het bouwen op armhf/aarch64 repareren
- QmlObjectIncubationController beëindigen
- render() verbreken bij wijziging van venster (bug 343576)

### KHolidays

- Allen Winter is nu officieel de onderhouder van KHolidays

### KI18n

- API dox: notitie toevoegen over aanroepen van setApplicationDomain na QApp creatie

### KIconThemes

- [KIconLoader] rekening houden met devicePixelRatio voor overlays 

### KIO

- Neen geen indeling aan van msghdr en iovec structuur (bug 391367)
- Protocolselectie in KUrlNavigator repareren
- qSort wijzigen naar std::sort
- [KUrlNavigatorPlacesSelector] KFilePlacesModel::convertedUrl gebruiken
- [Drop Job] juiste prullenbakbestand aanmaken bij linking
- Niet bewuste activatie van broodkruimelmenu-item repareren (bug 380287)
- [KFileWidget] plaatsenframe en header verbergen
- [KUrlNavigatorPlacesSelector] categorieën in submenus stoppen (bug 389635)
- Gebruikmaken van de standeard KIO testhelper-header
- Ctrl+H toevoegen aan de lijst met sneltoetsen voor "toon/verberg verborgen bestanden" (bug 390527)
- Verplaatsen van ondersteuning voor semantiek naar KIO::UDSEntry toevoegen
- Probleem met "dubbelzinnige sneltoets" geïntroduceerd met D10314 repareren
- Stop het berichtenvak "Kon uitvoerbaar programma niet vinden" in een "queued lambda" (bug 385942)
- Bruikbaarheid van dialoog "Openen met" verbeteren door toevoegen van optie om de boomstructuur van toepassingen te filteren
- [KNewFileMenu] KDirNotify::emitFilesAdded na storedPut (bug 388887)
- Toekenning repareren bij annulering van de dialoog rebuild-ksycoca (bug 389595)
- Bug #382437 repareren "Regression in kdialog causes wrong file extension" (bug 382437)
- Sneller starten van simplejob
- Kopiëren van bestand naar VFAT zonder waarschuwingen repareren
- kio_file: foutbehandeling voor initiële perms gedurende kopiëren van bestand overslaan
- Semantics verplaatsen toestaan om gegenereerd te worden voor KFileItem. De bestaande operator voor kopieerconstructor, -destructor en toekenning worden nu ook gegenereerd door de compiler
- Geen stat(/etc/localtime) tussen read() en write() bij kopiëren van bestanden (bug 384561)
- op afstand: maak geen items met lege namen
- supportedSchemes-mogelijkheid toevoegen
- F11 als de sneltoets gebruiken om de "aside preview" om te schakelen
- [KFilePlacesModel] groepeer netwerkshares onder categorie "Remote"

### Kirigami

- Hulpmiddelknop als geactiveerd tonen wanneer het menu wordt getoond
- niet-interactieve schuifbalkindicators op mobiel
- Submenu's van acties repareren
- Het mogelijk maken om QQC2.Action te gebruiken
- Het mogelijk maken om exclusieve actiegroepen te ondersteunen (bug 391144)
- De tekst tonen door de actiehulpmiddelknoppen van de pagina
- Het mogelijk maken voor acties om submenu's te tonen
- Heeft geen specifieke positie van de component in zijn ouder
- Start geen acties in SwipeListItem tenzij ze zichtbaar zijn
- Een isNull() controle toevoegen alvorens in te stellen of QIcon een masker is
- FormLayout.qml aan kirigami.qrc toevoegen
- kleuren van swipelistitem repareren
- beter gedrag voor koptekst en voettekst
- ToolBarApplicationHeader links aanvullen en afkapgedrag verbeteren
- Ga na dat de navigatieknoppen niet onder de actie raken
- ondersteuning voor header- en footer-eigenschappen in overlaysheet
- Onnodige opvulling onderaan verwijderen in OverlaySheets (bug 390032)
- Laat uiterlijk van ToolBarApplicationHeader schijnen
- een sluitknop op het bureaublad tonen (bug 387815)
- niet mogelijk het blad te sluiten met het muiswiel
- Vermenigvuldig de pictogramgrootte alleen als Qt het niet al doet (bug 390076)
- met globale voettekst rekening houden voor behandeling van positie
- gebeurtenis comprimeert de aanmaak en verwijdering van schuifbalken
- ScrollView: maak het schuifbalkbeleid publiek en repareer het

### KNewStuff

- Vokoscreen aan KMoreTools toevoegen en voeg het toe aan de groepering van "schermrecorder"

### KNotification

- QWidget gebruiken om te zien of het venster zichtbaar is

### KPackage-framework

- Automoc helpen om JSON-bestanden met metagegevens gerefereerd in de code te vinden

### KParts

- Oude, onbereikbare code opschonen

### KRunner

- Sjablonen voor krunner-plug-in bijwerken

### KTextEditor

- Pictogrammen voor KTextEditor Document-Export, Bookmark-Remove en Formatting Text Upppercase, Lowercase and Capitalize toevoegen

### KWayland

- Vrijgeven van door client vrijgegeven uitvoer implementeren
- [server] de situatie juist behandelen wanneer de DataSource voor slepen vernietigd wordt (bug 389221)
- [server] Geen crash wanneer een  subsurface gets committed whose parent surface got destroyed" (bug 389231)

### KXMLGUI

- QLocale internals resetten wanneer we een eigen app-taal hebben
- Sta niet toe om scheidingsacties te configureren via contextmenu
- Geen contextmenu tonen als rechtsklikken er buiten is (bug 373653)
- KSwitchLanguageDialogPrivate::fillApplicationLanguages verbeteren

### Oxygen-pictogrammen

- Artikulate-pictogram toevoegen (bug 317527)
- map-spellen-pictogram toevoegen (bug 318993)
- onjuist 48px pictogram voor calc.template repareren (bug 299504)
- media-playlist-repeat en shuffle-pictogram toevoegen (bug 339666)
- Oxygen: tagpictogrammen zoals in breeze toevoegen (bug 332210)
- emblem-mount aan media-mount koppelen (bug 373654)
- netwerkpictogrammen toevoegen die beschikbaar zijn in breeze-icons (bug 374673)
- oxygen met breeze-icons synchroniseren, pictogrammen toevoegen voor audio-plasmoid
- edit-select-none toevoegen aan Oxygen voor Krusader (bug 388691)
- Pictogram voor waardering-niet-aangebracht toevoegen (bug 339863)

### Plasma Framework

- de nieuwe waarde voor largeSpacing in Kirigami gebruiken
- Zichtbaarheid van PC3 TextField plaatshoudertekst verminderen
- Ook titels niet 20% transparanter maken
- [PackageUrlInterceptor] "inline" niet herschrijven
- Headings niet 20% transparanter maken om met Kirigami overeen te komen
- de fullrep niet in de popup stoppen indien niet ingevouwen
- Automoc helpen om JSON-bestanden met metagegevens gerefereerd in de code te vinden
- [AppletQuickItem] applet-expander alleen vooraf laden indien niet al uitgevouwen
- andere micro-optimalisaties van vooraf laden
- IconItem standaard instellen op smooth=true
- de expander (de dialoog) ook vooraf laden
- [AppletQuickItem] instellingen voor beleid voor vooraf laden repareren als er geen omgevingsvariabele is ingesteld
- RTL-uiterlijk voor ComboBox repareren (bug https://bugreports.qt.io/browse/QTBUG-66446)
- probeer bepaalde applets op een slimme manier vooraf te laden
- [Icon Item] Stel filtering in op FadingNode-textuur
- m_actualGroup naar NormalColorGroup initialiseren
- Ga na dat de FrameSvg en Svg exemplaren de juiste devicePixelRatio hebben

### Prison

- Koppelingen naar afhankelijkheden bijwerken en Android markeren als officieel ondersteund
- Afhankelijkheid van DMTX optioneel maken
- QML-ondersteuning voor Prison toevoegen
- Minimale grootte op 1D barcodes ook instellen

### Omschrijving

- Tier repareren, aanpassen voor KIO

### QQC2StyleBridge

- Syntaxisfout in vorige commit, gedetecteerd door opstarten van ruqola
- Een keuzerondje tonen wanneer we een exclusieve besturing tonen (bug 391144)
- MenuBarItem implementeren
- DelayButton implementeren
- Nieuwe component: ronde knop
- met positie van werkbalk van account rekening houden
- kleuren voor pictogrammen in knoppen ondersteunen
- support --reverse
- pictogrammen in Menu volledig functioneel
- consistente schaduwen met de nieuwe breeze stijl
- Enige QStyles schijnen geen zinvolle pixelmetrics hier terug te geven
- eerste ondersteuning voor ruwe pictogrammen
- niet afbreken van regels met muiswiel

### Solid

- een lek repareren en onjuiste controle op nullptr in DADictionary
- [UDisks] regressie in automatisch aankoppelen repareren (bug 389479)
- [UDisksDeviceBackend] meerdere keren opzoeken vermijden
- Mac/IOKit backend: ondersteuning voor drives, disks en volumes

### Sonnet

- Locale::name() gebruiken in plaats van Locale::bcp47Name()
- libhunspell build zoeken met msvc

### Accentuering van syntaxis

- Basis ondersteuning voor afgeschermde blokken code met PHP en Python in Markdown
- Ongevoelig voor hoofd-/kleine letter WordDetect ondersteunen
- Accentuering van schema: hard gecodeerde kleuren verwijderen
- Syntaxisaccentuering toevoegen voor SELinux CIL Policies &amp; contexten van bestanden
- ctp-bestandsextensie toevoegen aan de PHP syntaxisaccentuering
- Yacc/Bison: het $ symbool repareren en bijwerksyntaxis voor Bison
- awk.xml: gawk extensie sleutelwoorden toevoegen (bug 389590)
- APKBUILD om geaccentueerd te worden toevoegen zoals een Bash-bestand
- "APKBUILD om geaccentueerd te worden toevoegen zoals een Bash-bestand" terugdraaien
- APKBUILD om geaccentueerd te worden toevoegen zoals een Bash-bestand

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
