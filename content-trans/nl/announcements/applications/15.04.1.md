---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE stelt Applicaties 15.04.1 beschikbaar.
layout: application
title: KDE stelt KDE Applicaties 15.04.1 beschikbaar
version: 15.04.1
---
10 mei 2018. Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../15.04.0'>KDE Applicaties 18.04</a> Deze uitgave bevat alleen reparaties van bugs en updates van vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan 50 aangegeven bugreparaties inclusief verbeteringen aan kdelibs, kdepim, kdenlive, okular, marble en umbrello.

Deze uitgave bevat ook ondersteuning op de lange termijn versies van Plasma Workspaces 4.11.19, KDE Development Platform 4.14.8 en de Kontact Suite 4.14.8.
