---
aliases:
- ../announce-applications-18.12.3
changelog: true
date: 2019-03-07
description: KDE stelt KDE Applicaties 18.12.2 beschikbaar.
layout: application
major_version: '18.12'
release: applications-18.12.3
title: KDE stelt KDE Applicaties 18.12.3 beschikbaar
version: 18.12.3
---
{{% i18n_date %}}

Vandaag heeft KDE de derde stabiele update vrijgegeven voor <a href='../18.12.0'>KDE Applicaties 18.12</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Meer dan twintig aangegeven reparaties van bugs, die verbeteringen aanbrengen in Kontact, ARK, Cantor, Dolphin, Filelight, JuK, Lokalize, Umbrello, naast andere.

Verbeteringen bevatten:

- Laden van .tar.zstd archieven in Ark is gerepareerd
- Dolphin crasht niet langer  bij stoppen van een Plasma activiteit
- Omschakeling naar een andere partitie laat Filelight niet langer crashen
