---
aliases:
- ../announce-applications-16.08.2
changelog: true
date: 2016-10-13
description: KDE wydało Aplikacje KDE 16.08.2
layout: application
title: KDE wydało Aplikacje KDE 16.08.2
version: 16.08.2
---
13 października 2016. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../16.08.0'>Aplikacji KDE 16.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

Więcej niż 30 zarejestrowanych poprawek błędów uwzględnia ulepszenia do kdepim, ark, dolphin, kgpg, kolourpaint, okular oraz innych.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.25.
