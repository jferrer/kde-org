---
aliases:
- ../../kde-frameworks-5.34.0
date: 2017-05-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- balooctl, baloosearch, balooshow: Rätta ordning som QCoreApplication objekt skapas (fel 378539)

### Breeze-ikoner

- Lägg till ikoner för hotspot (https://github.com/KDAB/hotspot)
- Bättre version av kontrollsystemikoner (fel 377380)
- Lägg till plasmate-ikon (fel 376780)
- Uppdatera ikoner för mikrofonkänslighet (fel 377012)
- Höj standardvärde för panelikoner till 48

### Extra CMake-moduler

- Sanitizers: Använd inte GCC-liknande flaggor för t.ex. MSVC
- KDEPackageAppTemplates: Dokumentationsförbättringar
- KDECompilerSettings: Skicka -Wvla &amp; -Wdate-time
- Stöd äldre versioner av qmlplugindump
- Introducera ecm_generate_qmltypes
- Tillåt projekt att inkludera filen två gånger
- Rätta rx som matchar projektnamn från git webbadress
- Introducera byggkommandot fetch-translations
- Använd -Wno-gnu-zero-variadic-macro-arguments mer

### KActivities

- Vi använder bara Lager 1 ramverk, så flytta oss till Lager 2
- Tog bort KIO från beroenden

### KAuth

- Säkerhetsrättning: Verifiera att vem som än anropar oss verkligen är den han säger

### KConfig

- Rätta relativ sökvägsberäkning i KDesktopFile::locateLocal() (fel 345100)

### KConfigWidgets

- Ange ikon för åtgärden Donera
- Lätta på begränsningar för behandling av QGroupBoxes

### KDeclarative

- Ange inte ItemHasContents i DropArea
- Acceptera inte håll över-händelser i DragArea

### KDocTools

- Provisorisk lösning för problem med MSVC och kataloginläsning
- Lös en synlighetskonflikt för meinproc5 (fel 379142)
- Citera några andra variabler med sökväg (undvik problem med mellanslag)
- Citera några variabler med sökväg (undvik problem med mellanslag)
- Inaktivera tillfälligt det lokala dokumentet på Windows
- FindDocBookXML4.cmake, FindDocBookXSL.cmake - sök i hemmagjorda installationer

### KFileMetaData

- Gör KArchive valfritt och bygg inte extraheringar som behöver det
- Rätta kompileringsfel för duplicerade symboler med mingw på Windows

### KGlobalAccel

- bygg: ta bort beroende på KService

### KI18n

- Rätta hantering av basnamn för po-filer (fel 379116)
- Rätta ki18n igångkörning

### KIconThemes

- Försök inte ens skapa ikoner med tomma storlekar

### KIO

- KDirSortFilterProxyModel: Återinför naturlig sortering (fel 343452)
- Fyll UDS_CREATION_TIME med värdet i st_birthtime på FreeBSD
- http slave: Skicka felsida efter misslyckad behörighetskontroll (fel 373323)
- kioexec: Delegera uppladdning till en kded-modul (fel 370532)
- Rätta att KDirlister test av grafiskt användargränssnitt ställer in webbadresschema två gånger
- Ta bort kiod-modul vid avslutning
- Skapa en moc_predefs.h fil för KIOCore (fel 371721)
- kioexec: Rätta stöd för --suggestedfilename

### KNewStuff

- Tillåt flera kategorier med samma namn
- KNewStuff: Visa filens storleksinformation i rutnätsdelegering
- Om en posts storlek är känd, visa den i listvyn
- Registrera och deklarera KNSCore::EntryInternal::List som en metatyp
- Fall inte igenom switch. Dubbla poster? Nej tack.
- Stäng alltid den nerladdade filen efter nerladdning

### Ramverket KPackage

- Rätta deklarationssökväg i KF5PackageMacros.cmake
- Ignorera varningar vid generering av programdata (fel 378529)

### Kör program

- Mall: Registrera mallkategori på toppnivå in "Plasma"

### KTextEditor

- KAuth-integrering för spara dokument, volym 2
- Rätta assert när kodvikning som ändrar markörpositionen verkställs
- Använd &lt;gui&gt; rotelement i ui.rc-fil som inte avråds från
- Lägg också till rullningslistmarkeringar för inbyggd sök och ersätt
- KAuth-integrering för spara dokument

### Kwayland

- Validera att yta är giltig när lämna textinmatningshändelse skickas

### KWidgetsAddons

- KNewPasswordWidget: Dölj inte synlighetsåtgärd i enkelt textläge (fel 378276)
- KPasswordDialog: Dölj inte synlighetsåtgärd i enkelt textläge (fel 378276)
- Rätta KActionSelectorPrivate::insertionIndex()

### KXMLGUI

- kcm_useraccount är död, länge leve user_manager
- Reproducerbara byggen: Ta bort version från XMLGUI_COMPILING_OS
- Korrigera: Namn på DOCTYPE måste matcha rotelementets typ
- Rättade felaktig användning av ANY i kpartgui.dtd
- Använd rotelement &lt;gui&gt; som inte avråds från
- Rättningar av dokumentation av programmeringsgränssnitt: Ersätt 0 med nullptr eller ta bort där inte använd

### NetworkManagerQt

- Rätta krasch vid hämtning av aktiv anslutningslista (fel 373993)
- Ange standardvärde för automatisk förhandling baserat på NM-version som kör

### Oxygen-ikoner

- Lägg till ikon för hotspot (https://github.com/KDAB/hotspot)
- Höj standardvärde för panelikoner till 48

### Plasma ramverk

- Läs in ikon igen när usesPlasmaTheme ändras
- Installera Plasma komponenter 3 så att de kan användas
- Introducera units.iconSizeHints.* för att tillhandahålla användarinställningsbara tips om ikonstorlekar (fel 378443)
- [TextFieldStyle] Rätta felet textField är inte definierat
- Uppdatera ungrabMouse fix för Qt 5.8
- Skydda mot att miniprogram inte laddar AppletInterface (fel 377050)
- Kalender: Använd rätt språk för månads- och dagnamn
- Generera plugins.qmltypes-filer för  insticksprogram som installeras
- Om användaren ställde in en implicit storlek, behåll den

### Solid

- Lägg till inkludering som behövs i msys2

### Syntaxfärgläggning

- Lägg till Arduino-utökning
- Latex: Rätta felaktig terminering av iffalse kommentarer (fel 378487)

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
