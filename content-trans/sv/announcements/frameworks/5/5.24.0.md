---
aliases:
- ../../kde-frameworks-5.24.0
date: 2016-07-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Allmänna ändringar

- Listan över plattformar som stöds för varje ramverk är nu mer explicit. Android har lagts till i listan över plattformar som stöds i alla ramverk där det är fallet.

### Baloo

- DocumentUrlDB::del: Gör bara assert när underliggande kataloger verkligen existerar
- Ignorera felaktiga frågor som har en binär operator utan första argumentet

### Breeze-ikoner

- Många nya och förbättrade ikoner
- Rätta fel 364931 när ikonen för användaren overksam inte var synlig (fel 364931)
- Lägg till ett program för att konvertera symboliskt länkade filer till qrc-alias

### Extra CMake-moduler

- Integrera relativa bibliotekssökvägar i APK
- Använd "${BIN_INSTALL_DIR}/data" för DATAROOTDIR på Windows

### KArchive

- Säkerställ att uppackning av ett arkiv inte installerar filer utanför den uppackade katalogen, av säkerhetsskäl. Packa istället upp sådana filer i uppackningskatalogens rot.

### KBookmarks

- Städa KBookmarkManagerList innan qApp avslutas för att undvika låsning med D-Bus tråden

### KConfig

- Avråd från användning av authorizeKAction() till förmån för authorizeAction()
- Gör byggning reproducerbar genom att säkerställa att UTF-8 kodning används

### KConfigWidgets

- KStandardAction::showStatusbar: Returnera avsedd åtgärd

### KDeclarative

- Gör epoxy valfri

### KDED

- [OS X] Gör kded5 till en agent, och bygg den som ett vanligt program

### Stöd för KDELibs 4

- Ta bort klassen KDETranslator, det finns ingen kdeqt.po längre
- Dokumentera ersättningen av use12Clock()

### KDesignerPlugin

- Lägg till stöd för KNewPasswordWidget

### KDocTools

- Tillåt att KDocTools alltid hittar åtminstone sina eget installerade saker
- Använd CMAKE_INSTALL_DATAROOTDIR för att hitta docbook istället för share
- Uppdatera docbook-manualsidan för qt5options till Qt 5.4
- Uppdatera docbook-manualsidan för kf5options

### KEmoticons

- Flytta glastemat till kde-look

### KGlobalAccel

- Använd QGuiApplication istället för QApplication

### KHTML

- Rätta användning av ärvt värde för egenskapen outline shorthand
- Hantera ursprungligt och ärvt värde för kantradie
- Bortse från egenskap om en ogiltig längd eller ogiltigt procentvärde som bakgrundsstorlek
- cssText måste mata ut värden åtskilda med kommatecken för de egenskaperna
- Rätta tolkning av bakgrundsklipp i kortform
- Implementera bakgrundstolkning av storlek i kortform
- Markera egenskaper som tilldelade när mönster upprepas
- Rätta arv av bakgrundsegenskaper
- Rätta användning av ärvt värde för egenskapen bakgrundsstorlek
- Driftsätt khtml kxmlgui-filen i en Qt-resursfil

### KI18n

- Sök också i kataloger efter avkortade varianter av värden i miljövariabeln LANGUAGE
- Rätta tolkning av miljövariabler med avseende på modifierare och koduppsättning, gjord i fel ordning

### KIconThemes

- Tillägg av stöd för att automatiskt läsa in och använda ett ikontema i en RCC-fil
- Dokumentera driftsättning av ikontema på MacOS och Windows, se https://api.kde.org/frameworks/kiconthemes/html/index.html

### KInit

- Tillåt överskriden tidsgräns i reset_oom_protection vid väntan på SIGUSR1

### KIO

- KIO: Lägg till SlaveBase::openPasswordDialogV2 för bättre felkontroll, var vänlig och konvertera dina I/O-slavar till det
- Rätta att KUrlRequester öppnade fildialogruta i fel katalog (fel 364719)
- Rätta osäkra typkonverteringar av KDirModelDirNode*
- Lägg till cmake-alternativet KIO_FORK_SLAVES för att ställa in förvalt värde
- ShortUri-filter: Rätta filtrering av mailto:user@host
- Lägg till OpenFileManagerWindowJob för att markera fil i en katalog
- KRun: Lägg till metoden runApplication
- Lägg till söktjänsten soundcloud
- Rätta ett justeringsproblem med OS X inbyggda stil "macintosh"

### KItemModels

- Lägg till KExtraColumnsProxyModel::removeExtraColumn, som kommer att behövas av StatisticsProxyModel

### KJS

- kjs/ConfigureChecks.cmake: Tilldela egenskapen HAVE_SYS_PARAM_H

### KNewStuff

- Säkerställ att en storlek kan erbjudas (fel 364896).
- Rätta "Nerladdningsdialogruta fungerar inte när alla kategorier saknas"

### KNotification

- Rätta underrättelse av aktivitetsrad

### KNotifyConfig

- KNotifyConfigWidget: Lägg till metoden disableAllSounds() (fel 157272)

### KParts

- Lägg till väljare för att inaktivera KParts hantering av fönsterrubriker
- Lägg till menyalternativ för att ge bidrag i våra programs hjälpmeny

### Kross

- Rätta namn på QDialogButtonBox uppräkningstyp "StandardButtons"
- Ta bort första försök att ladda bibliotek eftersom vi ändå försöker med libraryPaths
- Rätta krasch när en metod synlig för Kross returnerar QVariant med ej relokerbar data
- Använd inte typkonverteringar till void* av C-stil (fel 325055)

### Kör program

- [QueryMatch] Lägg till ikonnamn

### KTextEditor

- Visa rullningslistens textförhandsgranskning efter en fördröjning på 250 ms
- Dölj förhandsgranskning och liknade när vyinnehållet rullas
- Ställ in parent + toolview, jag tror det behövs för att undvika inträde i uppgiftsbyte i Win10
- Ta bort "KDE-standard" från kodningsruta
- Förhandsgranskning av vikning normalt på
- Undvik streckad understrykning i förhandsgranskning och undvik förgiftning av cachen för radlayout
- Aktivera alltid alternativet "Visa förhandsgranskning av hopvikt text"
- Textförhandsgranskning: Justera höjden på fårad rektangel när rullning förbi slutet är aktiverad
- Rullningslistförhandsgranskning: Använd fårad rektangel om rullningslisten inte använder full höjd
- Lägg till KTE::MovingRange::numberOfLines() precis som för KTE::Range
- Förhandsgranskning av kodvikning: Ställ in rutans höjd så att alla dolda rader får plats
- Lägg till alternativ för att inaktivera förhandsgranskning av vikt text
- Lägg till modeline 'folding-preview' av typ bool
- View ConfigInterface: Stöd 'folding-preview' av typ bool
- Lägg till bool KateViewConfig::foldingPreview() och setFoldingPreview(bool)
- Funktion: Visa textförhandsgranskning när musen hålls över ett hopvikt kodblock
- KateTextPreview: Lägg till setShowFoldedLines() och showFoldedLines()
- Lägg till modelines 'scrollbar-minimap' [bool] och 'scrollbar-preview' [bool]
- Aktivera normalt miniavbildningens rullningslist
- Ny funktion: Visa textförhandsgranskning när musen hålls över rullningslisten
- KateUndoGroup::editEnd(): Skicka KTE::Range med const referens
- Rätta genvägshantering i vim-läge på grund av beteendeförändring i Qt 5.5 (fel 353332)
- autobrace: infoga inte tecknet ' i text
- ConfigInterface: Lägg till inställningstangent för att aktivera eller inaktivera rullningslistens miniavbildning
- Rätta KTE::View::cursorToCoordinate() när meddelandekomponent på toppnivå är synlig
- Omstrukturering av emulerad kommandorad
- Rätta förvrängningar av uppritning vid rullning medan underrättelser är synliga (fel 363220)

### Kwayland

- Lägg till händelsen parent_window i Plasma fönstergränssnitt
- Hantera att förstöra en pekar-, tangentbords-, eller plattresurs på ett riktigt sätt
- [server] Ta bort död kod: KeyboardInterface::Private::sendKeymap
- [server] Lägg till stöd för att ställa in klippbordets DataDeviceInterface för markering manuellt
- [server] Säkerställ att Resource::Private::get returnerar nullptr om det skickas nullptr
- [server] Lägg till resurskontroll i QtExtendedSurfaceInterface::close
- [server] Ta bort tilldelning av SurfaceInterface pekare i refererade objekt när den håller på att förstöras
- [server] Rätta felmeddelande i gränssnittet QtSurfaceExtension
- [server] Introducera signalen Resource::unbound skickad från hanteraren unbind
- [server] Ingen assert när ett BufferInterface som fortfarande har referenser förstörs
- Lägg till förstörelsebegäran till org_kde_kwin_shadow och org_kde_kwin_shadow_manager

### KWidgetsAddons

- Rätta läsning av Unihan-data
- Rätta minimal storlek för KNewPasswordDialog (fel 342523)
- Rätta tvetydig konstruktor för MSVC 2015
- Rätta ett justeringsproblem med OS X inbyggda stil "macintosh" (fel 296810)

### KXMLGUI

- KXMLGui: Rätta sammanfogning av index när xmlgui-klienter med åtgärder i grupper tas bor (fel 64754)
- Varna inte om "file found in compat location" om den inte alls hittas
- Lägg till menyalternativ för att ge bidrag i våra programs hjälpmeny

### NetworkManagerQt

- Ställ inte in peap-beteckning baserat på peap-version
- Gör kontroller av nätverkshanterarens version vid körning, för att undvika kompilering mot körning (fel 362736)

### Plasma ramverk

- [Calendar] Vänt pilknapparna för höger-till-vänster språk
- Plasma::Service::operationDescription() ska returnera en QVariantMap
- Inkludera inte inbäddade omgivningar i containmentAt(pos) (fel 361777)
- Rätta färgtema för ikonen för att starta om systemet (inloggningsskärm) (fel 364454)
- Inaktivera aktivitetsradens miniatyrbilder med llvmpipe (fel 363371)
- Skydda mot ogiltiga miniprogram (fel 364281)
- PluginLoader::loadApplet: Återställ kompatibilitet för felaktigt installerade miniprogram
- Aktuell katalog för PLASMA_PLASMOIDS_PLUGINDIR
- PluginLoader: Förbättra felmeddelanden om insticksversionens kompatibilitet
- Rätta kontroll för att behålla QMenu på skärmen för layout med flera skärmar
- Ny omgivningstyp för systembrickan

### Solid

- Rätta kontroll att processorn är giltig
- Hantera läsning av /proc/cpuinfo för Arm-processorer
- Sök efter processorer enligt delsystem istället för drivrutin

### Sonnet

- Markera exekverbara hjälpprogram som icke grafiska gränssnittsprogram
- Tillåt att nsspellcheck normalt kompileras på Mac

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
