---
aliases:
- ../../kde-frameworks-5.74.0
date: 2020-09-06
layout: framework
libCount: 70
---
### Attica

+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition

### Baloo

+ Licensera om många filer till LGPL-2.0-or-later
+ Använd också gemensam UDS skapningskod för taggar (fel 419429)
+ Ta bort gemensam UDS skapningskod från KIO arbetare
+ [balooctl] Visa tillåtna format i hjälptext
+ [balooctl] Visa aktuell fil i statusutmatning i indexeringstillstånd
+ [balooctl] Ställ in QDBusServiceWatcher läge från konstruktor
+ [balooctl] Städning av formatering
+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition
+ [OrPostingIterator] Avancera inte när önskat id är lägre en nuvarande
+ [Extractor] Ta bort QWidgets beroende från extraheringshjälpprogram
+ [Extractor] Ta bort KAboutData från körbart extraheringshjälpprogram
+ Uppdatera diverse referenser i README
+ [FileContentIndexer] Ta bort oanvända config konstruktorargument och medlem
+ [balooctl] Rätta varning om föråldrad QProcess::start, tillhandahåll tom argumentlista
+ [Engine] Propagera transaktionsfel (fel 425017)
+ [Engine] Ta bort oanvänd metod hasChanges från {Write}Transaction
+ Indexera .ytdl-filer (fel 424925)

### Breeze-ikoner

+ Gör keepassxc ikonen mer trogen den officiella
+ Lägg till flera symboliska länkar för ny keepassxc systembrickans ikonnamn (fel 425928)
+ Lägg till ett annat alias för keepass-ikon (fel 425928)
+ Lägg till ikon för Godot-projektets Mime-typ
+ Lägg till ikon för Anaconda installationsverktyg
+ Lägg till 96-bildpunkters platsikoner
+ Ta bor oanvända platser att kommunicera
+ Kör application-x-bzip-compressed-tar via <code>scour</code> (fel 425089)
+ Skapa application-gzip symbolisk länk till application-x-gzip (fel 425059)
+ Lägg till alias för MP3-ikoner (fel 425059)

### Extra CMake-moduler

+ Ta bort ledande nollor från numeriska versionsnummer i C++ kod
+ Lägg till tidsgräns för qmlplugindump anrop
+ Lägg till WaylandProtocols sökmodul
+ anropa update-mime-database med -n

### Integrering med ramverk

+ Klargör licensuppgift i enlighet med KDElibs historik

### KActivitiesStats

+ Använd Boost::boost för äldre versioner av CMake

### KActivities

+ Använd inte tom X-KDE-PluginInfo-Depends

### KDE Doxygen-verktyg

+ Dokumentera beroende i requirements.txt och installera dem i setup.py
+ Välj visning av bibliotekslicens

### KAuth

+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition

### KBookmarks

+ KBookmarkManager: rensa minnnesträd när filen tas bort

### KCalendarCore

+ Lagra alltid egenskaperna X-KDE-VOLATILE-XXX som flyktiga
+ Dokumentera förväntad TZ-övergångar i Prag

### KCMUtils

+ KCModuleData: Rätta till huvuden, förbättra dokumentation över programmeringsgränssnitt och byt namn på metod
+ Utöka KCModuleData med funktionalitet för revertToDefaults och matchQuery
+ Försök undvika horisontell rullningslist i KCMultiDialog
+ Lägg till KCModuleData som en basklass för insticksprogram
+ Tillåt extra knapp att läggas till i KPluginSelector (fel 315829)

### KConfig

+ Gör KWindowConfig::allConnectedScreens() statiskt och intern (fel 425953)
+ Lägg till standardgenväg för "Skapa katalog"
+ Introducera metod för förfrågan av KConfigSkeletonItem förvalda värde
+ Kom ihåg fönsterstorlek baserat på ett per-skärm arrangemang
+ Extrahera kod för att få lista över anslutna skärmar i en återanvändbar funktion
+ Lägg till funktioner för att spara och återställa fönsterpositioner på icke-Wayland plattformar (fel 415150)

### KConfigWidgets

+ Undvik att byta ut förval för att läsa KConfigSkeletonItem förvalda värde
+ Rätta KLanguageName::nameForCodeInLocale för koder som QLocale inte känner till något om
+ KLanguageName::allLanguageCodes: Ta hänsyn till att det kan finnas fler än en katalog för landsinställningar
+ KConfigDialog: Försök undvika horisontella rullningslister
+ Funktion som returnerar listan över språkkoder

### KContacts

+ Addressee::parseEmailAddress(): Kontrollera fullständiga namnets längd innan avkortning

### KCoreAddons

+ Lägg till *.kcrash glob-mönster i KCrash-rapport MIME-typ
+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition
+ Vänta inte på fam händelser för evigt (fel 423818)
+ [KFormat] Tillåt formateringsvärden för godtyckliga binära enheter
+ Gör det möjligt att använda KPluginMetadata från QML
+ [KFormat] Rätta binärexempel

### KDAV

+ metainfo.yaml: Lägg till nivåtangent och justera beskrivning

### KDeclarative

+ [KKeySequenceItem] Gör så att genvägar med Meta+Skift+siffra fungerar
+ Exponera egenskapen checkForConflictsAgainst
+ Lägg till ny klass AbstractKCM
+ Konvertera KRunProxy från KRun

### KDED

+ org.kde.kded5.desktop: Lägg till saknad nyckel "Name" som är nödvändig (fel 408802)

### KDocTools

+ Uppdatera contributor.entities
+ Använd platsmarkörer

### KEmoticons

+ Återvinn EmojiOne licensinformation

### KFileMetaData

+ Konvertera gamla Mac-radslut i sångtexttaggar (fel 425563)
+ Konvertera till den nya modulen FindTagLib i ECM

### KGlobalAccel

+ Läs in tjänstfiler för genvägar för programmens datakatalog som reserv (fel 421329)

### KI18n

+ Rätta möjlig preprocessor kapplöpningstillstånd med i18n-definitioner

### KIO

+ StatJob: gör så att mostLocalUrl bara fungerar med protoclClass == :local
+ KPropertiesDialog: ladda också insticksprogram med JSON metadata
+ Ångra "[KUrlCompletion] Lägg inte till / sist i kompletterade kataloger" (fel 425387)
+ Förenkla konstruktor KProcessRunner
+ Tillåt nyckelorden CCBUG och FEATURE för bugs.kde.org (fel )
+ Uppdatera hjälptext för redigering av programkommandot i en .desktop-post  för att uppfylla aktuell specifikation (fel 425145)
+ KFileFilterCombo: Lägg inte till alternativet allTypes om vi bara har ett objekt
+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition
+ Rätta potentiell krasch vid borttagning av en meny i händelsehanterare (fel 402793)
+ [filewidgets] Rätta vaddering av KUrlNavigatorButton för länkstig (fel 425570)
+ ApplicationLauncherJob: justera dokumentation
+ Rätta oöversättbara objekt i kfileplacesmodel
+ ApplicationLauncherJob: rätta krasch om ingen open-with hanterare är inställd
+ Byt namn på inställningsmodulen "Webbgenvägar" till "Webbsökningsnyckelord"
+ KFileWidget: tolka om inställningar för att hämta kataloger tillagda av andra instanser av programmet (fel 403524)
+ Flytta inställningsmodul för webbgenvägar till sökkategori
+ Ta automatiskt bort avslutande blanktecken istället för att visa en varning
+ Undvik att program startade av systemd från att stänga skapade underprocesser (fel 425201)
+ smb: rätta kontroll av tillgänglighet för delat resursnamn
+ smb: behåll standardfelutmatning för nätkommandon vid lägg till eller ta bort (fel 334618)
+ smb: arrangera om kontroll av gäst tillåten och publicera som areGuestsAllowed
+ KFileWidget: Rensa webbadresser innan listan byggs om
+ KFileWidget: ta bort kombinationsruta för förvald webbadress toppsökväg
+ KFilePlacesModel: lägg till standardplatser vid uppgradering från äldre version
+ Rätta två års regression i KUrlComboBox, setUrl() lade inte längre till

### Kirigami

+ kirigami.pri: Lägg till managedtexturenode.cpp
+ gör så att egen positionering av buddyfor fungerar igen
+ Ta hänsyn till storlek på knappen Mer om vi redan vet att den kommer att vara synlig
+ Lägg till en egenskap i ToolBarLayout för att styra hur objekthöjd ska hanteras (fel 425675)
+ gör beteckningar för kryssrutor synliga igen
+ stöd displayComponent för åtgärder
+ gör undermenyer synliga
+ Rätta framåtdeklaration av QSGMaterialType
+ Gör så att OverlaySheet sidhuvud och sidfot använder lämpliga bakgrundsfärger
+ Kontrollera värdet på miljövariabel för lågeffekt, inte bara om den finns
+ Lägg till funktionen setShader i ShadowedRectangleShader för att förenkla inställning av skuggning
+ Lägg till en lågeffektversion av sdf funktionens fil
+ Gör överlagringar av sdf_render använda funktionen sdf_render med fullständiga argument
+ Ta bort behov av kärnprofildefinitioner i skuggrektangel huvudprogram för skuggning
+ Basera inre rektangel på yttre rektangel för skuggrektangel med kant
+ Använd rätt skuggning för ShadowedTexture när kärnprofil används
+ Lägg till version med "låg effekt" av skuggrektanglarna i skuggning
+ Initiera medlemmen m_component i PageRoute
+ Rätta Material SwipeListItem
+ ny logik för att ta bort accelerationsmarkeringar (fel 420409)
+ ta bort forceSoftwarerendering för närvarande
+ visa bara källobjekt vid programvaruåtergivning
+ en reservversion i programvara för skuggad struktur
+ Använd den nya egenskapen showMenuArrow för bakgrunden i menypilen
+ dölj inte huvudet vid överskridande uppåt
+ flytta ManagedTextureNode i egen fil
+ ToolBarLayout: Lägg till mellanrum i visibleWidth om vi visar knappen mer
+ Överskrid inte Heading bildpunktsstorlek i BreadCrumbControl (fel 404396)
+ Uppdatera programmall
+ [passivenotification] Ange explicit vaddering (fel 419391)
+ Aktivera klippning i GlobalDrawer StackView
+ Ta bort ogenomskinlighet från inaktiverad PrivateActionToolButton
+ Gör ActionToolBar till en kontroll
+ swipenavigator: lägg till kontroll över vilka sidor som visas
+ Deklarera underliggande typ för DisplayHint uppräkningsvärde som uint
+ Lägg till ToolBarLayout/ToolBarLayoutDelegate i pri-fil
+ kirigami.pro: Använd lista över källkodsfiler från kirigami.pri
+ Ta inte bort inkubatorer i kompletteringsåteranrop
+ Försäkra att menuActions förblir ett fält istället för en listegenskap
+ Lägg till returtyp i DisplayHint singleton lambda
+ Ta hänsyn till objekthöjd när delegater centreras vertikalt
+ Köa alltid en ny layout, även om vi för närvarande håller på med en layout
+ Arbeta om InlineMessage layout genom att använda förankringar
+ Använd fullständig bredd för att kontrollera om alla åtgärder passar in
+ Lägg till en kommentar om det extra mellanrummet för centrering
+ Rätta check av verktygstipstext i PrivateActionToolButton
+ Provisorisk lösning för att qqc2-desktop-style ToolButton inte respekterar icke-platta IconOnly
+ Föredra att dra ihop KeepVisible åtgärder över att dölja senare KeepVisible åtgärder
+ Dölj alla följande åtgärder när den första åtgärden döljs
+ Lägg till underrättelsesignal för ToolBarLayout::actions och skicka den vid rätt tillfälle
+ Exponera action till avskiljare i ActionsMenu
+ Byt namn på ActionsMenu loaderDelegate egenskap kirigamiAction till action
+ Lägg till saknad loaderDelegate som kontrollerar synlighet
+ Dölj åtgärdsdelegater till de är positionerade
+ ToolBarLayout: Ersätt egen lat laddning av delegater med QQmlIncubator
+ Flytta displayHintSet till C++ så att den kan anropas därifrån
+ Stöd höger-till-vänster läge i ToolBarLayout
+ Lägg till egenskapen minimumWidth i ToolBarLayout
+ Arbeta om PrivateActionToolButton för bättre prestanda
+ Avråd från ActionToolBar::hiddenActions
+ Ange layouttips för ActionToolBar
+ Använd DisplayHint i ToolBarPageHeader som inte avråds från
+ Visa komponentfel om instansiering av delegatobjekt misslyckas
+ Dölj delegater för åtgärder som har tagits bort
+ Städa full/ikon delegatobjekt vid destruktion av delegat
+ Tvinga full/ikon synlighet för delegatobjekt
+ Lägg till ToolBarLayout för layouten av ActionToolBar
+ Lägg till några kommentarer i ToolBarLayout::maybeHideDelegate
+ Lägg till egenskapen visibleWidth i ToolBarLayout
+ Introducera ToolBarLayout inbyggt objekt
+ Ersätt DisplayHint från Action till C++ uppräkningsfil
+ Lägg till ikoner använda på om-sidan i cmake paketeringsmakro för ikoner

### KNewStuff

+ Rätta synkroniseringsproblem med cache för QtQuick dialogruta (fel 417985)
+ Använd inte tom X-KDE-PluginInfo-Depends
+ Ta bort poster om de inte längre matchar filtret (fel 425135)
+ Rätta specialfall då KNS fastnar (fel 423055)
+ Gör den interna jobbuppgiften för kpackage mindre känslig (fel 425811)
+ Ta bort nerladdad fil när en kpackage installation används
+ Stöd en annan stil för kpackage knsrc som reserv
+ Hantera notationen /* för RemoveDeadEntries (fel 425704)
+ Gör det enklare att hämta cache för ett redan initierat gränssnitt
+ Lägg till omvänd uppslagning för poster baserade på sina installerade filer
+ Använd notationen /* för uppackning av underkatalog och tillåt uppackning av underkatalog om filen är ett arkiv
+ Rätta varningsflöde "Pixmap is a null pixmap"
+ Ta bort snedstreck från postnamn när det tolkas som en sökväg (fel 417216)
+ Rätta en krasch då och då med aktiviteten KPackageJob (fel 425245)
+ Använd samma spinnare för inläsning och initiering (fel 418031)
+ Ta bort detaljknapp (fel 424895)
+ Kör avinstalleringsskript asynkront (fel 418042)
+ Inaktivera sökning när den inte är tillgänglig
+ Dölj inläsning av fler spinnare vid uppdatering/installation (fel 422047)
+ Lägg inte till katalog för nerladdningsmål i entry-filer
+ Ta bort tecknet * när katalog skickas till skript
+ Fokusera inte första elementet i ikonvisningsläge (fel 424894)
+ Undvik onödig start av jobb för avinstallering
+ Lägg till alternativet RemoveDeadEntries för knsrc-filer (fel 417985)
+ [QtQuick dialogruta] Rätta sista instansen av felaktig ikon för uppdatera
+ [QtQuick dialogruta] Använd mer lämpliga ikoner för avinstallera

### Ramverket KPackage

+ Ta inte bort paketets rot om paketet har tagits bort (fel 410682)

### KQuickCharts

+ Lägg till egenskapen fillColorSource i linjediagram
+ Beräkna jämnhet baserat på objektstorlek och enhetens bildpunktsförhållande
+ Använd medelvärde av storleken istället för maximum för att bestämma linjens jämnhet
+ Basera utjämningens storlek i linjediagram på diagrammets storlek

### Kör program

+ Lägg till mall för python körprogram
+ Uppdatera mallen för kompatibilitet med KDE:s butik och förbättra README
+ Lägg till stöd för körprogram syntax i D-bus körprogram
+ Spara RunnerContext efter varje matchningssession (fel 424505)

### KService

+ Implementera invokeTerminal på Windows med arbetskatalog, kommando och miljövariabler
+ Rätta programmets föredragna ordning av Mime-typer med multipelt arv (fel 425154)
+ Skicka med programmets tjänsttyp i en qrc-fil
+ Expandera tecknet tilde när arbetskatalogen läses (fel 424974)

### KTextEditor

+ Vimode: Gör det lilla borttagningsregistret (-) direct åtkomligt
+ Vimode: Kopiera beteendet hos numrerade register i vim
+ Vimode: Förenkla implementeringen av append-copy
+ Gör "sök efter markering" sökning om ingen markering finns
+ Flerradersläge är bara vettigt för flerraders reguljära uttryck
+ Lägg till kommentar om att kontrollera pattern.isEmpty()
+ Konvertera sökgränssnittet från QRegExp till QRegularExpression
+ Vimode: Implementera append-copy
+ Snabba upp inläsning av stora filer *mycket*
+ Visa bara zoomnivå när den inte är 100 %
+ Lägg till en zoomindikator på statusraden
+ Lägg till ett separat inställningsalternativ för förhandsgranskning av matchande parentes
+ Visa en förhandsgranskning av raden för matchande öppen parentes
+ Tillåt mer kontroll över anropande av kompletteringsmodeller när automatiskt anropande inte används

### Ramverket KWallet

+ Undvik krock med ett makro i ctype.h från OpenBSD

### KWidgetsAddons

+ Lägg till KRecentFilesMenu för att ersätta KRecentFileAction

### KWindowSystem

+ Installera insticksprogram för plattform i en katalog utan punkter i filnamnet (fel 425652)
+ [xcb] Skala ikongeometri riktigt överallt

### KXMLGUI

+ Tillåt att avstå från att komma ihåg fönsterpositioner på X11 (fel 415150)
+ Spara och återställ huvudfönstrets position (fel 415150)

### Plasma ramverk

+ [PC3/BusyIndicator] Undvik att utföra osynlig animering
+ Använd inte highlightedTextColor för TabButton
+ Ta bort Layout.minimumWidth från Button och ToolButton
+ Använd mellanrumsegenskapen för mellanrummet mellan ikoner för Button/ToolButton och beteckningar
+ Lägg till private/ButtonContent.qml för PC3 Button och ToolButton
+ Ändra PC3 Button och ToolButton implicitWidth och implicitHeight för att ta hänsyn till inset värden
+ Lägg till implicitWidth och implicitHeight i ButtonBackground
+ Rätta felaktigt förval i PlasmaExtras.ListItem (fel 425769)
+ Låt inte bakgrunden bli mindre än svg (Fel 424448)
+ Använd Q_DECLARE_OPERATORS_FOR_FLAGS i samma namnrymd som flaggdefinition
+ Gör så att PC3 BusyIndicator visuella utseende behåller en 1:1 proportion (fel 425504)
+ Använd ButtonFocus och ButtonHover i PC3 ComboBox
+ Använd ButtonFocus och ButtonHover i PC3 RoundButton
+ Använd ButtonFocus och ButtonHover i PC3 CheckIndicator
+ Förena det platta/normala beteendet hos PC3 knappar/verktygsknappar (fel 425174)
+ Gör så att rubriken använder PC3 Label
+ [PlasmaComponents3] Gör så att kryssrutans text fyller sin layout
+ Ge skjutreglaget PC2 implicitWidth och implicitHeight
+ Kopiera filer istället för felaktiga symboliska länkar
+ Rätta marginaler för toolbutton-hover i button.svg (fel nr. 425255)
+ [PlasmaComponents3] mycket liten omstrukturering av ToolButton skuggkod
+ [PlasmaComponents3] Rätta omvänt villkor för platt skugga i ToolButton
+ [PlasmaComponents3] Avlägsna mnemoniska och-tecken från verktygstipstext
+ Rita bara fokusindikator när vi får fokus via tangentbordet (fel 424446)
+ Använd inte minimal storleksändring från knapparna PC2 och PC3
+ Lägg till PC3 ekvivalent till PC2 ListItem
+ Rätta verktygsradens SVG
+ [pc3] Gör ToolBar mer likformig med den i qqc2-desktop-style
+ Exponera miniprogrammets metadata i AppletInterface
+ Avkorta inte DPR till ett heltal i cacheidentifierare
+ Lägg till egenskap för tidsgräns i ToolTipArea
+ Ställ in typ till Dialog i flaggor om typen är Dialog::Normal

### Syfte

+ Verkställ initial inställningsdata när grafiskt användargränssnitt för inställning laddas
+ Återställ beteende hos AlternativesView
+ [jobcontroller] Inaktivera separat process
+ Arbeta om hantering av jobbvisning (fel 419170)

### QQC2StyleBridge

+ Rätta att genvägssekvensen för StandardKey i MenuItem visas som tal
+ Använd inte överliggande höjd/bredd för implicit storleksändring av ToolSeparator (fel 425949)
+ Använd bara stilen "focus" för icke-platta verktygsknappar när de trycks ner
+ Lägg till övre och undre vaddering i ToolSeparator
+ Gör så att ToolSeparator respekterar värdena topPadding och bottomPadding
+ Gör så att MenuSeparator använder bakgrundens beräknade höjd och inte implicitheight
+ Rätta verktygsknappar med menyer som använder nyare Breeze
+ Rita hela kontrollen CheckBox via QStyle

### Solid

+ Lägger till statisk metod storageAccessFromPath, som behövs av https://phabricator.kde.org/D28745

### Syndikering

+ Rätta licensundantag

### Syntaxfärgläggning

+ konvertera alla teman till nya nycklar för editorfärger
+ ändra json temaformat, använd metaobjekt uppräkningsnamn för editorfärger
+ kontrollera kateversion ≥ 5.62 för fallthroughContext utan fallthrough="true" och använd attrToBool för Boolesk egenskap
+ Lägg till syntaxdefinition för todo.txt
+ rätta matchEscapedChar(): det sista tecknet på en rad ignoreras
+ Rätta isDigit(), isOctalChar() och isHexChar(): får bara matcha ascii-tecken
+ generera temaöversikt
+ lägg till metadata för tema i huvudet på HTML-sidor vi genererar
+ börja generera temasamlingssida
+ byt namn på 'Förval' till 'Breeze ljus'
+ Varnish, Vala och TADS3: använd förvald färgstil
+ Förbättra färgtemat Vim mörk
+ Lägg till färgtemat Vim mörk
+ lägg till temafiler för syntaxfärgläggning i json-färgläggning
+ Ruby/Rails/RHTML: lägg till spellChecking i itemDatas
+ Ruby/Rails/RHTML: använd förvald färgstil och andra förbättringar
+ LDIF, VHDL, D, Clojure och ANS-Forth94: använd förvald färgstil
+ ASP: använd förvald färgstil och andra förbättringar
+ låt objective-c vinna för .m-filer
+ .mm är troligare Objective-C++ än meta math
+ använd notAsciiDelimiters bara för annat än ascii-tecken
+ optimera isWordDelimiter(c) med ascii-tecken
+ optimera Context::load
+ använd std::make_shared som tar bort minnestilldelning för kontrollblocket
+ lägg till riktig licens för uppdaterad scripty
+ flytta uppdateringsskript för kate-editor.org/syntax till syntaxarkiv
+ SELinux CIL och Scheme: uppdatera parentesfärger för mörka teman
+ POV-Ray: använd förvald färgstil
+ Använd krita installationsverktygets NSIS-skript som exempel på indata, taget från krita.git
+ lägg till tips för att uppdatera skript för webbplats
+ Lägg till exempel på minimal Django-mall
+ uppdatera referenser efter senaste färgläggningsändringar
+ CMake: rätta oläsliga färger i mörka teman och andra förbättringar
+ Lägg till exempel för pipe och ggplot2
+ rätta namngivningsfel för varnish färgläggning
+ använd riktig färgläggning för 68k ASM: Motorola 68k (VASM/Devpac)
+ rätta XML så att den är giltig i förhållande till XSD
+ BrightScript: Lägg till syntax för exception
+ Förbättra kommentarer i vissa syntaxdefinitioner (del 3)
+ R Script: använd förvald färgstil och andra förbättringar
+ PicAsm: rätta färgläggning av okända nyckelord för preprocessor
+ förbättra kommentarer i vissa syntaxdefinitioner (del 2)
+ Modelines: ta bort regler med LineContinue
+ snabbfix av felaktig färgläggningsfil
+ Optimering: kontrollera om vi är i en kommentar innan användning av ##Doxygen som innehåller flera RegExpr
+ Assemblerspråk: flera rättningar och mer sammanhangsberoende färgläggning
+ ColdFusion: använd förvald färgstil och ersätt några RegExpr-regler
+ förbättra kommentarer i vissa syntaxdefinitioner, del 1
+ Använd vinkelparenteser för sammanhangsinformation
+ Återställ borttagning av byte-order-mark
+ xslt: ändra färg på XSLT-taggar
+ Ruby, Perl, QML, VRML och xslt: använd förvald färgstil och förbättra kommentarer
+ txt2tags: förbättringar och rättningar, använd förvald färgstil
+ Rätta fel och lägg till fallthroughContext=AttrNormal för varje diff.xml sammanhang eftersom alla regler innehåller column=0
+ rätta problem hittade av statisk kontroll
+ importera Pure färgläggning från https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/pure.xml
+ rätta egenskapen kateversion
+ gör uppslagningsordning för färgläggning oberoende av översättningar
+ rätta versionsformat + mellanrumsproblem
+ importera Modula-3 färgläggning från https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/modula-3.xml
+ rätta versionsformat
+ rätta mellanslagsgrejer hittade av statisk kontroll
+ importera LLVM färgläggning från https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/llvm.xml
+ rätta mellanslagsgrejer hittade av statisk kontroll
+ importera Idris från https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/idris.xml
+ rätta fel hittade av statisk kontroll
+ importera ATS från https://github.com/jgm/skylighting/blob/master/skylighting-core/xml/ats.xml
+ koppla bara bort för icke nyskapad data
+ skapa StateData vid behov

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
