---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Strip down and re-write the baloo tags KIO slave (bug 340099)

### BluezQt

- Do not leak rfkill file descriptors (bug 386886)

### Icônes « Breeze »

- Ajout des tailles manquantes d'icônes (bogue 384473)
- Ajouter, installer et désinstaller les icônes pour « Discover »

### Modules additionnels « CMake »

- Ajout d'une étiquette de description pour les fichiers générés par « pkgconfig »
- ecm_add_test: Use proper path sep on Windows
- Ajout de « FindSasl2.cmake » à « ECM »
- Only pass the ARGS thing when doing Makefiles
- Add FindGLIB2.cmake and FindPulseAudio.cmake
- ECMAddTests: set QT_PLUGIN_PATH so locally built plugins can be found
- KDECMakeSettings: more docu about the layout of the build dir

### Intégration avec l'environnement de développement

- Support downloading the 2nd or 3rd download link from a KNS product (bug 385429)

### KActivitiesStats

- Start fixing libKActivitiesStats.pc: (bug 386933)

### KActivities

- Fix race that starts kactivitymanagerd multiple times

### KAuth

- Allow to only build the kauth-policy-gen code generator
- Ajout d'une note concernant l'appel à l'assistant pour les applications multi-processus.

### KBookmarks

- Do not show edit bookmarks action if keditbookmarks is not installed
- Port from deprecated KAuthorized::authorizeKAction to authorizeAction

### KCMUtils

- Navigation au clavier avec et sans « QML kcms »

### KCompletion

- Do not crash when setting new line edit on an editable combo box
- KComboBox: Return early when setting editable to previous value
- KComboBox: Reuse the existing completion object on new line edit

### KConfig

- Don't look for /etc/kderc every single time

### KConfigWidgets

- Utiliser une couleur par défaut pour correspondre aux nouvelles couleurs dans « D7424 »

### KCoreAddons

- Validation des entrées de « SubJobs »
- Signaler les erreurs lors du traitement de fichiers « json »
- Install mimetype definitions for kcfg/kcfgc/ui.rc/knotify &amp; qrc files
- Ajout d'une nouvelle fonction pour mesurer la longueur par texte
- Fix KAutoSave bug on file with white space in it

### KDeclarative

- Exécution de sa compilation sous Windows
- make it compile with QT_NO_CAST_FROM_ASCII/QT_NO_CAST_FROM_BYTEARRAY
- [MouseEventListener] Allow accepting mouse event
- Utilisation du moteur simple « QML »

### KDED

- kded : suppression des appels « D-Bus » vers « ksplash »

### KDocTools

- Mise à jour de la traduction en portugais brésilien
- Mise à jour de la traduction en russe
- Mise à jour de la traduction en russe
- Update customization/xsl/ru.xml (nav-home was missing)

### KEmoticons

- KEmoticons: port plugins to JSON and add support for loading with KPluginMetaData
- Do not leak symbols of pimpl classes, protect with Q_DECL_HIDDEN

### KFileMetaData

- L'utilisation de « usermetadatawritertest » nécessite « Taglib »
- If the property value is null, remove the user.xdg.tag attribute (bug 376117)
- Ouvrir les fichiers en lecture seule dans l'extracteur « TagLib »

### KGlobalAccel

- Regroupement de certains appels « D-Bus » bloquants
- kglobalacceld: Avoid loading an icon loader for no reason
- Générer des chaînes de raccourcis correctes

### KIO

- KUriFilter : filtrer les modules externes dupliqués
- KUriFilter: simplify data structures, fix memory leak
- [CopyJob] Don't start all over after having removed a file
- Fix creating a directory via KNewFileMenu+KIO::mkpath on Qt 5.9.3+ (bug 387073)
- Created an auxiliary function 'KFilePlacesModel::movePlace'
- Expose KFilePlacesModel 'iconName' role
- KFilePlacesModel: Avoid unnecessary 'dataChanged' signal
- Return a valid bookmark object for any entry in KFilePlacesModel
- Create a 'KFilePlacesModel::refresh' function
- Create 'KFilePlacesModel::convertedUrl' static function
- KFilePlaces : création d'une section « A distance »
- KFilePlaces : ajout d'une section pour les périphériques amovibles
- Ajout des « URL » de « baloo » dans le modèle d'emplacements
- Correction de « KIO::mkpath » avec qtbase 5.10 « bêta » 4
- [KDirModel] Emit change for HasJobRole when jobs change
- Change label "Advanced options" &gt; "Terminal options"

### Kirigami

- Offset the scrollbar by the header size (bug 387098)
- bottom margin based on actionbutton presence
- Ne pas prendre l'hypothèse de disponibilité de « applicationWidnow()  »
- Don't notify about value changes if we are still in the constructor
- Remplacement du nom de bibliothèque dans la source
- Prise en charge de couleurs dans plus d'emplacements
- Icônes de couleurs pour les barres d'outils, si nécessaire.
- Considérer les couleurs des icônes dans les boutons principaux d'actions
- Création d'une propriété groupée « Icône »

### KNewStuff

- Revert "Detach before setting the d pointer" (bug 386156)
- do not install development tool to aggregate desktop files
- [knewstuff] Do not leak ImageLoader on error

### Environnement de développement « KPackage »

- Properly do strings in the kpackage framework
- Don't try to generate metadata.json if there's no metadata.desktop
- Correction de la mise en cache de « kpluginindex »
- Améliorer l'affichage des erreurs

### KTextEditor

- Correction des commandes de mise en buffer « VI-Mode »
- Se prémunir d'un zoom accidentel

### KUnitConversion

- Portage de « QDom » vers « QXmlStreamReader »
- Utiliser « https » pour télécharger les taux de conversion de devises

### KWayland

- Expose wl_display_set_global_filter as a virtual method
- Correction de « kwayland-testXdgShellV6 »
- Add support for zwp_idle_inhibit_manager_v1 (bug 385956)
- [server] Support inhibiting the IdleInterface

### KWidgetsAddons

- Éviter des incohérences dans « passworddialog »
- Définition de l'indication « enable_blur_behind » sur demande
- KPageListView: Update width on font change

### KWindowSystem

- [KWindowEffectsPrivateX11] Add reserve() call

### KXMLGUI

- Fix translation of toolbar name when it has i18n context

### Environnement de développement de Plasma

- The #warning directive is not universal and in particular is NOT supported by MSVC
- [IconItem] Use ItemSceneHasChanged rather than connect on windowChanged
- [Icon Item] Explicitly emit overlaysChanged in the setter rather than connecting to it
- [Dialog] Use KWindowSystem::isPlatformX11()
- Reduce the amount of spurious property changes on ColorScope
- [Icon Item] Emit validChanged only if it actually changed
- Suppress unnecessary scroll indicators if the flickable is a ListView with known orientation
- [AppletInterface] Emit change signals for configurationRequired and -Reason
- Utiliser « setSize()  » au lieu de « setProperty » pour la hauteur et à la largeur
- Fixed an issue where PlasmaComponents Menu would appear with broken corners (bug 381799)
- Fixed an issue where context menus would appear with broken corners (bug 381799)
- API docs: add deprecation notice found in the git log
- Synchroniser le composant avec celui dans Kirigami
- Search all KF5 components as such instead as separate frameworks
- Reduce spurious signal emissions (bug 382233)
- Add signals indicating if a screen was added or removed
- Installer des éléments de « Switch »
- Ne pas se fier aux inclusions des fichiers « include »
- Optimiser les noms de rôles pour « SortFilterModel »
- Suppression de « DataModel::roleNameToId »

### Prison

- Ajout du générateur de code « Aztec »

### QQC2StyleBridge

- determine QQC2 version at build time (bug 386289)
- Par défaut, laisser invisible l'arrière-plan
- Ajout d'une couleur d'arrière-plan dans « ScrollView »

### Opaque

- « UDevManager::devicesFromQuery » plus rapide

### Sonnet

- Rendre possible la compilation croisée de « Sonnet »

### Coloration syntaxique

- Ajout de « PKGUILD » à la syntaxe de « bash »
- JavaScript : inclusion des types standards « MIME »
- debchangelog : ajout de « Bionic Beaver »
- Update SQL (Oracle) syntax file (bug 386221)
- SQL: move detecting comments before operators
- crk.xml : ajout d'une ligne d'en-tête &lt;?xml&gt;

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Vous pouvez discuter et partager vos idées sur cette version dans la section des commentaires de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article</a>.
