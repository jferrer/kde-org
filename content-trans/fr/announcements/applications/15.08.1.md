---
aliases:
- ../announce-applications-15.08.1
changelog: true
date: 2015-09-15
description: KDE publie les applications 15.08.1
layout: application
title: KDE publie les applications 15.08.1
version: 15.08.1
---
15 Septembre 2015. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../15.08.0'>applications 15.08 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 40 corrections de bogues apportent des améliorations à kdelibs, kdepim, kdenlive, dolphin, marble, kompare, konsole, ark et umbrello.

Cette mise à jour inclut aussi une version de prise en charge à long terme pour la plate-forme de développement 4.14.12 de KDE.
