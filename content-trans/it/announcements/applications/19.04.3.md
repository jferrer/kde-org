---
aliases:
- ../announce-applications-19.04.3
changelog: true
date: 2019-07-11
description: KDE rilascia Applications 19.04.3.
layout: applications
major_version: '19.04'
release: applications-19.04.3
title: KDE rilascia KDE Applications 19.04.3
version: 19.04.3
---
{{% i18n_date %}}

Oggi KDE ha rilasciato il terzo aggiornamento di stabilizzazione per <a href='../19.04.0'>KDE Applications 19.04</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Oltre sessanta errori corretti includono, tra gli altri, miglioramenti a Kontact, Ark, Cantor, JuK, K3b, Kdenlive, KTouch, Okular e Umbrello.

I miglioramenti includono:

- Konqueror e Kontact non si bloccano più in uscita con QtWebEngine 5.13
- Il taglio di gruppi con composizioni non blocca più l'editor video Kdenlive
- L'importazione Python nel designer UML Umbrello ora gestisce i parametri con argomenti predefiniti
