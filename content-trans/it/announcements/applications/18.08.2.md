---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE rilascia KDE Applications 18.08.2
layout: application
title: KDE rilascia KDE Applications 18.08.2
version: 18.08.2
---
11 ottobre 2018. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../18.08.0'>KDE Applications 18.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, costituisce un aggiornamento sicuro e gradevole per tutti.

Più di una dozzina di errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Gwenview, KCalc e Umbrello.

I miglioramenti includono:

- Trascinare un file in Dolphin non attiva più per errore la rinomina in linea
- KCalc consente di nuovo l'uso dei tasti «punto» e «virgola» quando si inseriscono i decimali
- È stato corretto un errore visivo nel mazzo di carte Parigi per i giochi di carte di KDE
