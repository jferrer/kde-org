---
aliases:
- ../announce-applications-15.04-rc
date: '2015-03-26'
description: KDE lanza la versión candidata de las Aplicaciones 15.04.
layout: application
title: KDE lanza la candidata a versión final para las Aplicaciones 15.04
---
Hoy, 26 de march de 2015, KDE ha lanzado la candidata a versión final de las nuevas Aplicaciones. Una vez congeladas las dependencias y las funcionalidades, el equipo de KDE se ha centrado en corregir errores y en pulir aún más la versión.

Debido a las numerosas aplicaciones que se basan en KDE Frameworks 5, es necesario probar la versión 15.04 de manera exhaustiva con el fin de mantener y mejorar la calidad y la experiencia del usuario. Los usuarios reales son de vital importancia para mantener la alta calidad de KDE porque los desarrolladores no pueden probar todas las configuraciones posibles. Contamos con ustedes para ayudar a encontrar errores de manera temprana de forma que se puedan corregir antes de la versión final. Considere la idea de unirse al equipo instalando la versión <a href='https://bugs.kde.org/'>e informando de cualquier error que encuentre</a>.
