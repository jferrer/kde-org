---
aliases:
- ../announce-applications-16.12.1
changelog: true
date: 2017-01-12
description: KDE lanza las Aplicaciones de KDE 16.12.1
layout: application
title: KDE lanza las Aplicaciones de KDE 16.12.1
version: 16.12.1
---
Hoy, 12 de enero de 2017. KDE ha lanzado la primera actualización de estabilización para las <a href='../16.12.0'>Aplicaciones de KDE 16.12</a>. Esta versión solo contiene soluciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Este lanzamiento corrige un error de pérdida de datos en el recurso iCal, que fallaba al crear std.ics si no existía previamente.

Entre las más de 40 correcciones de errores registradas, se incluyen mejoras en Kdepim, Ark, Gwenview, Kajongg, Okular, Kate y Kdenlive, entre otras aplicaciones.

También se incluye la versión de la Plataforma de desarrollo de KDE 4.14.28 que contará con asistencia a largo plazo.
