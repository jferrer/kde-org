---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [ModifiedFileIndexer] se han corregido las comprobaciones de la hora de los archivos nuevos.
- [ModifiedFileIndexer] Omit BasicIndexingJob run when not required
- Sync IndexerConfig on exit (bug 417127)
- [FileIndexScheduler] Force evaluation of indexerState after suspend/resume

### BluezQt

- Fix errors in the QRegularExpression porting commit

### Iconos Brisa

- Add network-wireless-hotspot icon
- Move telegram panel icons to status category
- [Iconos Brisa] Se han añadido iconos para «telegram-desktop» en la bandeja del sistema (fallo 417582).
- [Iconos Brisa] Nuevo icono de 48 píxeles para Telegram.
- Añadir iconos «rss» en la acción.
- Remove 48px telegram icons
- Hotfix to make sure validation is not done in parallel to generation
- New yakuake logo/icon
- Fix inconsistencies and duplicates in network-wired/wireless icons
- Fix old text color values for osd-* icons
- only install generated icons if they were generated
- escape all paths to ensure the CI system works
- set -e on the generator script so it properly errors out on errors
- build: fix the build where install prefix is not user-writable
- hotfix new 24px generator to use bash instead of sh
- Also auto-generate 24@2x compatibility symlinks
- Auto-generate 24px monochrome icons
- Add icons that were only in actions/24 to actions/22
- Set document scale to 1.0 for all actions/22 icons
- Se han añadido nuevos iconos <code>smiley-add</code>.
- Make shapes and shape-choose icons consistent with other -shape icons
- Make smiley-shape consistent with other -shape icons
- Make flower-shape and hexagon-shape icons consistent with other -shape icons
- Replace &lt;use/&gt; with &lt;path/&gt; in muondiscover.svg
- Add status icons: data-error, data-warning, data-information
- Se ha añadido el icono para «org.kde.Ikona».
- Se ha añadido el icono «vvave».
- Se ha añadido el icono «puremaps».
- Unify the look of all icons containing 🚫 (no sign)
- New icon for KTimeTracker (bug 410708)
- Optimize KTrip and KDE Itinerary icons
- Actualizar los iconos de «travel-family».

### Módulos CMake adicionales

- Support NDK r20 and Qt 5.14
- Load QM files from assets: URLs on Android
- Add ecm_qt_install_logging_categories &amp; ecm_qt_export_logging_category
- ECMGeneratePriFile: unbreak for usages with LIB_NAME not a target name
- ECMGeneratePriFile: Fix static configurations

### Integración con Frameworks

- [KStyle] Set the color of KMessageWidgets to the correct one from the current color scheme

### KActivities

- Fix issue finding the Boost include directories
- Use exposed DBus methods to switch activities in CLI

### KAuth

- [KAuth] Add support for action details in Polkit1 backend
- [policy-gen] Fix the code to actually use the correct capture group
- Drop Policykit backend
- [polkit-1] Simplify Polkit1Backend action exists lookup
- [polkit-1] Return an error status in actionStatus if there is an error
- Calculate KAuthAction::isValid on demand

### KBookmarks

- Se ha cambiado el nombre de las acciones para que sean consistentes.

### KCalendarCore

- Update visibility cache when notebook visibility is changed

### KCMUtils

- Comprobar «activeModule» antes de usarlo (fallo 417396).

### KConfig

- [KConfigGui] Clear styleName font property for Regular font styles (bug 378523)
- Fix code generation for entries with min/max (bug 418146)
- KConfigSkeletonItem : allow to set a KconfigGroup to read and write items in nested groups
- Fix is&lt;PropertyName&gt;Immutable generated property
- Se ha añadido «setNotifyFunction» a «KPropertySkeletonItem».
- Add an is&lt;PropertyName&gt;Immutable to know if a property is immutable

### KConfigWidgets

- Change "Redisplay" to "Refresh"

### KCoreAddons

- add hint that QIcon can be used as a program logo

### KDBusAddons

- Desaconsejar el uso de «KDBusConnectionPool».

### KDeclarative

- Expose capture signal on KeySequenceItem
- Fix size of the header in GridViewKCM (bug 417347)
- Allow ManagedConfigModule derived class to register explicitly KCoreConfigSkeleton
- Allow to use KPropertySkeletonItem in ManagedConfigModule

### KDED

- Add a --replace option to kded5

### Complementos KDE GUI

- [UrlHandler] Handle opening the online docs for KCM modules
- [KColorUtils] Change getHcy() hue range to [0.0, 1.0)

### KHolidays

- Actualización de las festividades japonesas.
- holiday_jp_ja - fix spelling for National Foundation Day (bug 417498)

### KI18n

- Support Qt 5.14 on Android

### KInit

- Make kwrapper/kshell spawn klauncher5 if needed

### KIO

- [KFileFilterCombo] Don't add invalid QMimeType to mimes filter (bug 417355)
- [src/kcms/*] Replace foreach (deprecated) with range/index-based for
- KIO::iconNameForUrl(): handle the case of a file/folder under trash:/
- [krun] Share implementation of runService and runApplication
- [krun] Drop KToolInvocation support from KRun::runService
- Improve KDirModel to avoid showing '+' if there are no subdirs
- Se ha corregido la ejecución de «Konsole» en Wayland (fallo 408497).
- KIO::iconNameForUrl: fix searching for kde protocol icons (bug 417069)
- Correct capitalization for "basic link" item
- Change "AutoSkip" to "Skip All" (bug 416964)
- Fix memory leak in KUrlNavigatorPlacesSelector::updateMenu
- file ioslave: stop copying as soon as the ioslave is killed
- [KOpenWithDialog] Automatically select the result if the model filter has only one match (bug 400725)

### Kirigami

- Show tooltip with full URL for URL button with overridden text
- Have pullback toolbars on scrollable pages also for footers
- Fix PrivateActionToolButton behaviour with showText vs IconOnly
- Fix ActionToolBar/PrivateActionToolButton in combination with QQC2 Action
- Move checked menu item always into range
- Watch for language change events, and forward those to the QML engine
- Support Qt 5.14 on Android
- don't have overlaysheets under page header
- use fallback when icon failed to load
- Missing links to pagepool source files
- Icon: fix rendering of image: urls on High DPI (bug 417647)
- No fallar cuando la anchura o la altura de los iconos es 0 (fallo 417844).
- fix margins in OverlaySheet
- [examples/simplechatapp] Always set isMenu to true
- [RFC] Reduce size of Level 1 headings and increase left padding on page titles
- properly sync size hints with state machine (bug 417351)
- Añadir compatibilidad con los complementos del tema de la plataforma.
- make headerParent correctly aligned when there is a scrollbar
- Se ha corregido el cálculo de la anchura de «tabbar».
- Se ha añadido «PagePoolAction» al archivo QRC.
- allow toolbar style on mobile
- Make the api docs reflect that Kirigami is not only a mobile toolkit

### KItemModels

- KRearrangeColumnsProxyModel: se ha desactivado temporalmente una aserción debido a un error en «QTreeView».
- KRearrangeColumnsProxyModel: reinicio en «setSourceColumns()».
- Move Plasma's SortFilterProxyModel into KItemModel's QML plugin

### KJS

- Exponer las funciones de gestión de tiempos de espera de evaluación en la API pública.

### KNewStuff

- Fix clicking thumb-only delegate (bug 418368)
- Se ha corregido el desplazamiento de la página «EntryDetails» (error 418191).
- Don't double delete CommentsModel (bug 417802)
- Cover also the qtquick plugin in the installed categories file
- Use the right translation catalog to show translations
- Fix the KNSQuick Dialog's close title and basic layout (bug 414682)

### KNotification

- Make kstatusnotifieritem available without dbus
- Adapt action numbering in Android to work like in KNotifications
- Write down Kai-Uwe as the knotifications maintainer
- Always strip html if server does not support it
- [android] Emit defaultActivated when tapping the notification

### KPeople

- Se ha corregido la generación del archivo «pri».

### KQuickCharts

- Do not print errors about invalid roles when roleName is not set
- Use offscreen platform for tests on Windows
- Remove glsl validator download from validation script
- Fix validation error in line chart shader
- Update linechart core profile shader to match compat
- Se ha añadido un comentario sobre la comprobación de límites.
- LineChart: se ha añadido la comprobación de los límites de mín/máx y.
- Add sdf_rectangle function to sdf library
- [linechart] Protección contra división por 0.
- Line charts: Reduce the number of points per segment
- Don't lose points at the end of a line chart

### Kross

- Qt5::UiTools is not optional in this module

### KService

- Nuevo mecanismo de consulta para las aplicaciones: KApplicationTrader.

### KTextEditor

- Add an option to dynamic-break inside words
- KateModeMenuList: no solapar la barra de desplazamiento.

### KWayland

- Add application menu dbus paths to org_kde_plasma_window interface
- Registry: don't destroy the callback on globalsync
- [surface] Fix buffer offset when attaching buffers to surfaces

### KWidgetsAddons

- [KMessageWidget] Allow the style to change our palette
- [KMessageWidget] Draw it with QPainter instead of using stylesheet
- Slightly reduce level 1 heading size

### ModemManagerQt

- Drop qmake pri file generation &amp; installation, currently broken

### NetworkManagerQt

- Support SAE in securityTypeFromConnectionSetting
- Drop qmake pri file generation &amp; installation, currently broken

### Iconos de Oxígeno

- Support data-error/warning/information also in 32,46,64,128 sizes
- Add "plugins" action item, to match Breeze icons
- Add status icons: data-error, data-warning, data-information

### Framework de Plasma

- Buttons: allow to scale up icons
- Try to apply the colorscheme of the current theme to QIcons (bug 417780)
- Dialog: disconnect from QWindow signals in destructor
- Se han corregido fugas de memoria en «ConfigView» y en «Dialog».
- Se han corregido las sugerencias del tamaño del diseño para las etiquetas de los botones.
- Se asegura que las sugerencias de tamaño son enteras y pares.
- Permitir el uso de icon.width/height (fallo 417514).
- Remove hardcoded colors (bug 417511)
- Construir «NullEngine» con «KPluginMetaData()» (error 417548).
- Slightly reduce level 1 heading size
- Centrar verticalmente el icono o la imagen de la ayuda emergente.
- Permitir la propiedad «display» de los botones.
- Don't warn for invalid plugin metadata (bug 412464)
- tooltips always have normal colorgroup
- [Tests] Make radiobutton3.qml use PC3
- Optimize code when dropping files into the desktop (bug 415917)

### Prison

- Fix pri file to not fail with CamelCase includes
- Fix pri file to have qmake name of QtGui as dependency

### Purpose

- Se ha reescrito el complemento de nextcloud.
- Se ha eliminado el soporte de twitter.

### QQC2StyleBridge

- ScrollView: Use scrollbar height as bottom padding, not width

### Solid

- Fix inverted logic in IOKitStorage::isRemovable

### Sonnet

- Fix segfault at exit

### Resaltado de sintaxis

- Fix out-of-memory due to too large context stacks
- Actualización general del resaltado de sintaxis para CartoCSS.
- Se ha añadido resaltado de sintaxis para Java Properties.
- TypeScript: add private fields and type-only imports/exports, and some fixes
- Se ha añadido la extensión «FCMacro» de «FreeCAD» a la definición de resaltado sintáctico de Python.
- Actualizaciones para CMake 3.17.
- C++: constinit keyword and std::format syntax for strings. Improvement printf format
- Especificación RPM: varias mejoras.
- Makefile highlight: fix variable names in "else" conditionals (bug 417379)
- Se ha añadido resaltado de sintaxis para Solidity.
- Pequeñas mejoras en algunos archivos XML.
- Makefile highlight: add substitutions (bug 416685)

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
