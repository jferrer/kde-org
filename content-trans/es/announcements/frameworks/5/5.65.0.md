---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
New module: KQuickCharts -- a QtQuick module providing high-performance charts.

### Iconos Brisa

- Pixel align color-picker
- Añadir nuevos iconos para «baloo».
- Se han añadido nuevos iconos de búsqueda de preferencias.
- Use an eyedropper for color-picker icons (bug 403924)
- Add "all applications" category icon

### Módulos CMake adicionales

- EBN extra-cmake-modules transport cleanup
- ECMGenerateExportHeader: se ha añadido el indicador «NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE».
- Explicitly use lib for systemd directories
- Add install dir for systemd units
- KDEFrameworkCompilerSettings: activar todas las advertencias de usos obsoletos de Qt y KF.

### Integración con Frameworks

- Conditionally set SH_ScrollBar_LeftClickAbsolutePosition based on kdeglobals setting (bug 379498)
- Set application name and version on the knshandler tool

### Herramientas KDE Doxygen

- Se ha corregido la importación de módulos con Python3.

### KAuth

- Instalar archivo «.pri» para «KAuthCore».

### KBookmarks

- Marcar como obsoletos «KonqBookmarkMenu» y «KonqBookmarkContextMenu».
- Move classes only used by KonqBookmarkMenu together with it

### KCalendarCore

- Fallback to system time zone on calendar creation with an invalid one
- Memory Calendar: evitar duplicación de código.
- Usar QDate como clave en «mIncidencesForDate» en «MemoryCalendar».
- Handle incidences in different time zones in MemoryCalendar

### KCMUtils

- [KCMultiDialog] Remove most special margins handling; it's done in KPageDialog now
- KPluginSelector: usar el nuevo «KAboutPluginDialog».
- Se ha añadido una salvaguardia para cuando falta kirigami (error 405023).
- Disable the restore defaults button if the KCModule says so
- Have KCModuleProxy take care of the defaulted state
- Make KCModuleQml conform to the defaulted() signal

### KCompletion

- Refactorizar «KHistoryComboBox::insertItems».

### KConfig

- Preferencia de notificadores de documento.
- Only create a session config when actually restoring a session
- kwriteconfig: añadida opción de borrado.
- Se ha añadido «KPropertySkeletonItem».
- Prepare KConfigSkeletonItem to allow inheriting its private class

### KConfigWidgets

- [KColorScheme] Make order of decoration colors match DecorationRole enum
- [KColorScheme] Fix mistake in NShadeRoles comment
- [KColorScheme/KStatefulBrush] Switch hardcoded numbers for enum items
- [KColorScheme] Add items to ColorSet and Role enums for the total number of items
- Registrar «KKeySequenceWidget» para «KConfigDialogManager».
- Adjust KCModule to also channel information about defaults

### KCoreAddons

- Desaconsejar el uso de «KAboutData::fromPluginMetaData», ahora se usa «KAboutPluginDialog».
- Add a descriptive warning when inotify_add_watch returned ENOSPC (bug 387663)
- Add test for bug "bug-414360" it's not a ktexttohtml bug (bug 414360)

### KDBusAddons

- Include API to generically implement --replace arguments

### KDeclarative

- EBN kdeclarative transfer protocol cleanup
- Adaptarse a los cambios en «KConfigCompiler».
- make header and footer visible when they get content
- Permitir el uso de «qqmlfileselectors».
- Allow to disable autosave behavior in ConfigPropertyMap

### KDED

- Eliminar la dependencia de «kdeinit» en «kded».

### Soporte de KDELibs 4

- Se ha eliminado el «kgesturemap» no usado en «kaction».

### KDocTools

- Catalan Works: Add missing entities

### KI18n

- No desaconsejar el uso de «I18N_NOOP2».

### KIconThemes

- Deprecate top-level UserIcon method, no longer used

### KIO

- Añadir nuevo protocolo para los archivos comprimidos 7z.
- [CopyJob] When linking also consider https for text-html icon
- [KFileWidget] Avoid calling slotOk right after the url changed (bug 412737)
- [kfilewidget] Cargar iconos por su nombre.
- KRun: don't override user preferred app when opening local *.*html and co. files (bug 399020)
- Repair FTP/HTTP proxy querying for the case of no proxy
- Ftp ioslave: Fix ProxyUrls parameter passing
- [KPropertiesDialog] provide a way of showing the target of a symlink (bug 413002)
- [Remote ioslave] Add Display Name to remote:/ (bug 414345)
- Fix HTTP proxy settings (bug 414346)
- [KDirOperator] Add Backspace shortcut to back action
- When kioslave5 couldn't be found in libexec-ish locations try $PATH
- [Samba] Improve warning message about netbios name
- [DeleteJob] Use a separate worker thread to run actual IO operation (bug 390748)
- [KPropertiesDialog] Hacer que la cadena de la fecha de creación también se pueda seleccionar con el ratón (error 413902).

Uso desaconsejado:

- Deprecated KTcpSocket and KSsl* classes
- Remove the last traces of KSslError from TCPSlaveBase
- Port ssl_cert_errors meta data from KSslError to QSslError
- Deprecate KTcpSocket overload of KSslErrorUiData ctor
- [http kio slave] use QSslSocket instead of KTcpSocket (deprecated)
- [TcpSlaveBase] port from KTcpSocket (deprecated) to QSslSocket

### Kirigami

- Se han corregido los márgenes de «ToolBarHeader».
- No producir un fallo cuando la fuente del icono está vacía.
- MenuIcon: Se han corregido las advertencias cuando el cajón está sin inicializar.
- Account for a mnemonic label to go back to ""
- Fix InlineMessage actions always being placed in overflow menu
- Corregir el color del fondo de las tarjetas (error 414329).
- Icon: solve threading issue on when the source is http
- keyboard navigation fixes
- i18n: extract messages also from C++ sources
- Fix cmake project command position
- Make QmlComponentsPool one instance per engine (bug 414003)
- Switch ToolBarPageHeader to use the icon collapse behaviour from ActionToolBar
- ActionToolBar: Automatically change to icon-only for actions marked KeepVisible
- Se ha añadido una propiedad «displayHint» a «Action».
- add the dbus interface in the static version
- Revert "take into account dragging speed when a flick ends"
- FormLayout: Fix label height if wide mode is false
- don't show the handle by default when not modal
- Se ha revertido «Asegurarse de que el topContent del GlobalDrawer siempre permanece encima».
- Use a RowLayout for laying out ToolBarPageHeader
- Vertically center left actions in ActionTextField (bug 413769)
- irestore dynamic watch of tablet mode
- Sustituir «SwipeListItem».
- support actionsVisible property
- start SwipeListItem port to SwipeDelegate

### KItemModels

- Desaconsejar el uso de «KRecursiveFilterProxyModel».
- KNumberModel: gracefully handle a stepSize of 0
- Expose KNumberModel to QML
- Add new class KNumberModel that is a model of numbers between two values
- Expose KDescendantsProxyModel to QML
- Add qml import for KItemModels

### KNewStuff

- Add some friendly "report bugs here" links
- Fix i18n syntax to avoid runtime errors (bug 414498)
- Turn KNewStuffQuick::CommentsModel into a SortFilterProxy for reviews
- Correctly set i18n arguments in one pass (bug 414060)
- These functions are @since 5.65, not 5.64
- Add OBS to screenrecorders (bug 412320)
- Fix a couple of broken links, update links to https://kde.org/applications/
- Se han corregido las traducciones de $GenericName.
- Show a "Loading more..." busy indicator when loading view data
- Give some more pretty feedback in NewStuff::Page while the Engine is loading (bug 413439)
- Add an overlay component for item activity feedback (bug 413441)
- Only show DownloadItemsSheet if there's more than one download item (bug 413437)
- Use the pointing hand cursor for the single-clickable delegates (bug 413435)
- Fix the header layouts for EntryDetails and Page components (bug 413440)

### KNotification

- Make the docs reflect that setIconName should be preferred over setPixmap when possible
- Ruta del archivo de configuración de documentos en Android.

### KParts

- Mark BrowserRun::simpleSave properly as deprecated
- BrowserOpenOrSaveQuestion: move AskEmbedOrSaveFlags enum from BrowserRun

### KPeople

- Allow triggering sort from QML

### kquickcharts

New module. The Quick Charts module provides a set of charts that can be used from QtQuick applications. They are intended to be used for both simple display of data as well as continuous display of high-volume data (often referred to as plotters). The charts use a system called distance fields for their accelerated rendering, which provides ways of using the GPU for rendering 2D shapes without loss of quality.

### KTextEditor

- KateModeManager::updateFileType(): validate modes and reload menu of the status bar
- Verificar los modos del archivo de configuración de sesión.
- LGPLv2+ after ok by Svyatoslav Kuzmich
- restore files pre-format

### KTextWidgets

- Deprecate kregexpeditorinterface
- [kfinddialog] Remove usage of kregexpeditor plugin system

### KWayland

- [servidor] No apropiarse de la implementación de «dmabuf».
- [server] Make double-buffered properties in xdg-shell double-buffered

### KWidgetsAddons

- [KSqueezedTextLabel] Añadir icono para la acción «Copiar todo el texto».
- Unify KPageDialog margin handling into KPageDialog itself (bug 413181)

### KWindowSystem

- Ajustar el contador tras añadir «_GTK_FRAME_EXTENTS».
- Permitir el uso de «_GTK_FRAME_EXTENTS».

### KXMLGUI

- Descartar el uso de «KGesture», que ya no se usaba y era defectuoso.
- Se ha añadido «KAboutPluginDialog» para usar con «KPluginMetaData».
- Also allow invoking session restoration logic when apps are manually launched (bug 413564)
- Se ha añadido una propiedad que faltaba a «KKeySequenceWidget».

### Iconos de Oxígeno

- Symlink microphone to audio-input-microphone on all sizes (bug 398160)

### Framework de Plasma

- move backgroundhints managment in Applet
- use the file selector in the interceptor
- Más uso de «ColorScope».
- Supervisar también los cambios en las ventanas.
- Permitir que el usuario pueda eliminar el fondo y la sombra automática.
- Permitir el uso de selectores de archivos.
- Permitir el uso de selectores de archivos QML.
- remove stray qgraphicsview stuff
- don't delete and recreate wallpaperinterface if not needed
- MobileTextActionsToolBar check if controlRoot is undefined before using it
- Añadir «hideOnWindowDeactivate» a «PlasmaComponents.Dialog».

### Purpose

- Incluir la orden «cmake» cuando estamos a punto de usarla.

### QQC2StyleBridge

- [TabBar] Usar color de ventana en lugar de color de botón (fallo 413311).
- Enlazar las propiedades activadas a la vista activa.
- [ToolTip] Base timeout on text length
- [ComboBox] Don't dim Popup
- [ComboBox] Don't indicate focus when popup is open
- [ComboBox] Seguir la política del foco.

### Solid

- [udisks2] fix media change detection for external optical drives (bug 394348)

### Sonnet

- Desactivar el motor «ispell» con «mingw».
- Implementar el motor «ISpellChecker» para Windows &gt;= 8.
- Compilación cruzada básica para «parsetrigrams».
- Integrar «trigrams.map» en la biblioteca compartida.

### Sindicación

- Fix Bug 383381 - Getting the feed URL from a youtube channel no longer works (bug 383381)
- Extraer código para poder corregir el código de análisis (fallo 383381).
- atom has icon support (So we can use specific icon in akregator)
- Convert as a real qtest apps

### Resaltado de sintaxis

- Actualizaciones de la versión final de CMake 3.16.
- reStructuredText: Fix inline literals highlighting preceding characters
- rst: permitir el uso de hiperenlaces aislados.
- JavaScript: move keywords from TypeScript and other improvements
- JavaScript/TypeScript React: se ha cambiado el nombre de las definiciones de sintaxis.
- LaTeX: fix backslash delimiter in some keywords (bug 413493)

### ThreadWeaver

- Use URL with transport encryption

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
