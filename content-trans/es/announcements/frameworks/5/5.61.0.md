---
aliases:
- ../../kde-frameworks-5.61.0
date: 2019-08-10
layout: framework
libCount: 70
---
### Baloo

- Link against KIOCore instead of KIOWidgets in kioslaves
- [IndexCleaner] ignore non-existent entries inside config

### BluezQt

- Fix crash due to the q pointer never being initialized
- Don't include bluezqt_dbustypes.h from installed headers

### Iconos Brisa

- Se ha añadido el icono «user-others» (error 407782).
- Make "edit-none" a symlink to "dialog-cancel"
- Delete redundant and monochrome versions of applications-internet
- Add view-pages-* icons, as needed in Okular for page layout selection (bug 409082)
- Use clockwise arrows for _refresh_ and update-* icons (bug 409914)

### Módulos CMake adicionales

- android: Allow overriding ANDROID_ARCH and ANDROID_ARCH_ABI as envvars
- Notify users when not using KDE_INSTALL_USE_QT_SYS_PATHS about prefix.sh
- Provide a more sensible CMAKE_INSTALL_PREFIX default
- Make the default build type "Debug" when compiling a git checkout

### KActivitiesStats

- Add Date term to KActivities Stats to filter on resource event date

### KActivities

- Simplify previous-/nextActivity code in kactivities-cli

### Herramientas KDE Doxygen

- Fix checking dirs for metainfo.yaml with non-ascii chars with Python 2.7
- Log bad pathnames (via repr()) instead of crashing entirely
- generate list of data files on the fly

### KArchive

- KTar::openArchive: Don't assert if file has two root dirs
- KZip::openArchive: No realizar una aserción al abrir archivos dañados.

### KCMUtils

- Se ha adaptado «KPageView» a los cambios de la interfaz gráfica.

### KConfig

- Security: remove support for $(...) in config keys with [$e] marker
- Incluir la definición de la clase usada en la cabecera.

### KCoreAddons

- Add KFileUtils::suggestName function to suggest a unique filename

### KDeclarative

- Scrollview - Don't fill the parent with the view (bug 407643)
- introduce FallbackTapHandler
- KRun QML proxy: fix path/URL confusion
- Calendar events: allow plugins to show event details

### KDED

- kded5 desktop file: use valid type (Service) to suppress warning from kservice

### Soporte de KDELibs 4

- Designer plugin: use consistently "KF5" in group names &amp; texts
- Don't advertise using KPassivePopup

### KDesignerPlugin

- expose new KBusyIndicatorWidget
- Remove designer plugin generation for KF5WebKit

### KDE WebKit

- Use preview of ECMAddQtDesignerPlugin instead of KF5DesignerPlugin
- Add option to build Qt Designer plugin (BUILD_DESIGNERPLUGIN, default ON)

### KFileMetaData

- Get mobipocket extractor up-to-date, but keep disabled

### KHolidays

- Add public holidays' substitute days in Russia, for 2019-2020
- Se han actualizado las festividades de Rusia.

### KIconThemes

- Restore "Check if group &lt; LastGroup, as KIconEffect doesn't handle UserGroup anyway"

### KIO

- Deprecate suggestName in favour of the one in KCoreAddons
- Fix can't enter directory error on some FTP servers with Turkish locale (bug 409740)

### Kirigami

- Revamp Kirigami.AboutPage
- Consistently use Units.toolTipDelay instead of hardcoded values
- properly size the card contents when the card size is constrained
- hide ripple when we don't want items clickable
- Hacer que el asa siga la altura arbitraria del cajón.
- [SwipeListItem] Take into account scrollbar visibility and form factor for handle and inline actions
- Remove scaling of iconsize unit for isMobile
- always show back button on layers&gt;1
- Ocultar las acciones con submenús del menú «Más».
- default ActionToolBar position to Header
- big z to not appear under dialogs
- use opacity to hide buttons that don't fit
- add the spacer only when it fills the width
- fully retrocompatible with showNavigationButtons as bool
- more granularity to globalToolBar.showNavigationButtons

### KItemModels

- David Faure is now the maintainer for KItemModels
- KConcatenateRowsProxyModel: add note that Qt 5.13 provides QConcatenateTablesProxyModel

### Framework KPackage

- Offer metadata.json when requesting the package metadata
- PackageLoader: Use the right scope for the KCompressionDevice

### KPeople

- declarative: refresh actions list when person changes
- Declarativo: no fallar cuando se usa la API de manera inadecuada.
- personsmodel: Add phoneNumber

### KService

- Expose X-KDE-Wayland-Interfaces
- Fix KService build on Android
- KService: remove broken concept of global sycoca database
- Remove very dangerous deletion code with kbuildsycoca5 --global
- Fix infinite recursion and asserts when the sycoca DB is unreadable by user (e.g. root owned)
- Deprecate KDBusServiceStarter. All usage in kdepim is now gone, DBus activation is a better solution
- Allow KAutostart to be constructed using an absolute path

### KTextEditor

- Save and load page margins
- Don't persist authentication
- Re-map default "Switch input mode" shortcut to not conflict with konsolepart (bug 409978)
- Make keyword completion model return HideListIfAutomaticInvocation by default
- Minimap: Do not grab the left-mouse-button-click on up/down buttons
- allow up to 1024 hl ranges instead of not highlighting the line at all if that limit is reached
- fix folding of lines with end position at column 0 of a line (bug 405197)
- Add option to treat some chars also as "auto bracket" only when we have a selection
- Add an action to insert a non-indented newline (bug 314395)
- Add setting to enable/disable text drag-and-drop (on by default)

### KUnitConversion

- Add Binary Data units (bits, kilobytes, kibibytes ... yottabytes)

### Framework KWallet

- Move kwalletd initialization earlier (bug 410020)
- Remove kde4 migration agent completely (bug 400462)

### KWayland

- Usar protocolos de Wayland.

### KWidgetsAddons

- introduce concept of header and footer for kpageview
- [Busy Indicator] Match duration of QQC2-desktop-style version
- Add a warning dialog with a collapsible details section
- new class KBusyIndicatorWidget similar to QtQuick's BusyIndicator

### KWindowSystem

- [platforms/xcb] Use XRES extension to get real window PID (bug 384837)
- Port KXMessages away from QWidget

### KXMLGUI

- Add expanding spacers as a customization option for toolbars
- Use monochrome action icons for KAboutData buttons
- Remove visibilityChanged connection in favor of existing eventFilter

### ModemManagerQt

- Allow updating default DBus timeout on every interface

### NetworkManagerQt

- device: include reapplyConnection() in the interface

### Framework de Plasma

- [ToolButtonStyle] Use same color group for hovered state
- Handle colors file in fake plasma theme installer
- Install plasma theme into local XDG_DATA_DIR for icon test
- Apply busy indicator duration change of D22646 to the QQC2 style
- Compile package structure plugins into expected subdirectory
- Change Highlight to ButtonFocus
- Fix running the dialognativetest without installing
- Buscar el complemento del otro plasmoide.
- [Busy Indicator] Match duration of QQC2-desktop-style version
- Se han añadido los componentes que faltaban en «org.kde.plasma.components 3.0».
- Actualizar los iconos «refresh» y «restart» para que reflejen las nuevas versiones de iconos Brisa (fallo 409914).
- «itemMouse» no está definido en «plasma.components 3.0».
- Usar «clearItems» cuando se borra la miniaplicación.
- Se ha corregido un fallo cuando «switchSize» se ajusta durante la configuración inicial.
- Se ha mejorado la caché de complementos.

### Purpose

- Phabricator: Abrir una nueva diferencia automáticamente en el navegador.
- Se ha corregido la extracción. Parche creado por Victor Ryzhykh.

### QQC2StyleBridge

- Se ha corregido una salvaguardia que no funcionaba para impedir deslizadores con valores negativos.
- Se ha disminuido la velocidad de rotación del indicador de ocupado.
- Fix "Type error" when creating a TextField with focus: true
- [ComboBox] Set close policy to close on click outside instead of only outside parent (bug 408950)
- [SpinBox] Definir «renderType» (fallo 409888).

### Solid

- Asegurarse de que los motores de «solid» son reentrantes.

### Resaltado de sintaxis

- TypeScript: se han corregido las palabras clave en las expresiones condicionales.
- Se han corregido las rutas del generador y de pruebas de CMake.
- Add support for additional QML keywords not part of JavaScript
- Actualizar el resaltado de sintaxis de cmake.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
