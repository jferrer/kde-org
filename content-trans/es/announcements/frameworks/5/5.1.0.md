---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE lanza la segunda versión de Frameworks 5.
layout: framework
qtversion: 5.2
title: KDE lanza la segunda versión de Frameworks 5
---
7 de agosto de 2014. Hoy, KDE anuncia la segunda versión de KDE Frameworks 5. En consonancia con la política de lanzamientos de KDE Frameworks, esta versión se presenta un mes después de la versión inicial y trae tanto soluciones de errores como nuevas funcionalidades.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
