---
aliases:
- ../announce-applications-19.04-rc
date: 2019-04-05
description: KDE veröffentlicht den Freigabekandidaten der Anwendungen 19.04.
layout: application
release: applications-19.03.90
title: KDE veröffentlicht den Freigabekandidaten der KDE-Anwendungen 19.04
version_number: 19.03.90
version_text: 19.04 Release Candidate
---
05. April 2019. Heute veröffentlicht KDE den Freigabekandidaten der neuen Version der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

In den <a href='https://community.kde.org/Applications/19.04_Release_Notes'>Veröffentlichungshinweisen der KDE-Gemeinschaft</a> finden Sie Informationen über die Quelltextarchive und bekannte Probleme. Eine vollständigere Ankündigung erscheint mit der endgültigen Version.

Es sind sind gründliche Tests für die Veröffentlichung der KDE-Anwendungen 19.04 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Benutzer, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim KDE-Team und installieren Sie den Freigabekandidaten und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.
