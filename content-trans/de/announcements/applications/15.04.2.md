---
aliases:
- ../announce-applications-15.04.2
changelog: true
date: 2015-06-02
description: KDE veröffentlicht die KDE-Anwendungen 15.04.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.04.2
version: 15.04.2
---
02. Juni 2015. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../15.04.0'>KDE-Anwendungen 15.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 30 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für die Programme Gwenview, Kate, Kdenlive, Konsole, Marble, Kgpg, Kig, Ktp-call-ui und Umbrello.

This release also includes Long Term Support versions of Plasma Workspaces 4.11.20, KDE Development Platform 4.14.9 and the Kontact Suite 4.14.9.
