---
date: 2013-08-14
hidden: true
title: KDE-Plasma-Arbeitsbereich 4.11 verbessert weiter die Benutzererfahrung
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE-Plasma-Arbeitsbereich 4.11` width="600px" >}}

In der 4.11-Veröffentlichung der Plasma-Arbeitsbereiche wurde die Fensterleiste – eines der am häufigsten genutzten Plasma-Miniprogramme  – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>nach QtQuick portiert</a>. Die neue Fensterleiste zeigt jetzt ein konsistenteres und flüssigeres Verhalten, behält aber ihr altes Aussehen und Funktionalität. Außerdem behebt diese Portierung auch eine Reihe lang bekannter Fehler. Die Akkuüberwachung (die bisher die Bildschirmhelligkeit einstellen konnte) unterstützt jetzt auch die Helligkeit von Tastaturbeleuchtungen und kann mit mehreren Batterien in Peripheriegeräten umgehen, etwa in drahtlosen Mäusen und Tastaturen. Es zeigt den Batterieladestand für jedes Gerät an und warnt, wenn einer niedrig wird. Das Kickoff-Menü zeigt jetzt Programme an, die in den letzten Tagen installiert worden sind. Zu guter Letzt haben Benachrichtigungen jetzt einen Knopf zum Einrichten, um auf einfache Weise die Einstellungen für die jeweilige Benachrichtigungsart ändern zu können.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Verbesserte Verarbeitung von Benachrichtigungen` width="600px" >}}

KMix – Der Sound-Mixer für KDE erhielt bedeutende Verbesserungen an Leistung und Stabilität wie auch <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>vollständige Unterstützung von Mediensteuerung</a> auf der Basis des Standards MPRIS2.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Das neu entwickelte Akku-Miniprogramm in Aktion ` width="600px" >}}

## KWin-Fensterverwaltung und -Compositor

Unsere Fensterverwaltung KWin hat auch wieder große Verbesserungen erhalten. Veraltete Technologien wurden entfernt und stattdessen das Kommunikationsprotokoll „XCB“ implementiert. Dadurch läuft die Fensterverwaltung flüssiger und schneller. Außerdem wurde Unterstützung für OpenGL 3.1 und OpenGL ES 3.0 eingebaut. Diese Veröffentlichung enthält des weiteren eine erste, experimentelle Unterstützung des X11-Nachfolgers Wayland. Dadurch wird es möglich, KWin mit X11 auf dem Wayland-System laufenzulassen. Um mehr über die Nutzung dieses experimentellen Modus zu erfahren, können Sie <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>diesen Artikel</a> lesen. Es gibt große Verbesserungen in der Skript-Schnittstelle, u.a. eine Konfigurationsoberfläche, neue Animationen und andere graphische Effekte sowie viele kleinere Dinge. Die Behandlung des Mehrbildschirmbetriebs wurde ausgebaut, einschließlich einer Glüh-Option für „aktive Ecken“, verbesserter Fensteranordnung an den Bildschirmrändern (einschließlich anpassbarer Verteilungsflächen) sowie dem üblichen Schub an Fehlerkorrekturen und Optimierungen. Siehe <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>hier</a> und <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>hier</a> für mehr Details.

## Umgang mit mehreren Monitoren und Webkürzel

Die Monitoreinrichtung in den Systemeinstellungen wurde durch das <a href='http://www.afiestas.org/kscreen-1-0-released/'>neue Dienstprogramm KScreen</a> ersetzt. KScreen bringt intelligentere Unterstützung für mehrere Bildschirme für die Plasma-Arbeitsbereiche, automatische Einrichtung neuer Bildschirme und Speicherung manueller Einstellungen. Es biete eine intuitive visuelle Bedienungsoberfläche und ermöglicht die neue Anordnung von Bildschirmen durch einfaches Ziehen und Ablegen.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Der neue Umgang mit Monitoren durch KScreen` width="600px" >}}

Die Webkürzel, der einfachste Weg um schnell Informationen im Web zu finden, wurden aufgeräumt und verbessert. Viele Webkürzel wurden aktualisiert, um sicher verschlüsselte Verbindungen (TLS/SSL) zu verwenden. Der Prozess zum Hinzufügen eigener Webkürzel wurde ebenso verbessert. Weitere Informationen finden Sie <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>hier</a>.

Diese Veröffentlichung markiert das Ende der Plasma Arbeitsbereiche 1 als Teil der KDE SC 4 Serie. Um den Übergang zur nächsten Hauptversion von KDE zu erleichtern, wird diese Version für die nächsten zwei Jahre unterstützt. Der Schwerpunkt der weiteren Entwicklung liegt jetzt auf dem Übergang zum Plasma Arbeitsbereich 2. Verbesserungen der Geschwindigkeit und Fehlerkorrekturen werden nur noch für die 4.11 Serie durchgeführt.

#### Plasma installieren

KDE-Software einschließlich all ihrer Bibliotheken und Anwendungen ist kostenlos unter Open-Source-Lizenzen erhältlich. KDE-Software läuft auf verschiedenen Betriebssystemen, Hardware-Konfigurationen und Prozessorarchitekturen wie z.B. ARM und x86, und es arbeitet mit jeder Art von Fensterverwaltung und Arbeitsumgebung zusammen. Neben Linux und anderen UNIX-basierten Betriebssystemen sind von den meisten KDE-Anwendungen auch Versionen für Microsoft Windows und Apple Mac OS X erhältlich. Sie sind auf den Seiten <a href='http://windows.kde.org'>KDE-Software für Windows</a> bzw. <a href='http://mac.kde.org/'>KDE-Software für den Mac</a> erhältlich. Experimentelle Versionen von KDE-Anwendungen für verschiedene Mobilplattformen wie MeeGo, MS Windows Mobile und Symbian sind im Internet erhältlich, werden zur Zeit aber nicht unterstützt. <a href='http://plasma-active.org'>Plasma Active</a> ist eine Benutzerumgebung für ein großes Spektrum an Geräten wie beispielsweise Tabletts und andere mobile Hardware.

Die KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> heruntergeladen und auch auf <a href='/download'>CD-ROM</a> und von vielen großen <a href='http://download.kde.org/stable/%1'>GNU/Linux- und UNIX-Systemen</a> bezogen werden.

##### Pakete

Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete von 4.11.0für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft, <br />

##### Paketquellen

Eine aktuelle Liste aller Binärpakete, von denen das Release-Team in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community-Wiki</a>.

Der vollständige Quelltext für 4.11.0 kann <a href='/info/4/4.11.0'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von %1 finden Sie auf der <a href='/info/4/4.11.0#binary'>Infoseite zu 4.11.0</a>.

#### Systemanforderungen

Um den größtmöglichen Nutzen aus den Veröffentlichungen zu ziehen, empfehlen wir, eine aktuelle Version von Qt zu verwenden, etwa 4.8.4. Dies ist nötig, um eine stabile und performante Erfahrung zu bieten, weil einige KDE-Verbesserungen in Wahrheit am darunterliegenden Qt-Framework ansetzen.<br />Um den vollen Umfang an Fähigkeiten der KDE-Software zu nutzen, empfehlen wir außerdem, den neuesten Grafiktreiber für Ihr System zu verwenden, weil dies den Nutzungseindruck maßgeblich verbessert, sowohl bei wählbaren Funktionen als auch bei der allgemeinen Leistung und Stabilität.

## Heute ebenfalls angekündigt:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> Die KDE-Anwendungen 4.11 bringen erhebliche Verbesserungen in der Verwaltung persönlicher Daten und allgemeine Verbesserungen</a>

Diese Veröffentlichung bringt enorme Verbesserungen für die KDE-PIM-Anwendungen mit verbesserter Leistung und vielen neuen Funktionen. Für Kate gibt es verbesserte Produktivität für Python- und JavaScript-Entwicklern mit neuen Erweiterungen. Die Geschwindigkeit von Dolphin wurde gesteigert und für die Lernprogramme gibt es verschiedene neue Funktionen.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> Die KDE-Plattform 4.11 liefert eine bessere Leistung</a>

Diese Veröffentlichung der KDE-Plattform 4.11 legt weiterhin großen Wert auf Stabilität. Neue Funktionen werden für unsere zukünftigen Veröffentlichung der KDE-Frameworks 5.0 implementiert, aber für diese stabile Version wurde das Nepomuk-Framework optimiert.
