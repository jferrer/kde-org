---
date: 2013-08-14
hidden: true
title: Die KDE-Anwendungen 4.11 bringen erhebliche Verbesserungen in der Verwaltung
  persönlicher Daten und allgemeine Verbesserungen
---
Für die Dateiverwaltung Dolphin gibt es viele kleinere Fehlerkorrekturen und Optimierungen in dieser Veröffentlichung. Das Laden von Ordnern mit vielen Einträgen wurde beschleunigt und benötigt bis zu 30&#37; weniger Arbeitsspeicher. Die Auslastung der Zugriffe auf die Festplatte und den Arbeitsspeicher wurde verringert, indem nur die Vorschaudaten um die sichtbaren Einträge in einer Ansicht des Ordners geladen werden. Es gibt viele Verbesserungen, zum Beispiel bei Fehlern in der Anzeige von ausgeklappten Ordnern in der Detailansicht, es werden keine Platzhalter für &quot;unbekannte&quot; Symbole beim Öffnen eines Ordners mehr angezeigt. Mit Klicken der rechten Maustaste auf ein Archiv wird jetzt ein neues Unterfenster mit dem Inhalt des Archivs geöffnet.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Der Arbeitsablauf für das spätere Senden in Kontact` width="600px">}}

## Verbesserungen für das Kontact-Paket

Beim Kontact-Paket wurde der Fokus wieder stark auf Stabilität, Leistung und Speicherbedarf gelegt. Das Importieren von Ordnern, Wechseln zwischen verschiedenen Maps, Abholen von E-Mails,Markieren und Verschieben einer großen Menge von Nachrichten und die Ladezeit wurden in den letzten 6 Monaten verbessert. <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>Dieser Blog</a> liefert mehr Details. Bei der<a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>Archivfunktion wurden viele Fehler beseitigt</a> und es gab Verbesserungen beim Import-Assistenten, der das Importieren von Einstellungen des E-Mail-Programms Trojitá unterstützt und besser von diversen anderen Programmen importiert. Mehr Informationen dazu gibt es <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>hier</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kmail-archive-agent.png" caption=`Der Agent zur E-Mail-Archivierung speichert E-Mails in komprimierten Formaten` width="600px">}}

Diese Veröffentlichung enthält auch viele bedeutsame neue Funktionen. Es gibt einen <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>neuen Design-Editor für E-Mail-Vorspannzeilen</a> und Bilder können direkt in der E-Mail in der Größe geändert werden. Mit der Funktion <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>später senden</a> kann das Verschicken von Nachrichten für ein bestimmtes Datum geplant werden einschließlich wiederholtem Senden nach einem gegebenen Intervall. Die Unterstützung für „Sieve“-Filter in KMail (eine IMAP-Funktion, mit der auf dem Server gefiltert werden kann) wurde verbessert, Benutzer können Filterskripte <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>in einer einfachen Benutzeroberfläche</a> erzeugen. Im Bereich der Sicherheit bietet KMail jetzt automatische Erkennung von Betrugs-E-Mails und <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>zeigt eine Warnmeldung</a>, wenn eine E-Mail typische Betrugsmerkmale aufweisen. Sie erhalten ab jetzt eine <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>aussagekräfte Benachrichtigung</a> beim Eintreffen neuer E-Mails. Zu guter Letzt verfügt der Blog-Editor Blogilo über einen deutlich verbesserten HTML-Editor auf Basis von QtWebKit.

## Erweiterte Sprachunterstützung für Kate

Der erweiterte Texteditor Kate hat zwei neue Erweiterungen: Python (2 und 3), JavaScript & JQuery, Django und XML. Sie bringen Funktionen wie z.B. statische und dynamische Autovervollständigung, Syntaxüberprüfung, Einfügen von Quelltextbausteinen und die Fähigkeit, mit Hilfe eines Kurzbefehls XML automatisch einzurücken. Es gibt noch mehr für Python-Freunde: eine Python-Konsole, die Details über geöffnete Dateien anzeigt. Es gab weitere einige kleine Verbesserungen in der Oberfläche, etwa <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>neue passive Benachrichtigungen in der Suche</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>Optimierungen des Vim-Modus</a> und <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>neue Funktionen zur Quelltextausblendung</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kstars.png" caption=`KStars zeigt interessante zukünftige Ereignisse, die von Ihrem Standort sichtbar sind` width="600px">}}

## Verbesserungen bei anderen Anwendungen

Im Bereich der Spiele und Lernprogramme sind einige kleine und große Änderungen und Optimierungen hinzugekommen. Zukünftige Zehn-Finger-Schreiber dürften sich über die Unterstützung für Rechts-nach-links in KTouch freuen und für die Sternengucker bietet KStars ein neues Werkzeug das interessante zukünftige Ereignisse in Ihrer Umgebung anzeigt. Die Mathematik-Programme Rocs, Kig, Cantor und KAlgebra wurden ebenfalls bedacht und unterstützen mehr Backends und Rechenoperationen. Außerdem gibt es im Spiel KJumpingCube jetzt größere Spielfelder, neue Schwierigkeitsstufen, kürzere Reaktionszeiten und eine verbesserte Benutzeroberfläche.

Das Zeichenprogramm Kolourpaint kann Bilder im Format WebP bearbeiten und der Dokumentbetrachter Okular für alle Formate hat jetzt Werkzeuge für Anmerkungen, für die jetzt eigene Voreinstellungen festgelegt werden können. Weiterhin gibt es Unterstützung für Zurücknehmen und Wiederherstellen für Formulare und Anmerkungen. Das Programm Juk unterstützt die Wiedergabe und Bearbeitung von Metadaten im neuen Audioformat „Ogg Opus“. Dazu müssen sowohl der Audiotreiber wie auch die Bibliothek TagLib ebenfalls „Ogg Opus“ unterstützen.

#### KDE-Programme installieren

KDE-Software einschließlich all ihrer Bibliotheken und Anwendungen ist kostenlos unter Open-Source-Lizenzen erhältlich. KDE-Software läuft auf verschiedenen Betriebssystemen, Hardware-Konfigurationen und Prozessorarchitekturen wie z.B. ARM und x86, und es arbeitet mit jeder Art von Fensterverwaltung und Arbeitsumgebung zusammen. Neben Linux und anderen UNIX-basierten Betriebssystemen sind von den meisten KDE-Anwendungen auch Versionen für Microsoft Windows und Apple Mac OS X erhältlich. Sie sind auf den Seiten <a href='http://windows.kde.org'>KDE-Software für Windows</a> bzw. <a href='http://mac.kde.org/'>KDE-Software für den Mac</a> erhältlich. Experimentelle Versionen von KDE-Anwendungen für verschiedene Mobilplattformen wie MeeGo, MS Windows Mobile und Symbian sind im Internet erhältlich, werden zur Zeit aber nicht unterstützt. <a href='http://plasma-active.org'>Plasma Active</a> ist eine Benutzerumgebung für ein großes Spektrum an Geräten wie beispielsweise Tabletts und andere mobile Hardware.

Die KDE-Software kann als Quelltext und in verschiedenen binären Formaten von <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> heruntergeladen und auch auf <a href='/download'>CD-ROM</a> und von vielen großen <a href='http://download.kde.org/stable/%1'>GNU/Linux- und UNIX-Systemen</a> bezogen werden.

##### Pakete

Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete von 4.11.0für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft, <br />

##### Paketquellen

Eine aktuelle Liste aller Binärpakete, von denen das Release-Team in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community-Wiki</a>.

Der vollständige Quelltext für 4.11.0 kann <a href='/info/4/4.11.0'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von %1 finden Sie auf der <a href='/info/4/4.11.0#binary'>Infoseite zu 4.11.0</a>.

#### Systemanforderungen

Um den größtmöglichen Nutzen aus den Veröffentlichungen zu ziehen, empfehlen wir, eine aktuelle Version von Qt zu verwenden, etwa 4.8.4. Dies ist nötig, um eine stabile und performante Erfahrung zu bieten, weil einige KDE-Verbesserungen in Wahrheit am darunterliegenden Qt-Framework ansetzen.<br />Um den vollen Umfang an Fähigkeiten der KDE-Software zu nutzen, empfehlen wir außerdem, den neuesten Grafiktreiber für Ihr System zu verwenden, weil dies den Nutzungseindruck maßgeblich verbessert, sowohl bei wählbaren Funktionen als auch bei der allgemeinen Leistung und Stabilität.

## Heute ebenfalls angekündigt:

## <a href="../plasma"><img src="/announcements/4/4.11.0/images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.11" width="64" height="64" />Plasma-Arbeitsbereich 4.11 verbessert weiter die Benutzererfahrung </a>

Mit dem Ziel der langfristigen Unterstützung im Auge wurde der Plasma-Arbeitsbereich in grundlegenden Funktionen weiter verbessert. So arbeitet die Fensterleiste flüssiger, die Batterieverwaltung wurde intelligenter und der Mixer erhielt Verbesserungen. Durch die Einführung von KScreen erhielt die Arbeitsfläche eine intelligente Verwaltung für Mehrbildschirmbetrieb. Insgesamt wurde die Nutzererfahrung sowohl durch große Leistungsverbesserungen als auch durch kleine Änderungen an der Nutzbarkeit verbessert,

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> Die KDE-Plattform 4.11 liefert eine bessere Leistung</a>

Diese Veröffentlichung der KDE-Plattform 4.11 legt weiterhin großen Wert auf Stabilität. Neue Funktionen werden für unsere zukünftigen Veröffentlichung der KDE-Frameworks 5.0 implementiert, aber für diese stabile Version wurde das Nepomuk-Framework optimiert.
