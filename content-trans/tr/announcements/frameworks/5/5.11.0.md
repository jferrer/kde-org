---
aliases:
- ../../kde-frameworks-5.11.0
date: 2015-06-12
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Ekstra CMake Modülleri

- Ecm_add_tests () için yeni argümanlar. (hata 345797)

### Framework Integration

- Use the correct initialDirectory for the KDirSelectDialog
- Make sure the scheme is specified when overriding the start url value
- Only accept existing directories in FileMode::Directory mode

### KActivities

(no changelog provided)

### KAuth

- Make KAUTH_HELPER_INSTALL_ABSOLUTE_DIR available to all KAuth users

### KCodecs

- KEmailAddress: Add overload for extractEmailAddress and firstEmailAddress which returns an error message.

### KCompletion

- Dosya iletişim kutusunda dosya adını düzenlerken istenmeyen seçimi düzeltin (hata344525)

### KConfig

- Prevent crash if QWindow::screen() is null
- Add KConfigGui::setSessionConfig() (bug 346768)

### KCoreAddons

- New KPluginLoader::findPluginById() convenience API

### KDeclarative

- support creation of ConfigModule from KPluginMetdata
- fix pressAndhold events

### KDELibs 4 Desteği

- Use QTemporaryFile instead of hardcoding a temporary file.

### KDocTools

- Çevirileri güncelleyin
- Update customization/ru
- Fix entities with wrong links

### KEmoticons

- Temayı entegrasyon eklentisinde önbelleğe alın

### KGlobalAccel

- [runtime] Move platform specific code into plugins

### KIconThemes

- Optimize KIconEngine::availableSizes()

### KIO

- Do not try to complete users and assert when prepend is non-empty. (bug 346920)
- Use KPluginLoader::factory() when loading KIO::DndPopupMenuPlugin
- Ağ proxy'lerini kullanırken kilitlenmeyi düzeltin (hata 346214)
- Fixed KIO::suggestName to preserve file extensions
- Kick off kbuildsycoca4 when updating sycoca5.
- KFileWidget: Don't accept files in directory only mode
- KIO::AccessManager: Make it possible to treat sequential QIODevice asynchronously

### KNewStuff

- Add new method fillMenuFromGroupingNames
- KMoreTools: birçok yeni grup ekle
- KMoreToolsMenuFactory: handling for "git-clients-and-actions"
- createMenuFromGroupingNames: make url parameter optional

### KNotification

- Fix crash in NotifyByExecute when no widget has been set (bug 348510)
- Kapatılan bildirimlerin işlenmesini iyileştirin (hata 342752)
- Replace QDesktopWidget usage with QScreen
- KNotification'ın GUI olmayan bir iş parçacığından kullanılabileceğinden emin olun

### Package Framework

- Qpointer erişim yapısını koruyun (bug 347231)

### KPeople

- Use QTemporaryFile instead of hardcoding /tmp.

### KPty

- Use tcgetattr &amp; tcsetattr if available

### Kross

- Fix loading of Kross modules "forms" and "kdetranslation"

### KService

- When running as root preserve file ownership on existing cache files (bug 342438)
- Akışı açamamaya karşı koruma (hata 342438)
- Fix check for invalid permissions writing file (bug 342438)
- Fix querying ksycoca for x-scheme-handler/* pseudo-mimetypes. (bug 347353)

### KTextEditor

- Allow like in KDE 4.x times 3rdparty apps/plugins to install own highlighting XML files into katepart5/syntax
- Add KTextEditor::Document::searchText()
- Bring back use of KEncodingFileDialog (bug 343255)

### KTextWidgets

- Dekoratörü temizlemek için bir yöntem ekleyin
- Özel sonnet dekoratörünün kullanımına izin ver
- Implement "find previous" in KTextEdit.
- Re-add support for speech-to-text

### KWidgetsAddons

- KAssistantDialog: Re-add the Help button that was present in KDELibs4 version

### KXMLGUI

- KMainWindow için oturum yönetimi ekleyin (bug 346768)

### NetworkManagerQt

- NM 1.2.0+ için WiMAX desteğini bırakın

### Plasma Framework

- Takvim bileşenleri artık hafta numaralarını görüntüleyebilir (hata 338195)
- Parola alanlarındaki yazıtipleri için QtRendering kullan
- Fix AssociatedApplicationManager lookup when a mimetype has (bug 340326)
- Panel arka plan rengini düzeltin (bug 347143)
- Get rid of "Could not load applet" message
- Capability to load QML kcms in plasmoid config windows
- Don't use the DataEngineStructure for Applets
- Port libplasma away from sycoca as much as possible
- [plasmacomponents] Make SectionScroller follow the ListView.section.criteria
- Kaydırma çubukları artık bir dokunmatik ekran mevcut olduğunda otomatik olarak gizlenmiyor (hata347254)

### Sonnet

- SpellerPlugins için bir merkezi önbellek kullanın.
- Reduce temporary allocations.
- Optimize: Do not wipe dict cache when copying speller objects.
- Optimise away save() calls by calling it once at the end if needed.

Bu sürüm hakkında fikir ve önerilerinizi <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot makalesinde</a> yorum olarak ekleyebilirsiniz.
