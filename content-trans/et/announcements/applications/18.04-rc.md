---
aliases:
- ../announce-applications-18.04-rc
date: 2018-04-06
description: KDE Ships Applications 18.04 Release Candidate.
layout: application
release: applications-18.03.90
title: KDE toob välja rakenduste 18.04 väljalaskekandidaadi
---
6. aprill 2018. KDE laskis täna välja rakenduste uute versioonide väljalaskekandidaadi. Kuna sõltuvused ja omadused on külmutatud, on KDE meeskonnad keskendunud vigade parandamisele ja tarkvara viimistlemisele.

Check the <a href='https://community.kde.org/Applications/18.04_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release

KDE rakendused 18.04 vajavad põhjalikku testimist kvaliteedi ja kasutajakogemuse tagamiseks ja parandamiseks. Kasutajatel on tihtipeale õigus suhtuda kriitiliselt KDE taotlusse hoida kõrget kvaliteeti, sest arendajad pole lihtsalt võimelised järele proovima kõiki võimalikke kombinatsioone. Me loodame oma kasutajate peale, kes oleksid suutelised varakult vigu üles leidma, et me võiksime need enne lõplikku väljalaset ära parandada. Niisiis - palun kaaluge mõtet ühineda meeskonnaga väljalaskekandidaati paigaldades <a href='https://bugs.kde.org/'>ja kõigist ette tulevatest vigadest teada andes</a>.
