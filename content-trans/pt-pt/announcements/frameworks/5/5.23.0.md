---
aliases:
- ../../kde-frameworks-5.23.0
date: 2016-06-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Possibilidade de indicação aos fornecedores do URL que foi passado
- Fornecimento de utilitários do QDebug para algumas classes do Attica
- Correcção do direccionamento dos URL's absolutos (erro 354748)

### Baloo

- Correcção do uso de espaços no 'kioslave' 'tags' (erro 349118)

### Ícones do Brisa

- Adição de uma opção do CMake para criar um recurso binário do Qt a partir de uma pasta de ícones
- Muitos ícones novos e actualizados
- actualização do ícone de rede desligada para uma maior diferença face à versão ligada (erro 353369)
- actualização do ícone de montagem e desmontagem (erro 358925)
- adição de alguns 'avatars' do 'plasma-desktop/kcms/useraccount/pics/sources'
- remoção do ícone do Chromium, dado que o ícone predefinido se integra bem (erro 363595)
- tornar os ícones do Konsole mais claros (erro 355697)
- adição de ícones de correio para o Thunderbird (erro 357334)
- adição do ícone de chave pública (erro 361366)
- remoção do 'process-working-kde' porque devem ser usados os ícones do Konqueror (erro 360304)
- actualização dos ícones do Krusader de acordo com o erro 359863
- mudança dos ícones do microfone de acordo com o D1291 (erro D1291)
- adição de alguns ícones de tipos MIME de programação (erro 363040)
- adição da funcionalidade de activação/desactivação do teclado virtual e rato por toque no OSD

### Integração da Plataforma

- Remoção de dependências não usadas e tratamento das traduções

### KActivities

- Adição da propriedade 'runningActivities' ao objecto Consumer

### Ferramentas de Doxygen do KDE

- Grande remodelação da geração da documentação da API

### KCMUtils

- Uso do QQuickWidget para os KCM's em QML (erro 359124)

### KConfig

- Evitar ignorar a verificação do KAuthorized

### KConfigWidgets

- Permitir o uso do novo estilo de sintaxe do 'connect' com o KStandardAction::create()

### KCoreAddons

- Impressão do 'plugin' com problemas quando notificar um aviso de conversão
- [kshareddatacache] Correcção do uso inválido do &amp; para evitar leituras não alinhadas
- Kdelibs4ConfigMigrator: evitar o novo processamento caso nada tenha sido migrado
- krandom: Adição de caso de testes para detectar o erro 362161 (incapacidade de valor de base automático)

### KCrash

- Verificação do tamanho do local do 'socket' de domínio UNIX antes de copiar para o mesmo

### KDeclarative

- Suporte para o estado seleccionado
- A importação do KCMShell pode agora ser usada para pesquisar se a abertura de um KCM é de facto permitida

### Suporte para a KDELibs 4

- Aviso sobre a não-implementação do KDateTimeParser::parseDateUnicode
- K4TimeZoneWidget: correcção da localização das imagens das bandeiras

### KDocTools

- Adição de entidades usadas geralmente como chaves do 'en/user.entities'
- Actualização do modelo Docbook das páginas de manual
- Actualização dos modelos de livros + páginas do 'man' + artigos
- Invocação do kdoctools_create_handbook apenas para o index.docbook (erro 357428)

### KEmoticons

- Adição do suporte de 'emojis' para os ícones do KEmoticon + Emoji One
- Adição do suporte para tamanhos de ícones personalizados

### KHTML

- Correcção de uma potencial fuga de memória comunicada pelo Coverity e simplificação do código
- O número de camadas é definido pelo número de valores separados por vírgulas na propriedade ‘background-image’
- Correcção do processamento do 'background-position' na declaração curta
- Não criar uma nova 'fontFace' se não existir uma origem válida

### KIconThemes

- Não fazer os KIconThemes depender do Oxygen (erro 360664)
- Conceito do estado seleccionado para os ícones
- Uso das cores do sistema nos ícones monocromáticos

### KInit

- Correcção de problema de concorrência no qual o ficheiro que contém o 'cookie' do X11 tem as permissões inválidas durante um breve período
- Correcção das permissões do /tmp/xauth-xxx-_y

### KIO

- Mensagem de erro mais esclarecedora quando o KRun(URL) recebe um URL sem esquema (erro 363337)
- Adição do KProtocolInfo::archiveMimetypes()
- uso do modo seleccionado dos ícones na barra lateral da janela de abertura de ficheiros
- kshorturifilter: correcção de regressão do 'mailto:' não ser adicionado quando não está instalado nenhum programa de envio de e-mail

### KJobWidgets

- Definição da opção correcta "dialog" para a janela de Progresso da Operação

### KNewStuff

- Não inicializar o KNS3::DownloadManager com as categorias erradas
- Alargamento da API pública do KNS3::Entry

### KNotification

- Uso do QUrl::fromUserInput para construir um URL de som (erro 337276)

### KNotifyConfig

- Uso do QUrl::fromUserInput para construir um URL de som (erro 337276)

### KService

- Correcção das aplicações associadas para tipos MIME com caracteres maiúsculos
- Conversão da chave de pesquisa de tipos MIME para minúsculas, para que se torne independente da capitalização
- Correcção das notificações do ksycoca quando a BD ainda não existe

### KTextEditor

- Mudança da codificação por omissão para UTF-8 (erro 362604)
- Correcção da configuração das cores no estilo predefinido "Erro"
- Pesquisa &amp; Substituição: Correcção da cor de fundo da substituição (regressão introduzida na versão v5.22) (erro 363441)
- Novo esquema de cores "Brisa Escuro", ver em https://kate-editor.org/?post=3745
- KateUndoManager::setUndoRedoCursorOfLastGroup(): passagem do Cursor como referência constante
- sql-postgresql.xml: melhoria do realce de sintaxe, ignorando os conteúdos de funções em várias linhas
- Adição do realce de sintaxe para o Elixir e o Kotlin
- Realce de sintaxe VHDL no ktexteditor: adição do suporte para as funções dentro de instruções 'architecture'
- vimode: Não estoirar quando for dado um intervalo para um comando inexistente (erro 360418)
- Remoção correcta dos caracteres compostos ao usar línguas do Índico

### KUnitConversion

- Correcção da transferência de taxas de câmbio (erro 345750)

### Plataforma da KWallet

- Migração do KWalletd: correcção do tratamento de erros, que impede a ocorrência da migração em cada novo arranque do sistema.

### KWayland

- [cliente] Não verificar a versão do recurso do PlasmaWindow
- Introdução de um evento de estado inicial no protocolo de janelas do Plasma
- [servidor] Emissão de um erro se um pedido transitório tentar usar-se a si próprio como pedido-pai
- [servidor] Tratamento adequado do caso em que uma janela PlasmaWindow desaparece antes de o cliente a associar
- [servidor] Tratamento adequado do destrutor da SlideInterface
- Adição do suporte para os eventos de toque no protocolo e na interface 'fakeinput'
- [servidor] Normalização do tratamento de pedidos do destrutor do Resources
- Implementação das interfaces 'wl_text_input' e 'zwp_text_input_v2'
- [servidor] Evitar uma remoção dupla dos recursos de chamadas de retorno na SurfaceInterface
- [servidor] Adição de verificação de ponteiros nulos dos recursos na ShellSurfaceInterface
- [servidor] Comparação do ClientConnection em vez do 'wl_client' na SeatInterface
- [servidor] Melhoria do tratamento do encerramento de ligações dos clientes
- server/plasmawindowmanagement_interface.cpp - correcção de aviso -Wreorder
- [cliente] Adição de um ponteiro de contexto nas ligações ao PlasmaWindowModel
- Diversas correcções relacionadas com a destruição de instâncias

### KWidgetsAddons

- Uso do efeito do ícone seleccionado para a página actual do KPageView

### KWindowSystem

- [XCB da plataforma] Respeito do tamanho do ícone pedido (erro 362324)

### KXMLGUI

- O carregar do botão direito do menu de uma aplicação não irá mais permitir passar ao lado

### NetworkManagerQt

- Reposição da "Eliminação do suporte de WiMAX para o NM 1.2.0+", dado quebrar a ABI

### Ícones do Oxygen

- Sincronização dos ícones de meteorologia com o Brisa
- Adição de ícones de actualização

### Plataforma do Plasma

- Adição do suporte para a bandeja do sistema Cantata (erro 363784)
- Estado seleccionado do Plasma::Svg e do IconItem
- DaysModel: limpeza do 'm_agendaNeedsUpdate' quando o 'plugin' enviar novos eventos
- Actualização do ícone de áudio e da rede para obter um melhor contraste (erro 356082)
- Descontinuação do 'downloadPath(const QString &amp;ficheiro)' em detrimento do 'downloadPath()'
- [miniaturas de ícones] Pedido do tamanho de ícones preferido (erro 362324)
- Os Plasmóides podem agora indicar se os elementos estão bloqueados por restrições do utilizador ou do administrador do sistema
- [ContainmentInterface] Não tentar mostrar um QMenu vazio
- Uso do SAX na substituição da folha de estilo do Plasma::Svg
- [DialogShadows] Acesso da 'cache' ao QX11Info::display()
- reposição dos ícones do tema Air do Plasma do KDE4
- Recarregamento do esquema de cores seleccionado em caso de mudança de cores

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
