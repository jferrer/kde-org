---
aliases:
- ../announce-applications-19.08.3
changelog: true
date: 2019-11-07
description: O KDE Lança as Aplicações do KDE 19.08.
layout: application
major_version: '19.08'
release: applications-19.08.3
title: O KDE Lança as Aplicações do KDE 19.08.3
version: 19.08.3
---
{{% i18n_date %}}

Hoje o KDE lançou a terceira actualização de estabilidade para as <a href='../19.08.0'>Aplicações do KDE 19.08</a>. Esta versão contém apenas correcções de erros e actualizações de traduções, pelo que será uma actualização segura e agradável para todos.

A dúzia de correcções de erros registadas inclui melhorias no Kontact, no Ark, no Cantor, no K3b, no Kdenlive, no Konsole, no Okular, no Spectacle, no Umbrello, entre outros.

As melhorias incluem:

- No editor de vídeo Kdenlive, as composições já não desaparecem mais ao reabrir um projecto com as faixas bloqueadas
- A área de anotações do Okular agora mostra as horas de criação no fuso-horário local em vez de ser em UTC
- O controlo do teclado foi melhorado no utilitário de capturas de ecrã Spectacle
