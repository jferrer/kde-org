---
version: "5.7.1"
title: "KDE Plasma 5.7.1, Bugfix Release"
errata:
    link: https://community.kde.org/Plasma/5.7_Errata
    name: 5.7 Errata
type: info/plasma5
signer: Jonathan Riddell
signing_fingerprint: 506e6d4dd4d5088
---

This is a Bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer.  Details in the
[Plasma 5.7.1 announcement](/announcements/plasma-5.7.1).

