---
version: "5.1.95"
title: "Plasma 5.1.95 Source Information Page"
errata:
    link: https://community.kde.org/Plasma/5.2_Errata
    name: 5.2 Errata
type: info/plasma5
unstable: true
---

This is a pre-release of Plasma 5.2, featuring Plasma Desktop and
other essential software for your desktop computer.  Like any beta
release it is feature complete but still has bugs we need help to find
and fix.  Details in the <a href="/announcements/plasma-5.1.95">Plasma 5.1.95 announcement</a>.

For an overview of the differences from Plasma 4 read the initial <a
href="/announcements/plasma5.0/">Plasma 5.0 announcement</a>.

It works with Qt 5.3 or Qt 5.4 but has some bugfixes which work only with Qt 5.4.
