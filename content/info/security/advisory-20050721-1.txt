-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1


KDE Security Advisory: libgadu vulnerabilities
Original Release Date: 2005-07-21
URL: http://www.kde.org/info/security/advisory-20050721-1.txt

0. References
        CVE CAN-2005-1852


1. Systems affected:

        All versions of Kopete as included in KDE 3.2.3 up to including
        KDE 3.4.1. KDE 3.2.2 and older are not affected.

        Kopete 0.9.x releases starting with 0.9.4 and Kopete 0.10.3
        or newer are unaffected.


2. Overview:

	Kopete contains a copy of libgadu that is used if
        no compatible version is installed in the system. Several
        input validation errors have been reported in libgadu
        that can lead to integer overflows and remote DoS or
        arbitrary code execution.


3. Impact:

	If the Gadu-Gadu protocol handler in Kopete is used,
        remote users can DoS the Kopete client or possibly even
        execute arbitrary code.


4. Solution:

        Source code patches have been made available that update
        the included copy of libgadu to 1.6rc3 which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        for information about how to obtain updated binary packages.


5. Patch:

        A patch for KDE 3.4.1 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        675008c8bc9d7edf4d0034a398d15cf0  post-3.4.1-kdenetwork-libgadu.patch

        A patch for KDE 3.3.2 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        73ebcef42173bf567d473414693898b0  post-3.3.2-kdenetwork-libgadu.patch

        A patch for KDE 3.2.3 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        69e3379085aeaeecf034468d18a900f6  post-3.2.3-kdenetwork-libgadu.patch


-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iD8DBQFC3w5pvsXr+iuy1UoRAuAyAKC5MQPmvhpYiOtypx50dk7fkLCxWACgg0Lv
XiS2yq32alcX2bEhEArot+Y=
=FoUx
-----END PGP SIGNATURE-----
