-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: rlogin.protocol and telnet.protocol URL KIO Vulnerability
Original Release Date: 2002-11-11
URL: http://www.kde.org/info/security/advisory-20021111-1.txt

0. References

        None.


1. Systems affected:

        All KDE 2 releases starting with KDE 2.1 and all KDE 3 releases
        (up to 3.0.4 and 3.1rc3).

2. Overview:
        
        KDE provides support for various network protocols via the KIO
	subsystem.  These protocols are implemented with text files
	containing the extension .protocol, normally stored in the
	shared/services/ subdirectory under the KDE installation root.

	The implementation of the rlogin protocol in all of the affected
        systems, and the implementation of the telnet protocol in affected
        KDE 2 systems, allows a carefully crafted URL in an HTML page,
        HTML email or other KIO-enabled application to execute arbitrary
        commands on the system using the victim's account on the
        vulnerable machine.

3. Impact:
        
        The vulnerability potentially enables local or remote attackers
        to compromise a victim's account and execute arbitrary commands
        on the local system with the victim's privileges, such as erasing
        files, accessing data or installing trojans.

4. Solution:
        
        The vulnerability has been fixed in KDE 3.0.5 and a patch is
        available for KDE 3.0.4.  For affected KDE 3 systems, we recommend
        upgrading to KDE 3.0.5, applying the patch provided or disabling
        the rlogin protocol.
        
        For affected KDE 2 systems, we recommend disabling both the rlogin
        and telnet KIO protocols.

        The rlogin protocol vulnerability can be disabled by deleting
        any rlogin.protocol files on the system and restarting the active
        KDE sessions.  The file is usually installed in
        [kdeprefix]/share/services/rlogin.protocol ([kdeprefix] is typically
        /opt/kde3 or /usr), but copies may exist elsewhere, such as in
        users' [kdehome]/share/services directory ([kdehome] is typically
        the .kde directory in a user's home directory).

        The telnet protocol vulnerability can be similary disabled in
        affected KDE 2 systems.
	
        kdelibs-3.0.5 can be downloaded from
        http://download.kde.org/stable/3.0.5/src/kdelibs-3.0.5.tar.bz2 :

        ff22bd58b91ac34e476c308d345421aa  kdelibs-3.0.5.tar.bz2

        Some vendors are building binary packages of kdelibs-3.0.5.
        Please check your vendors website and the KDE 3.0.5 information page
        (/info/1-2-3/3.0.5) periodically for availability.

5. Patch:

	Patches are available for KDE 3.0.x from the KDE FTP server
        (ftp://ftp.kde.org/pub/kde/security_patches/):

        5625501819f09510d542142aea7b85ab  post-3.0.4-kdelibs-kio-misc.diff
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.6 (GNU/Linux)
Comment: For info see http://www.gnupg.org

iD8DBQE90NkxlPwzJhSeGawRAv+ZAKCfceTFygY6elBJulf2OY/7dVDtcgCgnGvN
on3ElIqHK8623di5+AEG5Is=
=r3+S
-----END PGP SIGNATURE-----
