
KOffice Security Advisory: KOffice olefilters integer overflow
Original Release Date: 2006-12-05
URL: http://www.kde.org/info/security/advisory-20061205-1.txt

0. References

        CVE-2006-6120

1. Systems affected:

        KOffice 1.4.x and 1.6.0. 1.5.x releases are unaffected as well
        as 1.6.1 or newer.

2. Overview:

        The OLE import filter, which is used in KPresenter to open Microsoft
        Powerpoint files is vulnerable to an integer overflow problem that
        can be exploited to expose an heap memory overflow.  This
        issue was reported by Kees Cook from Ubuntu security.

3. Impact:

        A maliciously crafted file can cause to execute arbitrary code.

4. Solution:

        Source code patches have been made available which fix these
        vulnerabilities. Contact your OS vendor / binary package provider
        fo information about how to obtain updated binary packages.


5. Patch:

        A patch for KOffice 1.4.0 - KOffice 1.6.0 is available from
        ftp://ftp.kde.org/pub/kde/security_patches :

        20dff20ccd2e184f1874aa60d85f4380  post-koffice-1.6.0.diff


