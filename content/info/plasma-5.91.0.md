---
version: "5.91.0"
title: "KDE Plasma 5.91.0, beta Release"
type: info/plasma5
signer: Jonathan Esk-Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
draft: false
---

This is a beta release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.91.0 announcement](/announcements/plasma/6/5.91.0).
