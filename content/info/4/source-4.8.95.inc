<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">SHA1 Sum</th>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/analitza-4.8.95.tar.xz">analitza-4.8.95</a></td>
   <td align="right">138kB</td>
   <td><tt>21666a0120ec71d2096970440e46f1516f19ea62</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ark-4.8.95.tar.xz">ark-4.8.95</a></td>
   <td align="right">149kB</td>
   <td><tt>9b08a101409335ecfe99e1621553d7e6b0ef0336</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/audiocd-kio-4.8.95.tar.xz">audiocd-kio-4.8.95</a></td>
   <td align="right">53kB</td>
   <td><tt>7178d74a90a9257e5386f6f9eba4e5b675296e48</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/blinken-4.8.95.tar.xz">blinken-4.8.95</a></td>
   <td align="right">551kB</td>
   <td><tt>c55c39d8659431d96e96be288b08abe03c35ff19</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/cantor-4.8.95.tar.xz">cantor-4.8.95</a></td>
   <td align="right">257kB</td>
   <td><tt>6931d2027caaaff77f32658b2c033f8123e1c994</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/dragon-4.8.95.tar.xz">dragon-4.8.95</a></td>
   <td align="right">381kB</td>
   <td><tt>c0b7befb13112114e463d4fa6c2fc1953ef9235e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ffmpegthumbs-4.8.95.tar.xz">ffmpegthumbs-4.8.95</a></td>
   <td align="right">19kB</td>
   <td><tt>e4546523cd1335833b47d9cedb479fa8a9296fe8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/filelight-4.8.95.tar.xz">filelight-4.8.95</a></td>
   <td align="right">286kB</td>
   <td><tt>d572a5f64650ad8be09ab032aace0943fbdfcb2f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/gwenview-4.8.95.tar.xz">gwenview-4.8.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>253b4d7aa3daa46bb49bcdb3308cecf4bd45c073</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/jovie-4.8.95.tar.xz">jovie-4.8.95</a></td>
   <td align="right">357kB</td>
   <td><tt>c979886f9c240de02c1906bb0eed85e94ef20cf4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/juk-4.8.95.tar.xz">juk-4.8.95</a></td>
   <td align="right">429kB</td>
   <td><tt>94cdd8493f7751d2f69729aa735e68bc9b06be1f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kaccessible-4.8.95.tar.xz">kaccessible-4.8.95</a></td>
   <td align="right">19kB</td>
   <td><tt>d213142697ba9c8e7d062e4fd479d46ba0d98d90</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kactivities-4.8.95.tar.xz">kactivities-4.8.95</a></td>
   <td align="right">81kB</td>
   <td><tt>681157b54742cfccc5b77a71054dbe397863304b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kalgebra-4.8.95.tar.xz">kalgebra-4.8.95</a></td>
   <td align="right">428kB</td>
   <td><tt>da21d2d35f9e46871527d4abc22aa78a0f275863</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kalzium-4.8.95.tar.xz">kalzium-4.8.95</a></td>
   <td align="right">3.5MB</td>
   <td><tt>dae1ce57f929a545dce1064bcdd11404e25ac688</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kamera-4.8.95.tar.xz">kamera-4.8.95</a></td>
   <td align="right">35kB</td>
   <td><tt>d1ff14f499952b17b8a6fdeb6976329d7ec6b368</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kanagram-4.8.95.tar.xz">kanagram-4.8.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>afc181dcb6684420d028661b0885a23947475854</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kate-4.8.95.tar.xz">kate-4.8.95</a></td>
   <td align="right">2.0MB</td>
   <td><tt>14dae5c5bf0b9a32a0be27c2b9e1e5f34fec8d43</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kbruch-4.8.95.tar.xz">kbruch-4.8.95</a></td>
   <td align="right">883kB</td>
   <td><tt>29901160e135b6c970ab8ef745307a14182d560c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kcalc-4.8.95.tar.xz">kcalc-4.8.95</a></td>
   <td align="right">84kB</td>
   <td><tt>5ce3fcd0150ca63e184bdef8b9ed562c81360811</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kcharselect-4.8.95.tar.xz">kcharselect-4.8.95</a></td>
   <td align="right">83kB</td>
   <td><tt>d0104407fd10629ac43f89de41bb55bd0f97d1d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kcolorchooser-4.8.95.tar.xz">kcolorchooser-4.8.95</a></td>
   <td align="right">4kB</td>
   <td><tt>69cbf6a3308d002b600a27b07929773252e97606</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdeadmin-4.8.95.tar.xz">kdeadmin-4.8.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>f1a1fcb8595c41eb636fb6163de5caf08a089445</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdeartwork-4.8.95.tar.xz">kdeartwork-4.8.95</a></td>
   <td align="right">132MB</td>
   <td><tt>e511aad4e8aef12ed4b660cae11159ade834ad8d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-baseapps-4.8.95.tar.xz">kde-baseapps-4.8.95</a></td>
   <td align="right">2.3MB</td>
   <td><tt>35558eee1acdb796985b245cf3630a41dc7eb145</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-base-artwork-4.8.95.tar.xz">kde-base-artwork-4.8.95</a></td>
   <td align="right">15MB</td>
   <td><tt>97d44b874fd0f2d525123df2b331201437e86cf9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdegames-4.8.95.tar.xz">kdegames-4.8.95</a></td>
   <td align="right">53MB</td>
   <td><tt>0b42423fa219d1592a0590d86c24fc02582aabd7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdegraphics-mobipocket-4.8.95.tar.xz">kdegraphics-mobipocket-4.8.95</a></td>
   <td align="right">18kB</td>
   <td><tt>1c0f7491849097417a61730047a4c6b6a70f1a8c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdegraphics-strigi-analyzer-4.8.95.tar.xz">kdegraphics-strigi-analyzer-4.8.95</a></td>
   <td align="right">38kB</td>
   <td><tt>669af472c013a36bb3d83da280dc9636e105b145</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdegraphics-thumbnailers-4.8.95.tar.xz">kdegraphics-thumbnailers-4.8.95</a></td>
   <td align="right">40kB</td>
   <td><tt>037aeff675ef40e5118421473ac6abaa4833e075</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdelibs-4.8.95.tar.xz">kdelibs-4.8.95</a></td>
   <td align="right">11MB</td>
   <td><tt>dd7cd4f314b0a8723c782df6e33208d7e6d5813a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdenetwork-4.8.95.tar.xz">kdenetwork-4.8.95</a></td>
   <td align="right">8.5MB</td>
   <td><tt>adfddee2dbca233a37c68d132d09f889af956f60</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdepim-4.8.95.tar.xz">kdepim-4.8.95</a></td>
   <td align="right">13MB</td>
   <td><tt>b47cfec970608c516a4620235da8ad825c20c9b1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdepimlibs-4.8.95.tar.xz">kdepimlibs-4.8.95</a></td>
   <td align="right">2.4MB</td>
   <td><tt>224fe4bb1a5eb6f51dd96e70259ee5bfb60d03ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdepim-runtime-4.8.95.tar.xz">kdepim-runtime-4.8.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>dead714849ef22580abcfcf72ccb469517a45715</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdeplasma-addons-4.8.95.tar.xz">kdeplasma-addons-4.8.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>4b8d7f7111e8d77dd60d21b8da634b3d44199724</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-runtime-4.8.95.tar.xz">kde-runtime-4.8.95</a></td>
   <td align="right">6.0MB</td>
   <td><tt>3337f5ae6e2cd18e444a71b32875b7a39097b507</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdesdk-4.8.95.tar.xz">kdesdk-4.8.95</a></td>
   <td align="right">4.8MB</td>
   <td><tt>9c460d25106089bed6a5f2e8d4697422c270cb16</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdetoys-4.8.95.tar.xz">kdetoys-4.8.95</a></td>
   <td align="right">368kB</td>
   <td><tt>d45ac37b5476a421d83d7b24f8c5476ed9cf85c1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-wallpapers-4.8.95.tar.xz">kde-wallpapers-4.8.95</a></td>
   <td align="right">73MB</td>
   <td><tt>3ce19595a68a3b71bfbac7ab9ec20bfd8e3d67bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdewebdev-4.8.95.tar.xz">kdewebdev-4.8.95</a></td>
   <td align="right">2.4MB</td>
   <td><tt>b358eba78851139d2c015fea602aed070568b26a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-workspace-4.8.95.tar.xz">kde-workspace-4.8.95</a></td>
   <td align="right">21MB</td>
   <td><tt>460b8889c225114b5b0ed84a05ea82e851bfd6cf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kdf-4.8.95.tar.xz">kdf-4.8.95</a></td>
   <td align="right">151kB</td>
   <td><tt>f7907f8c85fbf3113c0e38128daf518d345f7986</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kfloppy-4.8.95.tar.xz">kfloppy-4.8.95</a></td>
   <td align="right">58kB</td>
   <td><tt>2f97331fe311960c3320a9f70b8a414ffe92ef60</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kgamma-4.8.95.tar.xz">kgamma-4.8.95</a></td>
   <td align="right">25kB</td>
   <td><tt>a3f3bad239b010b363d961fe7aff2a210bad2102</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kgeography-4.8.95.tar.xz">kgeography-4.8.95</a></td>
   <td align="right">6.4MB</td>
   <td><tt>939ae363cea92721c23a9dfbf4c952fff9a978b8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kgpg-4.8.95.tar.xz">kgpg-4.8.95</a></td>
   <td align="right">789kB</td>
   <td><tt>1c322b93b204ff831f69cb5379ea6ea93dc2df62</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/khangman-4.8.95.tar.xz">khangman-4.8.95</a></td>
   <td align="right">3.6MB</td>
   <td><tt>e6bb2e51cf9ee6333f84207c1a0f41a179faf8c0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kig-4.8.95.tar.xz">kig-4.8.95</a></td>
   <td align="right">1.4MB</td>
   <td><tt>afd895d7a1ca1d9570d7c27ab20f98bf1aea3d57</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kimono-4.8.95.tar.xz">kimono-4.8.95</a></td>
   <td align="right">910kB</td>
   <td><tt>3acb4568a0d66fcc6246a728945bf2eb63fd4db9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kiten-4.8.95.tar.xz">kiten-4.8.95</a></td>
   <td align="right">11MB</td>
   <td><tt>c8a127a407296232fbc27c1d45220f1f1fbe2684</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/klettres-4.8.95.tar.xz">klettres-4.8.95</a></td>
   <td align="right">2.5MB</td>
   <td><tt>4b5799abb659c2fee1e82781a6296841d650d4d6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kmag-4.8.95.tar.xz">kmag-4.8.95</a></td>
   <td align="right">89kB</td>
   <td><tt>8e62bf64dd0499dc5e2bb91623dbd9ea41c8dde1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kmix-4.8.95.tar.xz">kmix-4.8.95</a></td>
   <td align="right">292kB</td>
   <td><tt>ddd0f8723f20c1e847302d4ea69b585abf4b7698</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kmousetool-4.8.95.tar.xz">kmousetool-4.8.95</a></td>
   <td align="right">41kB</td>
   <td><tt>96bfecd0e7359e622bb336d7b20a24446a013379</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kmouth-4.8.95.tar.xz">kmouth-4.8.95</a></td>
   <td align="right">310kB</td>
   <td><tt>5dc66f0b762584b9e595ae84c1bc1e861f9a8c29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kmplot-4.8.95.tar.xz">kmplot-4.8.95</a></td>
   <td align="right">660kB</td>
   <td><tt>19f3075d5b2026c6226cc816ab9880d64ba2e455</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kolourpaint-4.8.95.tar.xz">kolourpaint-4.8.95</a></td>
   <td align="right">1.1MB</td>
   <td><tt>e1d82356f80f50860614a2903391ef5551a9125f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/konsole-4.8.95.tar.xz">konsole-4.8.95</a></td>
   <td align="right">422kB</td>
   <td><tt>1f6d8719fa9c82f3239e3ceb2c1e40f05fc835a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/korundum-4.8.95.tar.xz">korundum-4.8.95</a></td>
   <td align="right">156kB</td>
   <td><tt>10d25abd06c81a4bda3335ced345014fa5cb8e2b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kremotecontrol-4.8.95.tar.xz">kremotecontrol-4.8.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>fa04e1d5f1a54aac9938db3e8c8f386890a2390a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kross-interpreters-4.8.95.tar.xz">kross-interpreters-4.8.95</a></td>
   <td align="right">127kB</td>
   <td><tt>533e2149ca31cb501cc1d9400c42511ed31c3abb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kruler-4.8.95.tar.xz">kruler-4.8.95</a></td>
   <td align="right">129kB</td>
   <td><tt>1b7dc38c4834fda53d984f7a760f9f9f9fc668ac</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ksaneplugin-4.8.95.tar.xz">ksaneplugin-4.8.95</a></td>
   <td align="right">12kB</td>
   <td><tt>c77a2a588901845f4c91b101ad6169f94a45a938</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kscd-4.8.95.tar.xz">kscd-4.8.95</a></td>
   <td align="right">90kB</td>
   <td><tt>a7664847d85aa1b673de207dcf27d899fd8b02c2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ksnapshot-4.8.95.tar.xz">ksnapshot-4.8.95</a></td>
   <td align="right">256kB</td>
   <td><tt>ae27d8f92f8f95f1fe23269922247bdce8c935a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kstars-4.8.95.tar.xz">kstars-4.8.95</a></td>
   <td align="right">11MB</td>
   <td><tt>71b2d1a6e662f36094bc9d65fd896e1d4342e334</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ktimer-4.8.95.tar.xz">ktimer-4.8.95</a></td>
   <td align="right">146kB</td>
   <td><tt>a6db8c773d5cfc2462b859ad8a553cd28755287a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/ktouch-4.8.95.tar.xz">ktouch-4.8.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>8508cb5be8cad22fe78952f57c6c26e3ab6b04e1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kturtle-4.8.95.tar.xz">kturtle-4.8.95</a></td>
   <td align="right">206kB</td>
   <td><tt>c390050248bed85215d07bc4d5c2c27195603fbd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kwallet-4.8.95.tar.xz">kwallet-4.8.95</a></td>
   <td align="right">273kB</td>
   <td><tt>ccc20f1052b7f5c9016f23a8b45a837ddc677335</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kwordquiz-4.8.95.tar.xz">kwordquiz-4.8.95</a></td>
   <td align="right">1.1MB</td>
   <td><tt>73f8815178c06fb7e7e7a4058afe87d41cb5bf37</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkcddb-4.8.95.tar.xz">libkcddb-4.8.95</a></td>
   <td align="right">152kB</td>
   <td><tt>48be5e7df49e830b0908621a1be7a54cd3bc7c3a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkcompactdisc-4.8.95.tar.xz">libkcompactdisc-4.8.95</a></td>
   <td align="right">73kB</td>
   <td><tt>6820663a15ee6a668db3ca11cd678c2ca8525426</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkdcraw-4.8.95.tar.xz">libkdcraw-4.8.95</a></td>
   <td align="right">260kB</td>
   <td><tt>f75867e177fb7dd292d38155afcee651a28832c0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkdeedu-4.8.95.tar.xz">libkdeedu-4.8.95</a></td>
   <td align="right">204kB</td>
   <td><tt>55d37b387f972dc68d5f416c83d4522d03a5bea6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkexiv2-4.8.95.tar.xz">libkexiv2-4.8.95</a></td>
   <td align="right">129kB</td>
   <td><tt>769453952634967ee8fc987b2bec226b2e9c2c66</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libkipi-4.8.95.tar.xz">libkipi-4.8.95</a></td>
   <td align="right">73kB</td>
   <td><tt>b81bb608fad6d906579539b3cdddec72e1727baa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/libksane-4.8.95.tar.xz">libksane-4.8.95</a></td>
   <td align="right">77kB</td>
   <td><tt>25e023081c77cd7ba82c0e8ae449378164aec7b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/marble-4.8.95.tar.xz">marble-4.8.95</a></td>
   <td align="right">18MB</td>
   <td><tt>621fd58567c43f73fdb0ae3b078efa9e8e9eb56e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/mplayerthumbs-4.8.95.tar.xz">mplayerthumbs-4.8.95</a></td>
   <td align="right">26kB</td>
   <td><tt>5d426085810a799b152d4559f162e46cb71d1e53</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/nepomuk-core-4.8.95.tar.xz">nepomuk-core-4.8.95</a></td>
   <td align="right">363kB</td>
   <td><tt>4c1653adff9104afdd9297d735b3b90f8060d3ba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/okular-4.8.95.tar.xz">okular-4.8.95</a></td>
   <td align="right">1.2MB</td>
   <td><tt>5c01107a9ec9a521999e4236283277f814d139de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/oxygen-icons-4.8.95.tar.xz">oxygen-icons-4.8.95</a></td>
   <td align="right">262MB</td>
   <td><tt>6a6a9a778ba3100ce4529ca47ce3d741c0368782</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/pairs-4.8.95.tar.xz">pairs-4.8.95</a></td>
   <td align="right">1.8MB</td>
   <td><tt>e9346f591293266274aeda0c5b9a241a8c1d4c5c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/parley-4.8.95.tar.xz">parley-4.8.95</a></td>
   <td align="right">7.8MB</td>
   <td><tt>08ca3278bfe8a911cb1974fd12837da9a95d6416</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/perlkde-4.8.95.tar.xz">perlkde-4.8.95</a></td>
   <td align="right">39kB</td>
   <td><tt>bd7a711808c14df81e6a3e4d3198a88936a9a086</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/perlqt-4.8.95.tar.xz">perlqt-4.8.95</a></td>
   <td align="right">1.7MB</td>
   <td><tt>4eb3f12bde0b1a0bc1bc07ec9c11fb057e8ce9bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/printer-applet-4.8.95.tar.xz">printer-applet-4.8.95</a></td>
   <td align="right">34kB</td>
   <td><tt>7a3ee1d622c8001c60d88358276361a89cac1bba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/pykde4-4.8.95.tar.xz">pykde4-4.8.95</a></td>
   <td align="right">1.8MB</td>
   <td><tt>e0b674d266f679d9dc36b28bff46767cba97f280</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/qtruby-4.8.95.tar.xz">qtruby-4.8.95</a></td>
   <td align="right">517kB</td>
   <td><tt>2a336acd84308fbbb54c8d63da54c136edc7c8a7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/qyoto-4.8.95.tar.xz">qyoto-4.8.95</a></td>
   <td align="right">495kB</td>
   <td><tt>6760d580bc3fd8ee3dff377f1b72aae2d93a6a77</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/rocs-4.8.95.tar.xz">rocs-4.8.95</a></td>
   <td align="right">1.1MB</td>
   <td><tt>051b1b0c3bf0a844248d157b44a1a5fa0e1b41a6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/smokegen-4.8.95.tar.xz">smokegen-4.8.95</a></td>
   <td align="right">142kB</td>
   <td><tt>e0d26ff3d58848b01800779d4777aca86c10d31d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/smokekde-4.8.95.tar.xz">smokekde-4.8.95</a></td>
   <td align="right">35kB</td>
   <td><tt>0bee4ca382834d1d4a88c14a6a810c1f974155f6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/smokeqt-4.8.95.tar.xz">smokeqt-4.8.95</a></td>
   <td align="right">29kB</td>
   <td><tt>3a94b897c74b99be55050ffeff53b1354fcc400e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/step-4.8.95.tar.xz">step-4.8.95</a></td>
   <td align="right">361kB</td>
   <td><tt>56782292a440f6dcfda9364ac994646f8f38f5d5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/superkaramba-4.8.95.tar.xz">superkaramba-4.8.95</a></td>
   <td align="right">374kB</td>
   <td><tt>01bf61bfaae69590cc77b650ea9640db883b8acc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/svgpart-4.8.95.tar.xz">svgpart-4.8.95</a></td>
   <td align="right">8kB</td>
   <td><tt>b746419776ee3c5dbd956de86bb6417675771114</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/sweeper-4.8.95.tar.xz">sweeper-4.8.95</a></td>
   <td align="right">81kB</td>
   <td><tt>2f56dd4dd9a13936e1c658dd0021ce0d7ff207db</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ar-4.8.95.tar.xz">kde-l10n-ar-4.8.95</a></td>
   <td align="right">3.2MB</td>
   <td><tt>16e6414c0af5a0b5c0ce5edc76fa4b83b743b7f5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-bg-4.8.95.tar.xz">kde-l10n-bg-4.8.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>f844cfea0a18cfde2bd5a3e26d92d1e1e75beced</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-bs-4.8.95.tar.xz">kde-l10n-bs-4.8.95</a></td>
   <td align="right">2.2MB</td>
   <td><tt>9123162807fc2ad60490cdca9f02ed8cfcd71f98</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ca-4.8.95.tar.xz">kde-l10n-ca-4.8.95</a></td>
   <td align="right">9.9MB</td>
   <td><tt>936bddc8d78f6e7276dc7cd659f7a52ffbc2e6b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ca@valencia-4.8.95.tar.xz">kde-l10n-ca@valencia-4.8.95</a></td>
   <td align="right">2.1MB</td>
   <td><tt>a9f693f463f71a864e8e37854216739e119e6cc4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-cs-4.8.95.tar.xz">kde-l10n-cs-4.8.95</a></td>
   <td align="right">3.0MB</td>
   <td><tt>fa9909c93d229a86dc50d33f68fd0f76b0d6c0a6</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-da-4.8.95.tar.xz">kde-l10n-da-4.8.95</a></td>
   <td align="right">12MB</td>
   <td><tt>901a3d9734baab3541d21cf9be546389da7544c9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-de-4.8.95.tar.xz">kde-l10n-de-4.8.95</a></td>
   <td align="right">36MB</td>
   <td><tt>8b8837e083ac2aa83fb99195645f0e07f94a92c5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-el-4.8.95.tar.xz">kde-l10n-el-4.8.95</a></td>
   <td align="right">4.5MB</td>
   <td><tt>a3bcdcaae7091209060e8c594ac0cba0d832dcaf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-en_GB-4.8.95.tar.xz">kde-l10n-en_GB-4.8.95</a></td>
   <td align="right">3.0MB</td>
   <td><tt>bcb613f167d9d232369b25d5fccf18082f41a891</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-es-4.8.95.tar.xz">kde-l10n-es-4.8.95</a></td>
   <td align="right">22MB</td>
   <td><tt>e805da730da6cd65bb8cc0c1180db458e69e7630</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-et-4.8.95.tar.xz">kde-l10n-et-4.8.95</a></td>
   <td align="right">7.1MB</td>
   <td><tt>b97b930eebbeef8777ffa383a6ae2b8b4de9a89b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-eu-4.8.95.tar.xz">kde-l10n-eu-4.8.95</a></td>
   <td align="right">2.0MB</td>
   <td><tt>9d79742310d55145165ab4d555a375b9e59c7603</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-fa-4.8.95.tar.xz">kde-l10n-fa-4.8.95</a></td>
   <td align="right">1.8MB</td>
   <td><tt>71a47a2d94bc206024af60cd759d03c22d0ae757</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-fi-4.8.95.tar.xz">kde-l10n-fi-4.8.95</a></td>
   <td align="right">2.4MB</td>
   <td><tt>cbd01ae1416790355b51da9e9dac2c9c3c3082db</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-fr-4.8.95.tar.xz">kde-l10n-fr-4.8.95</a></td>
   <td align="right">35MB</td>
   <td><tt>65d8d68615f67ea410e0587c8911684d23de27f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ga-4.8.95.tar.xz">kde-l10n-ga-4.8.95</a></td>
   <td align="right">2.7MB</td>
   <td><tt>385fb84d05826f52e8ffb02c167a9b50441dc990</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-gl-4.8.95.tar.xz">kde-l10n-gl-4.8.95</a></td>
   <td align="right">4.0MB</td>
   <td><tt>d0e4365a4164e61ed75e7b908d480f3f73b11736</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-he-4.8.95.tar.xz">kde-l10n-he-4.8.95</a></td>
   <td align="right">2.0MB</td>
   <td><tt>ebd9e96d3957195652e6641542b93ca97f7eae12</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-hr-4.8.95.tar.xz">kde-l10n-hr-4.8.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>b365ab072217272bd20963604bb8bce195aa4d5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-hu-4.8.95.tar.xz">kde-l10n-hu-4.8.95</a></td>
   <td align="right">4.0MB</td>
   <td><tt>b242f7d843b41595e92bada8c3b472072110ddee</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ia-4.8.95.tar.xz">kde-l10n-ia-4.8.95</a></td>
   <td align="right">1.2MB</td>
   <td><tt>28b6faa83857b8ad4ca92f20ef09a0559f876868</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-id-4.8.95.tar.xz">kde-l10n-id-4.8.95</a></td>
   <td align="right">502kB</td>
   <td><tt>8b4fd0b0f30129ddae5549cefe24e4893fcaa57d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-is-4.8.95.tar.xz">kde-l10n-is-4.8.95</a></td>
   <td align="right">1.8MB</td>
   <td><tt>a9dd66f61470caca700427e51355f5b585335e0e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-it-4.8.95.tar.xz">kde-l10n-it-4.8.95</a></td>
   <td align="right">9.9MB</td>
   <td><tt>507ffc16e4594180df3328450b514aaa92f33297</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ja-4.8.95.tar.xz">kde-l10n-ja-4.8.95</a></td>
   <td align="right">2.2MB</td>
   <td><tt>0c0595dbc3d7c4c0b97c28669d67db49b6ea39fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-kk-4.8.95.tar.xz">kde-l10n-kk-4.8.95</a></td>
   <td align="right">2.4MB</td>
   <td><tt>f79193f3e159b9eea086103740c336ee995615c7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-km-4.8.95.tar.xz">kde-l10n-km-4.8.95</a></td>
   <td align="right">2.1MB</td>
   <td><tt>04db2f164b3e4c88914506d08219ac08627d1d7e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ko-4.8.95.tar.xz">kde-l10n-ko-4.8.95</a></td>
   <td align="right">1.7MB</td>
   <td><tt>83c93892b118ed7c1a1c60cb17e2eaecc7ad0464</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-lt-4.8.95.tar.xz">kde-l10n-lt-4.8.95</a></td>
   <td align="right">10MB</td>
   <td><tt>fbf2a2d2bde0bbe16a1470779bcea224f3a26a99</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-lv-4.8.95.tar.xz">kde-l10n-lv-4.8.95</a></td>
   <td align="right">2.0MB</td>
   <td><tt>934b0369e9f93d33c287692e5847ebba211148a0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-nb-4.8.95.tar.xz">kde-l10n-nb-4.8.95</a></td>
   <td align="right">2.3MB</td>
   <td><tt>60cecee48388bf995d9818d4549f5104f7468469</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-nds-4.8.95.tar.xz">kde-l10n-nds-4.8.95</a></td>
   <td align="right">3.0MB</td>
   <td><tt>11d3ee8b331fcae3ef8920dfc10bbcd2c5aac78d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-nl-4.8.95.tar.xz">kde-l10n-nl-4.8.95</a></td>
   <td align="right">15MB</td>
   <td><tt>cc4425d991fb1f41b842da4de0f28e6fd07caf1e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-nn-4.8.95.tar.xz">kde-l10n-nn-4.8.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>47157c51335164198d3aeba20a3bb286174590d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-pa-4.8.95.tar.xz">kde-l10n-pa-4.8.95</a></td>
   <td align="right">1.8MB</td>
   <td><tt>93988fd90f242ba853312da20454be008e95f81c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-pl-4.8.95.tar.xz">kde-l10n-pl-4.8.95</a></td>
   <td align="right">16MB</td>
   <td><tt>5c2db6093118df1c033e5eb26eb5de378e18e798</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-pt-4.8.95.tar.xz">kde-l10n-pt-4.8.95</a></td>
   <td align="right">5.6MB</td>
   <td><tt>6d8ef7ce180a151bf0293dcb27114061382ea1ab</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-pt_BR-4.8.95.tar.xz">kde-l10n-pt_BR-4.8.95</a></td>
   <td align="right">26MB</td>
   <td><tt>b50bafbe9539a395733a07696e1d84341a458464</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ro-4.8.95.tar.xz">kde-l10n-ro-4.8.95</a></td>
   <td align="right">2.8MB</td>
   <td><tt>4df8ce2e21eec3dc2038d9e413f4eee8c8464500</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ru-4.8.95.tar.xz">kde-l10n-ru-4.8.95</a></td>
   <td align="right">22MB</td>
   <td><tt>84863990237f5b72da09a5fe6f7382c64ce201af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-si-4.8.95.tar.xz">kde-l10n-si-4.8.95</a></td>
   <td align="right">1.0MB</td>
   <td><tt>789f1ee703565dcbfabe0867cdbcc29476aa0d78</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-sk-4.8.95.tar.xz">kde-l10n-sk-4.8.95</a></td>
   <td align="right">4.1MB</td>
   <td><tt>52287524dcde6c54fc4c29a863d4920384e0b559</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-sl-4.8.95.tar.xz">kde-l10n-sl-4.8.95</a></td>
   <td align="right">3.2MB</td>
   <td><tt>df9fd6026900d90ac6dfcb1ccc643121d9fae6b9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-sr-4.8.95.tar.xz">kde-l10n-sr-4.8.95</a></td>
   <td align="right">5.7MB</td>
   <td><tt>7ed3572e67dc2931f3a4cd3454c2417eb2dc3c1e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-sv-4.8.95.tar.xz">kde-l10n-sv-4.8.95</a></td>
   <td align="right">16MB</td>
   <td><tt>6d3177500f39154e92b5a7beb5f591141a3848b9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-tg-4.8.95.tar.xz">kde-l10n-tg-4.8.95</a></td>
   <td align="right">1.9MB</td>
   <td><tt>12436728a007830863726f953686134ee38a4d5a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-th-4.8.95.tar.xz">kde-l10n-th-4.8.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>9857ce63456bbb9202e0fcb1fbc8e9c668fdb722</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-tr-4.8.95.tar.xz">kde-l10n-tr-4.8.95</a></td>
   <td align="right">3.1MB</td>
   <td><tt>fc133991f49b627056f094fa9af24b339afbeee1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-ug-4.8.95.tar.xz">kde-l10n-ug-4.8.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>39e98b8034635dcefbada05534bfcaeb36958609</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-uk-4.8.95.tar.xz">kde-l10n-uk-4.8.95</a></td>
   <td align="right">21MB</td>
   <td><tt>2e082b1e1397808fe5a8ac9220444b1e25a73a28</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-vi-4.8.95.tar.xz">kde-l10n-vi-4.8.95</a></td>
   <td align="right">911kB</td>
   <td><tt>5c5ee723dbd3c03c5cbd96db34aa462e4ad8df5b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-wa-4.8.95.tar.xz">kde-l10n-wa-4.8.95</a></td>
   <td align="right">1.6MB</td>
   <td><tt>615d1ac41a2ad54e4b98d0c72a6a13b4b14b8ebc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-zh_CN-4.8.95.tar.xz">kde-l10n-zh_CN-4.8.95</a></td>
   <td align="right">3.4MB</td>
   <td><tt>2352b4658553d479b31bfeda678d48d36ca4b9fd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/4.8.95/src/kde-l10n/kde-l10n-zh_TW-4.8.95.tar.xz">kde-l10n-zh_TW-4.8.95</a></td>
   <td align="right">2.4MB</td>
   <td><tt>da408df59b46ede536d2889d60057d81849fc717</tt></td>
</tr>

</table>
