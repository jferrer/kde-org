---
title: "MetaStore - Buy KDE merchandising, books and more"
description: "Here you can find links to stores that provide KDE products and merchandising."
hidden: true
---


You can show your love for KDE in many ways. Here are a few places where you can buy t-shirts, books, mugs and more.

## KDE Slimbook - Laptop

Link: <a href="https://kde.slimbook.es/">https://kde.slimbook.es/</a>

<img width="152" height="100" src="https://kde.slimbook.es/assets/img/main_header.png">

## KDE e.V. Cafepress Store - T-shirts, pins, laptop sleeves and more!

Link: <a href="https://www.cafepress.com/shop/kde">https://www.cafepress.com/shop/kde</a>

<p>
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/118_350x350_Front_Color-NA.png?region={%22id%22:105764161,%22name%22:%22FrontCenter%22,%22width%22:3.0,%22height%22:3.0,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:450.0,%22crop_h%22:450.0,%22scale%22:1.0,%22dpi%22:150,%22template%22:{%22id%22:105764161,%22params%22:{}}}">
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/1241_350x350_Front_Color-White.png?region={%22id%22:100466231,%22name%22:%22FrontCenter%22,%22width%22:15.9981813,%22height%22:12.57,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:4200.0,%22crop_h%22:3300.0,%22scale%22:1.0,%22dpi%22:300,%22template%22:{%22id%22:100466231,%22params%22:{}}}">
<img width="100" height="100" src="https://i3.cpcache.com/merchandise/100_350x350_Front_Color-NA.png?region={%22id%22:105706193,%22name%22:%22FrontCenter%22,%22width%22:5.5,%22height%22:8.5,%22orientation%22:0,%22crop_x%22:0.0,%22crop_y%22:0.0,%22crop_w%22:825.0,%22crop_h%22:1275.0,%22scale%22:1.0,%22dpi%22:150,%22template%22:{%22id%22:105706193,%22params%22:{}}}">
</p>

## FreeWear.org - T-shirts, caps, mugs and more!

Link: <a href="https://www.freewear.org/?org=KDE">https://www.freewear.org/?org=KDE</a>

<div class="row">
<img class="col-12 col-md-6 my-2" alt="Freeware product: sweatshirt" src="/stuff/metastore/freeware-1.png">
<img class="col-12 col-md-6 my-2" alt="Freeware product: mug" src="/stuff/metastore/freeware-2.png">
<img class="col-12 col-md-6 my-2" alt="Freeware product: cap" src="/stuff/metastore/freeware-3.png">
<img class="col-12 col-md-6 my-2" alt="Freeware product: T-Shirt" src="/stuff/metastore/freeware-4.png">
</div>

## HELLOTUX - T-shirts, polos and sweatshirts

Link: <a href="https://www.hellotux.com/kde">https://www.hellotux.com/kde</a>

<p>
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_polo_shirt_black_index.JPG">
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_womens_tshirt_black_index.JPG">
<img width="133" height="100" src="https://www.hellotux.com/productimages/kde_hoodie_blackgray_index.JPG">
</p>

## Book: 20 Years of KDE: Past, Present and Future

Link: <a href="https://20years.kde.org/book/">https://20years.kde.org/book/</a>

<img width="71" height="100" src="https://20years.kde.org/book/assets/img/frontcover.png">

