---
date: 2024-05-21
changelog: 6.0.4-6.0.5
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Dr Konqi: Postman: don't run too often. [Commit.](http://commits.kde.org/drkonqi/c946568804b1d2846f9b39bd7107cfc517c211ed) 
+ Wallpaper of the Day: fix dragging preview image in Qt6.7. [Commit.](http://commits.kde.org/kdeplasma-addons/c62391fce7dd7adf841eac19a5dfa57450cf8797)
+ Fix keyboard navigation. [Commit.](http://commits.kde.org/plasma-desktop/86faa13193b51cc789d0f89d55f6a6ae20ccd984) Fixes bug [#485588](https://bugs.kde.org/485588). Fixes bug [#477348](https://bugs.kde.org/477348)
