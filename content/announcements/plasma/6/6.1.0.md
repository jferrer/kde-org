---
title: "Plasma 6.1"
subtitle: "Your Future Desktop is Ready"
description: "Plasma 6.1 brings improvements and powerful new features to every
part of your desktop"
date: 2024-06-18
images:
 - /announcements/plasma/6/6.1.0/fullscreen_with_apps.png
layout: plasma-6.1
scssFiles:
 - /scss/plasma-6-1.scss
authors:
 - SPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
 - SPDX-FileCopyrightText: 2024 Paul Brown <paul.brown@kde.org>
 - SPDX-FileCopyrightText: 2024 Oliver Beard <olib141@outlook.com>
SPDX-License-Identifier: CC-BY-4.0
draft: false
---

{{< container >}}

Plasma 6 hits its stride with version 6.1. While Plasma 6.0 was all about
getting the migration to the underlying Qt 6 frameworks correct (and what a
massive job that was), 6.1 is where developers start implementing the
features that will take your desktop to a new level.

In this release, you will find features that go far beyond subtle changes to
themes and tweaks to animations (although there is plenty of those too), as you
delve into interacting with desktops on remote machines, become more productive
with usability and accessibility enhancements galore, and discover
customizations that will even affect the hardware of your computer.

These features and more are being built directly into Plasma's Wayland version
natively, avoiding the need for third party software and hacky extensions
required by similar solutions implemented in X.

Things will only get more interesting from here. But meanwhile enjoy what
will land on your desktop with your next update.

{{< alert color="success" icon="tip" >}}

**Note:** Due to unforeseen circumstances, we have been unable to
ship the new wallpaper, "Reef", with this version of Plasma. However, there
will be a new wallpaper coming soon in the next 6.2 version.

If you can't wait, you can **[download "Reef" here](https://cdn.kde.org/promo/Announcements/Plasma/6.1/Reef.png)**.

We apologise for this delay and the inconvenience this may cause.

{{< /alert >}}

{{< /container >}}

{{< container >}}

## What's New

### Access Remote Plasma Desktops

One of the more spectacular (and useful) features added in Plasma 6.1 is that
you can now start up a remote desktop directly from the *System Settings* app.
This means that if you are sysadmin who needs to troubleshoot users' machines,
or simply need to work on a Plasma-enabled computer that is out of reach,
setting up a connection is just a few clicks away.

{{< video
src-webm="https://cdn.kde.org/promo/Announcements/Plasma/6.1/RDP_small_bg.webm"
src="https://cdn.kde.org/promo/Announcements/Plasma/6.1/RDP_small_bg.mp4"
poster="https://cdn.kde.org/promo/Announcements/Plasma/6.1/RDP_small.png"
fig_class="mx-auto max-width-800" >}}

Once enabled, you can connect to the remote desktop using a client such as
[KRDC](https://apps.kde.org/krdc/). You will see the remote machine's Plasma
desktop in a window and be able to interact with it from your own computer.

### Customization made (more) Visual

We all love customizing our Plasma desktops, don't we? One of the quickest ways
to do this is by entering Plasma's *Edit Mode* (right-click anywhere on the
desktop background and select *Enter Edit Mode* from the menu).

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/6.1/editmode_small.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/6.1/editmode_small.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/6.1/editmode_small.png" fig_class="mx-auto max-width-800" >}}

In version 6.1, the visual aspect of *Edit Mode* has been overhauled and you
will now see a slick animation when you activate it. The entire desktop zooms
out smoothly, giving you a better overview of what is going on and allowing you
to make your changes with ease.

### Persistent Apps

Plasma 6.1 on Wayland now has a feature that "remembers" what you were doing in
your last session like it did under X11. Although this is still work in
progress, If you log off and shut down your computer with a dozen open windows,
Plasma will now open them for you the next time you power up your desktop,
making it faster and easier to get back to what you were doing.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/6.1/persistent_small.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/6.1/persistent_small.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/6.1/persistent_small.png" fig_class="mx-auto max-width-800" >}}

### Sync your Keyboard's Colored LEDs

It wouldn't be a new Plasma release without at least one fancy aesthetic
customization features. This time, however, we give you the power to reach
beyond the screen, all the way onto your keyboard, as you can now synchronize
the LED colours of your keys to match the accent colour on your desktop.

{{< video src-webm="https://cdn.kde.org/promo/Announcements/Plasma/6.1/LEDs.webm" src="https://cdn.kde.org/promo/Announcements/Plasma/6.1/LEDs.mp4" poster="https://cdn.kde.org/promo/Announcements/Plasma/6.1/LEDs.png" fig_class="mx-auto max-width-800" >}}

The ultimate enhancement for the fashion-conscious user.

*Please note that this feature will not work on unsupported keyboards — support for additional keyboards is on the way!*

{{< /container >}}

{{< container class="text-center" >}}

### And all this too...

* We have simplified the choices you see when you try to exit Plasma by reducing the
number of confusing options. For example, when you press *Shutdown*, Plasma will only
list *Shutdown* and *Cancel*, not every single power option.

* **Screen Locking** gives you the option of configuring it to behave like a traditional
screensaver, as you can choose it not to ask you for a password to unlock it.

* Two visual accessibility changes make it easier to use the cursor in Plasma 6.1:

  * **Shake Cursor** makes the cursor grow when you "shake" it. This helps you locate
  that tiny little arrow on your large, cluttered screens when you lose it among all
  those windows.

  * **Edge Barrier** is useful if you have a multi-monitor setup and want to
  access things on the very edge of your monitor. The "barrier" is a sticky
  area for your cursor near the edge between screens, and it makes it easier to
  click on things (if that is what you want to do), rather than having the
  cursor scooting across to the next display.


* Two major Wayland breakthroughs will greatly improve your Plasma experience:

  * **Explicit Sync** eliminates flickering and glitches traditionally experienced by
  NVidia users.

  * **Triple Buffering** support in Wayland makes animations and screen rendering smoother.

{{< /container >}}

{{< container >}}

…and there's much more going on. To see the full list of changes, [check out the changelog for Plasma 6.1](/announcements/changelogs/plasma/6/6.0.5-6.1.0).

{{< /container >}}
