---
aliases:
- ../../plasma-5.18.2
changelog: 5.18.1-5.18.2
date: 2020-02-25
layout: plasma
peertube: cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5
figure:
  src: /announcements/plasma/5/5.18.0/plasma-5.18.png
asBugfix: true
---

- Discover: fix build on old flatpak versions. <a href="https://commits.kde.org/discover/7ff2de8d54ae749a142856c440816e764bfe5628">Commit.</a>
- Unify KSysGuard cpu clock speed names. <a href="https://commits.kde.org/ksysguard/4e656a45df16565e4273ae67d8dc4d530b5ca488">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D26857">D26857</a>
- Emojier: improve the fallback mechanism to detect languages. <a href="https://commits.kde.org/plasma-desktop/358e98a75a946abe76ffdfeddd0156483a66d4b3">Commit.</a> Fixes bug <a href="https://bugs.kde.org/417713">#417713</a>
