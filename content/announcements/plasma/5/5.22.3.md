---
date: 2021-07-06
changelog: 5.22.2-5.22.3
layout: plasma
youtube: HdirtBXW8bI
asBugfix: true
draft: false
---

+ Dr Konqi: Repair query construction. [Commit.](http://commits.kde.org/drkonqi/d4061d4889d45725d42e86de425dfe8b6ffb7b35)
+ Plasma Disks: Don't notify on instabilities. [Commit.](http://commits.kde.org/plasma-disks/e75432da5645bc4f9de96b5c9851ca8332396181) See bug [#438539](https://bugs.kde.org/438539)
+ [Task Manager] Load album art background asynchronously. [Commit.](http://commits.kde.org/plasma-desktop/7ea448ae8102876d77a12dd155912a2d34d05896) Fixes bug [#439512](https://bugs.kde.org/439512)
