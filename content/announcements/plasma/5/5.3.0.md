---
aliases:
- ../../plasma-5.3.0
changelog: 5.2.2-5.3.0
date: '2015-04-28'
layout: plasma
figure:
  src: /announcements/plasma/5/5.3.0/plasma-5.3.png
---

{{% i18n_date %}}

Today KDE releases a release of Plasma 5, versioned 5.3.

## Highlights

{{<figure src="/announcements/plasma/5/5.3.0/power-management-info.png" alt="Battery applet now informs what is blocking power saving" class="text-center" width="600px" caption="Battery applet now informs what is blocking power saving">}}

{{<figure src="/announcements/plasma/5/5.3.0/kinfocenter-energy.png" alt="New energy usage monitor" class="text-center" width="600px" caption="New energy usage monitor">}}

### Enhanced Power Management

- Power management settings can be configured differently for certain activities
- Laptop will not suspend when closing the lid while an external monitor is connected ('cinema mode', by default, can be turned off)
- Power management inhibitions block lock screen too
- Screen brightness changes are now animated on most hardware
- No longer suspends when closing the lid while shutting down
- Support for keyboard button brightness controls on lock screen
- KInfoCenter provides statistics about energy consumption
- Battery monitor now shows which applications are currently holding a power management inhibition for example ('Chrome is currently suppressing PM: Playing video')

{{<figure src="/announcements/plasma/5/5.3.0/bluedevil-applet3.png" alt="The new Bluedevil Applet" class="text-center" width="600px" caption="The new Bluedevil Applet">}}

### Better Bluetooth Capabilities

- New Bluetooth applet
- Bluedevil was ported to a new library from KDE, BluezQt
- Added support for blocking and unblocking Bluetooth
- Connected devices with Browse Files (ObexFTP) support are now displayed in the file dialog's Places panel

{{<figure src="/announcements/plasma/5/5.3.0/touchpad.png" alt="Configure your Touchpad" class="text-center" width="600px" caption="Configure your Touchpad">}}

A <strong>touchpad configuration module</strong> has been added

{{<figure src="/announcements/plasma/5/5.3.0/kicker-contacts.png" alt="Application Menu can access contacts" class="text-center" width="600px" caption="Application Menu can access contacts">}}

{{<figure src="/announcements/plasma/5/5.3.0/kicker-docs.png" alt="Application Menu can show recent documents" class="text-center" width="600px" caption="Application Menu can show recent documents">}}

### Improved Plasma Widgets

- Clipboard applet gains support for showing barcodes
- The Desktop and Folder View containment codebases were unified, and have seen performance improvements
- The Recent Documents and Recent Applications sections in Application Menu (Kicker) are now powered by KDE activities
- Comics widget returns
- System monitor plasmoids return, such as CPU Load Monitor and Hard Disk usage

### Plasma Media Center - Tech Preview

{{<figure src="/announcements/plasma/5/5.3.0/plasma-mediacenter.png" alt="Plasma Media Center" class="text-center" width="600px" caption="Plasma Media Center">}}

<strong>Plasma Media Center</strong> is added as a tech preview in this release. It is fully stable but misses a few features compared to version 1. You can log directly into a Plasma Media Center session if you want to use it on a media device such as a television or projector or you can run it from Plasma Desktop. It will scan for videos, music and pictures on your computer to let you browse and play them.

{{<figure src="/announcements/plasma/5/5.3.0/kwin-nested.png" alt="Plasma is now able to start a nested XWayland server" class="text-center" width="600px" caption="Plasma is now able to start a nested XWayland server">}}

### Big Steps Towards Wayland Support

- Plasma 5.3 makes a huge step towards to <strong>supporting the Wayland windowing system</strong> in addition to the default X11 windowing system. Plasma's window manager and compositor KWin is now able to start a nested XWayland server, which acts as a bridge between the old (X11) and the new (Wayland) world. X11 windows can connect to this server as if it were a normal X server, for KWin it looks like a Wayland window, though. This means that KWin learned to handle Wayland windows in this release, though full integration is only expected for Plasma 5.4.

* In addition KWin gained new output modes for Wayland allowing to start a nested KWin on X11 and to start KWin directly on a framebuffer device, which will be the fallback for the case that OpenGL and/or <a href='https://en.wikipedia.org/wiki/Direct_Rendering_Manager'>kernel mode settings</a> are not supported. A rendering backend on kernel mode settings is expected for Plasma 5.4. More information about these new backends and how to test them can be found in <a href='https://community.kde.org/KWin/Wayland'>the KWin wiki pages</a>. Please keep in mind that this is only a development preview and highly experimental new code.

### Desktop Tweaks - Press and Hold

{{< video src-ogv="/announcements/plasma/5/5.3.0/plasmoid-press-and-hold.ogv" caption="Press and Hold Tweak" >}}

A new Tweaks configuration option in Plasma allows you to enable or disable the desktop settings toolbox and enable a new <strong>Press and Hold</strong> mode for widget management. In this mode, widgets can be moved after pressing and holding anywhere on the widget, and the widget handle will only be shown after press and hold rather than on hover. When this mode is enabled, unlocking the desktop will show a helpful instruction notification (which can be dismissed forever from the popup).

### Bug Fixes Galore

393 bugs were fixed giving fewer crashes and more reliable use.
