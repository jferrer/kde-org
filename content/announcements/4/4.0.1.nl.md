---
aliases:
- ../announce-4.0.1
date: '2008-02-05'
title: KDE 4.0.1 Vrijgegeven
---

<h3 align="center">
   KDE-project levert eerste onderhoudsrelease van toonaangevende vrije desktop
</h3>

<p align="justify">
  KDE-project levert eerste vertalings- en onderhoudsrelease van de 4.0-generatie. Dit biedt gebruikers, organisaties en overheden een vrije en open desktopoplossing.
</p>


<p>
De KDE-gemeenschap kondigt de onmiddelijke beschikbaarheid aan van KDE 4.0.1. Dit is de eerste onderhoudsrelease van de laatste generatie van KDE, de meest geavanceerde en krachtige desktop. KDE 4.0.1 biedt een basisdesktop aan maar ook software voor administratie, netwerk, educatie, hulpmiddelen, multimedia, spellen, artwork, webontwikkeling en meer. Deze programms's zijn beschikbaar in bijna 50 talen.
</p>

<p align="justify">
Alle bibliotheken en programma's van KDE zijn vrij beschikbaar onder open source-licenties. KDE kan verkregen worden in broncode of in een groot aantal binaire formaten vanaf <a
href="http://download.kde.org/stable/4.0.1/">download.kde.org</a>, maar ook op <a href="/download">CD-ROM</a> of via de grotere leveranciers van <a href="/distributions">GNU/Linux- en UNIX-systemen</a>.
</p>

<h4>
  <a id="changes">Verbeteringen</a>
</h4>

<p align="justify">
KDE 4.0.1 is een onderhoudsrelease met reparaties van problemen die zijn gerapporteerd via de <a href="http://bugs.kde.org">KDE Bugtracker</a>. Bovendien bevat het verbeterde en toegevoegde vertalingen.</p>

<p>
De verbeteringen zijn onder andere:

<ul>
<li>Konqueror, de webbrowser van KDE, heeft een aantal stabiliteits- en prestatieverbeteringen ondergaan in de KHTML-renderingsengine KHTML, de Flash-pluginlader en de JavaScript-engine KJS.</li>
<li>Stabiliteitsproblemen zijn opgelost in componenten die door heel KDE worden gebruikt.</li>
<li>KWin, de windowmanager van KDE, heeft verbeterde herkenning van compositing-ondersteuning. Ook hebben enkele effecten wat reparaties ondergaan.</li>
</ul>
</p>

<p align="justify">
Naast deze verbeteringen, is er gewerkt aan vele programma's als Okular, systeeminstellingen en KStars. Nieuwe vertalingen die nog niet in 4.0 zaten zijn Fries, Deens, Kazachs en Tsjechisch.
</p>

<p align="justify">
Een meer gedetailleerde lijst van verbeteringen sinds de uitgave van KDE 4.0 in januari 2008 kunt u vinden in de <a
href="/announcements/changelogs/changelog4_0to4_0_1">KDE
4.0.1 Changelog</a>.
</p>

<p align="justify">
Meer informatie over de verbeteringen in de KDE 4.0.x uitgaveserie vindt u in de <a href="../4.0/">aankondiging van KDE 4.0</a>.
</p>

<h4>
  Het installeren van KDE 4.0.1 binaire pakketten
</h4>
<p align="justify">
  <em>Packagers</em>.
Enkele Linux/UNIX-distributeurs hebben binaire pakketten voor KDE 4.0.1 beschikbaar gesteld in hun distributie, of in sommige gevallen heeft de bijbehorende gemeenschap dit gedaan. Enkele van deze binaire pakketten zijn te downloaden van <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/">download.kde.org</a>. Meer binaire pakketten en updates van de huidig beschikbare pakketten zullen in de komende weken beschikbaar komen.
</p>

<p align="justify">
  <a id="package_locations"><em>Pakketlocaties</em></a>. Voor de huidige lijst van beschikbare binaire pakketten, tot zover het KDE-project daarvan op de hoogte is, kunt u vinden op de <a href="/info/4/4.0.1">KDE 4.0.1 informatiepagina</a>.
</p>

<h4>KDE 4.0.1 compileren</h4>
<p align="justify">
  <a id="source_code"><em>Broncode</em></a>. De volledige broncode voor KDE 4.0.1 kan <a
href="http://download.kde.org/stable/4.0.1/src/">vrij gedownload</a> worden. Instructies voor het compileren en installeren van KDE 4.0.1 zijn beschikbaar op de <a href="/info/4/4.0.1">KDE 4.0.1 informatiepagina</a>.
</p>

<h4>
  KDE ondersteunen
</h4>
<p align="justify">
  KDE is een <a href="http://www.gnu.org/philosophy/free-sw.html" target="_blank">vrij softwareproject</a> dat alleen kan bestaan en groeien dankzij de hulp van vele vrijwilligers die daar tijd en moeite in steken. KDE is altijd op zoek naar nieuwe vrijwilligers en bijdragen, of het nu gaat om programmeren, oplossen of rapporteren van bugs, het schrijven van documentatie, vertalingen, promotie, financieel, etc. Iedere bijdrage wordt erg gewaardeerd en geaccepteerd. Lees de <a href="http://www.kde.org/support" target="_blank">supportpagina</a> (Engels) door voor meer informatie.
</p>

<h4>
	Over KDE 4
</h4>

<p align="justify">
KDE 4.0 is de innovatieve vrije-software-desktop, en bevat zowel applicaties voor dagelijks gebruik als specifieke applicaties. Plasma is een nieuwe desktopomgeving die ontwikkeld is voor KDE 4, en biedt een intu&iuml;tieve interface voor desktop en applicaties. De webbrowser Konqueror integreert het web met de desktop. De bestandsbeheerder Dolphin, de documentviewer Okular en het configuratiescherm Systeeminstellingen completeren de basisdesktop.
<br />
KDE is gebouwd op de KDE-bibliotheken, waardoor hulpbronnen op het network eenvoudig toegankelijk zijn (met behulp van KIO), en geavanceerde visuele mogelijkheden beschikbaar zijn (door middel van QT4). Phonon en Solid, ook een onderdeel van de KDE-bibliotheken, bieden een multimediaframework en betere hardware-integratie voor alle KDE-applicaties.
</p>



