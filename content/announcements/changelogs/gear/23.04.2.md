---
aliases:
- ../../fulllog_releases-23.04.2
title: KDE Gear 23.04.2 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ LastPathSegment: don't strip slash if it is the only character in the path. [Commit.](http://commits.kde.org/ark/7ef6539de2c5d00f1b440132c7ff1a43832bc094) Fixes bug [#469795](https://bugs.kde.org/469795)
{{< /details >}}
{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ Make media duration key optional. [Commit.](http://commits.kde.org/audiotube/25cc1026a2b567eead875fd8cb777bdcf979c81f) 
{{< /details >}}
{{< details id="baloo-widgets" title="baloo-widgets" link="https://commits.kde.org/baloo-widgets" >}}
+ Allow FileMetata properties on desktop and recentlyused. [Commit.](http://commits.kde.org/baloo-widgets/fabbb486257597c1ba59c3d0981e711f0c20d329) Fixes bug [#460117](https://bugs.kde.org/460117)
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Fix displaying plots in sagemath>=9.7. [Commit.](http://commits.kde.org/cantor/d7d3ec2809eec1e818a5ec15c7018b3dc0b587cd) Fixes bug [#468598](https://bugs.kde.org/468598). See bug [#469404](https://bugs.kde.org/469404)
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Revert "Restrict attaching instances to those on the same activity or same virtual desktop". [Commit.](http://commits.kde.org/dolphin/70d0154adbae71be40ee6961272a7738ee5c9a9e) Fixes bug [#408919](https://bugs.kde.org/408919). See bug [#408919](https://bugs.kde.org/408919)
+ Revert "global.cpp: Ensure qApp does not emit signal because of local QEventLoop". [Commit.](http://commits.kde.org/dolphin/abc2de65556393755a6838f71f48915ad438fe17) 
+ Global.cpp: Ensure qApp does not emit signal because of local QEventLoop. [Commit.](http://commits.kde.org/dolphin/f91e691f26150b0c05c06897a8d85bf3a7fffc06) Fixes bug [#469656](https://bugs.kde.org/469656)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Don't use pointing finger cursor for non-link UI elements. [Commit.](http://commits.kde.org/elisa/f483552bb65efe666aaea18e6941eed35bc6a7df) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Fix build with exiv2 >= 0.28. [Commit.](http://commits.kde.org/gwenview/2598384cc70cb0e38550e2dee93d111e876683be) 
+ Fix running wrong application in 'Open with...' menu. [Commit.](http://commits.kde.org/gwenview/6ac28ea410a7eacbab12cf0e18b474d3793c15fb) Fixes bug [#469824](https://bugs.kde.org/469824)
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Add 23.04.2 release notes. [Commit.](http://commits.kde.org/itinerary/42755a7796074e4525e0fc5f9fb8fa67db3d2f13) 
+ Fix horizontal padding in editor page. [Commit.](http://commits.kde.org/itinerary/556bde78b1506f00df0524e3507fc1707b6cdbd5) 
+ Adapt test data to the changed name prefix cleaning in kitinerary. [Commit.](http://commits.kde.org/itinerary/05ca8ab26fcc0b7de6b6fb3be05ed92d1ceadd21) 
+ Don't attempt to select favorite locations based on invalid coordinates. [Commit.](http://commits.kde.org/itinerary/d45d143983e2f8e9bbab9ecfdf13b2936e7f6939) 
+ Set better transfer delta times for restaurant reservations. [Commit.](http://commits.kde.org/itinerary/78c3a65077d5e4180bc25f6565d7b1eb33c3ba7c) 
+ Be more strict in discarding elements we cannot get live updates for. [Commit.](http://commits.kde.org/itinerary/2c4c94e52de1ea66f84d859b10be082f16b63b66) 
+ Properly check if the element a transfer is attached is a location change. [Commit.](http://commits.kde.org/itinerary/99b175ae6036032c28cf8f52fc0ed3379bf209b0) 
+ Always pass passId to ActionsCard. [Commit.](http://commits.kde.org/itinerary/4ea5206ab3eef57b593ba8f0a4fae6fae9664098) 
+ Work around KUnitConversion deadlocking when needing a currency update. [Commit.](http://commits.kde.org/itinerary/794f66aa3d469b10007f889e6c5e7f31abd20c82) Fixes bug [#469562](https://bugs.kde.org/469562)
+ Cache holiday region lookups. [Commit.](http://commits.kde.org/itinerary/27ee5cf1e17f784cec19a2c20f3fd9fe326f6896) 
+ Fix attaching documents on multi-traveler reservations. [Commit.](http://commits.kde.org/itinerary/a90d36abdc3ac1109279578528ebdcfe7d5e9408) Fixes bug [#469347](https://bugs.kde.org/469347)
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Adjust kcm_cddb path. [Commit.](http://commits.kde.org/k3b/90a673a415e1b4c7af60a70c062303d1e8847528) Fixes bug [#469711](https://bugs.kde.org/469711)
+ Fix the layout of the Mixed CD burn dialog "Misc" tab. [Commit.](http://commits.kde.org/k3b/98462e91d7f6bf0de1b2832cd0106b49d65938f3) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Bug 468640: Implement workaround for memory leak in KCalendarCore. [Commit.](http://commits.kde.org/kalarm/ef7c3e328a7f42f00acd94b75b51ad9d503e6a8e) 
{{< /details >}}
{{< details id="kalendar" title="kalendar" link="https://commits.kde.org/kalendar" >}}
+ Move RemindersModel to calendar module. [Commit.](http://commits.kde.org/kalendar/88fdcb507aad5102c4b411fcb3949fbd13d8361e) 
+ Simplify MultiDayIncidenceModel. [Commit.](http://commits.kde.org/kalendar/d493e143bd5c317c39ea428b3fc1ae4137c605a5) 
+ Use grouped property. [Commit.](http://commits.kde.org/kalendar/0052d929c6b5068bce4390ede41c1ec9a5b39388) 
+ Modernize DayGridView. [Commit.](http://commits.kde.org/kalendar/7f85a894d84f80a368f39f8fc755c288a5857735) 
+ BasicMonthGridView.qml: Use const instead of let. [Commit.](http://commits.kde.org/kalendar/8e8392a477a232bae6dd749a0a88a600b651bbb7) 
+ Use strict equality for DayGridViewIncidenceDelegate.isInCurrentMonth. [Commit.](http://commits.kde.org/kalendar/03e6b3b9d3324fa185e107a6f2b0fd03726411ee) 
+ Remove usage of KCalPrefs. [Commit.](http://commits.kde.org/kalendar/e09f69516070e7a51722fa1a54111c8aca8dc0c5) 
+ Use Akonadi::Collection::Id to store color keys. [Commit.](http://commits.kde.org/kalendar/46e5731d441a95c621f4ee25e1776b97f5f711f2) 
+ Improve retrival of color from incidence. [Commit.](http://commits.kde.org/kalendar/623d2a08cf228a86f7af20f5e89e7a9750dc9d9b) 
+ Get collection color directly from collection when possible in IncidenceOccurrenceModel. [Commit.](http://commits.kde.org/kalendar/1939003b90e7f88c85e11693abf56ddebf746600) 
+ Fix wrong colour being set in color cache when generating it is required. [Commit.](http://commits.kde.org/kalendar/628e02fde00702c3306cbf31072761ab64a1a863) 
+ Disable capturing hover events globally. [Commit.](http://commits.kde.org/kalendar/afaf166fa34c0c9c1c7277ebdc3dc7e8f023d595) 
+ Fix bugs in IncidenceInfoContents. [Commit.](http://commits.kde.org/kalendar/6fefe9d744ddac5b7391b3920ffc45b8659a1bd4) 
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ [KMediaSession] Only send MRPIS2 positionChanged signal on seek. [Commit.](http://commits.kde.org/kasts/37c809d2435ba475183e4438925b270b6f0b6616) 
+ Also update fullscreen image if chapter image in HeaderBar changes. [Commit.](http://commits.kde.org/kasts/68baa0452213f82864f3a0aac19584267735c442) Fixes bug [#470194](https://bugs.kde.org/470194)
+ Load custom icons through fallbackSearchPaths. [Commit.](http://commits.kde.org/kasts/5ac9b0fdeb0b98abcb063199f78600b2ead3f241) Fixes bug [#469567](https://bugs.kde.org/469567)
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix double drive letters on "Run Current Doc". [Commit.](http://commits.kde.org/kate/b0efbc8ef62c48f950996871068b541a6e5be2e5) Fixes bug [#469340](https://bugs.kde.org/469340)
+ Send "\r\n" instead of "\n" on windows. [Commit.](http://commits.kde.org/kate/a6dc3919385606ea8ee3945978f153a38807bd6a) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix freeze on duplicate sequence. [Commit.](http://commits.kde.org/kdenlive/a25516e9e2a08d7f6056a01cb11cd25c59f63bad) See bug [#470465](https://bugs.kde.org/470465)
+ Get rid of mocking in some more tests. [Commit.](http://commits.kde.org/kdenlive/24eafb6bcf8691b46010cf0738dc7914832b564e) 
+ Re-enable timeline sequence thumbnails. [Commit.](http://commits.kde.org/kdenlive/a3c7897a0df8bd92819067c3e8b669c644d5d454) 
+ Fix tests. [Commit.](http://commits.kde.org/kdenlive/79d65fb4dd058d7f5cd3393dff8e73bfea7259ad) 
+ Nesting: refactor timeline sequence open/close, add tests. [Commit.](http://commits.kde.org/kdenlive/1e02d0baaa777d839f1fe9e3d12b60da9524b2fb) 
+ Display default folder for VOSK models in settings page when no custom folder is set. [Commit.](http://commits.kde.org/kdenlive/785f56d78e5e6b3e1aad8ca504ee471958d793bb) 
+ Fix closing sequence in reopened project losing all recent changes. [Commit.](http://commits.kde.org/kdenlive/36b3d1110eb009d48426665153620a132d1c9fff) 
+ Fix crash pasting subtitle in a timeline sequence without subtitles. [Commit.](http://commits.kde.org/kdenlive/ae3c7f60f500678764c8c05e6d6053b313718e7c) 
+ Fix dragging clip in timeline can cause out of view scrolling. [Commit.](http://commits.kde.org/kdenlive/6b56c14c56aa1710ebbf912a8514e675c9c7807d) 
+ Fix motion tracker not working on rotated clips or clip with distort effects. [Commit.](http://commits.kde.org/kdenlive/753b5984a2f2e76909f99f0d8ce804c08127f63c) 
+ Fix import clip on single click in Media Browser. [Commit.](http://commits.kde.org/kdenlive/c65b4917be5eaaac413ecb746d7f363ea6a819a6) 
+ Fix focus issue after switching from fullscreen monitor. [Commit.](http://commits.kde.org/kdenlive/99942a8752e8bc74fc3dc1bb8473137f960a524c) 
+ Cleaner version of previous patch. [Commit.](http://commits.kde.org/kdenlive/155c44e4e431bb8748acb805471c0c1f544aa91e) 
+ Fix multiple guides export. [Commit.](http://commits.kde.org/kdenlive/d88360736165c3c7c0fffa0aea6b6239d8b9f205) Fixes bug [#469435](https://bugs.kde.org/469435)
+ Fix color wheel resetting color on mouse wheel. [Commit.](http://commits.kde.org/kdenlive/e1e8641c61cadb5709ce43c874a03166a750f0cf) See bug [#470005](https://bugs.kde.org/470005)
+ Minor UI adjustments to timeline ruler. [Commit.](http://commits.kde.org/kdenlive/1ff43505341da5457c0efdcf34f68cbea7af070b) 
+ Use better option for Media Browser. [Commit.](http://commits.kde.org/kdenlive/a80b08bd8561d146d64262dff456fe9d99c48f31) 
+ Drop timeline zoom whatsthis (it interferes with zoom shortcut (Shift+Ctrl++). [Commit.](http://commits.kde.org/kdenlive/58266682e5242082c5121570cf6b89a2b6bdf070) 
+ Merge !399 with a few fixes (whisper disable FP16 on GTX 16xx). [Commit.](http://commits.kde.org/kdenlive/4f3463e8256e3636adca5a2feffea898a4088bff) 
+ Don't allow archiving unsaved project, show subtitle files in archive widget and project files list. [Commit.](http://commits.kde.org/kdenlive/f87960f015aeb589df0c2805eff034ec091c3f8e) 
+ Fix scaled rendering. [Commit.](http://commits.kde.org/kdenlive/71f4138ac61fd2a31f0b2ddd717743fd35cb36fd) 
+ Titler: shadow should include text outline. [Commit.](http://commits.kde.org/kdenlive/2acc4e3d44643524d4450fab8d35e8b99cfbe96e) 
+ Thumbnailer: ensure producer is valid, don't seek past clip end. [Commit.](http://commits.kde.org/kdenlive/97e72d574b553927649c846e0c137254ad4af607) 
+ Fix create sequence from selection resulting in incorrect clip length. [Commit.](http://commits.kde.org/kdenlive/e9a242f32bbf38a9d71b39e9b4d6312e7b16cfef) 
+ Don't trigger producer reinsert multiple times on change. [Commit.](http://commits.kde.org/kdenlive/08a4e92cacafb459554c38338f08b9a3c086fea3) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Implement retrieveItems(Item::List) for googleresource. [Commit.](http://commits.kde.org/kdepim-runtime/6bffef37350b7bcacaa2763ab8df5a462eb3f8b7) Fixes bug [#448106](https://bugs.kde.org/448106)
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Fix loading kig part. [Commit.](http://commits.kde.org/kig/bca478c18b1a64f96c63cbe87111c79155bf7eb5) Fixes bug [#469962](https://bugs.kde.org/469962)
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add Finnlines extractor script. [Commit.](http://commits.kde.org/kitinerary/1aeea2e9ac586f618ef4dbe19ef6436ef05598c6) 
+ Add basic Tallink extractor. [Commit.](http://commits.kde.org/kitinerary/91baa2067c5813e9e8dc7ef57034d4414a03bf0b) 
+ Add support for Estonian railway tickets. [Commit.](http://commits.kde.org/kitinerary/52d67647654ea44997aad5a83a3a116b42715925) 
+ Add extractor script for Lithuanian railway tickets. [Commit.](http://commits.kde.org/kitinerary/00e8406b08ca12625d3557cdc182b1568abba227) 
+ Add extractor script for Latvian railway tickets. [Commit.](http://commits.kde.org/kitinerary/6c2d5abf49b21f4fa35fae97f52a65f08f182fa4) 
+ Expose PDF page sizes to extractor scripts. [Commit.](http://commits.kde.org/kitinerary/a6d4377bcef8ec28a86af618d5ecb2881bfe19c7) 
+ Expand generic FCB extractor. [Commit.](http://commits.kde.org/kitinerary/bbc2cec60f2318dfe4d2d62e0951e3d612005965) 
+ Move FCB class code formatting to FcbUtil for reuse elsewhere. [Commit.](http://commits.kde.org/kitinerary/378824150b86327c4af7a501d2128a46ad6ce1e3) 
+ Decode departure time of FCB train link region validity constraints. [Commit.](http://commits.kde.org/kitinerary/e6629dc71d1a5ea047f6a7895ae1662f31d0fda2) 
+ Fix FCB arrival/departure date/time decoding. [Commit.](http://commits.kde.org/kitinerary/9311fa2128d4e47a5cd04b65a8f112dcdf18523b) 
+ Fix Fcb::ReservationData ASN.1 schema mismatch. [Commit.](http://commits.kde.org/kitinerary/e466ecf1e1991894af5f29a85e2c10cb2424e251) 
+ Extract ticket numbers for SBB passes. [Commit.](http://commits.kde.org/kitinerary/055b83c712036c579d22764586b80aa6afb1e3d5) 
+ Be a bit more thorough when stripping of name prefixes. [Commit.](http://commits.kde.org/kitinerary/b148dc36c82fd222f18430c2f00e27d5eb7b6dac) 
+ Add another time pattern, found on IndiGo boarding passes for example. [Commit.](http://commits.kde.org/kitinerary/06e51a509f5842ae3e2ca374c36ea3ad066e3e67) 
+ Prefer RCT2 fields fully contained in the specified area. [Commit.](http://commits.kde.org/kitinerary/436a900c5d405c36a6f1ed65be6e3e868ce7f64e) 
+ Support English Thalys tickets as well. [Commit.](http://commits.kde.org/kitinerary/ae39cadc8a85844411c20801e0bea312ef62b97b) 
+ Handle one alternative date format found in SNCB RCT2 tickets. [Commit.](http://commits.kde.org/kitinerary/7ee84fc0cf3e6a64aefc1471cdbf6e283d600211) 
+ Fold schema.org  mainEntityOfPage property into the parent object. [Commit.](http://commits.kde.org/kitinerary/6b7570cef517bf930eb91f1fbd094465696d350b) 
+ Allow to merge elements where one is a subtype of the other. [Commit.](http://commits.kde.org/kitinerary/26da5a1bb31a08c1fca1d46c039a044452566808) 
+ Generalize comparing hotels and restaurants. [Commit.](http://commits.kde.org/kitinerary/056dbe64ed683bd3f34935a835641da4e9013884) 
+ Unpack Country objects used in addresses. [Commit.](http://commits.kde.org/kitinerary/e00b7209826f03ca7de65b04901b8f3446380072) 
+ Clean up postal codes during post processing. [Commit.](http://commits.kde.org/kitinerary/2448504e273853a57dd3a9b2878d136163bea586) 
+ Also post-process LocalBusiness elements. [Commit.](http://commits.kde.org/kitinerary/c3bacb12675e5933f025c3560fe17c5c6e6855c4) 
+ Also merge canceled reservations with their minimal cancellation element. [Commit.](http://commits.kde.org/kitinerary/6c56a31a88338b8320ec5e6dd2d42de303e89836) 
+ Update train station tables from Wikidata. [Commit.](http://commits.kde.org/kitinerary/bff60db57c28425efa37413b2468440a6ebc3edb) 
+ Update power plug and driving side data from Wikidata. [Commit.](http://commits.kde.org/kitinerary/9554415af70e1c9c865e4c976877126eca24ff0f) 
+ Let git consider the train station data table to be a binary file. [Commit.](http://commits.kde.org/kitinerary/a8b2919b56298308f58c5d3e044b7d839b5eea42) 
+ Apply the new country SPARQL expression also to all subsequent queries. [Commit.](http://commits.kde.org/kitinerary/86546faaf504fc810c8f9621952f9fe4dc0ac48e) 
+ Update Wikidata country data SPARQL query to include some more corner cases. [Commit.](http://commits.kde.org/kitinerary/9062dfb55368325ac4bd10c1acf6e264a7c677a8) 
+ Remove manual (and broken) FCB handling from DB extractor script. [Commit.](http://commits.kde.org/kitinerary/aaff8f430b86db32e75787e243ec17566566088e) 
+ Also set the ticket name when extracting a full train reservation. [Commit.](http://commits.kde.org/kitinerary/fa49cb12806c897e694840eea6f392310c04450c) 
+ Extract European Sleeper seat reservation data. [Commit.](http://commits.kde.org/kitinerary/6a7a24585e67c9ca4904f2c7c604cca34ef66815) 
+ Add Qatar Airways booking confirmation extractor. [Commit.](http://commits.kde.org/kitinerary/251e81ac96c146d15ffc7e29f306ac5e65b90e1e) 
+ Add extractor script for European Sleeper. [Commit.](http://commits.kde.org/kitinerary/1165a72fcbcc1fe28e6079b4bc3e1ae8379af8d1) 
+ Add initial generic FCB extractor. [Commit.](http://commits.kde.org/kitinerary/768d33d8d2d1fd30ca947777319f03f18f9eb7d8) 
+ Use vocabulary types in the generic UIC 918.3 extractor. [Commit.](http://commits.kde.org/kitinerary/0de13187e381498a6a178b5c7c8e86169d30b46c) 
+ Check for MAV seat reservations actually having a seat number. [Commit.](http://commits.kde.org/kitinerary/b5f936b2ce79d506e56363518fa06ee7802c2f63) 
+ Support person comparison with one side only having partial names. [Commit.](http://commits.kde.org/kitinerary/3013fe7a740ce863a96b9916e8e3f7a0ef04c2c8) 
+ Fix expanding attached MIME messages. [Commit.](http://commits.kde.org/kitinerary/c73166011fa9710d16da4685248fd89bcd13b18f) 
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Set OrganizationDomain before registering the DBus service. [Commit.](http://commits.kde.org/kleopatra/9557e5ec15997ff0bd712c7d4b7016b98c2e9651) See bug [#470246](https://bugs.kde.org/470246)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Adjust selection point coords when scrollback shrinks. [Commit.](http://commits.kde.org/konsole/af2f32c7af485d3fc96b13215435272e0d6adb7e) Fixes bug [#470346](https://bugs.kde.org/470346)
+ Add tests to check fix of invalid parent in profile. [Commit.](http://commits.kde.org/konsole/2305dbb7c4b156735ba46e2b69bc45167a66b970) 
+ Do not allow an invalid parent to be set in profiles. [Commit.](http://commits.kde.org/konsole/d4b093dc642b76546e25de438ed194470e5dabfd) Fixes bug [#467790](https://bugs.kde.org/467790)
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Exclude test input files that don't pass the JSON validation CI step. [Commit.](http://commits.kde.org/kpublictransport/b0fc449a96866fd1fdb948b0d3f392fa99bc00ad) 
+ Drop substring notes during notes merging. [Commit.](http://commits.kde.org/kpublictransport/2275bac73935e5c58222e463b00f18c7994c6ad4) 
+ Fix typo in parsing expected arrival times in DB Zugportal journeys. [Commit.](http://commits.kde.org/kpublictransport/1a3de27b80ff36bc2d50856e4c83ebd9521c496a) 
{{< /details >}}
{{< details id="ksanecore" title="ksanecore" link="https://commits.kde.org/ksanecore" >}}
+ Also call renewImage() for 8-bit Gray images. [Commit.](http://commits.kde.org/ksanecore/fb876efdf896f6979f51bba252dec768df2fb3a3) Fixes bug [#469683](https://bugs.kde.org/469683)
+ Crop the image after a scan finished. [Commit.](http://commits.kde.org/ksanecore/d3e2c41182d58b5ab1b22de75f78ce4e7f8f9c0c) 
{{< /details >}}
{{< details id="libkexiv2" title="libkexiv2" link="https://commits.kde.org/libkexiv2" >}}
+ Fix build with exiv2 >= 0.28. [Commit.](http://commits.kde.org/libkexiv2/e0783d6e68d19f53c12433b4934130f93a3b273a) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Use QuoteHtmlChars. [Commit.](http://commits.kde.org/messagelib/643191342ce9d03b392ca8064e1026214930b1eb) 
+ Fix compare emails (some email is wroten as "<foo@kde.org>". [Commit.](http://commits.kde.org/messagelib/deaefb7461b164320973983f918fc7b09f4e0f62) 
+ Convert quote as html otherwise we can't see info. [Commit.](http://commits.kde.org/messagelib/22e69471d51cdc6388f217c27553202df8c22c8a) 
+ Remove unused includes. [Commit.](http://commits.kde.org/messagelib/29e41c93c461607cc2647aebf8957285d15d82b1) 
+ Fix typo. [Commit.](http://commits.kde.org/messagelib/f20f5d19c682acd5ee08d6003ce55c8dcbd0bf6f) 
+ Fix crash when switching signed/encrypted emails very fast. [Commit.](http://commits.kde.org/messagelib/624cee41de9780e73951913282eceb376fd34788) Fixes bug [#463083](https://bugs.kde.org/463083)
+ Align descript message and lock icon. [Commit.](http://commits.kde.org/messagelib/f5cff7a09d96ace06a501c209715ed36cc8d1e1a) 
+ Debug--. [Commit.](http://commits.kde.org/messagelib/7fd1371c72337b48b564d2ba1069031526b8d3b6) 
+ Const'ify variable. Initialize variable. [Commit.](http://commits.kde.org/messagelib/2e96fac2fbf3f0951510db74f41e8faf32bb0562) 
+ Add more autotests. [Commit.](http://commits.kde.org/messagelib/4172ed555eeec2afaae17de9ee7d0013484ea338) 
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Focus message search window's search field by default. [Commit.](http://commits.kde.org/neochat/5a1a3fbd06ca485396aac493aa29629086411b98) Fixes bug [#469879](https://bugs.kde.org/469879)
+ Fix asan runtime error in texthandler. [Commit.](http://commits.kde.org/neochat/ec76df5e475b597467fcb7310f0b9461f77219ea) 
+ Update appstream description. [Commit.](http://commits.kde.org/neochat/1243aef24331faa257253b7fefdcaf3b72c5df5e) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Bump c++ standard version. Poppler is about to use c++17 types in the api, so better be prepared. [Commit.](http://commits.kde.org/okular/cf13c2f6991fa3039906ca36ac31b3866de6cce9) 
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Do not hardcode English as the default OCR language. [Commit.](http://commits.kde.org/skanpage/a3c1e88a67d0feee0d1801b9f19a584b86e481c8) Fixes bug [#469865](https://bugs.kde.org/469865)
{{< /details >}}
{{< details id="spectacle" title="spectacle" link="https://commits.kde.org/spectacle" >}}
+ Fix the capture option panel width for some languages. [Commit.](http://commits.kde.org/spectacle/1e7fed15121320a475845f45acbe1f11a463d615) 
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Fix obvious copy/paste mistakes. [Commit.](http://commits.kde.org/tokodon/1b2f9254743e8920b2589a8ae673a22ecb6d1a06) 
+ When going to separate app pages, clear the whole pageStack. [Commit.](http://commits.kde.org/tokodon/566a2ae763ff6777a2b48ebf796e2e227933dd68) 
+ Squash yet another duplicate accounts bug. [Commit.](http://commits.kde.org/tokodon/0c7176debb260079fc1123a3bd899a1ca17c16c7) Fixes bug [#466440](https://bugs.kde.org/466440)
+ Fix reading invalid accounts from conf. [Commit.](http://commits.kde.org/tokodon/3800422858fdfd098546b334330a4b836773d70e) 
{{< /details >}}
{{< details id="yakuake" title="yakuake" link="https://commits.kde.org/yakuake" >}}
+ Prevent unnecessary tab switching on tab close. [Commit.](http://commits.kde.org/yakuake/baaac15a8f52d044dc6a712c96e2c68fc4209315) Fixes bug [#392626](https://bugs.kde.org/392626)
+ Fix access of negative 'screens' offset. [Commit.](http://commits.kde.org/yakuake/31610d47c457aa93428d9e0ba89496303a2bfcef) 
{{< /details >}}
