---
aliases:
- /announcements/plasma-5.6.0-5.6.1-changelog
hidden: true
plasma: true
title: Plasma 5.6.1 Complete Changelog
type: fulllog
version: 5.6.1
---

### <a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a>

- KCM Devices: Fix tooltip text for add/remove buttons. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=f9f44abf93a68e9c2d91e01cbd2c6719138941f3'>Commit.</a>

### <a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a>

- Fix drawing correct focus state of QtQuickControls Button. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=24cbc7312a5f77ea3997e49a84223a4deb315fcb'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127494'>#127494</a>
- Fix flags compare in drawTitleBarComplexControl. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c4a67d1f613b10c77d1f763f6dcc053a9b664351'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127500'>#127500</a>
- Fix 1px offset in QtQuickControls TabView frame. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=65a0c6256729a9cffa89702dbda518424de2efc0'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127464'>#127464</a>
- Expand size of CT_ItemViewItem also with QtQuickControls. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=9e7983a0896aa13d859b413a888a325bba177c0a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127463'>#127463</a>
- Fix drawing QtQuickControls ComboBox popups. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c2008d6d3a62b3f7f6e5198e047d39b4a40ecf7e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343369'>#343369</a>. Code review <a href='https://git.reviewboard.kde.org/r/127460'>#127460</a>
- For buttons that have neither icon nor text, assume custom button and use provided. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=84d19e95e3e7b6df64ea4aa2f44a8a8f6a8c92ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360061'>#360061</a>

### <a name='breeze-gtk' href='http://quickgit.kde.org/?p=breeze-gtk.git'>Breeze GTK</a>

- Fix installation of breeze-dark. <a href='http://quickgit.kde.org/?p=breeze-gtk.git&amp;a=commit&amp;h=9a6c73d8c418333992f9d1d872db26a64aed8d3d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127465'>#127465</a>

### <a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a>

- Use QtQuick.Layouts instead of HTML to layout files. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a53e0fb94434df388eee5a4d03b2528916a76f86'>Commit.</a>
- Don't remove the Science applications from the Education section. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ac1b53c4c5c6c48085ef09b1a41a986eac6998f6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360920'>#360920</a>
- Add test information. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6510b1ec769abce6062a1382fd25f461d14ce9fe'>Commit.</a>
- Make sure we don't change the progressing attribute before starting the update. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=19a8b4280e415fa5a38fd6f0dc7a84cce46190b5'>Commit.</a>
- Properly define what's the required Layouts version. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1ecd71d2431fe55da7fd03dc95587a46464ccd05'>Commit.</a>
- Improve test, to see if we can figure out what's going on in the CI. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=ef3fdff037adf2998d78301a1000cd50ba2ee405'>Commit.</a>
- Fall back to PackageKit when AppStream doesn't have information. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=534553506115a4edf7032752fcb555e33a18a8b2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360919'>#360919</a>
- Fix warnings. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=2905b6478c8ce393d7f6f8d8c3615228863723f6'>Commit.</a>
- Disable icon if the button is disabled. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=efe62115755906e4252d82ad4270fed1dcea3e19'>Commit.</a>
- Introduce a placeholder icon when appstream isn't providing an icon. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f18aa6665ee6e44941c31b20089584f3a86bc33b'>Commit.</a>
- No need to store some information in every resource. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=459aecec1cc8de683704150d24f9b8cb2140bf48'>Commit.</a>
- Remove some deprecated constructions. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=b873c53c42024363eefc652acb65c97f6f601e47'>Commit.</a>
- Improve display of the applications list view. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fcec6ef487376203df36145f650c0955169dbe82'>Commit.</a>
- Improve combined search and navigate behaviour. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5a9968754e29a4dd758c03aab960fa1e18f53664'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360085'>#360085</a>
- --warnings, --debug. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0b1ffc1db13f66bd48b1652d9f28f5cb5857f366'>Commit.</a>
- Offer some kind of ratings on the PackageKit backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=d68cb8a556e00ab8ff88679548d21a2cc0bb17d1'>Commit.</a>
- Move the popcon parsing code to libdiscover. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=dd161d5ac3222850ec4c9f5ac61591295480b40b'>Commit.</a>
- Reduce certain memory allocations. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1545ed4cb846bd98dd42e7767c0d4d7262c6bf0f'>Commit.</a>

### <a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a>

- [showdesktop] Fix sizing of the plasmoid in the panel. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e164ae7c02f5579e02d26842bee6a4a450e01871'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127499'>#127499</a>
- Fix 'Save as' option. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e469849a225a49e971e65ac84469b84b62de9e58'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127403'>#127403</a>

### <a name='khelpcenter' href='http://quickgit.kde.org/?p=khelpcenter.git'>KHelpCenter</a>

- Use KLocalizedString::languages() for language list. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=734b150c4a580553c67763977419d1ee66001ce5'>Commit.</a>

### <a name='khotkeys' href='http://quickgit.kde.org/?p=khotkeys.git'>KDE Hotkeys</a>

- Explicitly set HAVE_XTEST in CMakeLists.txt, fixing mouse button faking in KHotkeys. <a href='http://quickgit.kde.org/?p=khotkeys.git&amp;a=commit&amp;h=c788a3e65e573b881cb60eebbe59c340da1835b6'>Commit.</a>

### <a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>Info Center</a>

- Add alignment for label "KDE Frameworks Version". <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=c1de614956cbcdb6701b97d9bf1c1ae96df4bf22'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360858'>#360858</a>

### <a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a>

- Revert "desktop grid: zoom hovered window". <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5fecaf3fe7699886928fbc160dc69ea1da5ac8c1'>Commit.</a>
- Use a global static for animation ids. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=a9fad7396e30a380a27090538e7e66dec6984c52'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127276'>#127276</a>. Fixes bug <a href='https://bugs.kde.org/360068'>#360068</a>. See bug <a href='https://bugs.kde.org/352254'>#352254</a>
- Ensure panel, desktop and onscreendisplay windows cannot be moved/resized. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=23c505d71e376e0153ef8b008c8095125014966a'>Commit.</a>
- [Window Rules] Fix simple shortcut not being transfered to text field. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=f9da3fb0eb76dc9bb66c06b745a310d09689161f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360521'>#360521</a>

### <a name='milou' href='http://quickgit.kde.org/?p=milou.git'>Milou</a>

- [Milou] Fix arrow keys being inverted when in a bottom panel. <a href='http://quickgit.kde.org/?p=milou.git&amp;a=commit&amp;h=c5bfb95058ec58bfe4fcf41bda8420db2b579cc7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360789'>#360789</a>

### <a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a>

- Fix crash in relativeActivity. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=45bf19ea89f4fadd778d1561d0b0c59f20b3ed37'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361056'>#361056</a>
- TaskManager: Force grouping for Icons-Only Task Manager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b6a4964c5f218911475804c342995a58b38f566a'>Commit.</a>
- Kcm_activities: Make QML-defined UI translatable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=461f06f715f185bac9c14e408c71003f4e5d2d63'>Commit.</a>
- Kcms/activities: Fix wrong usage of CMake macro ki18n_wrap_ui(). <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=67c078bea2fe462b1bcd5ec9aed4074e3c5fd1eb'>Commit.</a>
- Fixuifiles. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=de7a20f848dd41d75a1251234ea91eaa6f03e3b8'>Commit.</a>
- Fix memory leak in Kicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1db2c0324fbb81851106a2118bb256322e4bdd5d'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127512'>#127512</a>
- Handle non-existing kickoffrc correctly. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=fc9a550fbd0cb51f5342972948c38129de1bac35'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357029'>#357029</a>
- Set config group. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bc28403a08c81d2c07c4582622d9c138ede24704'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357029'>#357029</a>
- Make kcm_activities translatable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=4f52285b8ba71f0130ae42ce01e72783996372e9'>Commit.</a>
- Make activity manager translatable. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0ada3d5c941b0e43fd8babb7875dd4432565d71a'>Commit.</a>
- [Task Manager] Initiate drag only with left mouse button drag. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=57d7d66445ef7b2629a068e4b69ce927ddb1aee9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360720'>#360720</a>
- Fix untranslatable string. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=027e9dc2f29c1997f67e553e8d3643465f7f031f'>Commit.</a>
- KActivitiesExperimentalStats translation domain changed. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5fb1c39add6bcf4e0cde91156140a8783907af68'>Commit.</a>

### <a name='plasma-integration' href='http://quickgit.kde.org/?p=plasma-integration.git'>plasma-integration</a>

- Remove possible file-name in KDEPlatformFileDialog::setDirectory(). <a href='http://quickgit.kde.org/?p=plasma-integration.git&amp;a=commit&amp;h=18e62e74e840f3353dfd88a7f2c6b8b878e8511f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126876'>#126876</a>
- Don't filter by name if we have mime types. <a href='http://quickgit.kde.org/?p=plasma-integration.git&amp;a=commit&amp;h=25be75542f831863e905b590b6429127df1b13d3'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127024'>#127024</a>
- Don't create ServerSideDecoration for non-toplevel QWindows. <a href='http://quickgit.kde.org/?p=plasma-integration.git&amp;a=commit&amp;h=ef045b3a2d60711541c8a6b8ea96690f155fc982'>Commit.</a>

### <a name='plasma-pa' href='http://quickgit.kde.org/?p=plasma-pa.git'>Plasma Audio Volume Control</a>

- [ListItemBase] Increase/decrease volume by scroll wheel, removing MouseArea. <a href='http://quickgit.kde.org/?p=plasma-pa.git&amp;a=commit&amp;h=b75cbce1cbfeb4962f616defde0f0297f28d3147'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127476'>#127476</a>

### <a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a>

- Engineexplorer: Fix i18n of .ui files. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=d86ffb90db778d5e86149a093e0184c5b21451be'>Commit.</a>
- Engineexplorer: Set translation domain for application. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=8c5760061bec9bffb95b9a35d997435683eb151a'>Commit.</a>

### <a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a>

- Disable text wrapping in BreezeHeading. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=1c9ad0a207ea68e89010578b9eff68417b19841d'>Commit.</a>
- PanelView: Skip invalid screen geometries in updateStruts. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=d2f3147fd27ddb6b49f98592d4148fff306b0ebe'>Commit.</a> See bug <a href='https://bugs.kde.org/348043'>#348043</a>
- Modify keyboard focus order in lock screen. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b553843aeda4c07013819ba5cd342b1ec508ea24'>Commit.</a>
- [DrKonqi] Reliably quit when closing report assistant. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=b7f7b4e18947b9abe528ab6424fd6bd1ade590d8'>Commit.</a>
- Fix length returned from PanelView::geometryByDistance. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3c9d4a1db209a24672f108cae88f1955ebf7bae9'>Commit.</a>