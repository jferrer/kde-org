---
aliases:
- /announcements/plasma-5.11.2-5.11.3-changelog
hidden: true
plasma: true
title: Plasma 5.11.3 Complete Changelog
type: fulllog
version: 5.11.3
---

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Don't refresh invalid backends. <a href='https://commits.kde.org/discover/785a1bb0ed14f70afcdbc93e4d09e3cb125cc520'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386045'>#386045</a>
- Remove unused class. <a href='https://commits.kde.org/discover/4e019b58264410f2f063d4cef0b4a21c8a6d2471'>Commit.</a>
- Don't delete resources whenever asked to clear the view. <a href='https://commits.kde.org/discover/93d990f560075c740789f9c4c66992a9d7bbca79'>Commit.</a>
- Extend the resetModel until the model is ready to be fetched again. <a href='https://commits.kde.org/discover/cbd001c49e1a7368c7fb03e7f1afbe1f2cd0a7ea'>Commit.</a>
- Make sure we set the page size. <a href='https://commits.kde.org/discover/9bf95fc48fe71c5be80afbf789ced924074ad1d6'>Commit.</a>
- Unneeded dependency in the library. <a href='https://commits.kde.org/discover/2ce69cd06c5736904464113a4a8f662ccf18e294'>Commit.</a>
- Fix warning. <a href='https://commits.kde.org/discover/c850a428c674d83a16f516473bcc9aa1203393ec'>Commit.</a>
- Remove unused code. <a href='https://commits.kde.org/discover/eac61fed3e9fef241a17e60f9ef4d3178fd84793'>Commit.</a>
- --warning: don't capture unused variable. <a href='https://commits.kde.org/discover/7142b9162fc4d5af469a5a02b8878e4f7d3950ae'>Commit.</a>
- Properly listen to ODRS replies. <a href='https://commits.kde.org/discover/42eecb7d667d96702ec5eae8dcd270b6494419af'>Commit.</a>
- Assert on ODRS reviews fetched. <a href='https://commits.kde.org/discover/1b57c69ed11454e28479434bbda116a524d1432e'>Commit.</a> See bug <a href='https://bugs.kde.org/385212'>#385212</a>
- Don't leak QNetworkAccessManager instances. <a href='https://commits.kde.org/discover/33678af51d3863edcf400fe39f11e850229119c1'>Commit.</a>
- Recover the right delegate background color. <a href='https://commits.kde.org/discover/b677afaabcf1b0818f067bb362389a4c9c13f77d'>Commit.</a>
- Improve global updates progress reporting. <a href='https://commits.kde.org/discover/6ca2bde0933d29933220d789d6d6490b69bc4ed8'>Commit.</a>
- Improve progress reporting in some PackageKit updates. <a href='https://commits.kde.org/discover/c5117367d2a9459444bd18e1785db68838e909c9'>Commit.</a>
- Show errors from refreshing. <a href='https://commits.kde.org/discover/899c821a2c816461f93c03f1c4c4efabb950670d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386244'>#386244</a>
- Include the progress of transactions. <a href='https://commits.kde.org/discover/44f03fc0fbcae5767d0d50fe1d20175614523266'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/384038'>#384038</a>

### <a name='kactivitymanagerd' href='https://commits.kde.org/kactivitymanagerd'>kactivitymanagerd</a>

- [resources] Nicely quit threads. <a href='https://commits.kde.org/kactivitymanagerd/27c0245b1715044cf4d401f1c9d7e7a915a4f3c5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8561'>D8561</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- Be flexible to systems without a gtkrc file in /etc. <a href='https://commits.kde.org/kde-gtk-config/952ab8f36e3c52a7ac6830ffd5b5c65f71fa0931'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382291'>#382291</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- Use wakeOne() instead of notify_one(). <a href='https://commits.kde.org/kdeplasma-addons/961078337c823e22691d74925a698d1f28f20ac0'>Commit.</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Don't re-align outputs when QMLOutput in clone mode moves. <a href='https://commits.kde.org/kscreen/baf36bc421b8b747baaa1168d8278e8d2979019e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8390'>D8390</a>
- Keep the unified QMLOutput always in center. <a href='https://commits.kde.org/kscreen/1a9164cdc7d138e9bcddc832bfb017964e0c367d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8391'>D8391</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- Don't use the global KConfig object on the OpenGL freeze detection thread. <a href='https://commits.kde.org/kwin/a7117e430454f8e39b1279695ff83bf73e1eb9ed'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372114'>#372114</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8356'>D8356</a>
- Fix rendering issues of subsurfaces by using proper clipping. <a href='https://commits.kde.org/kwin/d71792378e9aa9c99874092fda151bc66efa3df8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385924'>#385924</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8369'>D8369</a>
- Fix viewport for screenshot on multiple screens. <a href='https://commits.kde.org/kwin/9cafbb117984f746d4b71213effc3d27e1435f36'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8485'>D8485</a>
- Set scale on glScissor calls. <a href='https://commits.kde.org/kwin/69b2c2fceaba8f29216efe5df01318460a06231b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8490'>D8490</a>
- Fix glScissor calls with y-offset screens. <a href='https://commits.kde.org/kwin/019d4bf4257da2c5a601e6ec03c0134a606d5d74'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8503'>D8503</a>
- Fix viewport on vertically stacked monitors. <a href='https://commits.kde.org/kwin/5d7e22e5dcf8981f9ba65088b81c1c6d456efef3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386099'>#386099</a>. Fixes bug <a href='https://bugs.kde.org/385655'>#385655</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8479'>D8479</a>

### <a name='milou' href='https://commits.kde.org/milou'>Milou</a>

- Fix i18ncd -> i18ndc. <a href='https://commits.kde.org/milou/3bb29aca376e756b2a31dc4c969774ca4f279eea'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Default X font DPI to 96 on wayland. <a href='https://commits.kde.org/plasma-desktop/fae658ae90bf855b391061a5332a1a964045e914'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/8287'>8287</a>

### <a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a>

- Show shortcuts in menus. <a href='https://commits.kde.org/plasma-integration/59d0ad3064bb3c45d7e9ab0604e65a31e5a24155'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8466'>D8466</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Do not set password field as readonly, it doesn't make sense. <a href='https://commits.kde.org/plasma-nm/abd4e400910268a284bb4b47e77674eb4dc9da38'>Commit.</a>
- Disable password field when password is not needed. <a href='https://commits.kde.org/plasma-nm/8ae559e98598cdecf6181cef5fe5f07111c740bf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386426'>#386426</a>
- Make sure we store password for all users when kwallet is disabled. <a href='https://commits.kde.org/plasma-nm/ead62e9582092709472e1e76eb3940d742ea808b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386343'>#386343</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- DictEngine: set a scheme for the URLs, otherwise webengine won't handle them. <a href='https://commits.kde.org/plasma-workspace/208ad519efa8c7d6d59e9390dda757793f72631b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8534'>D8534</a>
- Align logout screen action buttons to the top. <a href='https://commits.kde.org/plasma-workspace/3e495b220fd09cdafa9873393be5c9e3fb055d44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371249'>#371249</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8397'>D8397</a>
- Sync xwayland DPI font to wayland dpi. <a href='https://commits.kde.org/plasma-workspace/a09ddff824a076b395fc7bfa801f81eaf2b8ea42'>Commit.</a>

### <a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a>

- Map rows to the source model. <a href='https://commits.kde.org/systemsettings/5ece4a6d3c8a179a460126b91d16aa356b49257f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/386454'>#386454</a>
- Fix DBus startup notification. <a href='https://commits.kde.org/systemsettings/1490607667954f9d5bf2b9ec6d466b89b9b582c4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8455'>D8455</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- Email portal: add support for attachments. <a href='https://commits.kde.org/xdg-desktop-portal-kde/18fb62cc7af2362fdfde2d984822f026eb56e171'>Commit.</a>