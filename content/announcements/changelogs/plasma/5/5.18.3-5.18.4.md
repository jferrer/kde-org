---
aliases:
- /announcements/plasma-5.18.3-5.18.4-changelog
hidden: true
plasma: true
title: Plasma 5.18.4 Complete Changelog
type: fulllog
version: 5.18.4
---

### <a name='bluedevil' href='https://commits.kde.org/bluedevil'>Bluedevil</a>

- Remove bold formatting of device name. <a href='https://commits.kde.org/bluedevil/c190802e1e0a3c1685b666e6f106dc8db10afd7f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27574'>D27574</a>

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Fix build with Qt 5.15. <a href='https://commits.kde.org/breeze/590203f1116066c7a4093010692a0d54b8c47a8f'>Commit.</a>
- Fix Defaults not being set properly in Breeze window decoration settings for 'Draw a circle around close button'. <a href='https://commits.kde.org/breeze/f944832fd0e51924ec1d0fe5654cd8d360fd0b97'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28087'>D28087</a>

### <a name='discover' href='https://commits.kde.org/discover'>Discover</a>

- Fix toplevels test. <a href='https://commits.kde.org/discover/1d3ebb139beedc0341fdcb1f521737579f2d049b'>Commit.</a>
- Fwupd: improve error message. <a href='https://commits.kde.org/discover/e1c145586ca33aaddffe1ee2a67788e21147ee70'>Commit.</a>
- Fwupd: improve cancellable usage. <a href='https://commits.kde.org/discover/6881fe5f8102655fb6b19257ab50e3228e1b58ea'>Commit.</a>
- Flatpak: Don't insist on performing tasks after cancellation. <a href='https://commits.kde.org/discover/a78788762f4babc7c11787216b754ae1443f175e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419062'>#419062</a>
- Odrs: Remove seemingly unused cache files. <a href='https://commits.kde.org/discover/a79eb4efa4a675bcf3f36b678866698a2da8f87c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28219'>D28219</a>
- Flatpak: fix crash. <a href='https://commits.kde.org/discover/13e034256c7713f9a3e161585d6a762bf7e4b547'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419107'>#419107</a>
- Cmake: Include messages about availability of Flatpak and Fwupd. <a href='https://commits.kde.org/discover/7776d3c9bcda9f838bc8d4689a3e118fa9ed809b'>Commit.</a>

### <a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a>

- Display PID & signal numbers unlocalized. <a href='https://commits.kde.org/drkonqi/af4cd7a4cf5f948a9d1947f692b61f94258774a2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28142'>D28142</a>
- Remove pointless and arbitrary 4 line frame limit. <a href='https://commits.kde.org/drkonqi/80efb36bc807a147756d4bf7196281938720a10f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28098'>D28098</a>

### <a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a>

- [GTK Config] Construct font style by hand instead of relying on Qt function. <a href='https://commits.kde.org/kde-gtk-config/a581035b3f4793d96e9b5d2cf6b55191cbb4be91'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/333146'>#333146</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27380'>D27380</a>
- [GTK Config] Add XSettingsd as a runtime dependency. <a href='https://commits.kde.org/kde-gtk-config/b6e7c6f056740e936adc94b7a843f6448f6830ab'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/418263'>#418263</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Only add devices to the smb mount model that are network shares. <a href='https://commits.kde.org/kinfocenter/23d52fa333941021916d57bc23edc51ad41eb52e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28276'>D28276</a>
- Skip over supposed network shares that aren't. <a href='https://commits.kde.org/kinfocenter/4847c9e2fed215f8d2bfe8bfd5a58c0f62d5d3a9'>Commit.</a> See bug <a href='https://bugs.kde.org/419220'>#419220</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28275'>D28275</a>
- Be more accurate in reporting the x86 CPU features solid detects. <a href='https://commits.kde.org/kinfocenter/53ff92062221c287bbd75432b5540a50072b6d36'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28059'>D28059</a>

### <a name='kwallet-pam' href='https://commits.kde.org/kwallet-pam'>kwallet-pam</a>

- Make kwallet-pam work with pam_fscrypt. <a href='https://commits.kde.org/kwallet-pam/2bb4c6dc870ff59c6f62c4b12bf9be229d9ff8da'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D27935'>D27935</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [kcmkwin/kwindesktop] Fix inability to create more than one row on the "Virtual Desktops" settings page. <a href='https://commits.kde.org/kwin/ae022e92c9f516877fa59d5104a84c981895cddf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419141'>#419141</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28378'>D28378</a>
- [wayland] Recursively destroy WindowPixmap objects. <a href='https://commits.kde.org/kwin/25276d30583c2b5d5c15aba077511458011d91d4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28145'>D28145</a>
- [scenes/opengl] Print a debug message when viewport limits aren't met. <a href='https://commits.kde.org/kwin/348e72c56ec7e157cec15d8e27065e4194490d89'>Commit.</a> See bug <a href='https://bugs.kde.org/418951'>#418951</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28139'>D28139</a>

### <a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a>

- Fix build with Qt 5.15. <a href='https://commits.kde.org/libksysguard/4cbb3e1a0afc1b5ea779c5432405ab4e269b169d'>Commit.</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- Enable wrapping of error messages which use KMessageWidget. <a href='https://commits.kde.org/plasma-desktop/d9bf6f53a0360cb61a3f98708871750467f6c995'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28200'>D28200</a>
- KCM/touchpad: Disables the middleEmulation when not supported. <a href='https://commits.kde.org/plasma-desktop/73f3855ed3c59dd08435227d33a88d872ece8080'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28297'>D28297</a>
- KCM/Mouse: fix the middleEmulation checkbox status. <a href='https://commits.kde.org/plasma-desktop/fb86174858b51f74eb9b8aee87ea556dbd8f1ce7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28296'>D28296</a>
- [Colors KCM] Also save colors when active scheme was edited. <a href='https://commits.kde.org/plasma-desktop/d0f673d0e92b297afb6cd0efa7547d256d3a6252'>Commit.</a> See bug <a href='https://bugs.kde.org/419168'>#419168</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28270'>D28270</a>
- [Cursor Theme KCM] Use standardized pointer and help cursor for preview. <a href='https://commits.kde.org/plasma-desktop/10c1a42b2cbfe4e3fdfc61e628517371c942b60e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28158'>D28158</a>
- [Icons KCM] Floor delegate height. <a href='https://commits.kde.org/plasma-desktop/c9f545a3c537a1a2fed29210f13ad9f10fcb4bed'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28120'>D28120</a>
- Make panel edit mode tooltip not unexpectedly disappear under certain circumstances. <a href='https://commits.kde.org/plasma-desktop/9768200d9a27e4e391ce9aa3cf261db95d99b0c8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413736'>#413736</a>. Phabricator Code review <a href='https://phabricator.kde.org/D27877'>D27877</a>

### <a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a>

- Openconnect: make sure the UI fits into the password dialog. <a href='https://commits.kde.org/plasma-nm/42681b384d72c244b63bf81e03efc723a56f2a8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403480'>#403480</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [Image Wallpaper] Fix thumbnail generation when model is reloaded in-flight. <a href='https://commits.kde.org/plasma-workspace/492301406a4656fbc6c9a1be0e77e68c5535bf93'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419234'>#419234</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28420'>D28420</a>
- [runners/recentdocuments] disable executables or .desktop files. <a href='https://commits.kde.org/plasma-workspace/97bf7d777e56a451eb91731d9209fb1d55689957'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419310'>#419310</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28369'>D28369</a>
- [applets/digital-clock] Fix inappropriately high QtQuick version. <a href='https://commits.kde.org/plasma-workspace/fbfea1112f929e0c636fde8556c5cdce70669deb'>Commit.</a>
- [applets/systemtray] Clear item from shown/hidden list when disabling entry. <a href='https://commits.kde.org/plasma-workspace/fede85a3d0ea5a30755582e947cabd6bc7d1e4b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419197'>#419197</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28311'>D28311</a>
- [applets/digital-clock] Fix date sizing in vertical panel. <a href='https://commits.kde.org/plasma-workspace/01691632b5238ac993b59eebe693fefc88130b8f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/417852'>#417852</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28172'>D28172</a>
- [VirtualDesktopInfo] Connect to rowsChanged singal. <a href='https://commits.kde.org/plasma-workspace/bf28d5d2b4f45108db65fe72d7efbdbdc23a5dc6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408783'>#408783</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28216'>D28216</a>
- Fix systemtray configuration. <a href='https://commits.kde.org/plasma-workspace/ce69fc4ea9146e8a60a5fe1827476ad4cc10dfd9'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D26923'>D26923</a>

### <a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a>

- FileChooser: make all opened files automatically writable. <a href='https://commits.kde.org/xdg-desktop-portal-kde/40a6e84e3ff12b4500e85c1723d297444b174106'>Commit.</a>
- Screensharing: code cleanup. <a href='https://commits.kde.org/xdg-desktop-portal-kde/8a2286317edf91017a4854814340c48c4552fafe'>Commit.</a>
- Avoid copying buffer twice. <a href='https://commits.kde.org/xdg-desktop-portal-kde/45092f756316a6b6718a2da5d319355d94abca45'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/419209'>#419209</a>. Phabricator Code review <a href='https://phabricator.kde.org/D28272'>D28272</a>
- Implement Keyboard Keycode support as suggested in the spec. <a href='https://commits.kde.org/xdg-desktop-portal-kde/cdd01df4e5397d52ecf544bddedafceb741c9f2f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D28176'>D28176</a>