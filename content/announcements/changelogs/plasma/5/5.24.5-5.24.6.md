---
title: Plasma 5.24.6 complete changelog
version: 5.24.6
hidden: true
plasma: true
type: fulllog
---
{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Remove redundant saveState call. [Commit.](http://commits.kde.org/bluedevil/d84ab44c9ce53f7c119b41bf58cc7ff7a0254151) Fixes bug [#454709](https://bugs.kde.org/454709)
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ Add cursor 'tcross' -> crosshair. [Commit.](http://commits.kde.org/breeze/613be7a939775a0b88c53e398965ef9ceb286cdf) Fixes bug [#452102](https://bugs.kde.org/452102)
+ Fix mixup of PM_ToolBarItemMargin & PM_ToolBarFrameWidth. [Commit.](http://commits.kde.org/breeze/3d96c3356358051c8d65181a4c90cb722d123f55) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Hide the search field on the SearchPage on wide mode. [Commit.](http://commits.kde.org/discover/7226c29603f4cccf4d364f5e9472c3827a4440dd) Fixes bug [#449770](https://bugs.kde.org/449770)
+ Correct vertical position for titleDelegate items. [Commit.](http://commits.kde.org/discover/c668e17ffdca7cd52c4b417728dc9d057ea81357) 
+ Updates: Do not reboot if one of the update transactions failed. [Commit.](http://commits.kde.org/discover/58b0b9ee2e619a5f2f0ca8dc42b1e413b253c6d5) Fixes bug [#453250](https://bugs.kde.org/453250)
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/potd: Give random chooser a seed in flickrprovider. [Commit.](http://commits.kde.org/kdeplasma-addons/326ae3a24ccb1f1b526f4742a84e4ce24046effd) Fixes bug [#453391](https://bugs.kde.org/453391)
+ Wallpapers/potd: Give random chooser a seed in simonstalenhagprovider. [Commit.](http://commits.kde.org/kdeplasma-addons/d3bb3dc1d96ef7d734503e6d78dc3ce35106a628) See bug [#453391](https://bugs.kde.org/453391)
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ X11: fix kded xcb resource leak. [Commit.](http://commits.kde.org/kscreen/93cfd297506e4106a7b6e7bcc649442c5bf0f7d1) Fixes bug [#453280](https://bugs.kde.org/453280)
+ Fix connecting external monitors when a monitor is rotated. [Commit.](http://commits.kde.org/kscreen/ff8dc215e8d2691fcf41d0bb305f820531d95150) 
+ Kcm: fix choosing the refresh rate. [Commit.](http://commits.kde.org/kscreen/6ecb832923612820c721f58d1d12dd176e10528a) 
+ Kcm: fix refresh rate list not being updated. [Commit.](http://commits.kde.org/kscreen/584ed8a067a7c0329e1572dd9970b6e3dae6a56f) Fixes bug [#453392](https://bugs.kde.org/453392)
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Set proper initial values for many SensorProperties. [Commit.](http://commits.kde.org/ksystemstats/118c4bdf3b46bf20e93d02bdc4470eac1fa80baa) Fixes bug [#446414](https://bugs.kde.org/446414)
{{< /details >}}

{{< details title="kwayland-server" href="https://commits.kde.org/kwayland-server" >}}
+ Wayland: Fix supported action initialization in data offer interface. [Commit.](http://commits.kde.org/kwayland-server/f669d7002b3966f53f8e17275123b24ec41e8e21) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Ignore fake input devices when detecting tablet mode. [Commit.](http://commits.kde.org/kwin/fd7ba1560d4557b71fa19c9f6cb73779c6ac1eb0) 
+ Fix typo in latency policy. [Commit.](http://commits.kde.org/kwin/47671af37b5c50a594e2a5fe4e9acb50c70734a6) Fixes bug [#453694](https://bugs.kde.org/453694)
+ Toplevel: set valid output in the constructor. [Commit.](http://commits.kde.org/kwin/a8477c1cf7acbf3358c85e53b236150dd43b4640) Fixes bug [#452433](https://bugs.kde.org/452433). Fixes bug [#448064](https://bugs.kde.org/448064)
+ Ensure that Toplevel::output() stays always in sync with geometry. [Commit.](http://commits.kde.org/kwin/4c3195270d6c8e1da8c3e2e3abe5aae75d5bf3c2) Fixes bug [#448064](https://bugs.kde.org/448064)
+ Update device outputName when setting output. [Commit.](http://commits.kde.org/kwin/1b96d21507a3b6b0a9ac6eac424d4e4db5602839) Fixes bug [#451279](https://bugs.kde.org/451279)
+ Platforms/drm: set read buffer in GbmSurface::makeContextCurrent. [Commit.](http://commits.kde.org/kwin/e2d271b6365188a86755af82745ad37535ceb4d7) 
+ Effects/overview: hide panels. [Commit.](http://commits.kde.org/kwin/7fcf36d636f62f101397d88ad6c448f54da79e46) Fixes bug [#444274](https://bugs.kde.org/444274)
+ Tabbox: don't dismiss popups. [Commit.](http://commits.kde.org/kwin/f7b1edc79e9526fe6bad69a8ad475a5d77e5aead) Fixes bug [#446318](https://bugs.kde.org/446318)
+ Backends/drm: consider escaped separators KWIN_DRM_DEVICES. [Commit.](http://commits.kde.org/kwin/3b58fe4bde0722b352e4114adb992a2ed2c96423) See bug [#453386](https://bugs.kde.org/453386)
{{< /details >}}

{{< details title="layer-shell-qt" href="https://commits.kde.org/layer-shell-qt" >}}
+ Mark required deps as required. [Commit.](http://commits.kde.org/layer-shell-qt/7f555ba9bd4801da1c1df9a40b0c1133b23f3780) Fixes bug [#454912](https://bugs.kde.org/454912)
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Faces: Do not write resolved sensors to preset file. [Commit.](http://commits.kde.org/libksysguard/0d06f3fdda71e87d70a4ca8d45af6badb3ab4ee1) 
+ Faces: Write preset faceproperties to the right location. [Commit.](http://commits.kde.org/libksysguard/b94b0e11f9c0f718f109f37dad8a889301f3a59a) Fixes bug [#450745](https://bugs.kde.org/450745)
+ Fall back to power average if input is not available. [Commit.](http://commits.kde.org/libksysguard/88722b18c92f3a688a69abef9022cbcdcb5163bb) Fixes bug [#445920](https://bugs.kde.org/445920)
{{< /details >}}

{{< details title="Milou" href="https://commits.kde.org/milou" >}}
+ ResultDelegate: Fix height binding loop on multiline. [Commit.](http://commits.kde.org/milou/2a735f5f80a1c5b436685739af51ff19647a50fa) Fixes bug [#454507](https://bugs.kde.org/454507)
{{< /details >}}

{{< details title="Oxygen" href="https://commits.kde.org/oxygen" >}}
+ Fix mixup of PM_ToolBarItemMargin & PM_ToolBarFrameWidth. [Commit.](http://commits.kde.org/oxygen/7a5e4793ea6b55f821d945be6c3c02481088b75a) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [applets/pager] Round displayed window geometry to avoid size "jumps". [Commit.](http://commits.kde.org/plasma-desktop/f1287cc7d3d1a5f4060006f8d444c31a5932840c) Fixes bug [#456488](https://bugs.kde.org/456488)
+ [applets/pager] Fix switching desktops on drag & hover. [Commit.](http://commits.kde.org/plasma-desktop/3ac4cbb6cc7f30109efaaeb4b6f63ae3de26d515) Fixes bug [#416878](https://bugs.kde.org/416878)
+ Applets/kicker: check model count before porting old favorite items. [Commit.](http://commits.kde.org/plasma-desktop/b827895ffb55a8bc57bba1d7a56c9892bfc9d7c1) Fixes bug [#456411](https://bugs.kde.org/456411)
+ Desktoppackage: add `Accessible.name` to email button. [Commit.](http://commits.kde.org/plasma-desktop/0e78fd4c3c39113b7793a10e63ca1b7074516047) 
+ [kcms/componentchooser] Pass parent window to ksycoca progress dialog. [Commit.](http://commits.kde.org/plasma-desktop/eaa9a76dc7e6903c0ac09372267589a05e041603) 
+ Applets/kickoff: fix grid delegate tooltips not appearing on hover. [Commit.](http://commits.kde.org/plasma-desktop/62af766fb46c44974c4879a3237a0663a7c9a071) 
+ Fix includes when building the kglobalaccel dbus interface. [Commit.](http://commits.kde.org/plasma-desktop/399c1288114d083199db7058710fd61945e2a181) 
+ [kcms/tablet] Fix crash when opening KCM for the second time. [Commit.](http://commits.kde.org/plasma-desktop/2a8d51a4ea4ed612829ac5ef0ce08df34d5b5518) Fixes bug [#451233](https://bugs.kde.org/451233)
+ Applets/kickoff: Prevent empty menu from opening. [Commit.](http://commits.kde.org/plasma-desktop/fd16264cc544730bdad1199273f886195c7954b3) Fixes bug [#455927](https://bugs.kde.org/455927)
+ Make the Keyboard KCM config spare layout spinbox enable the Save button (Fixes #36). [Commit.](http://commits.kde.org/plasma-desktop/ab44fbe2ff33ce3f0d88d1f6c8a04d4e427f6571) 
+ Desktoppackage: enable Apply button only after the wallpaper plugin is changed. [Commit.](http://commits.kde.org/plasma-desktop/900ad5cf6e56a195d2d31272880e45456d4a44c7) 
+ Desktoppackage: avoid loading wallpaper settings again when the plugin is changed. [Commit.](http://commits.kde.org/plasma-desktop/988fe02de567b972a941bcb3d785cae5423c5e1d) See bug [#407619](https://bugs.kde.org/407619)
+ Desktoppackage: fix unable to apply wallpaper settings after switching layout. [Commit.](http://commits.kde.org/plasma-desktop/29c79a56fd0f79e0b9b35130b2ea899b5d26ce7c) Fixes bug [#407619](https://bugs.kde.org/407619)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ [kcms/icons] Pass parent window to ksycoca progress dialog. [Commit.](http://commits.kde.org/plasma-workspace/08f72f625af05a5fc2f58b38065996f4b3f5474e) 
+ Avoid crash in plasma-session teardown. [Commit.](http://commits.kde.org/plasma-workspace/effe12a06dbdfaa53a3f74c71f03a3dfa1e00de2) Fixes bug [#454159](https://bugs.kde.org/454159)
+ Applets/kicker: Get rid of a separator just above title menu item. [Commit.](http://commits.kde.org/plasma-workspace/ca2f02e29bd30c8ea643402c04714b0f0d2f5e67) Fixes bug [#449132](https://bugs.kde.org/449132)
+ Revert "Prevent panel going out of screen boundaries". [Commit.](http://commits.kde.org/plasma-workspace/24229d78de805626d871ac25400ad465d97f4b74) See bug [#438114](https://bugs.kde.org/438114)
+ Shell: refresh geometries of all `DesktopView` and `PanelView` when receiving `logicalDotsPerInchChanged`. [Commit.](http://commits.kde.org/plasma-workspace/cea5cba30890fab992dab2be6ea69521aef0eac0) Fixes bug [#450443](https://bugs.kde.org/450443)
+ Kcms/fonts: Fix font hinting preview. [Commit.](http://commits.kde.org/plasma-workspace/8faab6bc54b6acf38708fd30b66593d3ccaa03be) Fixes bug [#413673](https://bugs.kde.org/413673)
+ Fix missing index validity check for cursortheme. [Commit.](http://commits.kde.org/plasma-workspace/e4d2ed569273f626ac8971689d46a6328de50e6d) Fixes bug [#454829](https://bugs.kde.org/454829)
+ Shell: preserve wallpaper type after changing desktop layout. [Commit.](http://commits.kde.org/plasma-workspace/9f17db0e121c3e0a2f102df4af89cb788ceb9d07) Fixes bug [#454597](https://bugs.kde.org/454597)
+ Wallpapers/image: move `setaswallpaper.desktop.in` out of imagepackage. [Commit.](http://commits.kde.org/plasma-workspace/fabf8613f1c94bd23e26e42f1d15be0546ac02f2) See bug [#358038](https://bugs.kde.org/358038)
+ Revert "Change cursortheme installpath from ~/.icons to XdgTargetDir/icons". [Commit.](http://commits.kde.org/plasma-workspace/e2b9fe33f119818fd9c073d5cbc6c1aee30ae862) Fixes bug [#453765](https://bugs.kde.org/453765)
+ Runners/webshortcuts: Do not use static regex for normal webshortcuts query. [Commit.](http://commits.kde.org/plasma-workspace/f956c2364e22c63b1eb0d240cee43726db5f5e99) Fixes bug [#454596](https://bugs.kde.org/454596)
+ Wallpapers/image: use the aspect ratio of the screen it will change the wallpaper for. [Commit.](http://commits.kde.org/plasma-workspace/0b23a298089dc30ce97c8489481d79d93b70877e) Fixes bug [#452308](https://bugs.kde.org/452308)
+ Wallpapers/image: enable `reuseItems` in wallpaper `GridView` if possible. [Commit.](http://commits.kde.org/plasma-workspace/8372084d02e1aef7bbcd5d80548b4b3f4927f523) 
+ Applets/batterymonitor: make string translatable. [Commit.](http://commits.kde.org/plasma-workspace/cd33dc7bb166df1df6aaabd53e908de5018889c9) Fixes bug [#454114](https://bugs.kde.org/454114)
+ Applets/kicker: refresh RootModel on session backend state change. [Commit.](http://commits.kde.org/plasma-workspace/766d220d96075f3aa6cc5c864ed7e5a5a188c186) Fixes bug [#427530](https://bugs.kde.org/427530)
+ Kcms/desktoptheme: Fix reading of metadata in ThemesModel. [Commit.](http://commits.kde.org/plasma-workspace/ae7adb368f52c0b7bf510e3c9cf12e368940745a) See bug [#453830](https://bugs.kde.org/453830)
+ Kcms/desktoptheme: find metadata.json when loading ThemesModel. [Commit.](http://commits.kde.org/plasma-workspace/f77b7284e39b14b0e9a8b4d2b77c0b93b2c9ea59) Fixes bug [#453830](https://bugs.kde.org/453830)
+ Applets/systemtray: remove unused <KConfigLoader>. [Commit.](http://commits.kde.org/plasma-workspace/c4dedd107e5111a7b2f4852b170f6f9ebac3de07) 
+ Applets/systemtray: disconnect KConfigLoader signal when instance is about to be deleted. [Commit.](http://commits.kde.org/plasma-workspace/b7a148ec05e25a98a7e41c49191d5ffeeacaaf68) Fixes bug [#453726](https://bugs.kde.org/453726)
+ Wallpapers/image: Fix missing folder action buttons. [Commit.](http://commits.kde.org/plasma-workspace/e8ad72a8eecc27fb928db0fb6f833b4579466ed2) 
+ No code duplication. [Commit.](http://commits.kde.org/plasma-workspace/09a759ca9b4f4344a3c8c5dd05cb0d52496970f4) 
+ Unset iconName when icon can't be loaded. [Commit.](http://commits.kde.org/plasma-workspace/a227774bb853794f3a1cbd5e5ec9823bf260e7e1) 
+ Applets/systemtray: Prefer IconName over IconPixmap. [Commit.](http://commits.kde.org/plasma-workspace/919207c0dd88b618a07a0daf09917b8e8912ed87) Fixes bug [#418996](https://bugs.kde.org/418996)
+ Applets/appmenu: fix top-level menu text coloration. [Commit.](http://commits.kde.org/plasma-workspace/4d3f99558cff95259590e70dfbf854a479f772ce) Fixes bug [#453348](https://bugs.kde.org/453348)
+ [sddm-theme] KeyboardButton: Fix layout name not being shown. [Commit.](http://commits.kde.org/plasma-workspace/e2b631d43528c3e41d53c8a359b6129c616bc271) 
{{< /details >}}

{{< details title="SDDM KCM" href="https://commits.kde.org/sddm-kcm" >}}
+ Fix broken connect in NewStuff.Action QML component usage. [Commit.](http://commits.kde.org/sddm-kcm/bb3e1394ee642cabe9f2211c972df4625bdb00bf) Fixes bug [#454884](https://bugs.kde.org/454884)
+ Clear cache when syncing. [Commit.](http://commits.kde.org/sddm-kcm/6ced60fb5d8c8337e358814972bfe8f550ee8a83) Fixes bug [#440957](https://bugs.kde.org/440957)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Make sidebar tooltips respect the "Display informational tooltips" global setting. [Commit.](http://commits.kde.org/systemsettings/62de371709b847a5a6e8d96be55e12ac9e2e4170) Fixes bug [#455073](https://bugs.kde.org/455073)
+ Fix activating running instance on Wayland. [Commit.](http://commits.kde.org/systemsettings/c48ecf5efb941cb3056da8f2c074005cb87417aa) 
+ Fix current index in subcategory list. [Commit.](http://commits.kde.org/systemsettings/684004217fd28ba45f7ce61df5b3600398ee1108) Fixes bug [#453543](https://bugs.kde.org/453543)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ RemoteDesktopDialog: fix "withTouch" property mismatch. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/2a211837113938f0c6147a6046646cf5a2498908) 
+ [filechooser] Make sure outgoing URIs are encoded. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4f88bd08dbd24a75e84a87b299535414f4ee1477) Fixes bug [#454850](https://bugs.kde.org/454850)
+ [screenshot] Encode result URI. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/09916db1178231bb05d1816fe42086f74a08c58e) 
+ UserInfo: Fix initialization error. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/3b734677d9688067411100c3559af16c5961ad7a) 
+ Screencast: When closing a session, only close the streams from that session. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/cd8275c722aa25dd22353121131c914d3ab3b438) 
+ Backport of 5f7fe7482ae3de6e5dabbd2712c283439b0eecac. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/3dbd366516c38d987025623feea18f79c15d72dd) 
+ Screencast: When we stop a stream, do it actively. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/d74e7dce1d333107760a8ae8d2bdd7521d31a09f) 
+ Screenshare: Show the screen sharing notifier more prominently. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/bbff465e6a005c737ee2e53eb5e072da006d9b4b) Fixes bug [#452980](https://bugs.kde.org/452980)
{{< /details >}}

