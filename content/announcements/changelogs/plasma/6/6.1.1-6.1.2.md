---
title: Plasma 6.1.2 complete changelog
version: 6.1.2
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ ApplicationPage: Fix share dialog. [Commit.](http://commits.kde.org/discover/1ba781bd504c7b04d6b827a4ea98ec5c400df0ed) Fixes bug [#488976](https://bugs.kde.org/488976)
{{< /details >}}

{{< details title="kwallet-pam" href="https://commits.kde.org/kwallet-pam" >}}
+ Switch to pkgconfig to find libgcrypt (allows building with libgcrypt 1.11). [Commit.](http://commits.kde.org/kwallet-pam/44fc23974b895b4d69709edaa6a2e63297f824a2) Fixes bug [#489303](https://bugs.kde.org/489303)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Backends/drm: disable triple buffering on NVidia. [Commit.](http://commits.kde.org/kwin/b437c65815b2898564748f3be48c4145671a750f) Fixes bug [#487833](https://bugs.kde.org/487833)
+ Don't assert on null output. [Commit.](http://commits.kde.org/kwin/453b81a19db94f8baaafc4bf11832a4b3bfecf75) 
+ Quicktiling: Reset layout when last quicktile ceases to exist. [Commit.](http://commits.kde.org/kwin/558edb284fe1ec636aaf1b695e0014df9f115126) Fixes bug [#465937](https://bugs.kde.org/465937)
+ Wayland: Bump default max buffer size to 1 MiB. [Commit.](http://commits.kde.org/kwin/7bc50dfadf0b0e4db1bf21a18762425fa1b85ccf) 
+ Plugins/qpa: set deprecated functions option correctly. [Commit.](http://commits.kde.org/kwin/2021529a7ff7ac63f7ceb3aed85af6a20fe77ace) See bug [#486460](https://bugs.kde.org/486460)
+ Plugins/kdecoration: Fix MenuButton not accepting button press events. [Commit.](http://commits.kde.org/kwin/176e338c370bcee12037b34ae5ffaf2d3bcb8ec8) Fixes bug [#488993](https://bugs.kde.org/488993)
+ Plugins/colorcorrection: simplify the effect, merge the shader files and support color management. [Commit.](http://commits.kde.org/kwin/9da53fc20bdbd40d96ebde9d476f32c2a2990924) 
+ Opengl: Reset OpenGlContext::currentContext() if it's destroyed. [Commit.](http://commits.kde.org/kwin/c6d2eac81ae9dba99df40aba185b50c3f1f9f021) Fixes bug [#488830](https://bugs.kde.org/488830)
+ Window: adhere to window rules in checkWorkspacePosition. [Commit.](http://commits.kde.org/kwin/deeeddb652d20086625a221788dff1926b26172f) Fixes bug [#489117](https://bugs.kde.org/489117)
+ Plugins/hidecursor: show the cursor on tablet events. [Commit.](http://commits.kde.org/kwin/d95dd8f98fc7a3d49f30e191d939b29bfd7a8250) Fixes bug [#489009](https://bugs.kde.org/489009)
+ Revert " Revert "update version for new release"". [Commit.](http://commits.kde.org/kwin/8d2892d3df48cf6180872f24f51f220ba53e3662) 
+ Revert "update version for new release". [Commit.](http://commits.kde.org/kwin/2eaa4f45e799a22258149a9e27a51cef3ffa1f07) 
+ 3rdparty: Reformat xcursor.{h,c}. [Commit.](http://commits.kde.org/kwin/af9b3dd54e54806c435b8e6ed51cd3808d84f7a4) See bug [#489241](https://bugs.kde.org/489241)
+ Utils: Load Xcursor themes using QFile. [Commit.](http://commits.kde.org/kwin/5d94ce9eac18a0ecf4dbbc8dbd04b385dc24c8ff) Fixes bug [#489241](https://bugs.kde.org/489241)
+ 3rdparty: Drop xcursor write hook. [Commit.](http://commits.kde.org/kwin/7527a9099cfc3f0127b87ddad488c8059cdf3a8d) See bug [#489241](https://bugs.kde.org/489241)
+ Plugins/hidecursor: Set minimum to allow disabling hiding cursor on inactivity; set maximum. [Commit.](http://commits.kde.org/kwin/1177a88092b60cc0a227b437dcc9259d54606bbb) 
+ WindowHeapDelegate: Label text background. [Commit.](http://commits.kde.org/kwin/9b7e0b70fbd16e5e00d29207b054ca0c5b5501b6) Fixes bug [#483016](https://bugs.kde.org/483016)
+ Plugins/backgroundcontrast,blur: correct support checks. [Commit.](http://commits.kde.org/kwin/21d8ac78854275b70d3a627023418adc4c0ea59a) 
+ Opengl/glframebuffer: handle missing support for blits on Wayland. [Commit.](http://commits.kde.org/kwin/a95a2196f380ce5208d37dd44ea802f97d6d4a9a) Fixes bug [#484193](https://bugs.kde.org/484193)
+ Opengl: glBufferStorage is not supported on GL ES by default. [Commit.](http://commits.kde.org/kwin/40a2dd823be444c7f89dc770ecd26bc632d67660) See bug [#484193](https://bugs.kde.org/484193)
+ Core/renderloop: assume high render times if the last frame has been a while ago. [Commit.](http://commits.kde.org/kwin/d2cbdbab307957b7284141e30dd9829a37413bed) See bug [#488843](https://bugs.kde.org/488843)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ On applet destroyed go out of configure mode. [Commit.](http://commits.kde.org/libplasma/ccfbf390d2ae4557adba049531980f783b726710) Fixes bug [#488977](https://bugs.kde.org/488977)
+ Workaround to possible infinite reize loops. [Commit.](http://commits.kde.org/libplasma/abd4653526271a1af6c8c351bf22d709fc799c10) Fixes bug [#488974](https://bugs.kde.org/488974)
+ Quick/dialogbackground: Ensure we delete the SVG item. [Commit.](http://commits.kde.org/libplasma/7d875a8080445fc72c6eec8da5065556c1a89ac1) Fixes bug [#486743](https://bugs.kde.org/486743)
{{< /details >}}

{{< details title="ocean-sound-theme" href="https://commits.kde.org/ocean-sound-theme" >}}
+ Create bell.oga symlinked to the existing bell sound. [Commit.](http://commits.kde.org/ocean-sound-theme/0dfec4bbfe2523f49f841786b1007b53880dae2a) Fixes bug [#489231](https://bugs.kde.org/489231)
{{< /details >}}

{{< details title="oxygen-sounds" href="https://commits.kde.org/oxygen-sounds" >}}
+ Create bell.ogg symlink to current bell sound. [Commit.](http://commits.kde.org/oxygen-sounds/b7f208f62cb83983969d3468391a25c220639181) Fixes bug [#488968](https://bugs.kde.org/488968)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ [kcm/keyboard] Add missing GENERATE_MOC to kconfig_add_kcfg_files. [Commit.](http://commits.kde.org/plasma-desktop/51eef292cee9cbc99e13c73d29403082b275baa4) 
+ Kcms/keyboard: fix width and immutability of layout table headers. [Commit.](http://commits.kde.org/plasma-desktop/9f0b2c7d0feb9f38f106d110c2b08d19310ce978) Fixes bug [#488957](https://bugs.kde.org/488957)
+ Make panel config tooltips ask for focus after each applet change. [Commit.](http://commits.kde.org/plasma-desktop/aad5814e70f42f4a44c42ec23ca6e921c6e739e6) Fixes bug [#487640](https://bugs.kde.org/487640)
+ Kcms/access: Bump the minimum cursor magnification factor. [Commit.](http://commits.kde.org/plasma-desktop/1dc2cfb2da53e8c988dde5986922b42aa7c75f92) Fixes bug [#489333](https://bugs.kde.org/489333)
+ Kcms/access: fix list item highlighting. [Commit.](http://commits.kde.org/plasma-desktop/3ac7d42daf268d9053f8bcb046b4e368e2fdad93) Fixes bug [#489206](https://bugs.kde.org/489206)
+ Always center "Add Widgets" button on empty panels. [Commit.](http://commits.kde.org/plasma-desktop/c264cd884ee4c1a0d3ac672aabeedb10463b86a7) Fixes bug [#488789](https://bugs.kde.org/488789)
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Panel: Fix it not being full-width at a certain width. [Commit.](http://commits.kde.org/plasma-mobile/bc2ab6345f996bade4c97218b31e44bf410f9a20) 
+ Kcms/wifi: Fix toggle not updating the first time. [Commit.](http://commits.kde.org/plasma-mobile/90a6e7f692e3d30108bcbf2b6e23dd6dd86a982d) 
+ Envmanager: Ensure KConfigGroup being written to is not derived from const. [Commit.](http://commits.kde.org/plasma-mobile/64d853a6549e3153db4e2a1ffd4d3ff5e95d8ecc) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Libtaskmanager: improve efficiency when window icon frequently changes. [Commit.](http://commits.kde.org/plasma-workspace/cd563dff3a5f5b61de0d3142768c47707f8d8e55) Fixes bug [#487390](https://bugs.kde.org/487390)
+ Do not hide panel settings when a panel-parented dialog takes focus. [Commit.](http://commits.kde.org/plasma-workspace/3acaa98ff89120cc93cdcb87210c061a7de2a004) Fixes bug [#487161](https://bugs.kde.org/487161)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Daemon/controllers: Reintroduce a mutex around ddca_open_display2(). [Commit.](http://commits.kde.org/powerdevil/209782d50796d8aa2bfe36753c69ed6c77ec5c99) Fixes bug [#489169](https://bugs.kde.org/489169)
+ Ddcutildisplay: give some time before changing brightness after the monitor resumes. [Commit.](http://commits.kde.org/powerdevil/0c084dcb444173273b60934af7bdb321d39dbf13) 
+ Core: reload actions on brightness controller changes. [Commit.](http://commits.kde.org/powerdevil/d91bc62fa6ff93ac62ded148fee3722deab41442) See bug [#482713](https://bugs.kde.org/482713)
{{< /details >}}

{{< details title="System Settings" href="https://commits.kde.org/systemsettings" >}}
+ Fix sub-category view remaining visible when moving to a top-level KCM. [Commit.](http://commits.kde.org/systemsettings/3b043fa20e83907cd8e642729ba388732f844892) Fixes bug [#434345](https://bugs.kde.org/434345)
{{< /details >}}

