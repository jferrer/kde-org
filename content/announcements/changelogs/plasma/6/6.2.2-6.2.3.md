---
title: Plasma 6.2.3 complete changelog
version: 6.2.3
hidden: true
plasma: true
type: fulllog
---

{{< details title="Bluedevil" href="https://commits.kde.org/bluedevil" >}}
+ Correct PIN entry behavior. [Commit.](http://commits.kde.org/bluedevil/3c65c991902a58f4ce224cdabd13baab39129819) 
{{< /details >}}

{{< details title="Breeze" href="https://commits.kde.org/breeze" >}}
+ KStyle: Don't dereference nullptr. [Commit.](http://commits.kde.org/breeze/14a8b082a9c265ed93d4808a9fead2ee6c98c2e6) 
{{< /details >}}

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ FwupdBackend: Do not use more API now removed in fwupd 2.0.0. [Commit.](http://commits.kde.org/discover/1db6d05d86d333e4a20df73c19ed339cdff4071c) 
+ ApplicationPage: Fix visibility of the reviews component. [Commit.](http://commits.kde.org/discover/9baf50fca7a37f5bed750a706f32d5c7f9528f1e) Fixes bug [#495597](https://bugs.kde.org/495597)
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Connectiontest: log bad requests. [Commit.](http://commits.kde.org/drkonqi/bd7bbb4ba1d55bf1638c97a726721d5a61305d2f) 
+ Connectiontest: fix for qt 6.8. [Commit.](http://commits.kde.org/drkonqi/8dd5b7e5c79a61da9cce3b86792aa77dcdfdbcc3) 
+ Auto starting jobs don't start early. [Commit.](http://commits.kde.org/drkonqi/eaf500c2081d44d97f35978175eb10697e6b5c1e) 
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Gracefully handle decoration plugin failing to load. [Commit.](http://commits.kde.org/kde-gtk-config/bd50b0e6ef1e362484d354cc341c32aa56e08a7b) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Applets/userswitcher: fix showing other logged-in users' avatars. [Commit.](http://commits.kde.org/kdeplasma-addons/52b7436f638bf0a88b9448f0529632e67271ce88) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Appiumtests: fix wayland. [Commit.](http://commits.kde.org/kinfocenter/6682bd9ab011cd84522a00148faa0282b26e6774) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Libx264encoder: Ensure stream size is always a multiple of 2. [Commit.](http://commits.kde.org/kpipewire/54a5077ba4eb48ef8d7df865c7a974e9fb59f71f) Fixes bug [#485733](https://bugs.kde.org/485733)
+ Encoder: Make it possble to override the filter graph used for software encode. [Commit.](http://commits.kde.org/kpipewire/461aa29bdc33168501651197e88f343b50f156ff) 
{{< /details >}}

{{< details title="ksystemstats" href="https://commits.kde.org/ksystemstats" >}}
+ Plugins/pressure: Use ulonglong for storing "total" time information. [Commit.](http://commits.kde.org/ksystemstats/b8c7a201f11d91b52165db24c8f060c0de2c6989) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Input: don't crash if the internal handle is nullptr (wheelEvent). [Commit.](http://commits.kde.org/kwin/63ec2601246e956afd807bc4b843bdff9f03032b) 
+ Backends/x11: fix colormap leak. [Commit.](http://commits.kde.org/kwin/40cea14dd850b8c229a715ab8ee9b0265a97487e) 
+ Update ExpoCell contentItem position upon its parent change. [Commit.](http://commits.kde.org/kwin/1dbe3863a734d14c24414a0a8d8bf4f85b33dc19) Fixes bug [#495501](https://bugs.kde.org/495501)
+ Scene: Fix item restacking repaints. [Commit.](http://commits.kde.org/kwin/aa9a9e1ec2abe3c5ba27f427408964d7bdc93bac) 
+ Xwayland: Fix a couple of file descriptor leaks. [Commit.](http://commits.kde.org/kwin/80cd83abeafae11ae2dc22622186910a17c7e7ab) Fixes bug [#442846](https://bugs.kde.org/442846)
+ Plugins/keynotification: Fix event ID for notification. [Commit.](http://commits.kde.org/kwin/1485605dbc1d7a792119dc70d131c6d47e32095f) Fixes bug [#495264](https://bugs.kde.org/495264)
+ Backends/drm: don't set backlight brightness to 1 in HDR mode. [Commit.](http://commits.kde.org/kwin/e2d0bcb01e3e51258c3485d5e45d63eb54b6a551) Fixes bug [#495242](https://bugs.kde.org/495242)
+ Core/colorspace: ensure that we don't create elevated blacks with black point compensation. [Commit.](http://commits.kde.org/kwin/f702ad9c4cee58e8e438e3d22ff202a80ac8b3ff) Fixes bug [#494854](https://bugs.kde.org/494854)
+ Autotests: test that ColorPipeline and OpenGL shader results at least somewhat match. [Commit.](http://commits.kde.org/kwin/cef793da627827d7ce83aea86f7b253df6bf0e1a) 
+ Core/colorpipeline: do ICtCp conversion the correct way around. [Commit.](http://commits.kde.org/kwin/d2f176afbc689946626fd234a66a97c604053501) 
+ Core/colorpipeline: use PQ with min. luminance zero for tone mapping. [Commit.](http://commits.kde.org/kwin/e74608eb7911a4c4fe96e31a0e5ddf60bcd31ebc) 
+ Core/colorpipeline: fix multiplier+matrix optimization. [Commit.](http://commits.kde.org/kwin/c6179331f6a60a452edb872e035f45d29a060279) 
+ Core/colorpipeline: don't transpose the ICtCp matrix. [Commit.](http://commits.kde.org/kwin/255c2a26d5c725d57b8102193ca236a0eac47eb7) 
+ Core/colorpipeline: fix tone mapping luminances being switched around. [Commit.](http://commits.kde.org/kwin/af80b1117dba92dbb312afcdc5ebc2141fa4cee4) 
+ Backends/drm: check if m_commits is empty after waiting for commitPending. [Commit.](http://commits.kde.org/kwin/594b24838629ba367891094f8da27e2b2012daaf) 
+ Use xcb_connection_has_error to check for failue. [Commit.](http://commits.kde.org/kwin/4997eedf5ceada39d9cd2a96b188de5890f8d618) 
+ Fix "window to next desktop" shortcut during interactive move/resize session. [Commit.](http://commits.kde.org/kwin/d174dc91484a7c29996ed313abd033af97fab5d3) 
+ Backends/drm: add an environment variable to force glFinish on multi gpu copies. [Commit.](http://commits.kde.org/kwin/06a08004dc454b2a83e395922a55721416f10563) See bug [#494675](https://bugs.kde.org/494675)
+ Backends/drm: disable software brightness if there was ever a hardware brightness device assigned. [Commit.](http://commits.kde.org/kwin/0aafc2ea64bc65c42ff0567c7d2aa9b22088f930) 
{{< /details >}}

{{< details title="libksysguard" href="https://commits.kde.org/libksysguard" >}}
+ Sensors: Improve robustness of SensorDaemonInterface. [Commit.](http://commits.kde.org/libksysguard/d1b87b9c63644072e5eaa4518d00836e432bd196) 
+ Faces: Skip null entries when trying to resolve sensors. [Commit.](http://commits.kde.org/libksysguard/aa6da0cd67c5abcb5109a0b04e94da1c20a7b7c2) 
+ Faces/grid: Specify updateRateLimit for FaceLoader. [Commit.](http://commits.kde.org/libksysguard/362832026668fff13e2d194d1c02e85c7fcde8f3) Fixes bug [#494019](https://bugs.kde.org/494019)
+ Faces: Explicitly emit signals on internal controller on property change. [Commit.](http://commits.kde.org/libksysguard/5e875abc4585331d5bb128f5b8441b8ce73ee02c) 
+ Faces: Allow specifying updateRateLimit for FaceLoader. [Commit.](http://commits.kde.org/libksysguard/90c81e3c0354b8bfd9188950d1b7a60ca174830f) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Application Dashboard: Show app tooltips properly. [Commit.](http://commits.kde.org/plasma-desktop/6b746e7d4b304933ca70e421f02a35db375be0fb) Fixes bug [#494802](https://bugs.kde.org/494802)
+ Applets/taskmanager: Fix icon alignment when is reversed. [Commit.](http://commits.kde.org/plasma-desktop/281cee2962b04be09e645436f512489eb292a8a9) Fixes bug [#489648](https://bugs.kde.org/489648)
+ Emojier: do substring search in emoji annotations as well. [Commit.](http://commits.kde.org/plasma-desktop/49abd66b12bbc4111d5e32bdef07b3c96c46698d) Fixes bug [#435787](https://bugs.kde.org/435787)
+ Activitymanager: Avoid infinite loop in ActivityList. [Commit.](http://commits.kde.org/plasma-desktop/68f2dacdf9cfb39b8a81b038d9bcecf509da1793) 
+ Positioner: Make sure we check deferMovePositions exist. [Commit.](http://commits.kde.org/plasma-desktop/816e982e0da6266e0a489b10296f4e3b3b833e11) See bug [#493569](https://bugs.kde.org/493569)
+ Appiumtests: fix desktoptest. [Commit.](http://commits.kde.org/plasma-desktop/5e7ebf627d3173ce496579d4ac5fe403ddf62cf9) 
+ Appiumtests: fix kcm_plasmasearch_test. [Commit.](http://commits.kde.org/plasma-desktop/30bcc49c20d34cd35ef95833afb01506b0034bdd) 
+ Applets/taskmanager: use TextMetrics directly to elide long recent items. [Commit.](http://commits.kde.org/plasma-desktop/f98c553c675755e633a32c59b6bd7b42c1373c8a) 
{{< /details >}}

{{< details title="Plasma Disks" href="https://commits.kde.org/plasma-disks" >}}
+ Smartmonitor: assert with context. [Commit.](http://commits.kde.org/plasma-disks/b5b44d7b1c598dc53e8700f2cc62cbb7ff1eeaeb) See bug [#495570](https://bugs.kde.org/495570)
{{< /details >}}

{{< details title="plasma-integration" href="https://commits.kde.org/plasma-integration" >}}
+ Update m_palettes even when KDE_COLOR_SCHEME_PATH is set. [Commit.](http://commits.kde.org/plasma-integration/332330ec4bb4134c012122597a5d71cba67e360a) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Actiondrawer: Cleanup and simplify. [Commit.](http://commits.kde.org/plasma-mobile/a86c3825d78f10a024f6e4ea046305b5b64f7de8) 
+ Envmanager: Fix KWin effect and script reloading. [Commit.](http://commits.kde.org/plasma-mobile/599f1fe38c0bda27febbeb0848213ec944b2bafd) 
+ Actiondrawer: Share components between portrait and landscape. [Commit.](http://commits.kde.org/plasma-mobile/8988d683dbd9235121e3b6bf9ac8487a270732fb) 
+ Homescreens/folio: Remove icon on one of the settings delegates. [Commit.](http://commits.kde.org/plasma-mobile/d36309d6fb10f524421c2f0c729167fdab2666e2) 
+ Homescreens/folio: Never open both search and app drawer. [Commit.](http://commits.kde.org/plasma-mobile/e9b739280ee53fdfc8ff377ccb2a7c657f2b624b) 
+ Homescreens: Use WheelHandler on app list. [Commit.](http://commits.kde.org/plasma-mobile/5ce64da5ccef14aeff0231bb15c7533238024a4e) 
+ Actioncenter: Reduce usage of transforms and cleanup. [Commit.](http://commits.kde.org/plasma-mobile/7259d2c79a03e0fb9ad1fc5c0608a572c9a03aeb) 
+ Homescreens/folio: Fix app icons being rounded to predefined sizes. [Commit.](http://commits.kde.org/plasma-mobile/aaba5e00f9c24cb53cf994f09c0abf8aadb520f8) 
+ Quicksettings: fix landscape not being scrollable. [Commit.](http://commits.kde.org/plasma-mobile/90dab38208730f9d0fcbfb5661d79b62850cd51d) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ [kcm] Update selected connection when opening KCM a second time. [Commit.](http://commits.kde.org/plasma-nm/2c33582e89a836fb0a7c369f3ae6d99e494c0dc8) Fixes bug [#461568](https://bugs.kde.org/461568)
{{< /details >}}

{{< details title="Plasma Audio Volume Control" href="https://commits.kde.org/plasma-pa" >}}
+ Rename: set correct device name upon reset. [Commit.](http://commits.kde.org/plasma-pa/e8ff50c050958057b796fe6c1758d31574fa9199) Fixes bug [#494546](https://bugs.kde.org/494546)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Do early return for ServciesRunner::resolvedArgs in case of error. [Commit.](http://commits.kde.org/plasma-workspace/24d72107f604c2acdc2370b9178856f80ff99e52) See bug [#495606](https://bugs.kde.org/495606)
+ Always show full logout screen for "Show Logout Screen" desktop menu item. [Commit.](http://commits.kde.org/plasma-workspace/7838c34434b5046bd54f7e4988e56ed6896965a6) Fixes bug [#495390](https://bugs.kde.org/495390)
+ Lookandfeel: Explicitly set theme to Breeze in defaults. [Commit.](http://commits.kde.org/plasma-workspace/2c651a920a446037967ac2dfdc21fc8da6ae2939) 
+ Kcms/users: crop character faces properly. [Commit.](http://commits.kde.org/plasma-workspace/ed1f137f62bf3164cf780001c7f83bc460e2e539) Fixes bug [#494487](https://bugs.kde.org/494487)
+ Kcms/users: Fix UI on mobile. [Commit.](http://commits.kde.org/plasma-workspace/29513fd71da8b3a0ba246de78e78597730cae53b) 
+ Logout-greeter: don't doubt the daemon. [Commit.](http://commits.kde.org/plasma-workspace/ef176f454de5d175df2fb25b3e89f91dacc3fe64) 
+ Logout-greeter: categorize warnings. [Commit.](http://commits.kde.org/plasma-workspace/42f46294bc19e63d51370ba0d703a3de0b51a172) 
+ Logout-greeter: don't wake up packagekit needlessly. [Commit.](http://commits.kde.org/plasma-workspace/8ec327eaaabeea77507f13790c8b6f9fb0ea792a) 
+ Appiumtests: fix kcm_cursortheme_test. [Commit.](http://commits.kde.org/plasma-workspace/7a9a6dfe7e18d2a9e7143bba2d17df0af0ad5e4e) 
+ Appiumtests: fix kcm_autostart_test. [Commit.](http://commits.kde.org/plasma-workspace/8a5a3c9b6c55fda6766611fd12ecb6c2a87d6dba) 
+ Appiumtests: fix kcm_users_test. [Commit.](http://commits.kde.org/plasma-workspace/a68db394f66b0059f72b9a9b5fbf096318e7f1c3) 
+ Appiumtests: fix digitalclocktest. [Commit.](http://commits.kde.org/plasma-workspace/b2b1d79fda6c0486a166d215ee338287ae5175d3) 
+ Components/dbus: fix invalid variant type since Qt 6.8. [Commit.](http://commits.kde.org/plasma-workspace/9793611eed75fa2298b1444bf240f6f4aef9a4e8) 
+ Appiumtests: fix systemdialogtest. [Commit.](http://commits.kde.org/plasma-workspace/964530b768681b98b35ede399c033c2c75ffe483) 
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Applets/batterymonitor: correct dbus watcher. [Commit.](http://commits.kde.org/powerdevil/fdaf3a64ca61c611178f9c3cb14d6bf91e884b31) See bug [#492859](https://bugs.kde.org/492859)
+ Applets: react to power-profile-daemon dbus registration. [Commit.](http://commits.kde.org/powerdevil/b159dec4edcb368d56b18dc1985b301ed40d6852) Fixes bug [#492859](https://bugs.kde.org/492859)
+ Daemon: Only write DDC brightness after reading and comparing first. [Commit.](http://commits.kde.org/powerdevil/e6ce2b0271ea79181f35d08a8ed996df3cd7fdfb) 
+ Daemon: Set ddcutil as missing if initialization fails. [Commit.](http://commits.kde.org/powerdevil/e69e8f99bfcb7065907d1a834a8d8c18fa504fec) 
+ Autotests: only enable dbus debug message for backlighthelper in brightnesstest. [Commit.](http://commits.kde.org/powerdevil/a1ec19c252627140e84c6afccf5fafe500f20b2c) 
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Fix calculation of width of DialogButtonBox. [Commit.](http://commits.kde.org/qqc2-breeze-style/9de6115354bc5f8e00148e27ebfc06e07a0afedb) 
+ Add missing dependencies on QtQuick.Window. [Commit.](http://commits.kde.org/qqc2-breeze-style/623f34fdafac5189f47911c248dded3eb319eb54) 
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ [FileChooser] Properly pass along requested current filter. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/d872c0cafa8caca0c8b4e2a0555fa7a36df5122c) 
+ Create region selection window before calling setScreen. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/a067b4f536ceeab9f01964e50e99392230aebc53) Fixes bug [#493293](https://bugs.kde.org/493293)
{{< /details >}}

