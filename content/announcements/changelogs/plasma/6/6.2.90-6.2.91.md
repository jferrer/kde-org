---
title: Plasma 6.2.91 complete changelog
version: 6.2.91
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ Libdiscover: always emit reviewsReady to fix crash when there is no review. [Commit.](http://commits.kde.org/discover/6bc6f0c5e3ada847251767c8ad058f4ee5de054e) 
+ Flatpak: Support flatpak:/ format. [Commit.](http://commits.kde.org/discover/3208c960aedf0fda29723bffb845e60681620b62) 
+ Use a filter model instead for pagination. [Commit.](http://commits.kde.org/discover/4804939acb1648a5a25a7e5e67035be8e22df489) 
+ ApplicationPage: Fix excessive horizontal flicking. [Commit.](http://commits.kde.org/discover/75a7db38af19624d09e68a6964ab1d890a974e87) 
{{< /details >}}

{{< details title="Dr Konqi" href="https://commits.kde.org/drkonqi" >}}
+ Add mapping for plasma-browser-integration-host. [Commit.](http://commits.kde.org/drkonqi/d247b08ef85e62ead9f68507a0798be2f9d6fa9f) 
+ Cmake: require systemd unless explicitly opted out of. [Commit.](http://commits.kde.org/drkonqi/24e9720e62f2cbf86654a3e9b9356ffc290289ce) 
+ Cmake: remove mingw support code. [Commit.](http://commits.kde.org/drkonqi/1d04a9b72a6e33f43fab6af7229db6813b4786b1) 
{{< /details >}}

{{< details title="KDE Window Decoration Library" href="https://commits.kde.org/kdecoration" >}}
+ Add KDecoration 3 porting guide. [Commit.](http://commits.kde.org/kdecoration/3a9e4189cc66a8b36b4604da266cd0f69b523139) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Wallpapers/hunyango: Remove broken accent color handling. [Commit.](http://commits.kde.org/kdeplasma-addons/52149463c91f5212711a659a818982efb41e2102) 
+ Fix highlight not following mouse downwards. [Commit.](http://commits.kde.org/kdeplasma-addons/9126a2f1575ff72a40977fcd5080e24fb47b40db) Fixes bug [#485477](https://bugs.kde.org/485477)
{{< /details >}}

{{< details title="kglobalacceld" href="https://commits.kde.org/kglobalacceld" >}}
+ Check migrated shortcut triplet size in GlobalShortcutsRegistry::migrateConfig(). [Commit.](http://commits.kde.org/kglobalacceld/d5eb1960f8b7b8af64226c99232d1f6892825a81) 
+ Ignore invalid X-KDE-Migrate-Shortcut values. [Commit.](http://commits.kde.org/kglobalacceld/e77fe374f9fe8f55c157aca2f9f1c2dcba768070) 
+ Ignore invalid services shortcuts in GlobalShortcutsRegistry::migrateConfig(). [Commit.](http://commits.kde.org/kglobalacceld/3c880b79a71ca6c2bebd487df1536b562f00b2a1) 
{{< /details >}}

{{< details title="Info Center" href="https://commits.kde.org/kinfocenter" >}}
+ Cmake: Call cmake_minimum_required before project(). [Commit.](http://commits.kde.org/kinfocenter/412c1ea0e847c458dc62b5040149db08d10c733f) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Add missing !. [Commit.](http://commits.kde.org/kpipewire/8af11e0d654986d8c38493bd72850c376cd9502c) 
+ PipeWireSourceStream::setActive demote assert to warning. [Commit.](http://commits.kde.org/kpipewire/ff0c999ae868a5056b55f07664c72c2a3fec5cd8) 
+ PipeWireSourceItem handle stream creation failure. [Commit.](http://commits.kde.org/kpipewire/2f8c2a15b1eeaebf520bf2337cd197c48ff8ccf0) 
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ Rename systemd unit to app- prefix. [Commit.](http://commits.kde.org/krdp/097eaf5d41c5f5d8ba99a6c75da4769b43c9d852) 
{{< /details >}}

{{< details title="KScreen" href="https://commits.kde.org/kscreen" >}}
+ Console: don't print xrandr output on Wayland. [Commit.](http://commits.kde.org/kscreen/9ced38043d7441cb56e955c54dfac6dffd39d9b7) 
+ Console: make kscreen-console print the correct config on Wayland. [Commit.](http://commits.kde.org/kscreen/268dc6365848db8eced51c1bfb5753c5cd2b5281) See bug [#464835](https://bugs.kde.org/464835)
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ Globalaccel: whitelist Toggle Keyboard Backlight. [Commit.](http://commits.kde.org/kscreenlocker/28fdb0641050fdc0029d90d6fdc7d53435b4175a) Fixes bug [#483919](https://bugs.kde.org/483919)
{{< /details >}}

{{< details title="KWayland" href="https://commits.kde.org/kwayland" >}}
+ Provide keys received in wl_keyboard.enter. [Commit.](http://commits.kde.org/kwayland/ad101486154cf4005810a4cc9d0bb3a102b4d12f) 
+ Emit Registry::interfaceAnnounced after registering the interface. [Commit.](http://commits.kde.org/kwayland/48d2d5e1ae6a340603e707d12cf1ecdac57167c0) 
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Xdgshell: Fix passing focus to transient parent on close. [Commit.](http://commits.kde.org/kwin/8babbae2a9f03a120fa3cda32bc72850f33ad4d0) Fixes bug [#498098](https://bugs.kde.org/498098)
+ Fix a crash in Workspace::initializeX11(). [Commit.](http://commits.kde.org/kwin/f34f893d34a22d5d72123126192789a8217c5d2b) 
+ Never show autohide/dodge windows panels when a window is fullscreen. [Commit.](http://commits.kde.org/kwin/31e0e256ad534c146bd42a6ad3c22bdc16d56685) Fixes bug [#497826](https://bugs.kde.org/497826). Fixes bug [#497829](https://bugs.kde.org/497829)
+ Backends/drm: allow some inaccuracy for matching a 3x3 matrix. [Commit.](http://commits.kde.org/kwin/77f76e998c2c1a20da90df0753c39ba1346d0641) 
+ Autotests: Make X11KeyReadTest less flaky. [Commit.](http://commits.kde.org/kwin/72c79d715ce3407408a56d6b92d9468440e734b1) 
+ Backends/drm: Harden DrmGpu::waitIdle() against faulty timeouts. [Commit.](http://commits.kde.org/kwin/f371a3cd73cb10b6df6b47db38263ef674fd3a01) 
+ Backends/drm: fix qpainter hardware cursor format. [Commit.](http://commits.kde.org/kwin/0b03dfa10dfbe20da66138711937b42bcea3a693) 
+ Backends/drm: also tell people where to report pageflip timeouts. [Commit.](http://commits.kde.org/kwin/fc8cc4267686bdee772d3372d3435271ec5864d3) 
+ Backends/drm: remove the chance for false positives in pageflip timeout detection. [Commit.](http://commits.kde.org/kwin/275b4ac9829720291cfae67d0ae37165b6140ce4) See bug [#498544](https://bugs.kde.org/498544)
+ Autotests/integration: add more cases to the color management test. [Commit.](http://commits.kde.org/kwin/885da39e6c57f45f88db7d25aa39776f090156ee) 
+ Wayland/color management: fail image descriptions with invalid primaries / whitepoint. [Commit.](http://commits.kde.org/kwin/4c8c667ffaa0e6986ea3cfb62618758621de14d2) 
+ Utils/edid: check if colorimetry could possibly be correct. [Commit.](http://commits.kde.org/kwin/d02b39b2a8a4a824c0c67a68afada31f51fa5b30) Fixes bug [#498616](https://bugs.kde.org/498616)
+ Core/colorspace: add helpers for checking if colorimetry is valid. [Commit.](http://commits.kde.org/kwin/291342d8f21a3898dfa289b681c2e6bad4d2fd35) 
+ Prevent including global shortcut keys in wl_keyboard.enter. [Commit.](http://commits.kde.org/kwin/af643f84b57092647e8afdbc86ccdfa898a83850) Fixes bug [#497245](https://bugs.kde.org/497245)
+ Backends/fakeinput: Destroy devices on teardown. [Commit.](http://commits.kde.org/kwin/a9933cc632e6a95115236476d1e4025b58562289) 
+ X11window: don't search for StartupWMClass if the resourceName is empty. [Commit.](http://commits.kde.org/kwin/a097c5bae9782f7aaca04bbdfabf84d3a4d7f307) 
+ Offscreenquickview: Keep DPR up to date. [Commit.](http://commits.kde.org/kwin/524fc538c83fc367438b977f076d4b7cec7eaf9b) 
+ Plugins/qpa: Clip buffer damage. [Commit.](http://commits.kde.org/kwin/865660f55378f8bd5b0b41de398b27b6975bee1c) Fixes bug [#498446](https://bugs.kde.org/498446)
+ Backends/drm: don't use ColorDescription::toOther for calculating channel factors. [Commit.](http://commits.kde.org/kwin/b883722857bcb7e971eb9aaeaaa83d2ccd22ba61) Fixes bug [#498668](https://bugs.kde.org/498668)
+ Backends/libinput: Destroy devices when backend shuts down. [Commit.](http://commits.kde.org/kwin/9fe2cb080e68af072f1012232d3f4ad0255553be) 
+ Outputconfigurationstore: increase the max. scale when reading the config. [Commit.](http://commits.kde.org/kwin/842648821e9552659b50a9772c26f2870e9765d9) 
+ Outputconfigurationstore: only round the scale factor to 5% steps. [Commit.](http://commits.kde.org/kwin/58157e03a9708305b3cbe4b8daaa427392593fa8) 
+ Include output physical size in support information. [Commit.](http://commits.kde.org/kwin/f26b1882fee4d0d10cc548937094fed6447bfea0) 
+ Outputconfigurationstore: don't scale low resolution screens too much. [Commit.](http://commits.kde.org/kwin/97f1fd52dc4c6c238b898b095637bd23c029cca9) 
+ Backends/drm: make night light correct with ICC profiles as well. [Commit.](http://commits.kde.org/kwin/59e0b8904fda5639dd5f63279d879a23cd3e00fa) 
+ Autotests: Add more test cases in OutputChangesTest::testGenerateConfigs(). [Commit.](http://commits.kde.org/kwin/842668a79bd903a9a75f055c6ce7658ea2a3897f) 
+ Autotests: Add laptop test cases in OutputChangesTest::testGenerateConfigs(). [Commit.](http://commits.kde.org/kwin/b9ac4492dedfa745621b5b42098940eb456ba7af) 
+ Utils: Make sure Edid::hash() returns a value if edid fails to parse. [Commit.](http://commits.kde.org/kwin/3a70ae0e037f07de3c387efed2b7d50bfe94c091) 
+ Dont assert in  Workspace::setupWindowShortcut. [Commit.](http://commits.kde.org/kwin/c72507977f42d12b24c697ed902dbae2def5909c) Fixes bug [#498456](https://bugs.kde.org/498456)
+ Input: don't notify user activity on warps. [Commit.](http://commits.kde.org/kwin/e8ad9770e245c5f3457af1fcd2cefed1ed1464f5) See bug [#480026](https://bugs.kde.org/480026)
+ Perform window mouse action for tablet tool buttons. [Commit.](http://commits.kde.org/kwin/827f166c8f3188afcbb05295e1130d233877cd8b) Fixes bug [#480227](https://bugs.kde.org/480227)
+ Opengl: skip deleting OpenGL objects if there's no current context. [Commit.](http://commits.kde.org/kwin/73e86df9f9b578d10140aef7b8b2003e9e7c20ea) 
+ Plugins/screencast: call ItemRenderer::begin/endFrame. [Commit.](http://commits.kde.org/kwin/45a5d8844b36404334301f5da6e75f1a345e0c80) Fixes bug [#495287](https://bugs.kde.org/495287)
{{< /details >}}

{{< details title="libkscreen" href="https://commits.kde.org/libkscreen" >}}
+ Backends/kwayland: Clean up output device initialization. [Commit.](http://commits.kde.org/libkscreen/9b67d7316e8e36355577a09df2c1868ab548a8ce) 
+ Backends/kwayland: Fix dangling globalRemoved connections. [Commit.](http://commits.kde.org/libkscreen/34117e75588fe72f0797cb7ac19c9d63e4df00f4) 
+ Revert "backends/kwayland: Remove unbalanced unblockSignals()". [Commit.](http://commits.kde.org/libkscreen/063fb1c5feed0e4fa14654ab1033465adf6a07e1) See bug [#499029](https://bugs.kde.org/499029)
+ Backends/kwayland: Don't leak output configuration if it contains no real changes. [Commit.](http://commits.kde.org/libkscreen/ed77a711ecf6e8950c599a61822da12479e5db2c) 
+ Backends/kwayland: Destroy wayland output configuration resources. [Commit.](http://commits.kde.org/libkscreen/9f3dcce64796a7edf4cbb281f5a0bc82686fc4cd) 
+ Backends/kwayland: Fix creating unnecessary output configuration object. [Commit.](http://commits.kde.org/libkscreen/a686f959b55f95ebc9ee1803049414b9bd2ffe7b) 
+ Backends/kwayland: Remove unbalanced unblockSignals(). [Commit.](http://commits.kde.org/libkscreen/04e45b469e33009b7dcf57325b7fa8be83529639) 
+ Backends/kwayland: Reject configs with unknown outputs. [Commit.](http://commits.kde.org/libkscreen/64156eb6868bf17d51b06f3abc5e90ccdd605140) Fixes bug [#498878](https://bugs.kde.org/498878)
+ Doctor: add dpms to help text. [Commit.](http://commits.kde.org/libkscreen/a641ee4ad28bd8574475d417f1b565dd51fb0c3b) Fixes bug [#495499](https://bugs.kde.org/495499)
+ Doctor: fix "--dpms show". [Commit.](http://commits.kde.org/libkscreen/e229d2901f82eaca5e04a6a40bfb2410c814702b) See bug [#495499](https://bugs.kde.org/495499)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ ButtonContent: Center the GridLayout itself. [Commit.](http://commits.kde.org/libplasma/8e06c3e2ee945662fdaccbaac919cd0aa7b21d39) 
+ Make mnemonics work again with Qt < 6.8 without animateClick. [Commit.](http://commits.kde.org/libplasma/9cf19e71f3b71cff5beac55506250dabbc983cdd) 
+ Plasmaquick: Avoid setting window position for OSD surfaces. [Commit.](http://commits.kde.org/libplasma/4f11fb2ad8971d59ddba936bd899bcbe8647a00d) 
+ Immediately update popup position upon setting margin. [Commit.](http://commits.kde.org/libplasma/0c6b954c89ea11a4957a3578e4aa02d4e5be598f) See bug [#481533](https://bugs.kde.org/481533)
{{< /details >}}

{{< details title="plasma-activities-stats" href="https://commits.kde.org/plasma-activities-stats" >}}
+ Avoid nested event processing waiting for DBus queries. [Commit.](http://commits.kde.org/plasma-activities-stats/1ddf939b53f883d0bae1e1e01c44eac2c0af8ef5) Fixes bug [#495974](https://bugs.kde.org/495974)
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/kickoff: change category at most once per frame. [Commit.](http://commits.kde.org/plasma-desktop/84aa5d2d1077829662885d730a68a4352e7fd6a9) Fixes bug [#483205](https://bugs.kde.org/483205)
+ Make panel settings comboboxes take as much space as their longest element. [Commit.](http://commits.kde.org/plasma-desktop/e24488e083837110984edf9229fc43921640a183) Fixes bug [#486901](https://bugs.kde.org/486901)
+ Kcms/landingpage: fix frequent kcms count. [Commit.](http://commits.kde.org/plasma-desktop/6aa0c64718c6a4d979d9aac575868af72683743a) Fixes bug [#498609](https://bugs.kde.org/498609)
+ Fix DesktopSchemeHelper's QML singleton-ness. [Commit.](http://commits.kde.org/plasma-desktop/80a9600751a97948b961675ad756f145a0688fa4) Fixes bug [#498783](https://bugs.kde.org/498783)
+ Containments/desktop: properly map position to find the mouseover item. [Commit.](http://commits.kde.org/plasma-desktop/362e4c7027bbf9eee931235d7ec652b6900d0d95) Fixes bug [#497498](https://bugs.kde.org/497498)
+ [taskmanager] Show full URL if not local. [Commit.](http://commits.kde.org/plasma-desktop/97f10b208f46b4afb55205881a9ca6c4cc2c28dd) 
+ Animate margin changes for applet popups. [Commit.](http://commits.kde.org/plasma-desktop/cf4aef91df8b918eb1078b3479e705374efd976f) Fixes bug [#481533](https://bugs.kde.org/481533)
+ [taskmanager] Use KIO::iconNameForUrl instead of homegrown logic. [Commit.](http://commits.kde.org/plasma-desktop/ba027ee44702ce33a3c778652f61634105199ea3) 
+ [kaccess] Trigger bell for active window only. [Commit.](http://commits.kde.org/plasma-desktop/5ee231e5876dcac43faf0bb872c344e5be5ca425) 
+ [kaccess] Use KWin to show visual bell. [Commit.](http://commits.kde.org/plasma-desktop/e11c9292d1b18ccafbf16df7610757f5ca69e35f) Fixes bug [#488967](https://bugs.kde.org/488967)
+ Kcms/kded: allow users to enable disabled-by-default services. [Commit.](http://commits.kde.org/plasma-desktop/cc342c6992c6a54fcc0bfc158862316d1a413a2f) 
+ Kcms/access: Remove labels for visual and audible bell. [Commit.](http://commits.kde.org/plasma-desktop/d1c873a8e8fab07f402067e21c2c68b45ad2e37d) 
+ [kcms/access] Notify bell settings. [Commit.](http://commits.kde.org/plasma-desktop/2eeb464ac28930983c87ad4cf4e871fe6038cee4) 
+ Appiumtests: fix kickofftest. [Commit.](http://commits.kde.org/plasma-desktop/29190a1c29d45704f2e4cf37b0c683e9ee0a4015) 
+ Kcms/kded: show information instead of warning when a service is skipped. [Commit.](http://commits.kde.org/plasma-desktop/58c8771d45dc736a43d9decc4c13b9259a0ab46f) 
+ Containments/desktop: Execute updatePositionsList only if we know how many icons per stripe. [Commit.](http://commits.kde.org/plasma-desktop/c387739d24a7b75fbf38563fe5cffc232decebc5) Fixes bug [#498411](https://bugs.kde.org/498411)
+ Applets/kicker: tooltip type is Floating. [Commit.](http://commits.kde.org/plasma-desktop/510c46587f7ddd8c287c3bd7e22fe84bdb027558) Fixes bug [#95262](https://bugs.kde.org/95262)
+ Pplets/kicker: make tooltip text appear. [Commit.](http://commits.kde.org/plasma-desktop/8a76e2b26cbb48261d67128432867acf9a9d8b38) Fixes bug [#495858](https://bugs.kde.org/495858)
+ Revert "applets/taskmanager: enable `retainWhileLoading` for player album art". [Commit.](http://commits.kde.org/plasma-desktop/8777971e393faaacf86810e440f34de2a64639b4) 
{{< /details >}}

{{< details title="plasma-mobile" href="https://commits.kde.org/plasma-mobile" >}}
+ Don't try to set parent of ComboBoxForm's internal dialog. [Commit.](http://commits.kde.org/plasma-mobile/7b90374c0568f44c7f1d52400cb270f54d54166b) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Emit LastUsedRole change when TimeStampRole changes. [Commit.](http://commits.kde.org/plasma-nm/071efc40b7082b56c0594a2fe781d21af859f043) 
+ Make openvpn cipher parsing more robust. [Commit.](http://commits.kde.org/plasma-nm/04faff964185d3a3d0dfc311936f36f135ffbb13) Fixes bug [#498791](https://bugs.kde.org/498791)
+ ConnectionItem: Show Configure item when there is an Uuid. [Commit.](http://commits.kde.org/plasma-nm/069b0cb14efb1c412356c48b87e7ecb2ef678b02) 
+ Kded: Silence connection change warning. [Commit.](http://commits.kde.org/plasma-nm/8a562613d7c00534fbcb900c5ed2e11c26e1f3ff) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Don't show context menu in table view when right-clicking on nothing. [Commit.](http://commits.kde.org/plasma-systemmonitor/678d56900793bebc097f269f7669faa281103f16) Fixes bug [#498672](https://bugs.kde.org/498672)
+ Always add widget to the first screen. [Commit.](http://commits.kde.org/plasma-systemmonitor/750c5f8e218db946469c991dc32910c283e85cd5) Fixes bug [#496768](https://bugs.kde.org/496768)
{{< /details >}}

{{< details title="plasma-welcome" href="https://commits.kde.org/plasma-welcome" >}}
+ Enjoy/Supporters: Update supporters. [Commit.](http://commits.kde.org/plasma-welcome/77538e746f0c0e07d996f5b4fab9922e99a4dea2) 
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Shell: fix crash in PanelView::relativeConfigRect. [Commit.](http://commits.kde.org/plasma-workspace/63fd34b3101860c18019f4724f86919d275427aa) 
+ Drop unused KF6IdleTime dependency. [Commit.](http://commits.kde.org/plasma-workspace/f751fa23498341f736d31b1016fb008e3b3311cd) 
+ Panel: explicitly SkipSwitcher and KeepAbove after losing focus. [Commit.](http://commits.kde.org/plasma-workspace/cdec5f942757d8df4c10ed7928cdb31da46335b6) Fixes bug [#497596](https://bugs.kde.org/497596)
+ Applets/systray: use alternate colors background for systray entry config dialog. [Commit.](http://commits.kde.org/plasma-workspace/7460912ee88bbeec30b715b35b43fcb6f7a158dd) Fixes bug [#498822](https://bugs.kde.org/498822)
+ Klipper: check duplicate uuids when saving clip data. [Commit.](http://commits.kde.org/plasma-workspace/59eccd3812daf214eaed28694b3d013193caa928) 
+ Klipper: always try to open database. [Commit.](http://commits.kde.org/plasma-workspace/c116214002b24c40a1eb07b092b4c9ae0b4366f5) 
+ ActionButton: Make mnemonics work with Qt < 6.8 without animateClick. [Commit.](http://commits.kde.org/plasma-workspace/e0bd22a08160bc323a5d28a74ae3021d161588f3) 
+ Klipper: fix database not open when KeepClipboardContents is disabled. [Commit.](http://commits.kde.org/plasma-workspace/357aa9cb1e309bd21aa405de5d1dd720a667821d) 
+ Use tooltip title as title if no title is provided for system tray elements. [Commit.](http://commits.kde.org/plasma-workspace/d0ac61eb17b09408b12a0596f4c9276b77331ecb) Fixes bug [#498153](https://bugs.kde.org/498153)
+ Make panels request attention when an alternatives dialog is shown. [Commit.](http://commits.kde.org/plasma-workspace/9cdb58f03202595753f388866b7836c98ffd442f) Fixes bug [#498195](https://bugs.kde.org/498195)
+ Plasmashell: fix applets positions after containment switch. [Commit.](http://commits.kde.org/plasma-workspace/fd1b861111dc63e0e08b36edbfb23cc0a5f93d74) Fixes bug [#419590](https://bugs.kde.org/419590)
+ Appiumtests: make clipboardtest less flakey. [Commit.](http://commits.kde.org/plasma-workspace/719fe1ac087bc4725a9a0ac6bc1cc635821ed861) 
+ Applets/clock: Fix tooltip seconds. [Commit.](http://commits.kde.org/plasma-workspace/7cce04053bb566f72e45a0296e3847e4544a1b20) Fixes bug [#497296](https://bugs.kde.org/497296)
+ Kcms/wallpaper: Guard access to screen variable. [Commit.](http://commits.kde.org/plasma-workspace/5202cf7e3cdedf9655dc49f4114bbd675ea6c5cd) Fixes bug [#489775](https://bugs.kde.org/489775)
+ Do not consider last pixel to be part of panel containment view. [Commit.](http://commits.kde.org/plasma-workspace/f2659427a165c43e4b6fcf8388ed9087f3ad8fa7) Fixes bug [#483808](https://bugs.kde.org/483808)
+ Shell/panel: ensure at most one resize per frame is done. [Commit.](http://commits.kde.org/plasma-workspace/8f32c912adc9f0d9f829f474f2e53a9e8fe954b1) Fixes bug [#487549](https://bugs.kde.org/487549)
+ Waylandtasksmodel: Clean up pending stacking order. [Commit.](http://commits.kde.org/plasma-workspace/9aa5e89c70606f028d28cdfe3dfb120e77102452) 
+ Klipper: avoid creating too many KIO::DeleteJob when clearing history to fix potential crash. [Commit.](http://commits.kde.org/plasma-workspace/619d05b17ca12b526761979f9f07449b254a1eb2) 
+ Dataengines/weather: Fix applet update on network reconnection. [Commit.](http://commits.kde.org/plasma-workspace/dce47eea1cc86a78888a57bebf3c382b19c59a3c) Fixes bug [#498009](https://bugs.kde.org/498009)
+ Animate margin changes for system tray popups. [Commit.](http://commits.kde.org/plasma-workspace/e4eac6ceedd36d3f24f5b04c76f7adadfdb09530) See bug [#481533](https://bugs.kde.org/481533)
+ Applets/calendar: Use KSvg for the calendar icon. [Commit.](http://commits.kde.org/plasma-workspace/6a23620c5efa93c88f5ef8e3916d44ba7db155ab) Fixes bug [#498288](https://bugs.kde.org/498288)
+ Kioworkers/desktop: use qputenv to avoid race in reading environment variables. [Commit.](http://commits.kde.org/plasma-workspace/e1b8d036f58808c269ca91656c485d4b4caf3c0b) 
+ Appiumtests: make clipboardtest less flaky. [Commit.](http://commits.kde.org/plasma-workspace/5f6f18006416a88ee2ac069e3f16ca1f1de0d694) 
+ [kcms/notifications] Notify running applications about config changes. [Commit.](http://commits.kde.org/plasma-workspace/3f6d0883cf714100acc583f7a794a29a5ee20ecf) See bug [#482045](https://bugs.kde.org/482045)
+ Applets/clipboard: close config window if actioncollection gets deleted. [Commit.](http://commits.kde.org/plasma-workspace/6f339e07658a9bd96699e5ee1d119723dd0bae41) Fixes bug [#497927](https://bugs.kde.org/497927)
+ Klipper: hide progress info when clearing history. [Commit.](http://commits.kde.org/plasma-workspace/345668aa9ccf34c28c0fde37f3b31d827ca8f861) 
+ Fix broken ksmserver sanity check. [Commit.](http://commits.kde.org/plasma-workspace/d91025d35640cbaacce328d3c84bd66d6f3a9a69) Fixes bug [#498508](https://bugs.kde.org/498508). Fixes bug [#498491](https://bugs.kde.org/498491)
+ Session-restore: only restore each app once. [Commit.](http://commits.kde.org/plasma-workspace/5e9af6e3711ccf220ec3e1043fb343c3da006049) Fixes bug [#497980](https://bugs.kde.org/497980)
+ Applets/digital-clock: explicitly sort time zones by time. [Commit.](http://commits.kde.org/plasma-workspace/eb53e3c3af8591a59b98301b231adb32f4333547) Fixes bug [#498379](https://bugs.kde.org/498379)
+ Runners/webshortcuts: save available icon name to avoid repeatedly creating QIcon. [Commit.](http://commits.kde.org/plasma-workspace/2efb8562861269d9c86ffedbe642de306cbabddd) 
+ Klipper: Avoid use of winId. [Commit.](http://commits.kde.org/plasma-workspace/d5496141e11a75764d134724fc5a715b77522e97) Fixes bug [#494169](https://bugs.kde.org/494169)
{{< /details >}}

{{< details title="polkit-kde-agent-1" href="https://commits.kde.org/polkit-kde-agent-1" >}}
+ Improve systemd-homed support. [Commit.](http://commits.kde.org/polkit-kde-agent-1/a81bab365b7f142eddf5402d708438fb6c1a83d8) Fixes bug [#430828](https://bugs.kde.org/430828)
+ Propagate selected user changes from qml to cpp. [Commit.](http://commits.kde.org/polkit-kde-agent-1/fdaa09b6b8bb8670653598f962a2969c0cc51aed) Fixes bug [#497522](https://bugs.kde.org/497522)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Daemon: Make sure to load newly supported actions after a refresh. [Commit.](http://commits.kde.org/powerdevil/fbcbe0ccebea85a25a3a77dbb29a185a53d8c255) Fixes bug [#497362](https://bugs.kde.org/497362)
+ Daemon: Ensure legacy display ids exist before comparing the first one. [Commit.](http://commits.kde.org/powerdevil/7e524025c4c602dd33b10bd44e093485c53b8539) 
+ Daemon/actions: don't immediately suspend when the external monitor gets removed. [Commit.](http://commits.kde.org/powerdevil/4c4d4dda325e717ed31e517cecc83d5fd8657857) Fixes bug [#486328](https://bugs.kde.org/486328)
+ Actions/handlebuttonevents: also trigger suspend if we wake up with the lid closed. [Commit.](http://commits.kde.org/powerdevil/9a3cd8b01868ab64dd69f93a73209d8711e8fdf8) Fixes bug [#476492](https://bugs.kde.org/476492). See bug [#466025](https://bugs.kde.org/466025)
{{< /details >}}

{{< details title="qqc2-breeze-style" href="https://commits.kde.org/qqc2-breeze-style" >}}
+ Menu: Put it higher in the Z-order. [Commit.](http://commits.kde.org/qqc2-breeze-style/5a754489e20bd3f160e3b3d477124327f2e5d4b4) 
{{< /details >}}

{{< details title="spectacle" href="https://commits.kde.org/spectacle" >}}
+ Force QR code inline message to always be HTML. [Commit.](http://commits.kde.org/spectacle/713cd2d57fbac84795cd6678dbac2a38334c8a77) Fixes bug [#498618](https://bugs.kde.org/498618)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ Account: Always send a valid URI. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/2d753e6396de906d929e5723d531d8efbf9965c2) 
+ Guard against session closed while dialogs are shown. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/4320091496f33ae7ac616915f92838f6cd11e775) 
+ Inputcapture: Fix crash due to wrong context arg. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/2405c0c84f2ac377e2ff4aa04537d6e5fe91a872) Fixes bug [#494410](https://bugs.kde.org/494410)
+ Make the component name for GlobalShortcuts only consist of appId if provided. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/31e124bb8451a58f43374881800639fd2b430aac) Fixes bug [#492992](https://bugs.kde.org/492992)
+ Implement a secondary permission system. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/c8b38515281d118b8873280bdc7c11a46f7eb915) 
{{< /details >}}

