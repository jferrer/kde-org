---
title: Plasma 6.2.5 complete changelog
version: 6.2.5
hidden: true
plasma: true
type: fulllog
---

{{< details title="Discover" href="https://commits.kde.org/discover" >}}
+ UpdatesPage: Fix update description box overlapping with its text. [Commit.](http://commits.kde.org/discover/453cb49cc9509aa9a00c40de04c32fae1f50cdf3) Fixes bug [#491821](https://bugs.kde.org/491821)
{{< /details >}}

{{< details title="KDE GTK Config" href="https://commits.kde.org/kde-gtk-config" >}}
+ Don't try to create buttons when the factory failed to load. [Commit.](http://commits.kde.org/kde-gtk-config/47bebd84d88bfe665ab8ddc98a6b24a7ad5c7fc0) 
{{< /details >}}

{{< details title="Plasma Addons" href="https://commits.kde.org/kdeplasma-addons" >}}
+ Fixed issue that title was Info. And Author was not correct when a more than one () around text were used. [Commit.](http://commits.kde.org/kdeplasma-addons/b4be8855ecdede716795428e22546d1bcf17babb) 
+ Applets/userswitcher: Fit dialog height to content. [Commit.](http://commits.kde.org/kdeplasma-addons/19c2013f1493af77e3485e9d780317efccb2f9f1) 
{{< /details >}}

{{< details title="kpipewire" href="https://commits.kde.org/kpipewire" >}}
+ Flush encoder queue when streaming doesn't provide an update. [Commit.](http://commits.kde.org/kpipewire/8220adec494bfb79ee2b2e7db267efc9b319e688) 
{{< /details >}}

{{< details title="krdp" href="https://commits.kde.org/krdp" >}}
+ Server: when session is found, close it before erasing. [Commit.](http://commits.kde.org/krdp/855015ff80c871c1f6397b4c534d93e5cd2f2656) 
+ Krdpserver: Add dummy clipboard to avoid crashing. [Commit.](http://commits.kde.org/krdp/b537fdcb10a66fe3ea28277a35eeebc9fbfcbec1) 
+ Kcm: show only on wayland. [Commit.](http://commits.kde.org/krdp/5c4808a82b831c8aec96bf07f35786d0d00ae174) 
{{< /details >}}

{{< details title="kscreenlocker" href="https://commits.kde.org/kscreenlocker" >}}
+ X11locker: lower m_background when hiding. [Commit.](http://commits.kde.org/kscreenlocker/2d258e6c72d4c08d5ff01c92ff6e954b34b3ebe6) See bug [#483163](https://bugs.kde.org/483163)
{{< /details >}}

{{< details title="KWin" href="https://commits.kde.org/kwin" >}}
+ Plugins/krunnner-integration: don't trust inputs from the dbus call. [Commit.](http://commits.kde.org/kwin/d88df012ffafd9e681418bb9b7d1f6d844b2b6ca) 
+ CMake: do not expand variables beforehand. [Commit.](http://commits.kde.org/kwin/658360a5c4044515ebb2534dfac5de86bab5f8b1) 
+ Plugins/krunner-integration: Fix crash. [Commit.](http://commits.kde.org/kwin/6862be82d27a229235b9daf349ef02091407d473) 
+ Plugins/screencast: take scaling into account for window sources. [Commit.](http://commits.kde.org/kwin/4048d208f72b74f2393e6f129a1802ae447f228b) Fixes bug [#497571](https://bugs.kde.org/497571)
+ Opengl: Fix cached size check in GLTexture::render(). [Commit.](http://commits.kde.org/kwin/c1fd3ba21c778f654fe42eed03618b9dc1165c80) 
+ Plugins/shakecursor: don't trigger for warp events. [Commit.](http://commits.kde.org/kwin/3f7c0e05c1f3fe5e4ab3aae2ce90dbcbbd533d18) 
+ Backends/drm: fix the incorrect use of std::optional. [Commit.](http://commits.kde.org/kwin/7b2944c28427bf3a53e59405fdf326947651a055) 
+ Do not call ScreenLocker::KSldApp::unlocked when it unlocked in the meantime. [Commit.](http://commits.kde.org/kwin/e18ecdbe719dfa47f21dad53a9915fbb3c24047c) 
+ Backends/x11: Fix a crash in KWin::X11WindowedEglPrimaryLayer::present(). [Commit.](http://commits.kde.org/kwin/445a784ef3dc9995d071ae2aefc238f4869d071c) 
+ Autotests/integration: add a color management test. [Commit.](http://commits.kde.org/kwin/ed3942b0701fb1c2fd969dcaed1c4df4044f53a1) 
+ Wayland/xx color management: fix max > lum luminance checks. [Commit.](http://commits.kde.org/kwin/90227d8f768f6826243226c3aa8495c79551818a) 
+ Wayland: Fix XdgToplevelWindow::moveResizeInternal() committing geometry with fractional client size. [Commit.](http://commits.kde.org/kwin/f611b895f224e25b731c13d89dedd13dea14b373) 
+ Wayland: Fix sending wl_pointer.leave event to Xwayland during dnd. [Commit.](http://commits.kde.org/kwin/4cf9235c9f95e6a0c8e1b7fc490cd3a5b903804c) 
+ Opengl/eglnativefence: fix file descriptor leak. [Commit.](http://commits.kde.org/kwin/da1bebbb4480cfc26467ad3b31737b0df044b551) 
+ Effects/overview: Animate if the thumbnail is dropped in an heap. [Commit.](http://commits.kde.org/kwin/c402845961fa10185d7c28a49a9b72e7581258f8) Fixes bug [#496646](https://bugs.kde.org/496646)
{{< /details >}}

{{< details title="libplasma" href="https://commits.kde.org/libplasma" >}}
+ Fix wallpaper templates. [Commit.](http://commits.kde.org/libplasma/a2feb5eec45b1edbe5c461f4c3cfd426ddced14b) 
+ Always ensure setDestroyed propagates to all children applets. [Commit.](http://commits.kde.org/libplasma/fb588e8cdb96a6feba11512ca2ca0dff3a800e40) See bug [#472937](https://bugs.kde.org/472937)
+ AppletQuickItem: Better handle missing mainScript. [Commit.](http://commits.kde.org/libplasma/d071a9855f9826fb8d62ab14b9a5028aa5690501) 
{{< /details >}}

{{< details title="Plasma Desktop" href="https://commits.kde.org/plasma-desktop" >}}
+ Applets/taskmanager: Don't try to find service for application/octet-stream. [Commit.](http://commits.kde.org/plasma-desktop/35cb08363476a39175c214203251af9f26eb3e34) 
+ Appiumtests: skip test_6_sentry_3516_load_layout in CI. [Commit.](http://commits.kde.org/plasma-desktop/a3d0115ebcfe00b62a6597ae8182249f5dff11dc) 
+ Appiumtests: click Cancel to close file dialog. [Commit.](http://commits.kde.org/plasma-desktop/58917699d855de4fee94e2f0b519bed436f8cb2f) 
+ [kcms/mouse] Handle missing backend more gracefully. [Commit.](http://commits.kde.org/plasma-desktop/87ed22d43dddb2f3f72238a2cd311dd1c4362053) 
+ [kcm/access] Fix description of what locking sticky keys does. [Commit.](http://commits.kde.org/plasma-desktop/42394b253764b751eddcb3faa8ea15f7406ccc03) 
+ [kcms/mouse] Don't emit needsSaveChanged when adding device. [Commit.](http://commits.kde.org/plasma-desktop/251b6215b7b44bf46d7c87d6a5f50c3a695ea1cb) Fixes bug [#495231](https://bugs.kde.org/495231)
{{< /details >}}

{{< details title="Plasma Firewall" href="https://commits.kde.org/plasma-firewall" >}}
+ Fix DelegateChoice import with Qt 6.9. [Commit.](http://commits.kde.org/plasma-firewall/bc847242f45af5cf123f5b4944a835c4c8afaa2b) 
{{< /details >}}

{{< details title="Plasma Networkmanager (plasma-nm)" href="https://commits.kde.org/plasma-nm" >}}
+ Fix saving keepalive interval for WireGuard. [Commit.](http://commits.kde.org/plasma-nm/f35e99f31c50d49338e688a0c1c0be3c1c4dfe79) Fixes bug [#461319](https://bugs.kde.org/461319)
+ Revert "Fix the connection speed tab remaining visible after disconnecting". [Commit.](http://commits.kde.org/plasma-nm/db964af6124479126d0afafed55932241ddbb9d7) 
{{< /details >}}

{{< details title="Plasma Systemmonitor" href="https://commits.kde.org/plasma-systemmonitor" >}}
+ Page: Fix rowSpacing for EditablePage contents. [Commit.](http://commits.kde.org/plasma-systemmonitor/9cb4b8defd5ac9c593ccae423784bc3e200eb919) 
+ Page: Replace EditablePage's OpacityAnimator with NumberAnimation. [Commit.](http://commits.kde.org/plasma-systemmonitor/667dc7d18a7894a640b534e68ee29e6051b08c42) 
+ Page: Hide new pages by default. [Commit.](http://commits.kde.org/plasma-systemmonitor/f2b2739b10ac0ea0575e84c21463a3231df56eab) Fixes bug [#496875](https://bugs.kde.org/496875)
{{< /details >}}

{{< details title="Plasma Workspace" href="https://commits.kde.org/plasma-workspace" >}}
+ Components/dbus: fix invalid variant type since Qt 6.8.1. [Commit.](http://commits.kde.org/plasma-workspace/6e92a0c6c15eea0057763185ba6892d792fa3228) 
+ Applets/systemtray: fix busy indicator position for hidden items. [Commit.](http://commits.kde.org/plasma-workspace/125fbd9ee151bd675b9be3e7434a913cf95bf502) Fixes bug [#496910](https://bugs.kde.org/496910)
+ Libtaskmanager: use QDateTime instead of QTime for lastActivated. [Commit.](http://commits.kde.org/plasma-workspace/1b6359a4db1670b01f05aabb24735879ed629261) Fixes bug [#493724](https://bugs.kde.org/493724)
+ Appmenu: Avoid repositioning the menu on wayland. [Commit.](http://commits.kde.org/plasma-workspace/8609d78f1ac63a88d309eaa73af12b1eed4ac914) Fixes bug [#495787](https://bugs.kde.org/495787)
+ Libtaskmanager: Fix transient losing needs attention state. [Commit.](http://commits.kde.org/plasma-workspace/08e79de5b9d5dc91982f643ab6b79695dc893f17) 
+ Notifications: Expose copied file through desktop portal. [Commit.](http://commits.kde.org/plasma-workspace/7b76d6433d70fe34c5bd945cfd7828359d906869) 
+ Fix DelegateChoice import with Qt 6.9. [Commit.](http://commits.kde.org/plasma-workspace/6b014a7ee190d590e7bd900f8bd974ea8e137912) Fixes bug [#496900](https://bugs.kde.org/496900)
+ Krunner: Clear faded completion text when prior search exists. [Commit.](http://commits.kde.org/plasma-workspace/5a341e11fd41d3bae1321fdfae91478d7307387a) 
+ Systray: Clean up leaking systrays. [Commit.](http://commits.kde.org/plasma-workspace/7357e228ba88a6a16acc9f97628ce6fd4f40e2ab) See bug [#472937](https://bugs.kde.org/472937). See bug [#404641](https://bugs.kde.org/404641)
+ Applets/appmenu: Workaround global menu getting dismissed unexpectedly. [Commit.](http://commits.kde.org/plasma-workspace/d5a123f997a06bacafcc2e0b68c43c75ef90f9a0) Fixes bug [#494635](https://bugs.kde.org/494635)
{{< /details >}}

{{< details title="Powerdevil" href="https://commits.kde.org/powerdevil" >}}
+ Daemon: Don't crash in PowerDevil::Core::unloadAllActiveActions(). [Commit.](http://commits.kde.org/powerdevil/29f9b949c3ad75f11499825c79ba2a39344a26b0) Fixes bug [#492349](https://bugs.kde.org/492349)
{{< /details >}}

{{< details title="xdg-desktop-portal-kde" href="https://commits.kde.org/xdg-desktop-portal-kde" >}}
+ [FileChooser] Don't ignore current_filter parameter with multiple name patterns. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e92de218b2e1bd75167a4bede437fe52db082c80) See bug [#497399](https://bugs.kde.org/497399)
+ ScreenShot: Don't access the result of a cancelled future. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/e717cd55ba890046931972e0f7c27d08c0cc9352) 
+ Notification: Allow dbus-activating applications that are not running. [Commit.](http://commits.kde.org/xdg-desktop-portal-kde/929267e72f46a17e0402251d5d6d66977ea2a07a) 
{{< /details >}}

