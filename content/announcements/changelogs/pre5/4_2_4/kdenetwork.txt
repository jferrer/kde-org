------------------------------------------------------------------------
r961951 | mueller | 2009-05-01 08:21:29 +0000 (Fri, 01 May 2009) | 2 lines

fix build with Qt < 4.5

------------------------------------------------------------------------
r962622 | fabo | 2009-05-02 18:49:32 +0000 (Sat, 02 May 2009) | 3 lines

Remove duplicated X11VidMode find_package().
Comment Decibel find_package() as it isn't used at the moment.

------------------------------------------------------------------------
r963627 | cfeck | 2009-05-05 00:50:30 +0000 (Tue, 05 May 2009) | 8 lines

Do not access empty string (backport r963625)

This crash will be fixed in KDE 4.2.4

CCBUG: 191567
CCMAIL: dirk@kde.org


------------------------------------------------------------------------
r966492 | scripty | 2009-05-11 09:07:13 +0000 (Mon, 11 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r966522 | osterfeld | 2009-05-11 10:11:30 +0000 (Mon, 11 May 2009) | 3 lines

don't assert if chatMembers is empty. The !first() check looks weird, but I better leave it untouched.
needs forward porting

------------------------------------------------------------------------
r967308 | scripty | 2009-05-13 08:05:24 +0000 (Wed, 13 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r971170 | rjarosz | 2009-05-21 20:40:52 +0000 (Thu, 21 May 2009) | 5 lines

Backport commit 971166.
Always set identity for account.

CCBUG: 193370

------------------------------------------------------------------------
r973452 | scripty | 2009-05-27 08:46:18 +0000 (Wed, 27 May 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r974227 | mueller | 2009-05-28 19:18:05 +0000 (Thu, 28 May 2009) | 2 lines

4.2.4 update

------------------------------------------------------------------------
