------------------------------------------------------------------------
r1018668 | scripty | 2009-09-02 03:18:28 +0000 (Wed, 02 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019175 | scripty | 2009-09-03 04:10:40 +0000 (Thu, 03 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019807 | pali | 2009-09-04 12:45:02 +0000 (Fri, 04 Sep 2009) | 2 lines

skypebuttons doesnt use any suffixes

------------------------------------------------------------------------
r1020035 | pali | 2009-09-05 07:20:52 +0000 (Sat, 05 Sep 2009) | 7 lines

Fixed winpopup scripts winpoup-install and winpopup-send
Fixed Comment in user info dialog
Fixed time and date in winpoup-message
Fixed display name of myself contact in winpopup-protocol
Fixed process smbclient
Fixed workgroup for LOCALHOST

------------------------------------------------------------------------
r1020296 | rkcosta | 2009-09-05 23:06:11 +0000 (Sat, 05 Sep 2009) | 8 lines

Backport r1020295.

Correct the icon name used for the "Add to Your Contact List" context menu entry.

Patch by Markus Slopianka.

REVIEW: 1403

------------------------------------------------------------------------
r1020625 | mattr | 2009-09-06 19:38:22 +0000 (Sun, 06 Sep 2009) | 1 line

version bump for KDE 4.3.2 when it's released
------------------------------------------------------------------------
r1021790 | scripty | 2009-09-10 03:10:58 +0000 (Thu, 10 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1022262 | gberg | 2009-09-11 09:48:36 +0000 (Fri, 11 Sep 2009) | 3 lines

Backport r1022261.

BUG: 162493
------------------------------------------------------------------------
r1022408 | mfuchs | 2009-09-11 17:50:35 +0000 (Fri, 11 Sep 2009) | 4 lines

Backport r1022402
Downloads with KIO that were downloaded elsewhere (e.g. Konqueror) do not restart anymore.
CCBUG:170496

------------------------------------------------------------------------
r1022554 | rkcosta | 2009-09-12 01:25:27 +0000 (Sat, 12 Sep 2009) | 6 lines

Backport r1022553.

Fix the email icon name in the Address Book Selector Widget.

Patch by Markus Slopianka.

------------------------------------------------------------------------
r1022563 | rkcosta | 2009-09-12 02:45:56 +0000 (Sat, 12 Sep 2009) | 6 lines

Backport r1022562.

Make building skypebuttons optional.

This way we don't always assume Firefox is installed, and Firefox 3.5 stops crashing on startup here on FreeBSD 7.2.

------------------------------------------------------------------------
r1022577 | mattr | 2009-09-12 03:55:47 +0000 (Sat, 12 Sep 2009) | 9 lines

Fix bug 193419

Prevent the user from having to press the enter key to add an emoticon
when using the emoticon selector.

Patch by Vincent Dupont. Thanks!

This is the commit for KDE 4.3.
BUG: 193419
------------------------------------------------------------------------
r1022578 | mattr | 2009-09-12 03:55:59 +0000 (Sat, 12 Sep 2009) | 6 lines

Fix Gadu going offline when setting a status description

Patch by Jakub Grandys. Thanks!

BUG: 163541
This is the backport for the KDE 4.3 branch.
------------------------------------------------------------------------
r1022579 | mattr | 2009-09-12 03:56:13 +0000 (Sat, 12 Sep 2009) | 6 lines

Fix a bug with the parsing of the Gadu contact list

Patch by Jakub Grandys. Thanks!

BUG: 184696
This is the backport for the KDE 4.3 branch.
------------------------------------------------------------------------
r1022759 | pino | 2009-09-12 13:10:31 +0000 (Sat, 12 Sep 2009) | 2 lines

properly show the translated string for "No Photo" in the identity dialog

------------------------------------------------------------------------
r1022928 | pali | 2009-09-13 15:16:36 +0000 (Sun, 13 Sep 2009) | 2 lines

Show correct icons in jingle

------------------------------------------------------------------------
r1022932 | pali | 2009-09-13 15:26:48 +0000 (Sun, 13 Sep 2009) | 2 lines

Show and correct change online status with status message

------------------------------------------------------------------------
r1022945 | dinkar | 2009-09-13 15:57:22 +0000 (Sun, 13 Sep 2009) | 2 lines

Backport of '1022944'
added setSocket in the Constructor of BonjourContactConnection
------------------------------------------------------------------------
r1023027 | dinkar | 2009-09-13 18:50:40 +0000 (Sun, 13 Sep 2009) | 2 lines

Backport of r1023025
Solved the case of the disappearing first message
------------------------------------------------------------------------
r1023623 | scripty | 2009-09-15 03:12:29 +0000 (Tue, 15 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024114 | scripty | 2009-09-16 03:16:36 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1024144 | dfaure | 2009-09-16 08:12:05 +0000 (Wed, 16 Sep 2009) | 2 lines

QList asserts when calling first() and it's empty, so use a proper isEmpty test instead.

------------------------------------------------------------------------
r1024443 | scripty | 2009-09-16 16:13:24 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025576 | pali | 2009-09-19 07:29:53 +0000 (Sat, 19 Sep 2009) | 2 lines

Fix winpopup scripts

------------------------------------------------------------------------
r1026975 | scripty | 2009-09-23 03:16:53 +0000 (Wed, 23 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027742 | lueck | 2009-09-24 16:36:46 +0000 (Thu, 24 Sep 2009) | 2 lines

backport from trunk r1027741: load translation catalog, fix wrong use of I18N_NOOP
BUG:208373
------------------------------------------------------------------------
r1028167 | scripty | 2009-09-26 03:11:29 +0000 (Sat, 26 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1029127 | rkcosta | 2009-09-29 02:04:12 +0000 (Tue, 29 Sep 2009) | 4 lines

Backport r1016025.

Fix compile with GCC 4.4

------------------------------------------------------------------------
r1030235 | markuss | 2009-10-01 23:33:38 +0000 (Thu, 01 Oct 2009) | 1 line

SVN_SILENT: Fix a missing icon
------------------------------------------------------------------------
