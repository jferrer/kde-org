------------------------------------------------------------------------
r838935 | scripty | 2008-07-29 06:43:05 +0200 (Tue, 29 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r843841 | scripty | 2008-08-08 06:53:40 +0200 (Fri, 08 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r844458 | gyurco | 2008-08-09 22:02:35 +0200 (Sat, 09 Aug 2008) | 2 lines

Backport fixes from trunk.

------------------------------------------------------------------------
r845033 | scripty | 2008-08-11 07:03:49 +0200 (Mon, 11 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846764 | scripty | 2008-08-14 07:12:56 +0200 (Thu, 14 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r848593 | sitter | 2008-08-18 10:13:12 +0200 (Mon, 18 Aug 2008) | 4 lines

partial backport of r848592
 + properly install and use knetworkconf's documentation
 - don't change installation path in backport

------------------------------------------------------------------------
r850621 | aacid | 2008-08-21 22:48:06 +0200 (Thu, 21 Aug 2008) | 7 lines

Backport r850619 | aacid | 2008-08-21 22:46:52 +0200 (Thu, 21 Aug 2008) | 4 lines

remove translation because it causes bugs, better untranslated than break lilo.conf

For more info see 57677


------------------------------------------------------------------------
