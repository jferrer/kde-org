2006-03-19 02:51 +0000 [r520127]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/main.cpp: * Don't treat directories
	  as files; ark should not complain when directories already exist
	  * Usability improvement, ark now uses the same overwrite dialog
	  from the context menu and the main app I used an existing string
	  message for this, i hope it is ok BUG: 108316

2006-03-20 11:30 +0000 [r520570-520569]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber_priv.cpp: some
	  stubs completed

	* branches/KDE/3.5/kdeutils/kcalc/knumber/knumber.cpp: try to fix
	  cast to (unsigned long long int), when sign exists

2006-03-20 11:38 +0000 [r520573]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.h: Removed the limit
	  on display_length. This seemed to cause some problems, and since
	  we support arbitrary precision since KCalc-2.0 we don't need it
	  anymore. Also this should be the last patch for BUG 116835, BUG
	  118300, BUG 118909 (I hope, please, please please)

2006-03-20 17:03 +0000 [r520733]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: Fix for my previous
	  commit. Also continued work to make context menu and main ap p
	  behave the same way CCBUG: 108316

2006-03-27 21:27 +0000 [r523314]  charis

	* branches/KDE/3.5/kdeutils/ark/arch.cpp: Changed
	  KMessageBox::detailedError to KMessageBox::errorList to show
	  better the error output and be able to have a resizable window.
	  KMessageBox::errorList is showing all the text in one line
	  (including "\n") unless it is supplied by a QStringList BUG:
	  105982

2006-04-01 20:28 +0000 [r525377]  henrique

	* branches/KDE/3.5/kdeutils/ark/sevenzip.cpp,
	  branches/KDE/3.5/kdeutils/ark/sevenzip.h: * Fix support for 7zip
	  files when using newer versions of p7zip

2006-04-01 20:41 +0000 [r525379]  henrique

	* branches/KDE/3.5/kdeutils/ark/main.cpp: * Increase the version
	  number

2006-04-06 01:03 +0000 [r526941]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp,
	  branches/KDE/3.5/kdeutils/ark/arkwidget.h,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: * Clear the shell output
	  every time an error message is shown * When extraction fails,
	  don't show the "Open with" window

2006-04-06 01:23 +0000 [r526945]  charis

	* branches/KDE/3.5/kdeutils/ark/arkwidget.cpp: * Small fix for my
	  previous commit

2006-04-06 01:52 +0000 [r526950]  charis

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp,
	  branches/KDE/3.5/kdeutils/ark/rar.cpp,
	  branches/KDE/3.5/kdeutils/ark/tar.cpp: * According to krazy:
	  Adding single characters to a QString is faster if the characters
	  are QChars and not QStrings.
	  http://www.englishbreakfastnetwork.org/krazy/reports/kde-4.0/kdeutils/ark/index.html

2006-04-07 00:11 +0000 [r527139]  charis

	* branches/KDE/3.5/kdeutils/ark/extractiondialog.cpp: Usability
	  improvement. Don't repeat the text in the buttons

2006-04-07 19:03 +0000 [r527336]  henrique

	* branches/KDE/3.5/kdeutils/ark/extractiondialog.cpp: * Reverting
	  last commit, "Yes" is definitely not better than "Create Folder"
	  CCMAIL: haris@mpa.gr

2006-04-08 14:42 +0000 [r527535]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalc_core.cpp: Fix Bug 125109
	  (Thanks to Philip Rodrigues)

2006-04-09 02:52 +0000 [r527663]  henrique

	* branches/KDE/3.5/kdeutils/ark/ark_part.cpp: * Use a
	  KSqueezedTextLabel instead of a QLabel, so that the window
	  doesn't resize itself if the text can't fit on it.

2006-04-09 16:24 +0000 [r527946]  charis

	* branches/KDE/3.5/kdeutils/ark/main.cpp: Changed my name according
	  to my svn account

2006-04-11 03:52 +0000 [r528452]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/doc/python_api/make_api_doc.sh,
	  branches/KDE/3.5/kdeutils/superkaramba/doc/python_api/api_html.rc:
	  Update the script that creates api documentation via robodoc.
	  Should be more full proof now and leave the existing source files
	  completely untouched. Enjoy!

2006-04-12 10:01 +0000 [r528986]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp: Quick
	  work-around for (the most embarrasing) Bug 125410 Need to think
	  later what to do, whether to remove any limitation on the
	  DSP_SIZE (display size) or what?

2006-04-16 23:06 +0000 [r530504]  mpyne

	* branches/KDE/3.5/kdeutils/khexedit/hexvalidator.cc,
	  branches/KDE/3.5/kdeutils/khexedit/hexvalidator.h,
	  branches/KDE/3.5/kdeutils/khexedit/hexbuffer.cc,
	  branches/KDE/3.5/kdeutils/khexedit/hexeditorwidget.cc: Fix issues
	  with KHexEdit pointed out by Christoph's suspicious code check.
	  With CHexValidator I was unable to bring myself to simply fix the
	  immediate bug as the code is almost scary what with sprintf()
	  everywhere. So I fixed the design as well. I have tested,
	  everything seems to work properly, would appreciate if others
	  would test as well however. I will forwardport to trunk.

2006-04-17 08:46 +0000 [r530648]  rohanpm

	* branches/KDE/3.5/kdeutils/kedit/kedit.cpp: Fix & operator
	  precedence bug.

2006-04-18 20:12 +0000 [r531251]  charis

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp: We already
	  convert QString to KURL, actually use it! BUG: 125788

2006-04-23 14:55 +0000 [r533059]  charles

	* branches/KDE/3.5/kdeutils/klaptopdaemon/daemon_state.cpp: that's
	  a stupid setting, especially because the UI doesn't by default
	  reflect that it is enabled.

2006-04-24 07:54 +0000 [r533240]  kniederk

	* branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.cpp,
	  branches/KDE/3.5/kdeutils/kcalc/kcalcdisplay.h: Removed remaining
	  code fractions that once limited the size of the display. With
	  this Bug 125410 should be fixed. But need to test a lot!

2006-04-25 14:20 +0000 [r533702]  charis

	* branches/KDE/3.5/kdeutils/ark/compressedfile.cpp: Don't crash if
	  the temporary folder is removed during the process CCBUG: 126215

2006-04-28 23:12 +0000 [r535243]  nickell

	* branches/KDE/3.5/kdeutils/superkaramba/src/sklineedit.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/input.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/karamba_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/input_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/sklineedit.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/input.h,
	  branches/KDE/3.5/kdeutils/superkaramba/src/input_python.h: Add
	  functions to set/clear input focus on the input box.

2006-04-29 00:15 +0000 [r535252]  charis

	* branches/KDE/3.5/kdeutils/ark/ar.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.h,
	  branches/KDE/3.5/kdeutils/ark/zip.cpp,
	  branches/KDE/3.5/kdeutils/ark/compressedfile.cpp,
	  branches/KDE/3.5/kdeutils/ark/lha.cpp,
	  branches/KDE/3.5/kdeutils/ark/tar.cpp,
	  branches/KDE/3.5/kdeutils/ark/zoo.cpp,
	  branches/KDE/3.5/kdeutils/ark/arch.cpp: Check for unarchiver
	  program too. verifyCompressUtilityIsAvailable needs a second
	  argument to indicate if compression utility can do uncompressing
	  as well Also fixed some indentations while i was there :) BUG:
	  126051

2006-05-01 16:50 +0000 [r536230]  jriddell

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: Detect the
	  new /proc/suspend2 file for suspend2

2006-05-03 19:09 +0000 [r537032]  coolo

	* branches/KDE/3.5/kdeutils/kwallet/allyourbase.cpp: avoid crash
	  (CID 1859)

2006-05-03 21:01 +0000 [r537082]  thiago

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: One ) too
	  many

2006-05-04 16:37 +0000 [r537374]  lunakl

	* branches/KDE/3.5/kdeutils/kwallet/konfigurator/konfigurator.cpp:
	  Match kdelibs/kio/misc/kwalletd/kwalletd.cpp .

2006-05-15 09:55 +0000 [r540996]  mueller

	* branches/KDE/3.5/kdeutils/klaptopdaemon/portable.cpp: fix memory
	  leak (CID 2095)

2006-05-16 10:03 +0000 [r541447]  charles

	* branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.cpp: BUG:104234 "kgpg
	  deletes key server when setting one to default" Patch contributed
	  by Rolf Eike Beer, thanks!

2006-05-19 11:46 +0000 [r542460]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/keyservers.cpp: Don't add default
	  keyserver from GPG config file to list of keyservers if the value
	  is empty. Doing so would actually prevent some automatic
	  keyserver operations to work. BUG: 127453

2006-05-19 12:36 +0000 [r542483]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/conf_servers.ui,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.h,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.cpp: Add a button to
	  modify item in list of keyservers FEATURE:104831 GUI

2006-05-22 07:31 +0000 [r543477]  dakon

	* branches/KDE/3.5/kdeutils/kgpg/conf_servers.ui,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.h,
	  branches/KDE/3.5/kdeutils/kgpg/kgpgoptions.cpp: Revert new stuff
	  after feature freeze. Thanks to Bram for pointing out that this
	  was completely garbage to do so.

2006-05-23 10:34 +0000 [r543997]  binner

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kontact/src/main.cpp,
	  branches/arts/1.5/arts/configure.in.in,
	  branches/KDE/3.5/kdepim/akregator/src/aboutdata.h,
	  branches/KDE/3.5/kdepim/kmail/kmversion.h,
	  branches/KDE/3.5/kdelibs/README,
	  branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdeutils/kcalc/version.h,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm,
	  branches/KDE/3.5/kdebase/startkde,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdelibs/kdecore/kdeversion.h,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdebase/konqueror/version.h,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: bump version for KDE
	  3.5.3

