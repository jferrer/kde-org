2008-02-27 21:24 +0000 [r780036]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makewidget.cpp: I
	  hate it when I do such stupid, stupid mistakes and can't fix them
	  before anybody notices... This fixes empty lines in the build
	  outputview. BUG:158236

2008-02-28 17:36 +0000 [r780290]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp:
	  Eeek, another stupid mistake. Fix the behaviour of the "Warnings"
	  checkbox.

2008-03-04 17:36 +0000 [r782253-782252]  apaku

	* branches/KDE/3.5/kdevelop/vcs/clearcase/clearcasemanipulator.cpp:
	  Also add Q_OS_FREEBSD as requested from Michael Nottebrock.
	  CCBUG:140907

	* branches/KDE/3.5/kdevelop/vcs/clearcase/clearcasemanipulator.cpp:
	  Need to remember to wait for the build to finish before
	  comitting. Fix the typo.

2008-03-09 15:16 +0000 [r783785-783783]  dymo

	* branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.cpp,
	  branches/KDE/3.5/kdevelop/languages/ruby/rubysupport_part.h: Two
	  features for Ruby/Rails support I had on my disk but never
	  committed: - action to run current test method at cursor
	  (executes things like "ruby test/unit/foo_test.rb -n test_foo") -
	  parser improvement to recognize method end position in the file -
	  this makes function navigator show function at the cursor for
	  ruby as well

	* branches/KDE/3.5/kdevelop/languages/ruby/rubytemplates: Another
	  uncommitted ruby support-related change: - add html tags to the
	  list of ruby templates abbreviations

	* branches/KDE/3.5/kdevelop/languages/ruby/kdevrubysupport.rc: Add
	  "run current test function" action to the menu

2008-03-09 15:22 +0000 [r783786]  dymo

	* branches/KDE/3.5/kdevelop/lib/util/kdeveditorutil.cpp,
	  branches/KDE/3.5/kdevelop/parts/abbrev/abbrevpart.cpp: One more
	  change I did but never committed: - fix the current word locator
	  so it correctly extracts words when the cursor is not at the end
	  of the line example ("|" denotes the cursor position: <a
	  href="pn| "></a> "pn" is now correctly extracted as the current
	  word - unbreak abbreviation part so it doesn't make KDevelop
	  crash when abbreviation expansion happens inside the abbrev and
	  there's no text before and after abbrev (for example, when
	  "cla|ssd" is at the beginning of the line and the cursor is after
	  "a")

2008-03-19 15:03 +0000 [r787603]  apaku

	* branches/KDE/3.5/kdevelop/vcs/subversion/integrator/svnintegratordlgbase.ui:
	  Add a small help label to explain what kind of url the subversion
	  support expects.

2008-03-28 18:48 +0000 [r791212]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/debuggerpart.cpp:
	  Hmm, seems I forgot to commit this one, output stderr output of
	  the application when debugging.

2008-03-31 03:52 +0000 [r792057]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/version.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/config.h,
	  branches/KDE/3.5/kdevelop/parts/doxygen/version.h: Update to
	  Doxygen 1.5.5

2008-04-03 22:01 +0000 [r793405]  dagerbo

	* branches/KDE/3.5/kdevelop/parts/filelist/filelist_widget.cpp:
	  Attempt to maintain FileList scroll position when files are
	  opened or closed. BUG: 160341

2008-04-08 20:42 +0000 [r794907]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/appoutputwidget.cpp,
	  branches/KDE/3.5/kdevelop/lib/widgets/processwidget.cpp,
	  branches/KDE/3.5/kdevelop/parts/outputviews/makewidget.cpp: When
	  the process has exited make sure we flush our buffers BUG: 160582

2008-04-16 11:10 +0000 [r797551]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: Minimize diff
	  to doxygen svn HEAD

2008-04-21 13:57 +0000 [r799409]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: Some more
	  fixes from doxygen HEAD

2008-04-21 14:12 +0000 [r799424]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: More updates

2008-05-06 12:53 +0000 [r804566]  mueller

	* branches/KDE/3.5/kdevelop/vcs/subversion/subversion_widget.cpp:
	  reshuffle a bit to avoid a compiler warning

2008-05-08 20:31 +0000 [r805592]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/script/scriptprojectpart.h,
	  branches/KDE/3.5/kdevelop/buildtools/script/scriptprojectpart.cpp:
	  Improve speed of loading script projects. Patch from Johannes
	  Jowereit BUG: 113254

2008-05-08 20:35 +0000 [r805593]  apaku

	* branches/KDE/3.5/kdevelop/parts/astyle/astyle_part.cpp: Allow
	  .hpp as file extension for astyle. Patch by Daniel Price
	  BUG:149584

2008-05-11 14:05 +0000 [r806492]  aclu

	* branches/KDE/3.5/kdevelop/parts/appwizard/licenses/NCSA (added),
	  branches/KDE/3.5/kdevelop/parts/appwizard/appwizarddlg.cpp,
	  branches/KDE/3.5/kdevelop/parts/appwizard/licenses/MIT (added),
	  branches/KDE/3.5/kdevelop/parts/appwizard/licenses/Makefile.am:
	  Add MIT and NCSA license templates. Original patch by Maciej
	  Pilichowski BUG 161954

2008-05-13 13:56 +0000 [r807257]  aclu

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm: KDevelop 3.5.2

2008-06-06 12:40 +0000 [r817621]  mueller

	* branches/KDE/3.5/kdevelop/vcs/subversion/configure.in.in: always
	  link the libraries you're using

2008-06-17 19:47 +0000 [r821556]  aclu

	* branches/KDE/3.5/kdevelop/languages/cpp/debugger/mi/Makefile.am,
	  branches/KDE/3.5/kdevelop/src/profileengine/lib/Makefile.am,
	  branches/KDE/3.5/kdevelop/lib/cppparser/Makefile.am,
	  branches/KDE/3.5/kdevelop/parts/documentation/interfaces/Makefile.am,
	  branches/KDE/3.5/kdevelop/lib/interfaces/Makefile.am,
	  branches/KDE/3.5/kdevelop/lib/interfaces/external/Makefile.am,
	  branches/KDE/3.5/kdevelop/buildtools/lib/widgets/Makefile.am,
	  branches/KDE/3.5/kdevelop/src/newui/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/lib/designer_integration/Makefile.am,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extras/Makefile.am,
	  branches/KDE/3.5/kdevelop/buildtools/lib/base/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/lib/debugger/Makefile.am:
	  Always link against the used libraries. It fixes compile with
	  LDFLAGS=" -Wl,--as-needed -Wl,--no-undefined"

2008-06-23 00:14 +0000 [r823314]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kofficepart/kopart.desktop,
	  branches/KDE/3.5/kdevelop/kdevassistant.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/khello/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/wxhello/app.desktop,
	  branches/KDE/3.5/kdevelop/kdevelop_scripting.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kde4app/kapp4.desktop,
	  branches/KDE/3.5/kdevelop/kdevelop_kde_cpp.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kconfig35/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kmake/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/kapp/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kapp/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/java/app_templates/kappjava/app.desktop,
	  branches/KDE/3.5/kdevelop/parts/appwizard/common/kde-app.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopia4app/example.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/khello2/app.desktop,
	  branches/KDE/3.5/kdevelop/kdevelop.desktop,
	  branches/KDE/3.5/kdevelop/kdevelop_ruby.desktop,
	  branches/KDE/3.5/kdevelop/languages/ruby/app_templates/kxt/app.desktop,
	  branches/KDE/3.5/kdevelop/kdevdesigner/src/kdevdesigner.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kxt/app.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kscreensaver/kscreensaver.desktop,
	  branches/KDE/3.5/kdevelop/kdevelop_c_cpp.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/opieapp/example.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kcmodule/module.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qtopiaapp/example.desktop:
	  More compliant to desktop file spec. Patch by Carsten Lohrke
	  carlo at gentoo dot org.

2008-06-24 09:03 +0000 [r823817]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp,
	  branches/KDE/3.5/kdevelop/parts/doxygen/version.cpp: Update to
	  Doxygen 1.5.6

2008-06-27 22:00 +0000 [r825359]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: Update
	  Doxygen to HEAD

2008-06-29 08:28 +0000 [r825722]  apaku

	* branches/KDE/3.5/kdevelop/lib/cppparser/Makefile.am,
	  branches/KDE/3.5/kdevelop/buildtools/lib/widgets/Makefile.am,
	  branches/KDE/3.5/kdevelop/languages/lib/designer_integration/Makefile.am,
	  branches/KDE/3.5/kdevelop/buildtools/lib/base/Makefile.am: Note
	  to self: Better try out external patches and don't just review
	  them. The last change to the Makefile.am's introduced new
	  dependencies from these libs to kdevinterfaces, unfortunately the
	  patch doesn't work with out-of-source builds. Thats why debian
	  package building broke - they use out-of-src boulds for their
	  packages. If you want to depends on a .la file you always need to
	  use ${top_builddir} not ${top_srcdir}. CCMAIL:
	  webmaster@kdevelop.org

2008-06-29 22:54 +0000 [r826186]  anagl

	* branches/KDE/3.5/kdevelop/editors/editor-chooser/kdeveditorchooser.desktop,
	  branches/KDE/3.5/kdevelop/parts/bookmarks/kdevbookmarks.desktop,
	  branches/KDE/3.5/kdevelop/parts/distpart/kdevdistpart.desktop,
	  branches/KDE/3.5/kdevelop/parts/regexptest/kdevregexptest.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kpartapp/app.desktop,
	  branches/KDE/3.5/kdevelop/parts/filelist/kdevfilelist.desktop,
	  branches/KDE/3.5/kdevelop/vcs/perforce/integrator/kdevperforceintegrator.desktop,
	  branches/KDE/3.5/kdevelop/vcs/cvsservice/kdevcvsservice.desktop,
	  branches/KDE/3.5/kdevelop/parts/quickopen/kdevquickopen.desktop,
	  branches/KDE/3.5/kdevelop/parts/tipofday/kdevtipofday.desktop,
	  branches/KDE/3.5/kdevelop/parts/tools/kdevtools.desktop,
	  branches/KDE/3.5/kdevelop/parts/classview/kdevclassview.desktop,
	  branches/KDE/3.5/kdevelop/languages/fortran/kdevfortransupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/compiler/gccoptions/kdevg77options.desktop,
	  branches/KDE/3.5/kdevelop/parts/appwizard/kdevappwizard.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/devhelp/docdevhelpplugin.desktop,
	  branches/KDE/3.5/kdevelop/parts/scripting/kdevscripting.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/djvu/docdjvuplugin.desktop,
	  branches/KDE/3.5/kdevelop/parts/ctags2/kdevctags2.desktop,
	  branches/KDE/3.5/kdevelop/kdevdesigner/designer/kdevdesigner_part.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/custom/doccustomplugin.desktop,
	  branches/KDE/3.5/kdevelop/parts/texttools/kdevtexttools.desktop,
	  branches/KDE/3.5/kdevelop/languages/csharp/kdevcsharpsupport.desktop,
	  branches/KDE/3.5/kdevelop/parts/valgrind/kdevvalgrind.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extras/kdevelopcompileroptions.desktop,
	  branches/KDE/3.5/kdevelop/parts/partexplorer/kdevpartexplorer.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kicker/applet.desktop,
	  branches/KDE/3.5/kdevelop/languages/pascal/kdevpascalsupport.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopcreatefile.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/kdevdocumentation.desktop,
	  branches/KDE/3.5/kdevelop/vcs/clearcase/integrator/kdevclearcaseintegrator.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/kdevtoc/dockdevtocplugin.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/pascal/kdevpascalproject.desktop,
	  branches/KDE/3.5/kdevelop/languages/kjssupport/kdevkjssupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kofficepart/x-kopart.desktop,
	  branches/KDE/3.5/kdevelop/parts/outputviews/kdevmakeview.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/kdevelopproject.desktop,
	  branches/KDE/3.5/kdevelop/parts/replace/kdevreplace.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/interfaces/kdevelopdocumentationplugins.desktop,
	  branches/KDE/3.5/kdevelop/languages/php/kdevphpsupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/ruby/debugger/kdevrbdebugger.desktop,
	  branches/KDE/3.5/kdevelop/parts/doxygen/kdevdoxygen.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/qt/docqtplugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/lib/interfaces/kdeveloppcsimporter.desktop,
	  branches/KDE/3.5/kdevelop/embedded/visualboyadvance/kdevvisualboyadvance.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/pdb/docpdbplugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/kjssupport/x-javascript-source.desktop,
	  branches/KDE/3.5/kdevelop/languages/pascal/compiler/fpcoptions/kdevfpcoptions.desktop,
	  branches/KDE/3.5/kdevelop/languages/fortran/compiler/pgioptions/kdevpgf77options.desktop,
	  branches/KDE/3.5/kdevelop/languages/bash/kdevbashsupport.desktop,
	  branches/KDE/3.5/kdevelop/parts/snippet/kdevsnippet.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevpart2/kdevpart.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopversioncontrol.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/pdf/docpdfplugin.desktop,
	  branches/KDE/3.5/kdevelop/parts/outputviews/kdevappoutputview.desktop,
	  branches/KDE/3.5/kdevelop/x-kdevelop.desktop,
	  branches/KDE/3.5/kdevelop/parts/vcsmanager/kdevvcsmanager.desktop,
	  branches/KDE/3.5/kdevelop/languages/ruby/kdevrubysupport.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopquickopen.desktop,
	  branches/KDE/3.5/kdevelop/vcs/subversion/integrator/kdevsubversionintegrator.desktop,
	  branches/KDE/3.5/kdevelop/parts/konsole/kdevkonsoleview.desktop,
	  branches/KDE/3.5/kdevelop/parts/grepview/kdevgrepview.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/custommakefiles/kdevcustomproject.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevpart/kdevpart.desktop,
	  branches/KDE/3.5/kdevelop/parts/appwizard/common/kde-part.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/script/kdevscriptproject.desktop,
	  branches/KDE/3.5/kdevelop/mimetypes/x-fortran.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopcodebrowserfrontend.desktop,
	  branches/KDE/3.5/kdevelop/vcs/subversion/kdevsubversion.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/kdeveloplanguagesupport.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/autotools/kdevkdeautoproject.desktop,
	  branches/KDE/3.5/kdevelop/parts/fullscreen/kdevfullscreen.desktop,
	  branches/KDE/3.5/kdevelop/vcs/clearcase/kdevclearcase.desktop,
	  branches/KDE/3.5/kdevelop/vcs/cvsservice/integrator/kdevcvsserviceintegrator.desktop,
	  branches/KDE/3.5/kdevelop/parts/filter/kdevfilter.desktop,
	  branches/KDE/3.5/kdevelop/vcs/subversion/kdevsvnd.desktop,
	  branches/KDE/3.5/kdevelop/vcs/perforce/kdevperforce.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/kdevelopplugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/kdevcppsupport.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/autotools/kdevautoproject.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extras/kdevelopvcsintegrator.desktop,
	  branches/KDE/3.5/kdevelop/parts/diff/kdevdiff.desktop,
	  branches/KDE/3.5/kdevelop/parts/uimode/kdevuichooser.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/ant/kdevantproject.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/compiler/gccoptions/kdevgppoptions.desktop,
	  branches/KDE/3.5/kdevelop/parts/fileview/kdevfileview.desktop,
	  branches/KDE/3.5/kdevelop/languages/ada/kdevadasupport.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopdifffrontend.desktop,
	  branches/KDE/3.5/kdevelop/buildtools/ada/kdevadaproject.desktop,
	  branches/KDE/3.5/kdevelop/parts/fileview/kdevfilegroups.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/compiler/gccoptions/kdevgccoptions.desktop,
	  branches/KDE/3.5/kdevelop/parts/openwith/kdevopenwith.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopmakefrontend.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopappfrontend.desktop,
	  branches/KDE/3.5/kdevelop/parts/fileselector/kdevfileselector.desktop,
	  branches/KDE/3.5/kdevelop/parts/astyle/kdevastyle.desktop,
	  branches/KDE/3.5/kdevelop/parts/abbrev/kdevabbrev.desktop,
	  branches/KDE/3.5/kdevelop/lib/interfaces/extensions/kdevelopsourceformatter.desktop,
	  branches/KDE/3.5/kdevelop/parts/documentation/plugins/chm/docchmplugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kfileplugin/kfile_plugin.desktop,
	  branches/KDE/3.5/kdevelop/languages/pascal/compiler/dccoptions/kdevdccoptions.desktop,
	  branches/KDE/3.5/kdevelop/parts/filecreate/kdevfilecreate.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/debugger/kdevdebugger.desktop,
	  branches/KDE/3.5/kdevelop/languages/sql/kdevsqlsupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kdevlang/kdevlang.desktop,
	  branches/KDE/3.5/kdevelop/languages/java/kdevjavasupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/python/kdevpythonsupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/fortran/compiler/pgioptions/kdevpghpfoptions.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/konqnavpanel/konqnavpanel.desktop,
	  branches/KDE/3.5/kdevelop/languages/perl/kdevperlsupport.desktop,
	  branches/KDE/3.5/kdevelop/languages/cpp/kdevcsupport.desktop:
	  Desktop validation fixes: remove deprecated entries for Encoding.

2008-07-05 19:54 +0000 [r828429]  apaku

	* branches/KDE/3.5/kdevelop/parts/replace/replace_part.h,
	  branches/KDE/3.5/kdevelop/parts/replace/replace_part.cpp: Disable
	  the action for find-replace when no project is open. BUG:79722

2008-07-06 19:36 +0000 [r828849]  apaku

	* branches/KDE/3.5/kdevelop/parts/fileview/filetreewidget.cpp:
	  Adjust the implementation a bit to make sure the joined QString
	  isn't somehow deleted when we don't want that. Should fix
	  #146111. BUG:146111

2008-07-06 20:01 +0000 [r828853]  apaku

	* branches/KDE/3.5/kdevelop/parts/outputviews/makewidget.cpp: Fix
	  tracing of subdir leaving with custom makefiles. Patch provided
	  by Martin Fuhrer (mfuhrer at alumni ucalgary ca). BUG:147718

2008-07-06 21:00 +0000 [r828875]  apaku

	* branches/KDE/3.5/kdevelop/languages/php/phpcodecompletion.cpp,
	  branches/KDE/3.5/kdevelop/languages/php/phpfile.cpp: Allow to use
	  final keyword in PHP. Bugfix submitted by Konstantin V. Arkhipov
	  (voxus at onphp org) BUG:151525

2008-07-06 21:35 +0000 [r828887]  apaku

	* branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakeapp/src.pro,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qt4makeapp/src.pro,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/kmake/src.pro,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qmakesimple/src.pro,
	  branches/KDE/3.5/kdevelop/languages/cpp/app_templates/qt4hello/src.pro:
	  Use TARGET as documented, only setting the name of the target,
	  use DESTDIR for the location. BUG:158722

2008-07-17 09:47 +0000 [r833662]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp:
	  Add support for Qt4.4 libraries: QtWebkit, Phonon and
	  XMLPatternist. Patch by Bernd Buschinski

2008-07-17 13:45 +0000 [r833868]  apaku

	* branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlgbase.ui,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/projectconfigurationdlg.cpp,
	  branches/KDE/3.5/kdevelop/buildtools/qmake/scope.cpp: Another
	  patch for Qt4.4 support (and some cleanup) Thanks to Bernd
	  Buschinski.

2008-07-31 13:37 +0000 [r840145]  apaku

	* branches/KDE/3.5/kdevelop/vcs/subversion/svn_kio.cpp: Fix a
	  problem when using svn support in non-utf-locales, by using
	  svn_cmdline_init. BUG:167824

2008-07-31 21:04 +0000 [r840311]  aclu

	* branches/KDE/3.5/kdevelop/parts/doxygen/config.cpp: Update to the
	  latest doxygen version

2008-08-16 20:48 +0000 [r848077]  apaku

	* branches/KDE/3.5/kdevelop/languages/php/doc/php.toc,
	  branches/KDE/3.5/kdevelop/languages/python/doc/python.toc: Update
	  toc files so the documentation works again at least at a basic
	  level. Patches from Christian Reiter, thanks for helping out
	  here. MAIL:167076

2008-08-20 22:18 +0000 [r850174]  apaku

	* branches/KDE/3.5/kdevelop/configure.in.in,
	  branches/KDE/3.5/kdevelop/kdevelop.lsm: Increase version for the
	  release to 3.5.3

2008-08-20 23:58 +0000 [r850200]  aclu

	* branches/KDE/3.5/kdevelop/kdevelop.lsm: bump date too

