------------------------------------------------------------------------
r1168375 | nienhueser | 2010-08-26 15:01:09 +0100 (dj, 26 ago 2010) | 1 line

fix compiler warnings about unused variables
------------------------------------------------------------------------
r1168365 | rahn | 2010-08-26 14:47:39 +0100 (dj, 26 ago 2010) | 3 lines

- Adjusting the GENERIC_LIB_VERSION to 0.10.1, too.


------------------------------------------------------------------------
r1168348 | rahn | 2010-08-26 14:20:18 +0100 (dj, 26 ago 2010) | 4 lines


- Bumping up version to 0.10.1


------------------------------------------------------------------------
r1168342 | nienhueser | 2010-08-26 14:04:08 +0100 (dj, 26 ago 2010) | 4 lines

Stricter plugin load checks: Refuse to load plugins with older interface versions. Fixes incompatible/old plugins crashing Marble. Backport of commit 1168304.
BUG: 239831
RB: 5149

------------------------------------------------------------------------
r1168293 | lueck | 2010-08-26 13:00:28 +0100 (dj, 26 ago 2010) | 1 line

backport from trunk screenshot update
------------------------------------------------------------------------
r1168241 | lueck | 2010-08-26 09:32:07 +0100 (dj, 26 ago 2010) | 1 line

backport rev 1168236 updated screenshots
------------------------------------------------------------------------
r1167912 | nienhueser | 2010-08-25 18:05:49 +0100 (dc, 25 ago 2010) | 3 lines

Restore the old behavior of not showing a progress animation for fast replies (<100ms).
Backport of commit 1167911.

------------------------------------------------------------------------
r1167901 | rahn | 2010-08-25 17:31:52 +0100 (dc, 25 ago 2010) | 13 lines

Backport of 

SVN commit 1167566 by beschow:

add support for AlphaBlending

* this allows for hill shading in OSM, for instance

- Tested with hikebikemap.dgml
- virtual methods checked for identity of signature wrt binary
  compatibility


------------------------------------------------------------------------
r1167745 | sitter | 2010-08-25 09:06:51 +0100 (dc, 25 ago 2010) | 5 lines

backport r1167743/r1167744

Change 'cunts' into 'cheap' so that edubuntu people can sleep well again.
https://bugs.launchpad.net/ubuntu/+source/kdeedu/+bug/622592

------------------------------------------------------------------------
r1167631 | rahn | 2010-08-25 06:56:37 +0100 (dc, 25 ago 2010) | 10 lines

Forward port of SVN commit 1167172 by nienhueser:

Replace the mng animation with an in-memory animation very similar to
the oxygen busy cursor (two white balls rotating around each other).
Provides a more consistent look and should fix missing animations where
mng support is not there (windows, maemo).

Tested a few times with various cases


------------------------------------------------------------------------
r1167627 | rahn | 2010-08-25 06:10:32 +0100 (dc, 25 ago 2010) | 4 lines


- Show map title in title bar.


------------------------------------------------------------------------
r1167618 | rahn | 2010-08-25 04:59:16 +0100 (dc, 25 ago 2010) | 4 lines


- echoMode Password for Password in Proxy settings


------------------------------------------------------------------------
r1167524 | jmhoffmann | 2010-08-24 21:35:09 +0100 (dt, 24 ago 2010) | 12 lines

Backport commit 1167517:

Switch Marble Qt version to maps-4.5.php.

In Marble 0.10 (released with KDE 4.5) there have been changes in the
way the clouds layer is handled. It now uses the generic blending classes.
Currently there are three map themes available for download which have a clouds
layer and for which we have to provide versions for Marble versions 0.10+.

In addition to that we might (hopefully) see new map themes which use the
blending feature. These maps will not work in Marble versions before 0.10.

------------------------------------------------------------------------
r1167522 | jmhoffmann | 2010-08-24 21:33:33 +0100 (dt, 24 ago 2010) | 12 lines

Backport commit 1167516:

Get Hot New Stuff / KNewStuff (GHNS/KNS): Switch to providers for 4.5+.

In Marble 0.10 (released with KDE 4.5) there have been changes in the
way the clouds layer is handled. It now uses the generic blending classes.
Currently there are three map themes available via GHNS which have a clouds
layer and for which we have to provide versions for Marble versions 0.10+.

In addition to that we might (hopefully) see new map themes which use the
blending feature. These maps will not work in Marble versions before 0.10.

------------------------------------------------------------------------
r1167489 | aacid | 2010-08-24 20:16:06 +0100 (dt, 24 ago 2010) | 2 lines

Backport fix Carmathenshire -> Carmarthenshire

------------------------------------------------------------------------
r1166805 | rahn | 2010-08-23 00:09:08 +0100 (dl, 23 ago 2010) | 4 lines


- This hopefully fixes the issue of cut of text on the windows platform


------------------------------------------------------------------------
r1166462 | rahn | 2010-08-21 22:32:40 +0100 (ds, 21 ago 2010) | 3 lines

- These didn't get added.


------------------------------------------------------------------------
r1166446 | rahn | 2010-08-21 21:37:47 +0100 (ds, 21 ago 2010) | 6 lines

- New dmg background.

-- Diese und die folgenden Zeilen werden ignoriert -

AM   install_mac/marble_mac.jpg

------------------------------------------------------------------------
r1166379 | rahn | 2010-08-21 18:00:09 +0100 (ds, 21 ago 2010) | 3 lines

- Additional graphics for the windows installer.


------------------------------------------------------------------------
r1166356 | rahn | 2010-08-21 17:17:49 +0100 (ds, 21 ago 2010) | 4 lines

- Icon refresh for Qt-Only version
- Should fix icons not being displayed in the plugins dialog


------------------------------------------------------------------------
r1166334 | rahn | 2010-08-21 16:28:08 +0100 (ds, 21 ago 2010) | 3 lines

- Adjust version number


------------------------------------------------------------------------
r1166295 | rahn | 2010-08-21 13:35:28 +0100 (ds, 21 ago 2010) | 4 lines


- class documentation for AbstractTile, TextureTile and StackedTile.


------------------------------------------------------------------------
r1166249 | rahn | 2010-08-21 11:25:17 +0100 (ds, 21 ago 2010) | 4 lines


- Class documentation for StackedTile


------------------------------------------------------------------------
r1164838 | aacid | 2010-08-17 20:04:32 +0100 (dt, 17 ago 2010) | 2 lines

new malawi flag

------------------------------------------------------------------------
r1164647 | rahn | 2010-08-17 12:31:00 +0100 (dt, 17 ago 2010) | 3 lines

Quick fix ...


------------------------------------------------------------------------
r1164388 | rahn | 2010-08-16 17:52:38 +0100 (dl, 16 ago 2010) | 3 lines

- Backport of 1135592


------------------------------------------------------------------------
r1164134 | scripty | 2010-08-16 03:11:59 +0100 (dl, 16 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1163591 | tcanabrava | 2010-08-14 14:52:26 +0100 (ds, 14 ago 2010) | 1 line

Adding an edge between two nodes that already had an edge resulted in a crash.
------------------------------------------------------------------------
r1163352 | nienhueser | 2010-08-13 21:49:00 +0100 (dv, 13 ago 2010) | 5 lines

The language code to generate instructions for is taken from the system locale in Marble. ORS does not handle all locales and reports an error if it does not know one. Currently Marble informs the user about the problem with an error dialog. The patch changes this to catch the error early and generate a new request with english as the fallback language code. This way routes are retrieved, although not localized.
RB: 4987
BUG: 247339


------------------------------------------------------------------------
r1162022 | rahn | 2010-08-11 08:22:44 +0100 (dc, 11 ago 2010) | 3 lines

- Don't suggest "http://" for the proxy.


------------------------------------------------------------------------
r1160575 | nienhueser | 2010-08-08 13:53:13 +0100 (dg, 08 ago 2010) | 2 lines

fix 'work offline' status indicator setting not being restored after startup. Backport of commit 1160571.

------------------------------------------------------------------------
r1159917 | gladhorn | 2010-08-06 15:14:02 +0100 (dv, 06 ago 2010) | 5 lines

Flash card mode: change the solution text before making it visible, otherwise for a short moment the last solution is visible.

backport r1159914 


------------------------------------------------------------------------
r1159721 | scripty | 2010-08-06 03:18:10 +0100 (dv, 06 ago 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1159222 | lueck | 2010-08-04 20:38:51 +0100 (dc, 04 ago 2010) | 1 line

backport fixes from trunk
------------------------------------------------------------------------
