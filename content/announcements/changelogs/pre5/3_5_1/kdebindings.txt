2005-12-06 14:02 +0000 [r486009]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: 2005-12-06 Richard
	  Dale <Richard_Dale@tipitina.demon.co.uk> * QtRuby didn't work
	  with versions of ruby > 1.8.3, as it didn't call initialize
	  methods correctly. It used the rb_respond_to() method to check it
	  a newly created qt instance responded to :initialize. However, in
	  newer versions of ruby rb_responds_to() ignores private methods
	  such as initialize(). The solution was to just remove the test,
	  as it was redundant anyway. * Fixes problem reported by Hans
	  Fugel and Caleb Tennis. CCMAIL: kde-bindings@kde.org

2005-12-06 15:39 +0000 [r486034]  ctennis

	* branches/KDE/3.5/kdebindings/smoke/kde/qtguess.pl.in,
	  branches/KDE/3.5/kdebindings/smoke/qt/qtguess.pl.in: We rely on
	  finding qglobal.h, but with the Qt4 headers in $includedir/Qt/
	  instead of just $includedir, we need to help this package find
	  it.

2005-12-06 15:49 +0000 [r486035]  ctennis

	* branches/KDE/3.5/kdebindings/smoke/qt/generate.pl.in: Let
	  kalyptus find Qt headers

2005-12-06 16:23 +0000 [r486045]  ctennis

	* branches/KDE/3.5/kdebindings/smoke/qt/header_list: Update the
	  header_list to cut down on kalyptus errors. Still not complete
	  yet, though.

2005-12-06 16:32 +0000 [r486050]  ctennis

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToSmoke.pm:
	  Update common lists

2005-12-06 17:53 +0000 [r486064]  ctennis

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToSmoke.pm:
	  Sorry, apparently svn switch didn't complete the switch on my
	  system

2005-12-06 18:00 +0000 [r486067]  ctennis

	* branches/KDE/3.5/kdebindings/smoke/qt/header_list,
	  branches/KDE/3.5/kdebindings/smoke/kde/qtguess.pl.in,
	  branches/KDE/3.5/kdebindings/smoke/qt/generate.pl.in,
	  branches/KDE/3.5/kdebindings/smoke/qt/qtguess.pl.in: Revert my
	  previous changes, as they weren't in trunk

2005-12-08 18:27 +0000 [r486770]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: 2005-12-08 Richard
	  Dale <Richard_Dale@tipitina.demon.co.uk> * The ruby display()
	  method was clashing with a display() method in some QtRuby
	  classes, and so it was aliased to _display(). However, this
	  caused problems with the ruby RMagick extension. The display
	  methods are now special cased in Qt::LCDNumber, Qt::WhatsThis and
	  Qt::TimeEdit. Fixes problem reported by David Corbin. * The slots
	  and signals methods are now module methods of Qt::Base, rather
	  than defined as ordinary methods in class Module, reducing name
	  space pollution. Thanks to Esteban Manchado for pointing this
	  out. CCMAIL: dcorbin@users.sf.net

2005-12-08 19:21 +0000 [r486791]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/korundum/kdehandlers.cpp,
	  branches/KDE/3.5/kdebindings/korundum/ChangeLog: 2005-12-08
	  Richard Dale <Richard_Dale@tipitina.demon.co.uk> * A marshaller
	  was defined for 'KFileItemList' as a value type, but not for a
	  'KFileItemList*' pointer type. Fixes problem reported by Diego
	  'Flameeyes' Pettenò CCMAIL: kde-bindings@kde.org

2005-12-16 13:55 +0000 [r488928]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: 2005-12-16 Richard
	  Dale <Richard_Dale@tipitina.demon.co.uk> * Improved the code to
	  call a C++ slot via qt_invoke() when a ruby slot hasn't been
	  defined. It now invokes the method in the Smoke lib directly,
	  rather than going via method_missing(). CCMAIL:
	  kde-bindings@kde.org

2005-12-19 18:07 +0000 [r489768]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * The
	  logger_backend() function has been removed and replaced with
	  qWarning() calls. This was because of problems with getting the
	  logger_backend() code to work on Windows.

2005-12-21 18:53 +0000 [r490400]  rdale

	* branches/KDE/3.5/kdebindings/korundum/rubylib/examples/uimodules/uidialogs.rb,
	  branches/KDE/3.5/kdebindings/korundum/rubylib/examples/uisampler.rb:
	  * Added a KDE::InputDialog example which supercedes the
	  KDE::LineEditDlg widget

2005-12-23 10:40 +0000 [r490825]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/handlers.cpp:
	  * Don't delete KCommand classes on garbage collection, as they
	  are owned by a KHistoryCollection. May help fix problem reported
	  by Kevin Brown. CCMAIL: kde-bindings@kde.org

2005-12-31 16:16 +0000 [r492930]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm,
	  branches/KDE/3.5/kdebindings/kalyptus/kalyptusDataDict.pm: *
	  Improved Qyoto C# bindings code generation. * Added Q_SLOT(),
	  Q_SIGNAL() and SmokeMethod() attributes with the C++ type
	  signature of methods. * Added Emit() method which returns a
	  transparent proxy built from the signatures of the signals
	  emitted by a QObject subclass.

2005-12-31 17:07 +0000 [r492938]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  Added return types to Q_SLOT() and Q_SIGNAL() attribute strings.

2005-12-31 17:39 +0000 [r492947]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  Improved the code generation for QApplication.cs and
	  QSizePolicy.cs so they don't need fixing up by hand.

2005-12-31 19:54 +0000 [r492969]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  Added SLOT() and SIGNAL() methods to Qt.cs, and some constants.

2005-12-31 20:44 +0000 [r492976]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  Set up the 'qApp' global variable in Qyoto.

2006-01-04 13:07 +0000 [r494255]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp: *
	  Raised the qtruby version to 1.0.11 in line with Caleb's
	  Rubyforge release CCMAIL: caleb@aei-tech.com

2006-01-06 09:12 +0000 [r494752]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  The '_signalInterceptor' field is now called 'Q_EMIT' * Added a
	  'SmokeClass' attribute with the C++ class name of a Qyoto/Kimono
	  class

2006-01-07 09:01 +0000 [r495132]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  If a class had any pure virtual methods, then constructors and
	  destructors weren't being generated. Fixes problem reported by
	  Arno Rehn.

2006-01-07 12:36 +0000 [r495199]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  If an enum name clashes with a method name when the method starts
	  with an upper case letter, then the method starts with a lower
	  case letter. * C# Enums are now used in method calls rather than
	  ints.

2006-01-10 10:24 +0000 [r496301]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/lib/Qt/qtruby.rb:
	  * The Qt::SizePolicy constructor was failing with uchar types
	  passed as ruby Integers. The following call now works correctly:
	  s = Qt::SizePolicy.new(Qt::SizePolicy::NoDirection,
	  Qt::SizePolicy::NoDirection, 1, 3) Fixes problem reported by orem
	  on #qtruby

2006-01-16 20:07 +0000 [r499005]  rdale

	* branches/KDE/3.5/kdebindings/kalyptus/kalyptusCxxToKimono.pm: *
	  The methods to create transparent proxies to forward calls to
	  SmokeInvocation.Invoke() have been renamed CreateProxy(). The
	  methods to create transparent proxies to forward calls to
	  SignaInvocation.Invoke() have been renamed CreateSignalProxy().

2006-01-18 20:42 +0000 [r499817]  rdale

	* branches/KDE/3.5/kdebindings/qtruby/rubylib/qtruby/Qt.cpp,
	  branches/KDE/3.5/kdebindings/qtruby/ChangeLog: * Improved the
	  debug logging of virtual method callbacks so that the arg types
	  are shown too CCMAIL: cs@kde.org

