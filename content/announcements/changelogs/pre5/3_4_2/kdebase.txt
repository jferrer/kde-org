2005-05-23 11:44 +0000 [r417307]  lunakl

	* kwin/geometry.cpp: Backport #106019.

2005-05-23 12:52 +0000 [r417334]  dgp

	* configure.in.in: Remove warnings on ./configure for FreeBSD.

2005-05-23 19:16 +0000 [r417485]  aseigo

	* kicker/taskbar/taskbar.cpp: backport show only minimized fix
	  BUG:104659

2005-05-31 11:32 +0000 [r420108]  lunakl

	* kwin/kcmkwin/kwinrules/main.cpp: Backport incorrect variable name
	  fix.

2005-05-31 19:11 +0000 [r420312]  binner

	* kxkb/kcmlayoutwidget.ui, kxkb/kcmlayout.cpp: fix disabled icons

2005-06-01 07:07 +0000 [r420645]  binner

	* khelpcenter/navigator.cpp: shorter string exists also against
	  expectations

2005-06-01 09:44 +0000 [r420751]  coolo

	* kcontrol/krdb/krdb.cpp: backporting fix for gtk1

2005-06-03 12:29 +0000 [r421561]  lunakl

	* kicker/applets/menu/menuapplet.cpp: I don't think this has
	  anything to do with KSharedPtr, and it certainly breaks 3.4.1.

2005-06-05 21:40 +0000 [r422596]  hindenburg

	* konsole/konsole/konsole.cpp: Backport to fix extra space for Icon
	  Only tabs. CCBUGS: 106684

2005-06-05 21:43 +0000 [r422597]  hindenburg

	* konsole/konsole/konsole.cpp: Make 'New Window' from tab bar popup
	  menu work.

2005-06-05 22:06 +0000 [r422609]  hindenburg

	* konsole/konsole/konsole.cpp: Fix crashes when
	  action/settings=false CCBUGS: 106829

2005-06-06 21:37 +0000 [r422926]  craig

	* kcontrol/kfontinst/viewpart/FontViewPart.cpp: Before displaying a
	  font from fonts:/, re-initialise fontconfig - just in case this
	  is a newly installed font.

2005-06-07 02:20 +0000 [r422964]  aseigo

	* kicker/taskbar/taskcontainer.cpp: backport fix for:
	  CCBUGS:106926,106935

2005-06-07 12:09 +0000 [r423080]  lunakl

	* kwin/rules.cpp: Backport, write rules cfg file before launching
	  kcm to edit it.

2005-06-07 12:58 +0000 [r423089]  coolo

	* kicker/applets/clock/clock.cpp: don't rely on undefined values

2005-06-09 09:24 +0000 [r423662]  lunakl

	* kwin/client.cpp: Backport #105809. CCBUG: 105809

2005-06-10 18:04 +0000 [r424076]  howells

	* kdesktop/krootwm.cc: Backport switch user kiosk restriction from
	  trunk

2005-06-10 18:12 +0000 [r424077]  howells

	* kicker/core/panelextension.cpp, kicker/ui/k_mnu.cpp,
	  kicker/ui/service_mnu.cpp: back port the kiosk fixes from trunk

2005-06-10 18:22 +0000 [r424080]  howells

	* kicker/core/panelextension.cpp: Forgot to update the backport
	  patch with aaron's suggestions

2005-06-11 21:38 +0000 [r424441]  craig

	* kcontrol/fonts/kxftconfig.cpp: Query fontconfig for locations of
	  config file.

2005-06-12 12:25 +0000 [r424584]  dfaure

	* kdesktop/krootwm.cc: Backport fix for "Disable desktop menu"

2005-06-15 16:04 +0000 [r425798]  dfaure

	* konqueror/client/kfmclient.cc: Fix for "kfmclient exec <url>
	  <mimetype>": restrict trader query to Applications.

2005-06-17 18:19 +0000 [r426572]  dfaure

	* konqueror/listview/konq_listviewwidget.cc: Fix crashes in tooltip
	  and preview code due to dangling KFileItems after a reload, or a
	  file gets deleted/moved. Thanks to Maks for his bug report
	  handling, merging, patch testing, and for reminding me to fix
	  this :) BUG: 100800 BUG: 96405

2005-06-18 15:32 +0000 [r426796]  hindenburg

	* konsole/konsole/konsole.cpp, konsole/konsole/konsole.h: Allow
	  saving/loading of Encoding in 'Save as Default'. CCBUG: 107329

2005-06-19 16:07 +0000 [r427115]  binner

	* konqueror/konq_frame.cc: Unbreak context menus of custom status
	  bar entries

2005-06-21 21:14 +0000 [r427794]  dhaumann

	* kate/app/kateviewspace.h: gcc4 compile fix. With this kdebase
	  should compile. (see kde-core-devel)

2005-06-23 08:36 +0000 [r428125]  gianni

	* ksysguard/ksysguardd/Linux/acpi.c: This fix the bug #105182 with
	  the merged code from the trunk by Tobias I hope no problem
	  BUG:105182

2005-06-23 20:03 +0000 [r428345]  aseigo

	* kicker/buttons/servicemenubutton.cpp: backport: use the group
	  comment for the tip if it exists CCBUG:108000

2005-06-24 23:00 +0000 [r428697]  thiago

	* kicker/menuext/find/websearch.desktop: Backporting the change to
	  the Exec= line so that the user-chosen browser is used.
	  BACKPORT:42869

2005-06-25 18:12 +0000 [r428890]  hindenburg

	* konsole/konsole/konsole.h: update version

2005-06-25 19:44 +0000 [r428916]  hindenburg

	* konsole/konsole/konsole.cpp: Fix sm/dcop crashes when
	  action/settings=false. CCBUG: 106829

2005-06-26 14:22 +0000 [r429089]  gianni

	* ksysguard/ksysguardd/Linux/acpi.h: I forgot to change this CCBUG:
	  105182

2005-06-27 15:18 +0000 [r429401]  binner

	* startkde, kdebase.lsm: 3.4.2 preparations

2005-06-27 15:38 +0000 [r429402]  binner

	* konqueror/version.h: more RELEASE-CHECKLIST fun

2005-06-30 01:14 +0000 [r430072]  aseigo

	* kicker/extensions/taskbar/taskbarextension.cpp: back fix for
	  broken external taskbar trans CCBUG:106556

2005-07-01 00:48 +0000 [r430346]  teske

	* konqueror/keditbookmarks/listview.h,
	  konqueror/keditbookmarks/listview.cpp: Backport the rewrite of
	  the custom selection code. (Which was quite buggy.) BUG: 80256

2005-07-01 04:13 +0000 [r430382]  pletourn

	* konqueror/konq_frame.cc: Restore the palette changing code

2005-07-01 04:21 +0000 [r430384]  pletourn

	* konqueror/konq_viewmgr.cc: Keep the current tab when removing the
	  other tabs via the keyboard BUG:108274

2005-07-04 09:58 +0000 [r431434]  binner

	* konqueror/konq_mainwindow.cc, konqueror/konq_actions.cc,
	  konqueror/konq_actions.h: Fix "Go" toolbar position (#89708) by
	  removing AlignItemRight hack, the reason for the hack given in
	  commit 94287 doesn't seem to apply anymore

2005-07-05 14:32 +0000 [r431905]  hasso

	* kcontrol/samba/kcmsambastatistics.cpp, khelpcenter/navigator.cpp:
	  Backport of i18n("Search") usage audit. No new strings, "&Search"
	  is already in the kdelibs.po. CCMAIL: kde-et@linux.ee

2005-07-05 23:55 +0000 [r432043]  mueller

	* kdm/kfrontend/genkdmconf.c: backport buffer overflow fix by Than
	  Ngo.

2005-07-07 10:33 +0000 [r432431]  ossi

	* kdm/kfrontend/kgreeter.cpp: run the entire face loading as
	  nobody.

2005-07-07 20:09 +0000 [r432554]  pletourn

	* kioslave/man/kmanpart.cpp: Build the KURL properly

2005-07-07 21:00 +0000 [r432572]  pletourn

	* konqueror/konq_mainwindow.cc: Build the KURL properly BUG:104417

2005-07-08 10:31 +0000 [r432738]  dfaure

	* konqueror/konq_mainwindow.cc, konqueror/konq_mainwindow.h: Fix
	  compilation error by backporting the following commits: (r401430
	  and r401576) Revised support for name filters: now that
	  KShortURIFilter supports them, they can be moved down to openURL
	  - which fixes pasting a url with a wildcard. (#93825) (except the
	  support for ? and [] in name filters).

2005-07-08 13:03 +0000 [r432768]  ossi

	* kdm/kfrontend/kdm_config.c: backport: saner debug

2005-07-09 01:13 +0000 [r432914]  thiago

	* kxkb/kcmlayout.cpp: Backporting the fix for bug #108787 to the
	  branch. BACKPORT:432912:432913 CCBUG:108787

2005-07-09 15:35 +0000 [r433057]  ossi

	* kdm/kfrontend/kgreeter.cpp: backport: don't crash on missing
	  pam-error item

2005-07-09 15:53 +0000 [r433064]  ossi

	* kcontrol/kdm/kdm-sess.cpp: disable outdated lilo config. fix
	  depends on i18n changes.

2005-07-09 16:11 +0000 [r433071]  binner

	* konqueror/konq_extensionmanager.cc,
	  konqueror/konq_extensionmanager.h: Remove another inactive help
	  button Simple rule: not implemented => don't show in GUI

2005-07-09 16:28 +0000 [r433076]  binner

	* konsole/konsole/konsole.cpp: One button too much in overwrite
	  confirmation dialog

2005-07-09 17:17 +0000 [r433087]  ossi

	* kdm/backend/bootman.c: backport: add --no-floppy to the grub
	  command line - saves some time.

2005-07-09 18:26 +0000 [r433114]  ossi

	* configure.in.in, kcheckpass/kcheckpass.c: backport: use
	  getpassphrase() instead of getpass() where available

2005-07-09 19:44 +0000 [r433142]  ossi

	* kdm/kfrontend/kdm_config.c: backport: fix %hostlist macros

2005-07-10 14:48 +0000 [r433353]  thiago

	* kioslave/sftp/sftpfileattr.h, kioslave/sftp/kio_sftp.cpp,
	  kioslave/sftp/sftpfileattr.cpp: Backporting the fix for kio_sftp.
	  No one complained of it being broken in two weeks, so I hope it's
	  safe. BACKPORT:428891:428892 CCBUG:66411

2005-07-10 23:49 +0000 [r433492]  mok

	* l10n/ru/entry.desktop: fixing CurrrencySymbol in
	  kdebase/l10n/ru/entry.desktop: was not in UTF-8. BUG: 108869

2005-07-12 10:21 +0000 [r433960]  aseigo

	* kicker/ui/k_mnu.cpp, kicker/ui/k_mnu.h: don't crash on exit due
	  to both the kmenu and klibloader trying to delete menuexts
	  BUG:106922

2005-07-16 03:42 +0000 [r435034]  adawit

	* kioslave/sftp/kio_sftp.cpp: - Backport fix for BR# 109117

2005-07-16 09:29 +0000 [r435115]  ossi

	* kdm/backend/krb5auth.c: backport double free fix

2005-07-16 11:52 +0000 [r435248]  ossi

	* startkde: backport attempt at fixing bug #104795

2005-07-16 12:01 +0000 [r435252]  lukas

	* kcontrol/kthememanager/ktheme.cpp: backport fix for #108500

2005-07-17 16:06 +0000 [r435603]  staikos

	* kcontrol/crypto/crypto.cpp: store/read case insensitive

2005-07-17 17:03 +0000 [r435616]  ossi

	* startkde: backport 2nd /env/ robustness improvement.

2005-07-17 20:00 +0000 [r435671]  ossi

	* kdm/backend/util.c: backport tty listing fix.

2005-07-17 20:47 +0000 [r435684]  ossi

	* kdesktop/lock/lockdlg.cc, kdesktop/lock/lockdlg.h: backport: fix
	  xkb layout switching.

2005-07-19 18:55 +0000 [r436431]  deller

	* kcontrol/usbview/usb.ids: import latest usb.ids file from
	  http://www.linux-usb.org/usb.ids: New version: "usb.ids,v 1.215
	  2005/07/10 17:15:33 vojtech"

2005-07-20 13:14 +0000 [r436890]  ossi

	* kdm/backend/socket.c: backport: unset ipv6_v6only

