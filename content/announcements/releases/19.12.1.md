---
layout: page
publishDate: 2020-01-09 10:00:00
summary: Over 120 individual programs plus dozens of programmer libraries and feature
  plugins are released simultaneously as part of KDE's release service.
title: 19.12.1 Releases
type: announcement
---

January 09, 2020. Over 120 individual programs plus dozens of programmer libraries and feature plugins are released simultaneously as part of KDE's release service.

Today they all get new bugfix source releases.

Distro and app store packagers should update their application packages.

+ [KDE's January Apps Update](https://www.kde.org/announcements/releases/2020-01-apps-update) for new features
+ [19.12.1 release notes](https://community.kde.org/Releases/19.12_Release_Notes) for information on tarballs and known issues.
+ [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [19.12.1 source info page](https://kde.org/info/releases-19.12.1)
+ [19.12.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.1)

## Press Contacts

For more information send us an email: [press@kde.org](mailto:press@kde.org).
