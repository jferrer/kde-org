---
description: Lots of updates, new features and improvements in KDE Gear ⚙️ 22.04
authors:
  - SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2022 Aniqa Khokhar <aniqa.khokhar@kde.org>
  - SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-04-21
hero_image: hero.webp
images:
  - /announcements/gear/22.04.0/hero.webp
layout: gear
scssFiles:
- /scss/gear-22-04.scss
outro_img:
  link: many_apps.webp
  alt: Screenshots of many applications
highlights:
  title: Highlights
  items:
  - header: Konsole
    text: Konsole gets snippets
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.04/konsole_commands.webp
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.04/konsole_commands.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.04/konsole_commands.webm
    hl_class: konsole
  - header: Kdenlive
    text: Kdenlive gets sections
    hl_video:
      poster: https://cdn.kde.org/promo/Announcements/Apps/22.04/kdenlive_sections.webp
      mp4: https://cdn.kde.org/promo/Announcements/Apps/22.04/kdenlive_sections.mp4
      webm: https://cdn.kde.org/promo/Announcements/Apps/22.04/kdenlive_sections.webm
    hl_class: kdenlive
draft: false
---

# Welcome to KDE Gear ⚙️ 22.04!

[Skip to _What's New_](#whatsnew)

KDE Gear ⚙️ 22.04 brings you all the updates added to a long list of KDE apps over the last four months. KDE programs allow you to work, create and play without having to submit yourself to extortionate licenses and intrusive advertising, or surrender your privacy to unscrupulous corporations.

Below you will discover a selection of the changes added in the last four months to software designed to make your life better. But remember, there is much, much more: games, social media apps, utilities for communicating, developing and creating stuff... All these things have been worked on to give you more stability and boost your productivity.

If you want to see a full list of everything we have done, check out the complete [changelog](/announcements/changelogs/gear/22.04.0/).

**WARNING:** There's a lot!

{{< highlight-grid >}}

[Skip to _What's New_](#whatsnew)

## What is KDE Gear

KDE Gear is a set of apps and other software created and maintained by the KDE Community that release all new versions at the same time. KDE Gear may also include new apps that we deem have reached enough maturity to be included in the release.

When we announce a new version of KDE Gear (like we are doing today), you can expect the new versions to appear in your Linux distros shortly afterwards. In some distributions, new versions will appear nearly immediately -- such as in the case of [Neon](https://neon.kde.org/). In others, it may take days or even weeks, as it depends on your distro's packagers.

New versions of KDE Gear apps also pop up in independent app stores, such as FlatHub or the Snap Store; and as updates on non-Linux platforms, such as on Windows.

# <a id="whatsnew"></a>What's New

## [Dolphin](https://apps.kde.org/dolphin/)

Dolphin users will find that KDE's powerful file manager now shows previews for more types of files and more information about each item in your file system in a more readable way. This makes it easier to determine accurately what is each thing and helps you decide what to open, move, copy, compress or erase before you even do it.

Dolphin can display thumbnails for ePub files, the dimensions of images under their previews, and give the extension `.part` for files that have only been partially downloaded or copied. When compressing single or several files, or whole directories, Dolphin now gives you a more meaningful file name, and also includes improvements for languages that are written from right to left.

Connectivity with MTP devices, such as cameras, now works much better overall and new users unfamiliar with Dolphin can find it when looking for applications just by searching for “Explorer”, “Finder”, “files”, “file manager” or “network share”.

![Dolphin shows image dimensions under the name of the file.](Dolphin_image-dimensions.webp)

## [Konsole](https://konsole.kde.org/)

Another application that makes itself easy to find is Konsole, KDE's powerful terminal emulator. Now, if you search for "cmd" or "command prompt", Konsole will pop up as an option.

Of course, that is not the only news regarding Konsole. Konsole's SSH plugin has been further enhanced and you can assign different visual profiles (with different colors for backgrounds, text, etc.) to each SSH account. Like that you always have a visual cue for where you are. And a completely new feature is *Quick Commands*: open a quick commands pane from *Plugins* > *Show Quick Commands* and you will be able to create short scripts you use frequently and invoke them when you need them with a couple of clicks.

{{< video src="https://cdn.kde.org/promo/Announcements/Apps/22.04/konsole_ssh_profiles.mp4" src-webm="https://cdn.kde.org/promo/Announcements/Apps/22.04/konsole_ssh_profiles.webm" >}}

Other new things are that Konsole now supports Sixel images that can be displayed right inside the window, and you can now open the current directory or any other folder you right-click on in any app, not just the file manager.

![Konsole opens Sixel images directly in the window](konsole_sixel.webp)

As for increasing usability, Konsole’s scrolling performance is now twice as fast and has been adapted for touch-scrolling.

## [Kdenlive](https://kdenlive.org)

The biggest news coming out of the Kdenlive project is that now macOS users can edit videos with Kdenlive on the newer M1 devices. This opens up a whole new audience of Mac users and can help make Kdenlive a serious contender in the video-editing world.

In other news, the render dialog has been revamped and it is easier to see all the available rendering options, while at the same time adding features. Two new options in particular stick out: you can create customised profiles so that your rendered movie adapts perfectly to your needs, and you can also render by zones, using the guides you set up on the timeline as references.

![Kdenlive lets you create customised render profiles.](kdenlive_render_profiles.webp)

There is also initial support for 10-bit color, although this is still a work in progress, as applying any effect to a 10-bit clip will drop your video down to 8-bit color. You can look forward to full 10-bit color support in future versions of Kdenlive.

## [Kate](https://kate-editor.org/)

Kate is much more than a text editor. It incorporates features that make it ideal for editing HTML, LaTeX and Markdown documents and provides utilities to make it an excellent programming and development environment, including comprehensive support for Git, syntax coloring, and "snippets" — templates of commonly used code.

In today's release, you will find Kate starts up faster and makes it easier to browse your project directories and find files. It also helps you differentiate files with the same names, but placed in different locations. Along with a menu reshuffle, enhancements to the indented layout of code, and improvements to Kate's support on Wayland, Kate improves its overall usability, stability and range of features.

![Kate's navigation bar lets you browse directories and files easier.](kate-navigation-bar.webp)

## [Okular](https://okular.kde.org/)

Okular is KDE's all-terrain document viewer. Apart from becoming the world's first computer program to be awarded an [eco-certification backed by a government](https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/), Okular has improved its usability and user-friendliness and now presents a welcome screen when opened without a document and alerts you immediately when you go to sign a document but have no valid digital certificates.

![Okular starts with a welcomes screen when opened without a document.](Okular_welcome_screen_with_docs.webp)

## And also this...

* [Elisa](https://elisa.kde.org/), KDE's elegant and modern music player, improves its support for touch screens, is faster, more stable and you can now drag and drop music and playlist files from your file manager onto the playlist panel.
* With [Skanpage](https://apps.kde.org/skanpage/), KDE's scanning utility, you can now share scanned documents (including multi-page PDFs) using KDE's general sharing system, which allows you to push documents to instant messaging apps, online cloud services, social services, and through Bluetooth to other devices.
* [Spectacle](https://apps.kde.org/spectacle/)'s annotation tools add functionality to crop, scale, undo, redo, and in general do many more things with the images you screenshot. Also, any annotation settings you change will be remembered the next time you startup the program.
* [Gwenview](https://apps.kde.org/gwenview/), KDE's image viewer, detects and guides you through the installation of camera importers that are lacking support packages. There is also a new *Print Preview* functionality for when you need a hard copy.
* [KDE Itinerary](https://apps.kde.org/itinerary/), KDE's travel assistant (also available for your phone), improves support for more train companies (such as Renfe and Amtrak) and airlines. It also adds more detailed information for weather and an inbuilt barcode scanner so you can scan your ticket's information directly from the app.
* ... And much, much more

# New Arrivals

[Kalendar](https://apps.kde.org/kalendar/) is a modern calendaring and task managing app with an attractive interface and lots of useful features you can use to sync with all your other calendars. It works on your desktop and on Plasma Mobile.

![Kalendar helps you keep track of your appointments and tasks.](kalendar.webp)

---

{{< announcements/gear_major_outro >}}
