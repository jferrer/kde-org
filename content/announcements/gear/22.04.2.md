---
date: 2022-06-09
appCount: 120
image: true
layout: gear
---
+ ark: Support ‘7zip’ as well as ‘p7zip’. [Commit](http://commits.kde.org/ark/5943a22ac4696158b40cee3a3873e88b81d5539b) [#440135](https://bugs.kde.org/440135)
+ konsole: Fix scroll position jumps regression. [Commit](http://commits.kde.org/konsole/a6fce70f6c4e4d355c14475be8f5669a83c7023f), [#452955](https://bugs.kde.org/452955), [#453112](https://bugs.kde.org/453112)
+ okular: Fix crash while undoing with the menu on an empty annotation. [Commit](http://commits.kde.org/okular/1a980008be43e006c21254c45a06b7dd17a215bd), [#453987](https://bugs.kde.org/453987)
