---
qtversion: 6.5.0
date: 2024-08-09
layout: framework
libCount: 72
draft: false
---
{{< details title="Attica" href="https://commits.kde.org/attica" >}}
+ Support new version field on DownloadDescription. [Commit.](http://commits.kde.org/attica/bea7d12c8285cbd1e9b440f0a4ead6cc8d042576) 
{{< /details >}}

{{< details title="Bluez Qt" href="https://commits.kde.org/bluez-qt" >}}
+ Manager: Fix connections for the connectedDevices list property. [Commit.](http://commits.kde.org/bluez-qt/ac79b321fe2230e55c29cdbd3ad87551155f031e) 
{{< /details >}}

{{< details title="Breeze Icons" href="https://commits.kde.org/breeze-icons" >}}
+ Add 22px color (and monochrome, using *-symbolic) category icons. [Commit.](http://commits.kde.org/breeze-icons/ded08f5f243e6ae6283fb6177a2c245598e7ec3c) Fixes bug [#489347](https://bugs.kde.org/489347)
+ Make 16px list-remove-symbolic be a link to the base icon. [Commit.](http://commits.kde.org/breeze-icons/486a6fcffa4705aaeed2f7c9def0bc5f5d9f40d9) 
+ Add symlinks for Crow Translate icons. [Commit.](http://commits.kde.org/breeze-icons/067fb89346c29c29f1b8b59f17ae48ac06bbdab3) Fixes bug [#489984](https://bugs.kde.org/489984)
+ Add flatpak symlink for Git Cola. [Commit.](http://commits.kde.org/breeze-icons/30df399c933cd934a81413bfc03a14ad1a1c9250) 
+ Add icon to indicate adding to a playlist immediately after the current track. [Commit.](http://commits.kde.org/breeze-icons/991796c581ba159dc199ec0c0332f4ee944f6e6f) See bug [#429942](https://bugs.kde.org/429942)
{{< /details >}}

{{< details title="Extra CMake Modules" href="https://commits.kde.org/extra-cmake-modules" >}}
+ Prefix.sh.cmake add MANPATH. [Commit.](http://commits.kde.org/extra-cmake-modules/54646d3b5dd8d1112d8be370bd34a3fc63ab067a) 
+ FindWaylandScanner: Add flag to use private-code. [Commit.](http://commits.kde.org/extra-cmake-modules/b8a1464674ff33ab94ad626fb47d2abfab5f4aad) 
{{< /details >}}

{{< details title="Framework Integration" href="https://commits.kde.org/frameworkintegration" >}}
+ Plasma_workspace.notifyrc: relabel to match button. [Commit.](http://commits.kde.org/frameworkintegration/188c70df22d78fd7103a1d554d5af48773e40ad0) 
{{< /details >}}

{{< details title="KArchive" href="https://commits.kde.org/karchive" >}}
+ Allow passing QIODevice::Truncate in open mode. [Commit.](http://commits.kde.org/karchive/a2b5e3ab883335463d34138df8bb51eae82fdebe) 
{{< /details >}}

{{< details title="KAuth" href="https://commits.kde.org/kauth" >}}
+ Allow the dbus backend to pass file descriptors as arguments. [Commit.](http://commits.kde.org/kauth/9ab87948c9d3312378f13a68ec68edd904855c1b) 
+ Ensure that qt dbus macros are available with KAUTH_BUILD_CODEGENERATOR_ONLY=ON. [Commit.](http://commits.kde.org/kauth/47f7effa01ad1784443704dcae13e4c8f9b14773) 
{{< /details >}}

{{< details title="KCMUtils" href="https://commits.kde.org/kcmutils" >}}
+ AbstractKCM: Document what extraFooterTopPadding is for. [Commit.](http://commits.kde.org/kcmutils/db265a386dbd34d70d3f3f90026f46730144729b) 
+ AbstractKCM: remove false deprecation notice. [Commit.](http://commits.kde.org/kcmutils/ccd6ae25900ff49d7224fa8812df3e736b49f03d) 
{{< /details >}}

{{< details title="KCodecs" href="https://commits.kde.org/kcodecs" >}}
+ Add static build support. [Commit.](http://commits.kde.org/kcodecs/27cf85b37fadabe9b8df6d34f107908f95169788) 
{{< /details >}}

{{< details title="KColorScheme" href="https://commits.kde.org/kcolorscheme" >}}
+ Bundle essential breeze color schemes as resource. [Commit.](http://commits.kde.org/kcolorscheme/042d87f1eb7f7be923d56215575f7170c24a0b91) 
{{< /details >}}

{{< details title="KConfig" href="https://commits.kde.org/kconfig" >}}
+ Ksharedconfig: move statercs to state dir. [Commit.](http://commits.kde.org/kconfig/604c988ae0444b7e9a8a74c258f5fb235164f298) 
+ Explain what "desktop entry spec separator semantics" means. [Commit.](http://commits.kde.org/kconfig/4cb169303ac6c163a8ab67b0dde6d69ad7ee3b71) 
+ Reapply "don't create root-only readable files in /etc/xdg". [Commit.](http://commits.kde.org/kconfig/87393689b3cadb5d00b478f4581b1c3a6d994167) 
+ Make Qt6Quick dependency for QML plugin private. [Commit.](http://commits.kde.org/kconfig/86fca1ec54dc5f6edfdf70e84cd3bdcbf8817cae) 
+ Expose KWindowStateSaver in the QML interface. [Commit.](http://commits.kde.org/kconfig/d193d4eb4ee20ae37304cf67164b02b63cb528ea) 
+ Sync config group on KWindowStateSaver::timerEvent. [Commit.](http://commits.kde.org/kconfig/b926fefc776782027732983abeb7a6e3b95a9993) 
+ Don't do tests in ~/.kde-unit-test. [Commit.](http://commits.kde.org/kconfig/52f90f2826ec47293e808c1b69bb1908c9c9ec1e) 
{{< /details >}}

{{< details title="KCoreAddons" href="https://commits.kde.org/kcoreaddons" >}}
+ Enable cppcheck. [Commit.](http://commits.kde.org/kcoreaddons/1b0cc44da65b376b09d26c2ccdf34a6962b64930) 
+ Add missing license header. [Commit.](http://commits.kde.org/kcoreaddons/85a2a0b4b227f5517abae5e8b291fc02a6717d4b) 
+ Add QML Format example. [Commit.](http://commits.kde.org/kcoreaddons/c0f028f751e3f3161dc5d3f1683a60623a0a854b) 
+ [qml/formats] Drop no longer needed function. [Commit.](http://commits.kde.org/kcoreaddons/51e749442adcdd197134dc62b6076294b6ea1e68) 
+ Remove obsolete doc comment. [Commit.](http://commits.kde.org/kcoreaddons/2dffdd6408533f1438835912d46621372eaee685) 
+ Replace deprecated globalMatch with globalMatchView. [Commit.](http://commits.kde.org/kcoreaddons/3d347776cdbfb27959b540e609b8a67177ff3b1c) 
{{< /details >}}

{{< details title="KCrash" href="https://commits.kde.org/kcrash" >}}
+ Metadata: Include frameworks version number. [Commit.](http://commits.kde.org/kcrash/5980173bcd712d6ba896dad979151181389888f1) 
+ Metadata: pass along qt version. [Commit.](http://commits.kde.org/kcrash/fc841f4a19c249bb3885cdfefb35ea95826ac035) 
+ Don't capture unused variable. [Commit.](http://commits.kde.org/kcrash/c3dc3078ae7f658226e94144e253915ecc7a8a0f) 
+ Add CMakelint formatting commit to .git-blame-ignore-revs. [Commit.](http://commits.kde.org/kcrash/ac0e9e0cf6759df2dd6ee0cfcc29f2e97612580b) 
+ ./CMakeLists.txt:26: Extra spaces between 'if' and its () [whitespace/extra]. [Commit.](http://commits.kde.org/kcrash/ed5edff11439fe431a7cc04dbeaffaf14a9f9f81) 
{{< /details >}}

{{< details title="KDeclarative" href="https://commits.kde.org/kdeclarative" >}}
+ KeySequenceItem: be Accessible.Button, not Checkbox. [Commit.](http://commits.kde.org/kdeclarative/65e9dd6b666ba3b98efa57ef6e98d7c14f7945ec) 
+ KeySequenceItem: Use KMessageDialog for displaying conflict messages. [Commit.](http://commits.kde.org/kdeclarative/0d3e51d263b4cfa49ee9fe1e9839ea5b248287f6) 
+ Kquickcontrols: Drop validation code from KeySequenceHelper. [Commit.](http://commits.kde.org/kdeclarative/5643c6d7608a37768bef5047e35dd784e21ba41e) 
+ Kquickcontrols: Use KeySequenceValidator in KeySequenceItem. [Commit.](http://commits.kde.org/kdeclarative/028f92fab8cf06611d2fb9b29f5282cc5b91ec03) Fixes bug [#483199](https://bugs.kde.org/483199)
+ Kquickcontrols: Introduce private KeySequenceValidator type. [Commit.](http://commits.kde.org/kdeclarative/f05d0f22d46d33880ee72dc01de58b1129e968b6) See bug [#483199](https://bugs.kde.org/483199)
{{< /details >}}

{{< details title="KFileMetaData" href="https://commits.kde.org/kfilemetadata" >}}
+ [Extractors] Clarify Qt::Gui dependencies. [Commit.](http://commits.kde.org/kfilemetadata/1f0655127368888789d573890af838d39202b3a5) 
{{< /details >}}

{{< details title="KGuiAddons" href="https://commits.kde.org/kguiaddons" >}}
+ Add missing license text. [Commit.](http://commits.kde.org/kguiaddons/e405f19685943e7e8abb83c79824131ee0d2f252) 
{{< /details >}}

{{< details title="KHolidays" href="https://commits.kde.org/kholidays" >}}
+ Kazakhstan: Add National Book Day. [Commit.](http://commits.kde.org/kholidays/02e96bb196833f8d4762072a64ff819cbf15dda7) 
{{< /details >}}

{{< details title="KI18n" href="https://commits.kde.org/ki18n" >}}
+ Use more specific names for the functions. [Commit.](http://commits.kde.org/ki18n/88c5af65c2fe6da1961c207463c57c505d293a8f) 
+ Add comment for translators in example code. [Commit.](http://commits.kde.org/ki18n/de5c5478a0edc54d26ac09414997bae3864fb3a9) 
+ Code cleanup. [Commit.](http://commits.kde.org/ki18n/d6b0494ea44916cb7291244204715c4a2abcaf0e) 
+ Minor Doxygen change. [Commit.](http://commits.kde.org/ki18n/c9243999045a01cb1a7c7b4e854fca37e343c259) 
+ Build with older clang versions too. [Commit.](http://commits.kde.org/ki18n/603ee7e641a7af90cc43750b6930e0fde9cf7651) 
+ Test also retranslateFormatString. [Commit.](http://commits.kde.org/ki18n/8e537d2413ccca1a6634acc9c699b60de0e5a1a1) 
+ Make retranslateFormatString() call function from correct namespace and adjust code documentation. [Commit.](http://commits.kde.org/ki18n/319ddfe9a69a2beeb43bc19a70421ef9bd3deea1) 
+ Add helper method for setting a plural-aware spin box prefix/suffix. [Commit.](http://commits.kde.org/ki18n/08ab2d3998600beac1bca4c5e004fb91ce64613d) 
{{< /details >}}

{{< details title="KIconThemes" href="https://commits.kde.org/kiconthemes" >}}
+ Fix icon recoloring for portable installers. [Commit.](http://commits.kde.org/kiconthemes/2f40d4eef6099f9e89a9d83f25e7f302aae9a5bf) 
+ Overhaul KIconEngine docs. [Commit.](http://commits.kde.org/kiconthemes/e48badad4832dc0e426d2f6d26a977c3613f4aa6) 
+ Deprecate KIconLoader::drawOverlays. [Commit.](http://commits.kde.org/kiconthemes/8884c230062441e7ca116512c2a0d92bbf926ad0) 
+ Deprecate movie loading API. [Commit.](http://commits.kde.org/kiconthemes/2d922281eedc135db0c7ddefdc7349110433930f) 
+ Add back effect into cache key. [Commit.](http://commits.kde.org/kiconthemes/3caa03c2ca0eac54abeffe8074281c7519c0ca3e) Fixes bug [#490795](https://bugs.kde.org/490795)
+ Deprecate KIconEffect::apply API. [Commit.](http://commits.kde.org/kiconthemes/c5ea1f762ae4d522f3b58ce297049cd303bc74b9) 
+ Refactor icon effect application. [Commit.](http://commits.kde.org/kiconthemes/ce83f4a47b355f73f45b7b2dd7dd1760ca4038ea) 
+ [kiconeffect] Add static API for active effect. [Commit.](http://commits.kde.org/kiconthemes/3e55abaacfeadea5c2eb01622944fccc0689ba57) 
+ [kiconeffect] Add static API for disabled effect. [Commit.](http://commits.kde.org/kiconthemes/0f653f64065de6b4cf0ce3bc9f3feb1c88b5b7df) 
+ [kiconeffects] Drop color and color2 members. [Commit.](http://commits.kde.org/kiconthemes/c84a72683e7edb89b90e0dfa23ab7c408f8bb8a0) 
+ [kiconeffects] Don't read effect settings from kconfig. [Commit.](http://commits.kde.org/kiconthemes/1965c25e799f72e27c829d060aa40cbcaeb63106) 
+ [kiconeffect] Replace raw numbers with relevant enum. [Commit.](http://commits.kde.org/kiconthemes/dc5bdf885ad328bc661d6bdc6f4f4204979506ea) 
+ Add note to prefer QIcon::fromTheme over KIconLoader. [Commit.](http://commits.kde.org/kiconthemes/cb2e467fa537f67fa14eda3936c3fc03042fdbc5) 
+ Remove edit-specific directives. [Commit.](http://commits.kde.org/kiconthemes/fd6ef609ab569009eeb7dab462ef597a1368fe7b) 
{{< /details >}}

{{< details title="KImageformats" href="https://commits.kde.org/kimageformats" >}}
+ JXL: added ImageTransformation option. [Commit.](http://commits.kde.org/kimageformats/219d9cb2c2fbe1097054dc59874d261bec7eec36) 
+ Xcf: Fix crash on malformed files. [Commit.](http://commits.kde.org/kimageformats/51921e8ee574cbf93fe655aee203e01e6e1532ef) 
+ Pcx: Fix crash in broken files. [Commit.](http://commits.kde.org/kimageformats/23e9fec869261fce86d57b89ecdc253c19595f2f) 
+ Xcf: Fix crash on broken files. [Commit.](http://commits.kde.org/kimageformats/4478bc8d2bc63c98fa87b26e19b3b4a42fcffa8f) 
+ Pcx: fix crash on invalid files. [Commit.](http://commits.kde.org/kimageformats/acd6b3970c96ed7f4e0ed6ab2f0abb63bef304a8) 
+ Pcx: fix crash on invalid files. [Commit.](http://commits.kde.org/kimageformats/638fdfcbdd85014b31f1f5dddaf4e54d1669d932) 
+ Exr: added some usefull attributes. [Commit.](http://commits.kde.org/kimageformats/a497ab789b56a6faa5fc96544749dcd1d70e29dc) 
+ Pcx: Read 16 color images that are 4bpp and 1 plane. [Commit.](http://commits.kde.org/kimageformats/3590a43fc506afca40a20b09fb6e8007b6dcf5b8) 
+ Full range HDR support. [Commit.](http://commits.kde.org/kimageformats/f5a6de72803567d7221dd0f9288037d7bb514613) 
+ Prepare gitlab for files that are coming int the next commit. [Commit.](http://commits.kde.org/kimageformats/4c0f49295b0920b140a84d4a4e6f67eee3bf276f) 
+ Avif: check return values. [Commit.](http://commits.kde.org/kimageformats/e9da5edb9a9dd518a603d7a19facf0f2fe38dde6) 
+ Raw: Fix compiler warning with macro redefinition in Windows. [Commit.](http://commits.kde.org/kimageformats/e10f5aa9a5595dbc341a9a205d5906f15c314462) 
{{< /details >}}

{{< details title="KIO" href="https://commits.kde.org/kio" >}}
+ KFileWidget: reset preview to selected file on hover leave. [Commit.](http://commits.kde.org/kio/25a0c69729f70b41c502539e2300418215ae3887) See bug [#418655](https://bugs.kde.org/418655)
+ Drop unused methods from ConnectionServer. [Commit.](http://commits.kde.org/kio/797b26832d1ff70c6ab599b3957b6e9804861549) 
+ Un-PIMPL ConnectionServer. [Commit.](http://commits.kde.org/kio/355ad00a7a55563a9a60f1d7aed0862acb7e3e49) 
+ Properly log errors in ConnectionServer::listenForRemote(). [Commit.](http://commits.kde.org/kio/e1182af2c747886aeffb9fa5534aae96e1808966) 
+ Beef up connection error logging. [Commit.](http://commits.kde.org/kio/6f61932981c267de7573095cac27f95af069095b) 
+ [kprocessrunner] Also request an XDG activation token with StartupNotify=false. [Commit.](http://commits.kde.org/kio/c9dea35c5a7f6188fb068a3436f43e861e717239) 
+ Port from KIconLoader::drawOverlays to KIconUtils::addOverlays. [Commit.](http://commits.kde.org/kio/e1d1b18bd34926c1bd6517161700930c929c98ad) 
+ Kdirmodel: fix case when first deleted item was hidden. [Commit.](http://commits.kde.org/kio/15a4151222a847ccaf4fc873f80cab924b1ab1e6) 
+ Kfileitem: show relative path for rel symlink. [Commit.](http://commits.kde.org/kio/7ba3f8c4462fb6ed3d7046c4933772b56c3ecce9) Fixes bug [#456198](https://bugs.kde.org/456198)
+ File_unix: extract readLinkToBuffer to fn. [Commit.](http://commits.kde.org/kio/779797db5bf5a3b1d73291a590aa044c6ed13e0a) 
+ Kshellcompletion: remove unused variable. [Commit.](http://commits.kde.org/kio/dd9c29cb18225789bc0ce03d00a6a5a7b8fc4740) 
+ KFilePlacesView: Show free space info in the tooltip. [Commit.](http://commits.kde.org/kio/74ab1d3c888a20e2ac52e8e4ddaa2fe5e3f1ba4e) 
+ KFilePlacesView: Show label in tooltip if it is elided. [Commit.](http://commits.kde.org/kio/d553b5f2d585fbccc900cae2eea64d77ea54e407) Fixes bug [#454436](https://bugs.kde.org/454436)
+ KFilePlacesItem: Add tooltip. [Commit.](http://commits.kde.org/kio/a6820a89aee9e6706da22b9637d5a3b58d69591e) 
+ Ksamba: correct isAclValid regex. [Commit.](http://commits.kde.org/kio/ce7e7eb945875d3c430ac3653a06e10f6b89b869) 
+ DeleteOrTrashJob: emitResult with ERR_USER_CANCELED when cancelling prompt. [Commit.](http://commits.kde.org/kio/4c30eac810dceda0c4ed2eaee6aa1fdaf0c974b7) 
+ Alter permissions of the right file. [Commit.](http://commits.kde.org/kio/45fa8819120d689c1291c76eb26f2b86dc9a4418) 
+ Try to fix test if umask is not as expected. [Commit.](http://commits.kde.org/kio/287426fb2ff3adef858e3c9c1ebc03a3fec6f1fd) 
+ Enable one more test that works for me. [Commit.](http://commits.kde.org/kio/58f52556bdf379e05817c0248402e7c74f67c381) 
+ Ensure the wanted one second diff. [Commit.](http://commits.kde.org/kio/ead8e26186b357264bbf1a253c9cce23f9fb7169) 
+ Make test more deterministic. [Commit.](http://commits.kde.org/kio/2a153f7f81cdca970ac43f16c81ad2122f2b17ed) 
+ Less pollution of the home with test tmp files. [Commit.](http://commits.kde.org/kio/87cdc2a3a0da68b061b0c43b3ecb73ffa87470df) 
+ Port away from deprecated KIconEffect API. [Commit.](http://commits.kde.org/kio/14161afdd143dfae1f688f90d55ebee0a7fcc65e) 
+ Kprocessrunner: small code improvements. [Commit.](http://commits.kde.org/kio/812b3561db94f596cb2bcd953db306cfbfc0a58f) 
+ Lookup QWidget parent at runtime. [Commit.](http://commits.kde.org/kio/a3d38a002a28c588dab2994750be0d248e058c7a) 
+ Remove unused includes. [Commit.](http://commits.kde.org/kio/21a4e0e9ff136292330a84f48945bdab46ad5b1f) 
+ Drop SessionData. [Commit.](http://commits.kde.org/kio/ecd80f8c7956ff80fdefa0da77fb6fb1e44869a6) 
+ Move useragent handling code into HTTP worker. [Commit.](http://commits.kde.org/kio/156eb97f6f5a6da12a6a69e3bc0c85e9714f8d19) 
+ Remove unused member. [Commit.](http://commits.kde.org/kio/1d08e004e479ba2ea2ac05a0210b0b3cf30e7515) 
+ Drop dead metadata. [Commit.](http://commits.kde.org/kio/781a9c18a71a9f1dc557a49dc2a890ef4cb4a3c1) 
+ Cache KFileItem::isHidden result. [Commit.](http://commits.kde.org/kio/f6dff2519926cd3d042f3841677f2ec2516d8b7b) 
+ Avoid reparsing mount points for each directory on destruction. [Commit.](http://commits.kde.org/kio/f2b98614035f307f4e8b5642b02b2b3990ae7739) 
+ Remove parent for DropMenu. [Commit.](http://commits.kde.org/kio/e0ea91afdf0dccef7e3afbf23a159bf5a8d6b249) Fixes bug [#490183](https://bugs.kde.org/490183)
+ Systemdprocessrunner: escape $ in arguments. [Commit.](http://commits.kde.org/kio/4b6fc86cd698ed3127fe4118676bafb4e3b9f480) 
+ API dox: help doxygen to not auto-detect first sentence end after "i.e.". [Commit.](http://commits.kde.org/kio/c5d5f38af4a18626181ad136b834a6a021100415) 
+ API dox: document CamelCase include for KFileFilter. [Commit.](http://commits.kde.org/kio/288deb61cdec15515ac616ee7bf6ab0f02fcfe84) 
{{< /details >}}

{{< details title="Kirigami" href="https://commits.kde.org/kirigami" >}}
+ Relicense MenuDialog and PromptDialog to LGPL. [Commit.](http://commits.kde.org/kirigami/b995663fb26fc57ce67a7b16b1b6e4e49d34e994) 
+ GlobalDrawerActionItem: Fix tooltip. [Commit.](http://commits.kde.org/kirigami/9148d4d32f57cd2ce8b014c4e5d974ade71c559c) 
+ ContextualHelpButton: make text always accessible. [Commit.](http://commits.kde.org/kirigami/f2deb4ea2d026c7924e52ae14a1e13de95acae85) 
+ Convert license headers to SPDX. [Commit.](http://commits.kde.org/kirigami/d67b7ffca82c6e99e46702ecc1862921cea72439) 
+ Port dialogs to pragma ComponentBehavior: Bound. [Commit.](http://commits.kde.org/kirigami/bea315711197e94164c0b8fa38492480c4450905) 
+ Dialog: Support variable base padding. [Commit.](http://commits.kde.org/kirigami/30776e639d742e265be9433e9b0e7997811b361e) 
+ Dialog: Add some null safety. [Commit.](http://commits.kde.org/kirigami/72363a1b79ca7f29d8b68b1fb55210ba2d6358a2) 
+ Dialog: Fix ScrollView::contentItem management. [Commit.](http://commits.kde.org/kirigami/b848e8198609910824332c5dd6ed37a1b7d2e84c) 
+ We can Use directly nullptr. [Commit.](http://commits.kde.org/kirigami/f7024fff2f6c4b2efba1ad63b07e1835bedc8656) 
+ Fix: install KirigamiPrivate soversion. [Commit.](http://commits.kde.org/kirigami/c671251bbd52d5ab0de815b5cef857598fd8670a) 
+ NavigationTabButton: Drop bold font workaround. [Commit.](http://commits.kde.org/kirigami/911ff451787d7752037532fc3bc1546bef5743f8) 
{{< /details >}}

{{< details title="KItemModels" href="https://commits.kde.org/kitemmodels" >}}
+ Fix incorrect QML element name in documentation. [Commit.](http://commits.kde.org/kitemmodels/ad06b482d50a5ebe34390af114912350ded37033) 
{{< /details >}}

{{< details title="KNewStuff" href="https://commits.kde.org/knewstuff" >}}
+ Support new version field on DownloadLinkInformation. [Commit.](http://commits.kde.org/knewstuff/2a70812b00793596ce7f9ba0bb0ae0c84a6d85b9) 
+ QML: Use some better property types. [Commit.](http://commits.kde.org/knewstuff/2f1bfedc6c90470d9307a98dfdb36c7fffb95e83) 
+ EntryDetails.qml: Qualify newStuffModel, use downloadItemId. [Commit.](http://commits.kde.org/knewstuff/f8fb221d6f355c4f0873592fbf7dce358f4f0b40) Fixes bug [#483659](https://bugs.kde.org/483659)
+ Use KPackageJob::update instead of ::install. [Commit.](http://commits.kde.org/knewstuff/5cf114a994ecee75aed48fad6bb178a3f6d792ec) 
{{< /details >}}

{{< details title="KNotifications" href="https://commits.kde.org/knotifications" >}}
+ Allow to build on oses without dbus and any other backend. [Commit.](http://commits.kde.org/knotifications/a730d3808aa4b41642d7b9f50b262cf5073028bd) 
{{< /details >}}

{{< details title="KParts" href="https://commits.kde.org/kparts" >}}
+ Use the correct check to determine whether initial preference should be read from KParts or KPlugin. [Commit.](http://commits.kde.org/kparts/30fd9dc6527670e33f5ae063ce440e6ee1b49b5d) 
{{< /details >}}

{{< details title="KPeople" href="https://commits.kde.org/kpeople" >}}
+ Make locks actually lock. [Commit.](http://commits.kde.org/kpeople/f6f611ec56a8182f51344c0a662ff06571a3e436) 
{{< /details >}}

{{< details title="KService" href="https://commits.kde.org/kservice" >}}
+ Store canonicalFilePath instead of looking it up twice. [Commit.](http://commits.kde.org/kservice/41f0303135803ce97defc7ba81569e3bff68b117) 
+ Use canonicalFilePath instead of absoluteFilePath. [Commit.](http://commits.kde.org/kservice/0eedb04d3be4b2fe8cdc66bfaf3c374261b76310) 
+ Use absolute path to mimeapps.list. [Commit.](http://commits.kde.org/kservice/95f61557da54fa2258a80586fe4eda291d0b4b02) 
{{< /details >}}

{{< details title="KStatusNotifieritem" href="https://commits.kde.org/kstatusnotifieritem" >}}
+ Add API to control quit behavior. [Commit.](http://commits.kde.org/kstatusnotifieritem/f4622bb8058d7a6e785a201f6d129d461a059b59) 
{{< /details >}}

{{< details title="KSVG" href="https://commits.kde.org/ksvg" >}}
+ FindInCache: Compare last modified to boot time if timestamp is 0. [Commit.](http://commits.kde.org/ksvg/4b55e9e88bd7e6ecbbe845c04d0bfc6dfb0d6f3a) Fixes bug [#453876](https://bugs.kde.org/453876)
+ Drop dead code. [Commit.](http://commits.kde.org/ksvg/8b7d87284d7a3046fcff4a44aa12f357df30cf8a) 
{{< /details >}}

{{< details title="KTextEditor" href="https://commits.kde.org/ktexteditor" >}}
+ Fix drag pixmap with wrapped lines. [Commit.](http://commits.kde.org/ktexteditor/db25cdc5541e01429177cdd0c3b787658b276b3b) Fixes bug [#476979](https://bugs.kde.org/476979)
+ Add actions to convert spaces to tabs and vice versa. [Commit.](http://commits.kde.org/ktexteditor/62bfaa50055a589ad923caf58726249ea0b9e6c6) Fixes bug [#456736](https://bugs.kde.org/456736)
+ Use ktexteditor-script-tester6 for javascript tests. [Commit.](http://commits.kde.org/ktexteditor/1fb130f4def36bd6d4e272957aeb13abd148eb5d) 
+ Fix -Woverflow warning with gcc. [Commit.](http://commits.kde.org/ktexteditor/f11412a9e49c5277c44e71fccd32412478adc6c4) 
+ Templates: Remove one level of nesting. [Commit.](http://commits.kde.org/ktexteditor/19887711bc7743d4595b6e25a8dcc446d5f9e5e8) 
+ Ignore buffer signals in completion on undo/redo. [Commit.](http://commits.kde.org/ktexteditor/9a335652aa6791985b4237c3e169d6584d9672b5) 
+ CursorToOffset: remove unnecessary line variable. [Commit.](http://commits.kde.org/ktexteditor/a6e97d1fff00b14c64cad7f30be619f2e3a3b5fb) 
+ Fix formatting. [Commit.](http://commits.kde.org/ktexteditor/ab88a14e534fe41f739d70f36819d422ed576dc9) 
+ Src/view/kateview.cpp (KTextEditor::ViewPrivate::setupActions) : Fix typo in m_toggleShowSpace and m_toggleDynWrap whatsThis text. [Commit.](http://commits.kde.org/ktexteditor/ae71373cc4f244f834f6d61e44cf11455e7d9634) 
+ Treat 0x0000 to 0x001F as non-printable. [Commit.](http://commits.kde.org/ktexteditor/ae1fcc6ede9474ec1124e911436eb89255327312) 
+ Add action to quick toggle space visibility. [Commit.](http://commits.kde.org/ktexteditor/d64fd0c27c48d1ec88a39cbafbea8f5c24ff677c) 
{{< /details >}}

{{< details title="KWidgetsAddons" href="https://commits.kde.org/kwidgetsaddons" >}}
+ Fix that last line in fonts list is only half visible. [Commit.](http://commits.kde.org/kwidgetsaddons/1a2c1e29ece234c44ba94d7c83b108447a3bd1f1) Fixes bug [#488079](https://bugs.kde.org/488079)
+ Avoid the creation of toplevel widget. [Commit.](http://commits.kde.org/kwidgetsaddons/ded75732730b7dd166baf10537f07a4d17b83586) Fixes bug [#490712](https://bugs.kde.org/490712)
+ Remove unused license text. [Commit.](http://commits.kde.org/kwidgetsaddons/a2b9af75edf69f6d29a8527936b34006dd8f44e0) 
+ Partially revert cccaa8f89897146043a535d45a276e3a57412815. [Commit.](http://commits.kde.org/kwidgetsaddons/185e946c8b918471ee5164815d3296bc06d8a2e7) 
+ Deprecate KJobWidgets::setWindowHandle,windowHandle. [Commit.](http://commits.kde.org/kwidgetsaddons/2cae6bf05ea88d038296bbc8a957cc367548aa21) 
+ Fix tab not highlighted in some cases. [Commit.](http://commits.kde.org/kwidgetsaddons/5d8e84bdcca9c8508c9e88f11947927b725afb63) 
+ Fix multiple overlays being installed over tab. [Commit.](http://commits.kde.org/kwidgetsaddons/d2b7f7413210ca36d2b896a965aefd7d9b91a99f) Fixes bug [#490213](https://bugs.kde.org/490213)
{{< /details >}}

{{< details title="KXMLGUI" href="https://commits.kde.org/kxmlgui" >}}
+ Remove QTreeWidgetHack, the methods are public now. [Commit.](http://commits.kde.org/kxmlgui/4de0770c88f1fd7f57fc7368a40b901f414dfa3e) 
+ Fix compile error of krichtexteditor on Craft on Windows. [Commit.](http://commits.kde.org/kxmlgui/4cc340b912ce4cd9a94223a0420836597e7a7f0e) 
{{< /details >}}

{{< details title="Network Manager Qt" href="https://commits.kde.org/networkmanager-qt" >}}
+ ConnectionSettings: Support MeCard SAE security type. [Commit.](http://commits.kde.org/networkmanager-qt/bc283683dd5f297b7a297be2043ae5f10be25ee6) 
{{< /details >}}

{{< details title="QQC2 Desktop Style" href="https://commits.kde.org/qqc2-desktop-style" >}}
+ ScrollBar: Fix custom scrolling behavior. [Commit.](http://commits.kde.org/qqc2-desktop-style/40b55f9a0fef21a02671efe7f87b95b3702dec13) Fixes bug [#488092](https://bugs.kde.org/488092)
{{< /details >}}

{{< details title="Solid" href="https://commits.kde.org/solid" >}}
+ Udev/cpuinfo_arm: add M2 Pro/Max core variants. [Commit.](http://commits.kde.org/solid/95d3ad09a8fd4a3bf87dee33cbe687f79d38f747) 
{{< /details >}}

{{< details title="Sonnet" href="https://commits.kde.org/sonnet" >}}
+ Downgrade qCWarning to qCDebug. [Commit.](http://commits.kde.org/sonnet/864f072d96c5fc1b471a99a81d9eb742929bf558) 
+ Avoid initializing full HSpellDict when getting languages. [Commit.](http://commits.kde.org/sonnet/2a9ce5916367967f6321ded4f4a75374f9b76d4e) Fixes bug [#421451](https://bugs.kde.org/421451)
+ Fix SONNET_USE_QML=OFF by moving ECMQmlModule behind the conditional. [Commit.](http://commits.kde.org/sonnet/3619143373123ea0815b7fe7a308e4b53f451565) 
{{< /details >}}

{{< details title="Syntax Highlighting" href="https://commits.kde.org/syntax-highlighting" >}}
+ Earthfile.xml: modernize highlighter to support Earthly 0.8 features. [Commit.](http://commits.kde.org/syntax-highlighting/830cbd9da0e63d0629639d196e27aaecd54b4eae) 
+ C++: add floating-point literal suffixes of C++23. [Commit.](http://commits.kde.org/syntax-highlighting/4f5aa5a030458731dc9cf75dd612a26eaa975a9d) 
+ Python: add some special variables and fix '\' line continuation after a string. [Commit.](http://commits.kde.org/syntax-highlighting/65d36ce600d01b398c7c49795bdf45dee93bbb17) 
+ Add .clang-format and .clang-tidy files with YAML. [Commit.](http://commits.kde.org/syntax-highlighting/9a823a7a9023ac78fbf0d5cdd2dd12d33ea4912e) 
+ Zig: fix range operator preceded by a number: [0..]. [Commit.](http://commits.kde.org/syntax-highlighting/6c05b42decd1c9d610f90ed9456b3e0bfe30133e) 
+ Odin: add raw string color. [Commit.](http://commits.kde.org/syntax-highlighting/22f2e036050db360654a50e71577724e4fa700fa) 
+ Update syntax highlighting for kdesrc-buildrc. [Commit.](http://commits.kde.org/syntax-highlighting/8be7db128ac14b05540a0ecc5971323ea9a3c4c2) 
+ Cmake.xml: update syntax for CMake 3.30. [Commit.](http://commits.kde.org/syntax-highlighting/706b84ac18551f11235b6b7d3677e9160b8baf8d) 
+ Inc version after done type adding. [Commit.](http://commits.kde.org/syntax-highlighting/fca36dce3fb315b9649a79e8baf627d3bc6a2d7c) 
+ Hare: add done keyword. [Commit.](http://commits.kde.org/syntax-highlighting/add0facfebf4761fddc4f81ad31d995e8cf8617d) 
{{< /details >}}

{{< details title="Threadweaver" href="https://commits.kde.org/threadweaver" >}}
+ Ensure windows tests work. [Commit.](http://commits.kde.org/threadweaver/fc0195842c6dc2ae07bfaf059c76481118e4a56e) 
+ Only enter loop if signal didn't arrive before that. [Commit.](http://commits.kde.org/threadweaver/d07f9bf93be7752d98519f234ed13068b50a973c) 
+ Try to fix windows hang in tests. [Commit.](http://commits.kde.org/threadweaver/7526545372a1e258f1f5826ae1cef8af36ea75f7) 
{{< /details >}}

